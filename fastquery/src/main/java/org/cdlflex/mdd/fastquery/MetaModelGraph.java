package org.cdlflex.mdd.fastquery;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The meta model is threaded as a graph, which can be traversed to 
 * identify / look for patterns and paths.
 */
public class MetaModelGraph {
    private static final Logger LOGGER = LoggerFactory.getLogger(MetaModelGraph.class);
    private static final Integer MAX_PATH_LENGTH = 10;
    
    private MetaModel m;
    private List<List<MetaAssociation>> paths;
    private List<List<MetaAssociation>> targetPaths;

    private MetaClass start;
    private MetaClass target;

    public MetaModelGraph(MetaModel m) {
        this.m = m;
    }

    public MetaClass getStartNode() {
        return start;
    }

    public MetaClass getTargetNode() {
        return target;
    }

    public List<List<MetaAssociation>> getTargetPaths() {
        return this.targetPaths;
    }

    /**
     * Look for all paths between a start node and an end node (two classes).
     * @param startNodeName the name of the start node.
     * @param targetNodeName the name of the target node.
     * @return a list of paths. each path contains the associations which build the path.
     * @throws Exception is thrown if the search fails.
     */
    public List<List<MetaAssociation>> search(String startNodeName, String targetNodeName) throws Exception {
        paths = new LinkedList<List<MetaAssociation>>();
        targetPaths = new LinkedList<List<MetaAssociation>>();
        MetaPackage p = this.m.getPackages().iterator().next();
        start = p.findClassByName(startNodeName);
        target = p.findClassByName(targetNodeName);
        if (start == null) {
            throw new Exception("Start Class not found!");
        } else if (target == null) {
            throw new Exception("Target Class not found!");
        }

        // BFS
        MetaClass c = start;
        List<MetaAssociation> aList = this.getAllAssociations(c);
        this.queryPaths(aList, start, target, new LinkedList<MetaAssociation>());

        Collections.sort(targetPaths, new QueryComparator());
        int i = 1;
        for (List<MetaAssociation> o : this.targetPaths) {
            LOGGER.trace("Path " + i + ": ");
            int j = 0;
            for (MetaAssociation x : o) {
                if (j > 0) {
                    LOGGER.trace(" -> ");
                }
                LOGGER.trace(x.getName() + "(" + x.getTo().getMetaClass().getName() + ")");
                j++;
            }
            LOGGER.trace("");
            i++;

            if (i > MAX_PATH_LENGTH) {
                LOGGER.trace(String.format("Output skipped (%s entries available).",
                        Math.max(0, this.targetPaths.size() - MAX_PATH_LENGTH)));
                break;
            }
        }
        return targetPaths;
    }

    //CHECKSTYLE:OFF: 4 parameters for a private, recursive method are quite ok.
    private void queryPaths(List<MetaAssociation> aList, MetaClass startNode, MetaClass targetNode,
        List<MetaAssociation> previousPart) {
    //CHECKSTYLE:ON
        for (MetaAssociation a : aList) {
            List<MetaAssociation> newPath = new LinkedList<MetaAssociation>();
            newPath.addAll(previousPart);

            boolean alreadyVisited = false;
            for (MetaAssociation x : newPath) {
                if (x.getName().equals(a.getName())) {
                    alreadyVisited = true;
                    break;
                }
            }

            if (alreadyVisited) {
                continue;
            } else {
                if (a.getTo().getMetaClass() == targetNode) {
                    newPath.add(a);
                    paths.add(newPath);
                    this.targetPaths.add(newPath);
                } else {

                    newPath.add(a);
                    paths.add(newPath);
                    List<MetaAssociation> bList = this.getAllAssociations(a.getTo().getMetaClass());
                    this.queryPaths(bList, startNode, targetNode, newPath);
                }
            }
        }
    }

    private List<MetaAssociation> getAllAssociations(MetaClass c) {
        List<MetaClass> list = new LinkedList<MetaClass>();
        list.add(c);
        list.addAll(this.getSuperClasses(c));

        List<MetaAssociation> aList = new LinkedList<MetaAssociation>();
        Set<String> names = new HashSet<String>();
        for (MetaClass x : list) {
            for (MetaAssociation a : x.getAssociations()) {
                if (!names.contains(a.getName())) {
                    names.add(a.getName());
                    aList.add(a);
                }
            }
        }
        return aList;
    }

    private List<MetaClass> getSuperClasses(MetaClass c) {
        List<MetaClass> list = new LinkedList<MetaClass>();
        return this.getSuperClasses(c, list);
    }

    private List<MetaClass> getSuperClasses(MetaClass c, List<MetaClass> list) {
        if (c.getSuperclasses().isEmpty()) {
            return list;
        } else {
            for (MetaClass x : c.getSuperclasses()) {
                list.addAll(x.getSuperclasses());
                list = this.getSuperClasses(x, list);
            }
            return list;
        }
    }
}
