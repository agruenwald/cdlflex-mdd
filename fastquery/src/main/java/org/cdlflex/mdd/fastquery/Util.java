package org.cdlflex.mdd.fastquery;

/**
 * Utility to format strings.
 */
public final class Util {
    
    private Util() {
        
    }
    
    /**
     * Shortcut to capitalize a word.
     * @param s the word
     * @return the capitalized word.
     */
    public static String toFirstUpper(String s) {
        if (s.length() > 0) {
            return s.substring(0, 1).toUpperCase() + s.substring(1);
        } else {
            return s;
        }
    }

    /**
     * Shortcut to un-capitalize a word.
     * @param s the word.
     * @return the uncapitalized word.
     */
    public static String toFirstLower(String s) {
        if (s.length() > 0) {
            return s.substring(0, 1).toLowerCase() + s.substring(1);
        } else {
            return s;
        }
    }
}
