package org.cdlflex.mdd.fastquery;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.cdlflex.mdd.umltuowl.main.Main;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.cdlflex.mdd.umltuowl.utils.SettingsReader;

/**
 * Test interface to the umlTUowl models.
 */
public final class UMLtuOWLLoader {
    private static final Logger LOGGER = Logger.getLogger(UMLtuOWLLoader.class);
    private static final String UML_CONFIG = "umltuowl-config.properties";
    private static MetaModel metaModel;

    private static List<String> args;

    private UMLtuOWLLoader() {
        
    }
    
    /**
     * Initialize the settings (empty arguments).
     */
    public static void initializeSettings() {
        initializeSettings(new String[] {});
    }

    private static String repPath(String s) {
        return s.replace("{projectPath}", "/");
    }

    /**
     * Initialize the settings and pass some parameters.
     * @param javaArgs arguments.
     */
    public static void initializeSettings(String[] javaArgs) {
        Properties prop = new Properties();
        args = new LinkedList<String>();
        try {
            // load a properties file
            prop.load(ClassLoader.getSystemResourceAsStream(UML_CONFIG));
            args.add("input=" + repPath(prop.getProperty("input")));
            args.add("converter=" + repPath(prop.getProperty("converter")));
            args.add("settings=" + repPath(prop.getProperty("settings")));
            args.add("output=" + repPath(prop.getProperty("output")));
            args.add("IRI=" + repPath(prop.getProperty("IRI")));
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage());
        }

        for (String a : args) {
            String[] s = a.split("=");
            SettingsReader.overwriteProperty(s[0], s[1]);
        }
        for (String a : javaArgs) {
            String[] s = a.split("=");
            SettingsReader.overwriteProperty(s[0], s[1]);
        }

    }

    /**
     * load the meta model (harmonized). 
     * @return the meta model.
     */
    public static MetaModel loadMetaModel() {
        try {
            if (metaModel != null) {
                return metaModel;
            }
            // check input parameters and create meta model.
            metaModel = Main.loadMetaModel(args.toArray(new String[args.size()]));
            if (metaModel == null) {
                return null;
            }
            // Harmonize, e.g. create IRI's.
            metaModel = Main.harmonizeModel(metaModel);
            // Create OWL Ontology
            MetaModel ontoModel = metaModel;
            if (Main.createOutput(ontoModel)) {
                LOGGER.info(String.format("UMLtuOWLLoader: Finished transformation process "
                                + "(%s class elements contained in resulting ontology).",
                                metaModel.getSize()));
                for (MetaPackage pkg : metaModel.getPackages()) {
                    LOGGER.info(String.format(" *Package %s:", pkg.getName()));
                    LOGGER.info(String.format("  ==> %s classes", pkg.getClasses().size()));
                    LOGGER.info(String.format("  ==> %s associations", pkg.getSizeAssociations()));
                    LOGGER.info(String.format("  ==> %s attributes", pkg.getSizeAttributes()));
                    LOGGER.info(String.format("  ==> %s generalizations", pkg.getSizeGeneralizations()));
                }
            }
        //CHECKSTYLE:OFF
        } catch (Exception e) {
            LOGGER.fatal(String.format("Transformation process failed. "
                        + "Please make shure that you used the correct input format " 
                        + "(e.g. Visual Paradigm XMI 2.1 export files and not the vpp - project files)."
                        + "Exception: %s. If you have further problems, send a mail to <a.gruenw@gmail.com> " 
                        + "and describe/attach your input file.",
                            e.getMessage()));
            e.printStackTrace();
            return metaModel;
        }
        //CHECKSTYLE:ON
        return metaModel;
    }
}
