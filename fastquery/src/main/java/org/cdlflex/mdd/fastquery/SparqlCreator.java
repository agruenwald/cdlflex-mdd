package org.cdlflex.mdd.fastquery;

import java.util.List;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;

/**
 * Create a SPARQL query based on two metaclasses and a known path between.
 */
public class SparqlCreator {
    /**
     * Create a SPARQL query based on the information about the start and the end node (two classes),
     * and a path.
     * @param startNode the start node (class name).
     * @param targetNode the target node (class name).
     * @param path a path (associations).
     * @return a SPARQL query.
     */
    public String toString(MetaClass startNode, MetaClass targetNode, List<MetaAssociation> path) {
        String start = "SELECT * WHERE { ";
        String end = "}";

        String result = "";
        result += "itpm:" + "has" + startNode + "?" + startNode + ".\n";
        int i = 0;
        for (MetaAssociation a : path) {
            result +=
                "?"
                    + (i == 0 ? Util.toFirstLower(startNode.getName()) : Util.toFirstLower(path.get(i - 1).getName()))
                    + " " + "itpm:" + "has" + Util.toFirstUpper(a.getName()) + " ?" + a.getName() + ".\n";
            i++;
        }
        return start + result + end;
        /*
         * SELECT * WHERE { ?x itpm:hasCollaboratorName ?n. " + "?x itpm:hasEmail ?m."+ "?x itpm:hasPhone ?p." +
         * "?x itpm:hasIsExternal ?e." + "?x itpm:hasIsUnit ?u." + "?x itpm:hasCollaboratorDescription ?d." + "}";
         */
    }
}
