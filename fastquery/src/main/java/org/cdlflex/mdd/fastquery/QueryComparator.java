package org.cdlflex.mdd.fastquery;

import java.util.Comparator;
import java.util.List;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;

/**
 * Compare the distance between different meta associations. The score is the highest the shorter the path between two
 * meta classes is (the path is defined by a sequence of meta associations linked with each other).
 */
public class QueryComparator implements Comparator<List<MetaAssociation>> {

    @Override
    public int compare(List<MetaAssociation> o1, List<MetaAssociation> o2) {
        int score1 = getScore(o1);
        int score2 = getScore(o2);

        if (score1 > score2) {
            return 1;
        } else if (score1 == score2) {
            return 0;
        } else {
            return -1;
        }
    }

    private int getScore(List<MetaAssociation> path) {
        int score = 0;
        for (int i = 0; i < path.size(); i++) {
            MetaAssociation a = path.get(i);
            if (a.getFrom().getMetaClass() == a.getTo().getMetaClass()) {
                score = score + 2;
            } else {
                score = score + 1;
            }
        }
        return score;
    }
}
