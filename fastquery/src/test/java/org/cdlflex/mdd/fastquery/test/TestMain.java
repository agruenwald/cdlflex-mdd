package org.cdlflex.mdd.fastquery.test;

import java.util.Scanner;

import org.cdlflex.mdd.fastquery.MetaModelGraph;
import org.cdlflex.mdd.fastquery.SparqlCreator;
import org.cdlflex.mdd.fastquery.UMLtuOWLLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cdlflex.mdd.umltuowl.metamodel.*;

/**
 * This class is for experimental testing purposes.
 */
public final class TestMain {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestMain.class);

    private TestMain() {
        
    }
    
    /**
     * Main method.
     * @param args main arguments.
     * @throws Exception is thrown in severe cases.
     */
    public static void main(String[] args) throws Exception {
        UMLtuOWLLoader.initializeSettings();
        MetaModel m = UMLtuOWLLoader.loadMetaModel();
        Scanner sc = new Scanner(System.in);
        boolean run = true;
        while (run) {
            run = false;
            LOGGER.info("Please enter the query (e.g. BacklogItem of Collaborator; enter \"show\" to " 
                    + "list metaclasses):");
            String ui = sc.nextLine();
            if (ui.trim().equalsIgnoreCase("show")) {
                for (MetaClass c : m.getAllClasses()) {
                    LOGGER.info(" => " + c.getName());
                }
                run = true;
                continue;
            }
            String[] concepts = ui.split(" of ");
            if (concepts.length != 2) {
                LOGGER.error("Wrong input");
            } else {
                MetaModelGraph graph = new MetaModelGraph(m);
                graph.search(concepts[1], concepts[0]);

                LOGGER.info("Please enter the path for which you want to create the SPARQL query (e.g.2)...");
                int n = sc.nextInt();
                SparqlCreator cr = new SparqlCreator();
                LOGGER.info("SPARQL for path 2: \n"
                    + cr.toString(graph.getStartNode(), graph.getTargetNode(), graph.getTargetPaths().get(n - 1)));

            }
        }
        sc.close();
    }
}
