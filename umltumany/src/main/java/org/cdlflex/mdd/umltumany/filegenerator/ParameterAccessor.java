package org.cdlflex.mdd.umltumany.filegenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.umltumany.generators.BasicGenerator;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;

/**
 * Read the property values from the gen-conf.properties file in a systematic way.
 * 
 * {originalPkg} will be replaced with the inferred name from the umlTUowl transformation. e.g. CoreITProjectMGMT
 * {modelNamespacePrefix} will be replaced with the namespace of a model, for instance "itpm", or "config" Please
 * checkout the github repo for the generated code according to this relative path! Take care of trailing spaces!
 * 
 * The following rules apply to the code generation: For each metamodel, the following parameters will be used (in the
 * given order), depending whether they have been specified in this file, or not: Assume a metamodel with the name
 * "Jira", and which has been specified with the openengSbType="connector".
 * 
 * 1) srcgenClassGeneratorOWLAPI_itpm and packageNamespaceClassGeneratorOWLAPI_itpm are used if available (also applied
 * for Jena) 2) srcgenClassGeneratorOWLAPIOpenengsbType_connector is used if available (the same rule is applied for the
 * packageNamespace and Jena) 3) srcgenClassGeneratorOWLAPI is finally used if 1) and 2) are not available (the same
 * rules is of course applied for the packageNamespace and Jena) 4) srcgenClassGenerator..... (1, 2, 3) without the
 * technology (e.g. OWLAPI).
 **/

public final class ParameterAccessor {
    private static final String OPENENGSB_TYPE = "openengsbType";

    private ParameterAccessor() {
        
    }
    
    /**
     * Find a parameter in the generator configuration file based on a hierarchy system. Replaces placeholders in the
     * resulting value. 
     * @param genConf the generic properties file for the generator.
     * @param parameterPrefix the prefix of the property variable, e.g. srcgen or packageNamespace
     * @param generator the generator, e.g. classGeneratorOWLAPI, or classGeneratorJena
     * @return a path where the code is generated for a specific generator and type of model.
     * @throws MDDException if the parameter could not be found.
     */
    public static String findGeneratorModelConfigParameter(Properties genConf, String parameterPrefix,
        BasicGenerator generator) throws MDDException {
        MetaModel mm = generator.getMetaModel();
        String openengSbModelType = getOpenEngSbType(mm);

        List<String> generatorPostfixes = new ArrayList<String>();
        generatorPostfixes.add(generator.getGeneratorName()); // e.g. ClassGeneratorOWLAPI
        generatorPostfixes.add(generator.getGeneratorSuperType()); // e.g. ClassGenerator

        for (String generatorPostfix : generatorPostfixes) {
            String parameter = parameterPrefix + generatorPostfix; // e.g. srcgenClassGenerator or
                                                                   // srcgenClassGeneratorOWLAPI.

            List<String> searchParams = new ArrayList<String>();
            searchParams.add(parameter + "_" + mm.getNamespacePrefix());
            if (!openengSbModelType.isEmpty()) {
                searchParams.add(parameter + StringUtils.capitalize(OPENENGSB_TYPE) + "_" + openengSbModelType);
            }
            searchParams.add(parameter);

            for (String param : searchParams) {
                String paramValue = genConf.getProperty(param);
                if (paramValue != null) {
                    return substitutePlaceholders(mm, paramValue, openengSbModelType, generator.getTechnology());
                }
            }
        }
        throw new MDDException(String.format("Could not read property %s (generator %s).", parameterPrefix,
                generator.getGeneratorName()));
    }

    /**
     * Get the generation mode.
     * @param metaModel the meta model.
     * @param parameterPrefix the prefix of the parameter.
     * @param genConf the properties file for the generation configuration.
     * @return the correct generation mode.
     * @throws MDDException is thrown if the generation mode cannot be determined.
     */
    public static String getGenModeConsiderOpenEngSbType(MetaModel metaModel, String parameterPrefix,
        Properties genConf) throws MDDException {
        String parameter = parameterPrefix;
        String openEngsbType = ParameterAccessor.getOpenEngSbType(metaModel); // e.g. connector, domain, etc.
        List<String> searchParams = new ArrayList<String>();
        searchParams.add(parameter + "_" + metaModel.getNamespacePrefix());
        if (!openEngsbType.isEmpty()) {
            searchParams.add(parameter + StringUtils.capitalize(OPENENGSB_TYPE) + "_" + openEngsbType);
        }
        searchParams.add(parameter);

        for (String param : searchParams) {
            String paramValue = genConf.getProperty(param);
            if (paramValue != null) {
                return substitutePlaceholders(metaModel, paramValue, openEngsbType, "");
            }
        }
        throw new MDDException(String.format("Could not determine correct genmode for %s.", metaModel));
    }

    /**
     * Substitute placeholders in a parameter value.
     * @param mm the meta model.
     * @param parameterValue the parameter value.
     * @param technology the technology value (e.g. to select between owlapi and jena)..
     * @return the replaced parameter value.
     */
    public static String substitutePlaceholders(MetaModel mm, String parameterValue, 
        String technology) {
        return substitutePlaceholders(mm, parameterValue, ParameterAccessor.OPENENGSB_TYPE, technology);
    }
    //CHECKSTYLE:OFF 4 instead of 3 parameters in this case is not a big deal.
    private static String substitutePlaceholders(MetaModel mm, String parameterValue, String openEngSbType,
        String technology) {
    //CHECKSTYLE:ON
        String modelName = mm.getName().trim().replaceAll(" ", "");
        parameterValue = parameterValue.replace("{modelName}", modelName);
        parameterValue = parameterValue.replace("{modelNamespacePrefix}", mm.getNamespacePrefix());
        parameterValue =
            parameterValue.replace("{" + StringUtils.capitalize(openEngSbType) + "}",
                    ParameterAccessor.getOpenEngSbType(mm));
        parameterValue = substitutePlaceholderTechnology(parameterValue, technology);
        return parameterValue;
    }

    /**
     * Substitute the technology parameter of the parameter value (e.g. to select between owlapi and jena).
     * @param parameterValue the parameter value.
     * @param technology the technology value (e.g. to select between owlapi and jena)..
     * @return the replaced parameter value.
     */
    public static String substitutePlaceholderTechnology(String parameterValue, String technology) {
        parameterValue = parameterValue.replace("{technology}", technology.toLowerCase());
        return parameterValue;
    }

    /**
     * Get the OpenEngSb type.
     * @param mm the meta model.
     * @return the OpenEngSb type of the model (if existing), extracted from the comments/annotations
     * of the meta model.
     */
    public static String getOpenEngSbType(MetaModel mm) {
        List<MetaComment> res =
            mm.findComments(MetaComment.TYPE_INTERNAL_ANNOTATION, "@" + ParameterAccessor.OPENENGSB_TYPE);
        if (res.size() != 1) {
            return "";
        } else {
            return res.get(0).getAnnotationSubtype().get(1);
        }
    }
}
