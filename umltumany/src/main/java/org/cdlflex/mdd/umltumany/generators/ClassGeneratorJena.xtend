package org.cdlflex.mdd.umltumany.generators

import org.cdlflex.mdd.umltumany.filegenerator.FileGenerator
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel

class ClassGeneratorJena extends ClassGenerator {
 	new (FileGenerator fsa, MetaModel metaModel) {
        super(fsa,metaModel);
    }
   
	new () {
       
    }	
	
	override getTechnology() {
		return "Jena";
	}
	
}