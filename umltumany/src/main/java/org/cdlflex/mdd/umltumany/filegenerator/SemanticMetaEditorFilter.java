package org.cdlflex.mdd.umltumany.filegenerator;

import java.util.ArrayList;
import java.util.List;

import org.cdlflex.mdd.umltuowl.harmonizer.PruneStrategySemanticEditor;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaElement;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The MDD approach supports code generation based on various UML class diagram editors (e.g. MS Visio, Visual Paradigm,
 * Argo UML). The Semantic Web Editor however provides the most integrated solution. The editor provides features to
 * specifiy whether a concept (class, attribute, association) is for information purpose only, local available, globally
 * available, wished, etc. These information is stored as comments/annotations. The information is used by the code
 * generator to decide whether to generate beans/DAOs/unit tests or to avoid it.
 * 
 * Info: The code generator supports the definition of specific status information based on any UML editor. Via adding
 * comments, the same results can be achieved with MS Visio or Visual Paradigm. However, the Semantic Web Editor is the
 * most visual solution.
 * 
 * Note: Suggestions and info - concepts are already pruned by the umltuowl harmonizer.
 * 
 * @see PruneStrategySemanticEditor.class. Hence this class is more coarse-grained and does only recommend the
 *      generation of DAOs and unit tests based on the locality status.
 */
public class SemanticMetaEditorFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(SemanticMetaEditorFilter.class);
    private final MetaModel mm;
    private final MetaPackage p;

    public SemanticMetaEditorFilter(MetaModel mm, MetaPackage p) {
        this.mm = mm;
        this.p = p;
    }

    /**
     * Provides information whether to generate a unit-test for a specific class or not.
     * 
     * @param clazz the current meta-class.
     * @return true if the test should generated and false if either the metaclass, the package, or the metamodel have
     *         been defined as only local available.
     */
    public boolean shouldGenerateTest(MetaClass clazz) {
        return !isStatus(clazz, PruneStrategySemanticEditor.ANNOTATION_LOCAL, "Test");
    }

    /**
     * Provides information whether to generate a DAO for a specific class or not.
     * 
     * @param clazz the current meta-class.
     * @return true if the DAO should generated and false if either the metaclass, the package, or the metamodel have
     *         been defined as only local available.
     */
    public boolean shouldGenerateDAO(MetaClass clazz) {
        return !isStatus(clazz, PruneStrategySemanticEditor.ANNOTATION_INFO, "DAO");
    }

    /**
     * Provides information whether to generate a clas for a specific class or not.
     * 
     * @param clazz the current meta-class.
     * @return true in any case at the moment.
     */
    public boolean shouldGenerateClass(MetaClass clazz) {
        return true;
    }

    private boolean isStatus(MetaClass clazz, String statusValue, String artifact) {
        boolean status;
        if (this.lookForStatus(clazz, statusValue).size() > 0) {
            status = true;
        } else if (this.lookForStatus(p, statusValue).size() > 0) {
            status = true;
        } else if (this.lookForStatus(mm, statusValue).size() > 0) {
            status = true;
        } else {
            return false;
        }
        LOGGER.info(String.format("Filtered %s for meta-class %s (defined %s).", artifact, clazz.getName(),
                statusValue));
        return status;
    }

    private List<MetaComment> lookForStatus(MetaElement e, String statusValue) {
        List<String> statusList = new ArrayList<String>();
        statusList.add(statusValue);
        return e.findComments(MetaComment.TYPE_INTERNAL_ANNOTATION, PruneStrategySemanticEditor.STATUS_ANNOTATION,
                statusList);
    }
}
