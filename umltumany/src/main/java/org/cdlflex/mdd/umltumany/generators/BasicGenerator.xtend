package org.cdlflex.mdd.umltumany.generators

import java.util.HashSet
import java.util.LinkedList
import java.util.List
import java.util.Calendar
import org.cdlflex.mdd.umltumany.filegenerator.IGenerator
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass
import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage
import org.cdlflex.mdd.umltumany.filegenerator.FileGenerator
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel

abstract class BasicGenerator implements IGenerator {
    protected FileGenerator fsa;
    protected MetaModel metaModel;
    
	def boolean generateSpecificsImpl()
	
	def getMetaModel() {
		return metaModel;
	}
	
	def boolean generateCode() {
		return this.generateSpecificsImpl;	
	}
	
	/** return a specific generator name. The name is equals to the
	 *  name of the class.
	 */
    def final getGeneratorName() {
	    return getGeneratorSuperType() + getTechnology();
	}
    
    /** return a shortcut for the technology, e.g. Jena or OWLAPI. 
     * Throws exceptions for technology-independet implementations
    **/
    def abstract String getTechnology();
    
    /** return the supertype of the generator for a specific implementation
     *  e.g. ClassGenerator, DAOGenerator, or JUnitGenerator. Throws an
     * exception for technology-independent implementations.
     */
    def abstract String getGeneratorSuperType();
	
	def getPkgSlashed(String packageName) {
		'''«packageName.replace('.','/')»/'''
	}
	
	def noSpaces(String name) {
		return name.replaceAll(" ","")
	}
	
	def generateSerialId() {
	    var calendar = Calendar::getInstance();
        var s = calendar.getTimeInMillis() / 1000L;
        var sid =  Long::toString(s) + (Math::random*1000).intValue.toString + "L";
        '''
        @IgnoredModelField
        private static final long serialVersionUID =  «sid»;'''
	}
	
	
	def MetaClass getFirstConcreteClass(MetaClass clazz) {
		if(clazz.abstractClass || clazz.interfaceClass) {
			for(c: clazz.subclasses) {
			    if(!(c.firstConcreteClass.abstractClass || c.firstConcreteClass.interfaceClass)) {
					return c
				}
				return c
			}
			
		} else {
			return clazz
		}	
		throw new RuntimeException("MetaClass " + clazz.getName() + " is abstract and has no concrete classes hence auto-initialization is impossible.");
	}
	
	def boolean isFirstConcreteClassAvailable(MetaClass clazz) {
		try {
			clazz.getFirstConcreteClass();
			return true;
		} catch (RuntimeException e) {
			return false;
		}
	}
	
	def parseType(MetaAttribute attr, String variable) {
		'''«IF attr.range.equals("String")»«variable»«
			ELSEIF attr.range.equals("DateTime")»Util.parseDateTime(«variable»)«
			ELSEIF attr.range.equals("int") || attr.range.equals("Integer")»Integer.parseInt(«variable»)«
			ELSE»«attr.range.toFirstUpper».parse«attr.range.toFirstUpper»(«variable»)«ENDIF»'''
	}
	
	def detectId(MetaClass clazz) {
		'''get«clazz.detectIdAttribute»()'''
	}
	
	def String detectIdAttribute(MetaClass clazz) {
		'''«IF clazz.selectAttributes.size()>0»«clazz.selectAttributes.get(0).originalName.toFirstUpper»«ELSE»Id«ENDIF»'''
	}
	
	def String detectIdRange(MetaClass clazz) {
		'''«IF clazz.selectAttributes.size()>0»«clazz.selectAttributes.get(0).range»«ELSE»String«ENDIF»'''
	}
	
	def getDefaultValue(MetaAttribute a) {
		if (!a.multiple) {
			return a.range.defaultValue	
		} else {
			return '''new TreeSet<«a.range.castToGeneric»>()'''
		}
	}
	
    def getDefaultValue(String range) {
       '''«IF range.equalsIgnoreCase("DateTime")»null«
            ELSEIF range.equalsIgnoreCase("String")»""«
            ELSEIF range.equalsIgnoreCase("int")»0«
            ELSEIF range.equalsIgnoreCase("double")»0.0«
            ELSEIF range.equalsIgnoreCase("boolean")»false«
        ENDIF»'''   
    }
		 
	def getDataType(String range) {
		var dataTypes = new HashSet<String>();
		'''«IF range.equals("DateTime") && !dataTypes.contains("Date")»Date«
		  	ELSE»«range»«
		ENDIF»'''	
	}
	
	def getCompareOperator(MetaAttribute a, String compareValue) {
		return a.range.getCompareOperator(compareValue);
	}
	
	def getCompareOperator(String range, String compareValue) {
		var type = range;
		if(type.equals("int") || type.equals("boolean") || type.equals("double")) { //important: double and boolean are case sensitive
			return " == " + compareValue; //Double doesn't work like this.
		} else { //note: Integer requires equals
			return ".equals(" + compareValue + ")";
		}
	}
	
	def castToGeneric(String range) {
		if(range.equalsIgnoreCase("string")) {
			return "String";
		} else if (range.equalsIgnoreCase("int") || range.equalsIgnoreCase("integer")) {
			return "Integer";
		} else if (range.equalsIgnoreCase("datetime")) {
			return "Date";
		} else {
			return range;
		}
	}
	
	def getIndividualAddress(MetaClass clazz) {
		'''"«clazz.indPrefix»"+Util.filterChars(«clazz.name.toFirstLower».«clazz.detectId»)'''
	}
	
	def getIndividualAddressName(MetaClass clazz, String name) {
		'''"«clazz.indPrefix»"+«name».«clazz.detectId»'''
	}
	
	def getIndPrefix(MetaClass clazz) {
		'''«clazz.name»-'''
	}
	
	var classList = new LinkedList<MetaClass>();
	def selectAttributes(MetaClass clazz) {
		var List<MetaAttribute> list = new LinkedList<MetaAttribute>();
		classList = new LinkedList<MetaClass>()
		for(MetaClass c : clazz.selectClassesD.reverse) {
			list.addAll(c.attributes)
		}
		return list
	}
	
	/**
	 * get all attributes of a class, including its superclass attributes.
	 */
	def selectAttributesSingleValued(MetaClass clazz) {
		var List<MetaAttribute> list = new LinkedList<MetaAttribute>();
		classList = new LinkedList<MetaClass>()
		for(MetaClass c : clazz.selectClassesD.reverse) {
			for (MetaAttribute a : c.attributes) {
				if (!a.isMultiple) {
					list.add(a);
				}
			}
		}
		return list
	}
	
	/**
	 * get a human friendly name.
	 * For enum classes: The class name itself.
	 * Otherwise: Attribute "name" available? -> use that.
	 * else: "any attribute that contains name?" -> use that
	 */
	def getHumanFriendlyName(MetaClass clazz) {
		if (clazz.enumClass) {
			return clazz.name;
		}
		var attributes = clazz.selectAttributesSingleValued;
		for (MetaAttribute a : attributes) {
			if (a.name.equalsIgnoreCase("name") && a.range.equalsIgnoreCase("String")) {
				return a.name;
			}
		}
		for (MetaAttribute ax : attributes) {
			if (ax.name.toLowerCase.contains("name") && ax.range.equalsIgnoreCase("String")) {
				return ax.name;
			}
		}
		return clazz.detectId;
		
	}
	
	def getFirstAttribute(List<MetaAttribute> att) {
		if(att.size>0) {
			return att.get(0)
		} else {
			var a = new MetaAttribute("id");
			a.setRange("String");
			return a;
		}
	}
	
	/** get all direct attributes and all attributes
	 * from those abstract classes that cannot be validated
	 * via inferred DAOs. Currently only 1 abstract step is supported.
	 */
	def attributesAndAbstractAttributes(MetaClass clazz) {
		var List<MetaAttribute> attrsToValidate = new LinkedList<MetaAttribute>();
		for (MetaClass s : clazz.selectDirectAbstractSuperClasses) {
			attrsToValidate.addAll(s.attributes);
		}
		attrsToValidate.addAll(clazz.attributes);
		return attrsToValidate;
	}
	
	def List<MetaClass> selectClassesD(MetaClass clazz) {
		classList.add(clazz)
		for(MetaClass su : clazz.superclasses) {
			su.selectClassesD
		}
		return classList
	}
	
	def selectAssociations(MetaClass clazz) {
		var List<MetaAssociation> list = new LinkedList<MetaAssociation>();
		classList = new LinkedList<MetaClass>()
		for(MetaClass c : clazz.selectClassesD.reverse) {
			list.addAll(c.associations)
		}
		return list
	}
	
	/**
	 * get all associations of a class but exclude those who link
	 * to an enum class.
	 */
	def selectAssociationsWithoutEnums(MetaClass clazz) {
		var List<MetaAssociation> list = new LinkedList<MetaAssociation>();
		for (MetaAssociation a : clazz.selectAssociations) {
			if (!a.to.metaClass.enumClass) {
				list.add(a);
			}
		}
		return list;
	}
	
	def selectDirectAbstractSuperClasses(MetaClass clazz) {
		val classes = new LinkedList<MetaClass>();
		for(MetaClass c : clazz.superclasses) {
			if (c.abstractClass || c.interfaceClass) {
				classes.add(c);
			}
		}
		return classes;
	}

	def selectNonAbstractSubclasses(MetaClass clazz) {
		val classes = new LinkedList<MetaClass>();
		return clazz.selectSubclassesD(classes)
	}
	
	def List<MetaClass> selectSubclassesD(MetaClass clazz, LinkedList<MetaClass> classes) {
		if(!clazz.abstractClass && !clazz.interfaceClass) {
			classes.add(clazz)	
		}
		for(MetaClass c : clazz.subclasses) {
			c.selectSubclassesD(classes)
		}
		return classes
	}
	
	protected var imported=false;
	def handleImports(MetaClass clazz) {
		imported=false;
		'''
		«FOR a : clazz.attributes»
			«IF a.range.equals("DateTime") && !imported»
				«clazz.generateDateImport»
			«ENDIF»
		«ENDFOR»
		'''
	}
	

	protected var importItpm=false
	def importAll(ClassGenerator classGenerator, MetaPackage pkg) { 
		importItpm=false
		'''import «fsa.getPackageName(classGenerator, pkg)».*;'''
	}
	
	def generateDateImport(MetaClass clazz){
		imported=true
		return "import java.util.Date;"
	}
	
	def classTypeGeneration(MetaClass clazz) {	
		'''public «IF clazz.abstractClass»abstract «ENDIF»«
		    IF clazz.interfaceClass»interface «ELSE»class «ENDIF»«clazz.name»«
		    IF !clazz.superclasses.isEmpty() && !clazz.superclasses.iterator.next.interfaceClass» extends «clazz.superclasses.iterator.next»«ENDIF»«
		    IF !clazz.superclasses.isEmpty() && clazz.superclasses.iterator.next.interfaceClass» implements «FOR s : clazz.superclasses SEPARATOR ','»«s.name.toFirstUpper»«ENDFOR»«clazz.addSerializable(true)»«ELSE
		    »«clazz.addSerializable(false)»«ENDIF»
		     '''
	}
	
	def addSerializable(MetaClass clazz, boolean isOtherInterfaces) {
		if(!isOtherInterfaces) {
			if(!clazz.interfaceClass) {
				''' implements java.io.Serializable, org.cdlflex.mdd.sembase.semaccess.MDDBeanInterface'''
			} else {
				''' extends java.io.Serializable'''
			}
		} else {
			''',java.io.Serializable, «fsa.getGenericPackageName()».MDDBeanInterface'''	
		}
	}
	
	
	def hasList(MetaClass clazz) {
		for(a : clazz.associations) {
			if(a.maxRange==-1 || a.minRange>1) {
				return true
			}
		}
		for (a : clazz.selectAttributes) {
			if (a.multiple) {
				return true
			}
		}
		return false
	}
	/**
	 * returns -1 for unlimited associations,
	 * otherwise the maximum value
	 */
	def getMaxRange(MetaAssociation ass) {
		var i = -1
		var unlimited = false
		for(r : ass.to.range) {
			if (r.matches("[0-9]+")) {
				var j=Integer::parseInt(r)
				if(j>i) {
					i=j
				}
			} else if (r.contains("*") || r.trim.isEmpty) {
				unlimited=true
			}
		}
		if(unlimited) {
			return -1
		} else {
				return i	
		}
	}
	
	/**
	 * returns -1 for unlimited associations,
	 * otherwise the maximum value
	 */
	def getMinRange(MetaAssociation ass) {
		var i = 0
		var start = true
		for(r : ass.to.range) {
			if (r.matches("[0-9]+")) {
				var j=Integer::parseInt(r);
				if(j<i || start==true) {
					i=j
					start=false
				}
			}
			if(r.trim.isEmpty){
				i=0
			}
		}
		return i
	}
}
