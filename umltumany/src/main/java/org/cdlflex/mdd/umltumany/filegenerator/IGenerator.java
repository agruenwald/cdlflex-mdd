package org.cdlflex.mdd.umltumany.filegenerator;

/**
 * Interface to communicate to xtend that the
 * type is a generator. 
 */
public interface IGenerator {
    /**
     * Generate the code.
     * @return true if generation succeeded.
     */
    boolean generateCode();
}
