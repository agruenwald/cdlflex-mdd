package org.cdlflex.mdd.umltumany;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.cdlflex.mdd.modelrepo.AdvancedModelStore;
import org.cdlflex.mdd.modelrepo.AdvancedModelStoreImpl;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.umltumany.filegenerator.FileGenerator;
import org.cdlflex.mdd.umltumany.filegenerator.ParameterAccessor;
import org.cdlflex.mdd.umltumany.generators.AllGenerator;
import org.cdlflex.mdd.umltumany.generators.BasicGenerator;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is the main interface for the code generation.
 */
//CHECKSTYLE:OFF ignore coupling abstraction (9 instead of max. 7): irrelevant
public class CodeGenerator {
//CHECKSTYLE:ON
    private final ConfigLoader configLoader;
    private static final Logger LOGGER = LoggerFactory.getLogger(CodeGenerator.class);

    public static final String GENERATOR_CONFIG = "generator-conf.properties";
    private static final String ONLY_JENA = "jena";
    private static final String ONLY_OWLAPI = "owlapi";
    public static final String BOTH = "both";
    public static final String GENERATOR_CONFIG_MODELREPO_PATH = "umltumany/" + GENERATOR_CONFIG;

    private FileGenerator fg;

    public CodeGenerator(ConfigLoader configLoader) {
        this.configLoader = configLoader;
        this.fg = null;
    }

    /**
     * Generate the code and take the entire settings from the model-repo store (including the mode details).
     * @return a list of information about the type of generated models and their location. 
     * @throws MDDException is thrown if information for the generation is missing or the code generation
     * failed, e.g., because of errors in the model or wrong configuration specifications.
     */
    public List<String> generateCode() throws MDDException {
        File fProp = configLoader.findWritableFile(GENERATOR_CONFIG_MODELREPO_PATH);
        LOGGER.info("Determined code generator file is: {}", fProp);
        Properties p = getPropertyFromFile(GENERATOR_CONFIG_MODELREPO_PATH);
        String generatorMode = p.getProperty("generatorMode");
        if (generatorMode.isEmpty()) {
            throw new MDDException(String.format("Generator mode in config file %s missing.", fProp));
        }
        return this.generateCode(p, generatorMode, generatorMode.toLowerCase().equals(BOTH.toLowerCase()) ? false
            : true);
    }

    /**
     * Generate the code, but take some settings from the parameters. Also, read the generator configuration files from
     * the explicitly specified configuration directory (//{configPath}generator-config.properties). The configuration
     * path is passed in the configLoader settings (constructor).
     * @param genMode the generator mode. one of ONLY_OWLAPI, ONLY_JENA, or BOTH variables of CodeGenerator.java
     * @param cleanOnlyMode true if only the directories of ther certain generator (e.g. Jena) should be cleaned.
     * @return a list of information about the type of generated models and their location. 
     * @throws MDDException is thrown if information for the generation is missing or the code generation
     * failed, e.g., because of errors in the model or wrong configuration specifications.
     */
    public List<String> generateCode(String genMode, boolean cleanOnlyMode) throws MDDException {
        String generatorConfigFile = ConfigLoader.CONFIG_PATH 
                + CodeGenerator.GENERATOR_CONFIG; // {configPath}generator-config.properties
        return this.generateCode(this.getPropertyFromFile(generatorConfigFile), genMode, cleanOnlyMode);
    }

    /**
     * Generate code for a single model.
     * @param modelNamespacePrefix the unique namespace prefix for the model.
     * @return information about the generation.
     * @throws MDDException is thrown if information for the generation is missing or the code generation
     * failed, e.g., because of errors in the model or wrong configuration specifications.
     */
    public List<String> generateCodeSingleModel(String modelNamespacePrefix) throws MDDException {
        List<String> srcPaths = new ArrayList<String>();
        AdvancedModelStore store = new AdvancedModelStoreImpl(configLoader);
        MetaModel metaModel = store.findMetaModel(modelNamespacePrefix);
        if (metaModel == null) {
            throw new MDDException(String.format("Model %s not found.", modelNamespacePrefix));
        }
        metaModel = store.retrieveMetaModelMerged(metaModel);
        File fProp = configLoader.findWritableFile(GENERATOR_CONFIG_MODELREPO_PATH);
        LOGGER.info("Determined code generator file is: {}", fProp);
        Properties p = getPropertyFromFile(GENERATOR_CONFIG_MODELREPO_PATH);
        String generatorMode = ParameterAccessor.getGenModeConsiderOpenEngSbType(metaModel, "generatorMode", p);
        LOGGER.info("Generator mode is \"{}\".", generatorMode);
        if (generatorMode.isEmpty()) {
            throw new MDDException(String.format("Generator mode in config file %s missing.", fProp));
        }

        fg = null;
        try {
            fg = new FileGenerator(configLoader, p);
            srcPaths = getAllSrcgenPaths(Arrays.asList(new MetaModel[] {metaModel}), generatorMode, p);
            for (String s : srcPaths) {
                fg.cleanDirectory(s);
            }
        } catch (FileNotFoundException e) {
            throw new MDDException(e.getMessage());
        } catch (IOException e) {
            throw new MDDException(e.getMessage());
        }
        this.generateInternal(metaModel, generatorMode);
        return srcPaths;
    }

    private List<String> generateCode(Properties genConfigFile, String genMode, boolean cleanOnlyMode)
        throws MDDException {
        AdvancedModelStore store = new AdvancedModelStoreImpl(configLoader);
        List<String> srcPaths = new ArrayList<String>();
        fg = null;
        List<MetaModel> installedMetaModels = store.findAllMetaModels();
        try {
            fg = new FileGenerator(configLoader, genConfigFile);
            srcPaths = getAllSrcgenPaths(installedMetaModels, genMode, genConfigFile);
            for (String s : srcPaths) {
                fg.cleanDirectory(s);
            }
            // fg.cleanDirectory(genMode, cleanOnlyMode);
        } catch (FileNotFoundException e) {
            throw new MDDException(e.getMessage());
        } catch (IOException e) {
            throw new MDDException(e.getMessage());
        }
        for (MetaModel metaModelC : installedMetaModels) {
            MetaModel metaModel = metaModelC;
            metaModel = store.retrieveMetaModelMerged(metaModel);
            LOGGER.debug(metaModel.toString());
            this.generateInternal(metaModel, genMode);
        }
        LOGGER.info(String.format("Generated code."));
        return srcPaths;
    }

    private void generateInternal(MetaModel metaModel, String genMode) throws MDDException {
        fg.setCurrentMetaModel(metaModel);
        AllGenerator gen = pickApproriateGenerator(metaModel, genMode);
        gen.generate();
    }

    private AllGenerator pickApproriateGenerator(MetaModel metaModel, String genMode) throws MDDException {
        AllGenerator gen = new AllGenerator(fg, metaModel);
        switch (genMode.toLowerCase()) {
            case ONLY_JENA:
                gen.setOnlyJenaAPI();
                break;
            case ONLY_OWLAPI:
                gen.setOnlyOWLAPI();
                break;
            case BOTH:
                gen.setGeneratorsAll();
                break;
            default:
                throw new MDDException(String.format("Unkown generator argument: %s.", genMode));
        }
        return gen;
    }

    private Properties getPropertyFromFile(String fileName) throws MDDException {
        File fProp = configLoader.findWritableFile(fileName);
        if (fProp == null) {
            throw new MDDException(String.format("Could not load %s (home-dir is %s).", fileName,
                    configLoader.getHomeDir()));
        }
        LOGGER.info("Determined code generator file is: {}", fProp);
        Properties p = new Properties();
        try {
            p.load(configLoader.getUrl(fProp.getAbsolutePath()).openStream());
            return p;
        } catch (MalformedURLException e) {
            throw new MDDException(e.getMessage());
        } catch (IOException e) {
            throw new MDDException(e.getMessage());
        }
    }

    /**
     * returns a list of all paths which have been detected by the current source code generation, depending on the
     * picked models. Can be used to clean up specific parts depending on specific models.
     * 
     * @param pickedModels
     * @param genMode the generation mode
     * @param genConf the properties file with the settings.
     * @return
     * @throws MDDException
     */
    private List<String> getAllSrcgenPaths(List<MetaModel> pickedModels, String genMode, Properties genConf)
        throws MDDException {
        List<String> list = new LinkedList<String>();
        for (MetaModel metaModel : pickedModels) {
            AllGenerator generator = this.pickApproriateGenerator(metaModel, genMode);
            for (BasicGenerator basicGenImpl : generator.getAllGenerators()) {
                String path =
                    ParameterAccessor.findGeneratorModelConfigParameter(genConf, FileGenerator.SRCGEN, basicGenImpl);
                if (!list.contains(new File(path).getAbsolutePath())) {
                    list.add(new File(path).getAbsolutePath());
                }
            }
        }
        return list;
    }

}
