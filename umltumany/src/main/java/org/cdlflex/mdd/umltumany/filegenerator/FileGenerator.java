package org.cdlflex.mdd.umltumany.filegenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.umltumany.generators.BasicGenerator;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents the interface to the xtend2 template generator engine.
 */
public class FileGenerator {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileGenerator.class);
    private static final String PARAMETER_IGNORE_MANUAL = "ignoreManualPaths";
    private final Properties genConf;
    public static final String SRCGEN = "srcgen";
    private static final String PACKAGE_NAMESPACE = "packageNamespace";
    private MetaModel metaModel;
    private ConfigLoader configLoader;

    /**
     * Create a new File generator.
     * @param configLoader the configloader with the correct test flag.
     * @param the generator configuration property file.
     * @throws MDDException reserved for future use.
     */
    public FileGenerator(ConfigLoader configLoader, Properties genConf) throws MDDException {
        this.configLoader = configLoader;
        this.genConf = genConf;
    }

    /**
     * This method must be called each time before a new code generation takes place.
     * 
     * @param m fill the code generator with the current metamodel.
     */
    public void setCurrentMetaModel(MetaModel m) {
        this.metaModel = m;
    }

    /**
     * The global package name is used to set a common namespace with which helper-classes, etc. can be referred.
     * 
     * @param generator the specific generator.
     * @param pkg a specific package.
     * @return the package name.
     */
    public String getPackageNameGlobal(BasicGenerator generator, MetaPackage pkg) {
        String pkgName = "";
        pkgName = pkg.getName().trim().replaceAll(" ", "");

        String pName = PACKAGE_NAMESPACE + "Global";

        String packageStructure = genConf.getProperty(pName);
        if (packageStructure == null) {
            LOGGER.error(String.format("Could not read property %s.", pName));
            return pkgName;
        } else {
            pkgName =
                ParameterAccessor.substitutePlaceholders(metaModel, packageStructure, generator.getTechnology());
            return pkgName;
        }
    }

    /**
     * The global package name is used to set a common namespace with which helper-classes, etc. can be referred.
     * 
     * @param generator the specific generator.
     * @param pkg a specific package.
     * @return the package name.
     * @throws MDDException is thrown if the package name cannot be computed.
     */
    public String getPackageName(BasicGenerator generator, MetaPackage pkg) throws MDDException {
        String packageName =
            ParameterAccessor.findGeneratorModelConfigParameter(genConf, PACKAGE_NAMESPACE, generator);
        return packageName;
    }

    /**
     * Get the generic package name based on the default properties.
     * @return the generic package name.
     */
    public String getGenericPackageName() {
        String gen = genConf.getProperty("packageNamespaceGenericClasses");
        if (gen == null) {
            return "";
        }
        return gen;
    }

    /**
     * Return the package name in file system format (with slashes).
     * @param generator the specific generator.
     * @param pkg a specific package.
     * @return the package name, file-based format (dots are replaced by slashes).
     * @throws MDDException is thrown if the package cannot be accessed.
     */
    public String getSlashedPackageName(BasicGenerator generator, MetaPackage pkg) throws MDDException {
        String n = this.getPackageName(generator, pkg);
        n = n.replace('.', '/');
        return n;
    }

    /**
     * Get the name of the output directory for a specific code, based on the generator name.
     * @param generatorClassName the class name of the generator.
     * @return the output path.
     */
    public String getOutputDirName(String generatorClassName) {
        String outputDir = genConf.getProperty(SRCGEN + generatorClassName);
        return outputDir;
    }

    /**
     * Generates a new file, independent if an old file is existing yet or not.
     * @param generator the specific generator.
     * @param fileName the file name within a project, e.g., DAOXyz.java
     * @param pkg a specific package.
     * @param content the content of the file.
     * @throws MDDException is thrown if the file path cannot be detected, or the generation of a file fails.
     */
    //CHECKSTYLE:OFF 4 instead of 3 parameters in this case is not a big deal.
    public void generateFile(BasicGenerator generator, String fileName, MetaPackage pkg, CharSequence content)
        throws MDDException {
    //CHECKSTYLE:ON
        String fP = this.getPackageFilePath(generator, pkg) + "/" + fileName;
        File f = new File(fP);
        PrintWriter out = null;
        try {
            f.getParentFile().mkdirs();
            f.createNewFile();
            out = new PrintWriter(f);
            out.write(content.toString());
        } catch (FileNotFoundException e) {
            LOGGER.error(String.format(e.getMessage()));
        } catch (IOException e) {
            LOGGER.error(String.format(e.getMessage()));
        } finally {
            out.close();
        }
    }

    /**
     * return the file path to a specific directory (and meta-model), i.e. the place where the code for the model is
     * generated based on a specific generator implementation.
     * 
     * @param generator a specific generator.
     * @param pkg a specific package.
     * @return the path to the package.
     * @throws MDDException is thrown if the package path cannot be detected correctly.
     */
    public String getPackageFilePath(BasicGenerator generator, MetaPackage pkg) throws MDDException {
        String outputDir = ParameterAccessor.findGeneratorModelConfigParameter(genConf, SRCGEN, generator);
        String fP =
            outputDir + (outputDir.charAt(outputDir.length() - 1) == '/' ? "" : "/")
                + this.getSlashedPackageName(generator, pkg);
        return fP;
    }

    /**
     * Clean a specific directory.
     * @param startFileNameAbsolute the leading path of the file name.
     * @throws IOException is thrown if the directory (or one of the sub-directories) cannot
     * be cleaned.
     */
    public void cleanDirectory(String startFileNameAbsolute) throws IOException {
        File start = new File(startFileNameAbsolute);
        if (!start.exists()) {
            return;
        }
        this.cleanDirectory(start, new HashSet<String>());
    }

    private void cleanDirectory(File f, Set<String> ignoredPaths) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                cleanDirectory(c, ignoredPaths);
            }
        }

        boolean skip = false;
        List<String> manualTerms = configLoader.propertyChainReader(this.genConf, PARAMETER_IGNORE_MANUAL);
        for (String manualTerm : manualTerms) {
            if (f.getAbsolutePath().contains(manualTerm)) {
                skip = true;
                ignoredPaths.add(f.getAbsolutePath());
                break;
            }
        }

        if (skip) {
            LOGGER.trace(String.format("SKIPPED cleaning of manual directory %s.", f.getAbsolutePath()));
        } else {
            Iterator<String> it = ignoredPaths.iterator();
            while (it.hasNext()) {
                String man = it.next();
                if (man.contains(f.getAbsolutePath()) && f.isDirectory()) {
                    LOGGER.trace(String.format("SKIPPED cleaning of directory %s due to manual dependency.",
                            f.getAbsoluteFile()));
                    skip = true;
                    break;
                }
            }
            if (!skip) {
                LOGGER.trace("Delete " + f.getAbsolutePath());
                f.delete();
            }
        }
    }
}
