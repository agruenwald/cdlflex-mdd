package org.cdlflex.mdd.umltumany.generators

import org.cdlflex.mdd.umltumany.filegenerator.FileGenerator
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel

class JUnitGeneratorOWLAPI extends JUnitGenerator {
	
	new(FileGenerator fsa, MetaModel metaModel, ClassGenerator classGenerator, DAOGenerator daoGenerator) {
		super(fsa, metaModel, classGenerator, daoGenerator)
	}
    
    override getTechnology() {
		return "OWLAPI";
	}
	
	override getDAOConnectorName() {
		return "OwlApiConnection";
	}
}