package org.cdlflex.mdd.umltumany.generators

import org.cdlflex.mdd.umltumany.filegenerator.FileGenerator
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage
import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation
import org.cdlflex.mdd.sembase.MDDException
import java.util.LinkedList
import org.cdlflex.mdd.umltumany.filegenerator.SemanticMetaEditorFilter

abstract class DAOGenerator extends BasicGenerator {
    
	protected ClassGenerator classGenerator
	
	new (FileGenerator fsa, ClassGenerator classGenerator, MetaModel metaModel) {
        this.fsa = fsa;
        this.metaModel = metaModel;
        this.classGenerator = classGenerator;
    }
   
    new () {
       
    }
    
    override getGeneratorSuperType() {
		return "DAOGenerator";
	}
	
	override boolean generateSpecificsImpl() {
		var it = metaModel.packages.iterator;
		while(it.hasNext) {
			var metaPackage = it.next
			var semFilter = new SemanticMetaEditorFilter(metaModel, metaPackage);
			var itClasses = metaPackage.classes.iterator
			while(itClasses.hasNext) {
				var clazz = itClasses.next
				if (semFilter.shouldGenerateDAO(clazz)) {
					generateDAO(clazz,metaPackage)
				}
			}
		}
		return true
	}
	
	def String generateDateImportDAO(MetaClass clazz);

	def handleImportsDAO(MetaClass clazz,MetaPackage pkg) {
		imported=false;
		importItpm=true
		'''
		«FOR a : clazz.attributes»
			«IF a.range.equals("DateTime") && !imported»
				«clazz.generateDateImportDAO»
			«ENDIF»
		«ENDFOR»
		«IF clazz.selectAssociations.size > 0»
			«FOR ass : clazz.selectAssociations»
				«FOR ax: ass.to.metaClass.selectNonAbstractSubclasses»
					«IF importItpm»
						«this.classGenerator.importAll(pkg)»
					«ENDIF»
				«ENDFOR»
			«ENDFOR»
		«ENDIF»
		'''
	}

	def generateDAO(MetaClass clazz, MetaPackage pkg) {
		if(!clazz.abstractClass && !clazz.enumClass && !clazz.interfaceClass) {
			fsa.generateFile(this,'''«clazz.name»DAO.java''',pkg, this.generateDAO(pkg, clazz))
        
		}
	}
	
	def String generateDAO(MetaPackage pkg, MetaClass clazz);
	
    def validateId(MetaClass clazz, String name) {
	 	'''
	 	«IF clazz.enumClass»
	 		if(!Util.checkMandatory(«name».«clazz.detectId», "«clazz.selectAttributes.firstAttribute.name.toFirstUpper»").isEmpty()) {
	 			throw new MDDException(String.format("%s-%s invalid.","«clazz.name.toFirstUpper»", "«clazz.selectAttributes.firstAttribute.name»"));
	 		}
		«ELSEIF !(clazz.abstractClass || clazz.interfaceClass)»
	 	if(daoFactory.create("«clazz.name.toFirstUpper»", this.con).validateAttributes(«name»)
	 		.get("«clazz.selectAttributes.firstAttribute.name.toFirstLower»") != null) {
	 		throw new MDDException(String.format("%s has got an invalid ID.","«clazz.name.toFirstUpper»"));
	 	}
		«ELSE»
	 	do {
	 		«FOR subclass : clazz.selectNonAbstractSubclasses.reverse»
	 			if(«name» instanceof «subclass.name.toFirstUpper») {
	 				//put into one line because of duplicate vars. Map<String,String> results = ;
	 				if(daoFactory.create("«subclass.name.toFirstUpper»", this.con)
	 					.validateAttributes((«subclass.name.toFirstUpper»)«name»)
	 					.get("«subclass.selectAttributes.firstAttribute.name.toFirstLower»") != null) {
	 					throw new MDDException(String.format("%s has got an invalid ID.","«subclass.name.toFirstUpper»"));
	 				}
	 				break;
	 			}
			«ENDFOR»
	 	} while(false);
		«ENDIF»'''			
	 }
	 
	 
	def validateAttributes(MetaClass clazz) {
		'''public Map<String,String> validateAttributes(«clazz.name.toFirstUpper» «clazz.name.toFirstLower») {
			 Map<String,String> map = new LinkedHashMap<String,String>();
			 «FOR c : clazz.superclasses»
			 	/* superclasses cannot be validated via DAOs if they are abstract */
			 	«IF !c.abstractClass && !c.interfaceClass»
			 		«c.name.toFirstUpper»DAO «c.name.toFirstLower»DAO = null;
			 		try {
			 			«c.name.toFirstLower»DAO = («c.name.toFirstUpper»DAO)this.daoFactory.createNativeDAO("«c.name.toFirstUpper»", this.con); 
			 		} catch (MDDException e) {
			 			logger.error(e.getMessage());
			 		}
			 		map.putAll(«c.name.toFirstLower»DAO.validateAttributes(«clazz.name.toFirstLower»));
			 	«ENDIF»
			 «ENDFOR»
			 «FOR a : clazz.attributesAndAbstractAttributes»
			 	«IF a.multiple»	 		
			 		boolean valid«a.name.toFirstUpper» = true;
			 		«IF a.multMin > 0»
			 			if («clazz.name.toFirstLower».get«a.originalName.toFirstUpper»().size() < «a.multMin») {
			 				valid«a.name.toFirstUpper» = false;
			 				map.put("«a.originalName.toFirstLower»", Util.undercutsMsg("«a.originalName.toFirstLower»",
			 					«clazz.name.toFirstLower».get«a.originalName.toFirstUpper»().size(),
			 					«a.multMin»
			 				));
			 			} 
			 		«ENDIF»
			 		«IF a.multMax >= 0»
			 			if («clazz.name.toFirstLower».get«a.originalName.toFirstUpper»().size() > «a.multMax») {
			 				valid«a.name.toFirstUpper» = false;
			 				map.put("«a.originalName.toFirstLower»", Util.exceedsMsg("«a.originalName.toFirstLower»",
			 					«clazz.name.toFirstLower».get«a.originalName.toFirstUpper»().size(),
			 					«a.multMax»
			 				));
			 			} 
			 		«ENDIF»
			 		if (valid«a.name.toFirstUpper») {
			 			for («a.range.castToGeneric» attr : «clazz.name.toFirstLower».get«a.originalName.toFirstUpper»()) {
			 				String «a.name.toFirstLower» = Util.checkMandatory(attr, "«a.originalName»");
			 				if(!«a.name.toFirstLower».isEmpty()) {
			 					map.put("«a.originalName.toFirstLower»", «a.name.toFirstLower»);
			 				}
			 				if (!this.isDisableManualConstraints()) {
			 					«FOR constr : a.getRegexConstraints»
			 						if(!attr.matches("«constr»")) {
			 							map.put("«a.originalName.toFirstLower»", String.format("%s does not match the pattern %s (value:%s).", "«a.originalName.toFirstUpper»", "«constr»", attr));
			 						}
			 					«ENDFOR»
			 				}
			 			}
			 		}
			 	«ELSE»
			 	String «a.name.toFirstLower» = Util.checkMandatory(«clazz.name.toFirstLower».get«a.originalName.toFirstUpper»(), "«a.originalName»");
			 	if(!«a.name.toFirstLower».isEmpty()) {
			 		map.put("«a.originalName.toFirstLower»", «a.name.toFirstLower»);
			 	} else {
			 		«FOR constr : a.getRegexConstraints»
			 			if(!«clazz.name.toFirstLower».get«a.originalName.toFirstUpper»().matches("«constr»")) {
			 				map.put("«a.originalName.toFirstLower»", String.format("%s does not match the pattern %s (value: %s).", "«a.originalName.toFirstUpper»", "«constr»", «clazz.name.toFirstLower».get«a.originalName.toFirstUpper»()));
			 			}
			 		«ENDFOR»
			 	}
			 	«ENDIF»
			«ENDFOR»
			map = this.revalidateAttributes(«clazz.name.toFirstLower», map);
			return map;
		}'''
	}
	
	def revalidateAttributes(MetaClass clazz) {
		'''/**
			    This method is prepared to be overridden to revalidate attributes.
			**/
			public Map<String,String> revalidateAttributes(«clazz.name.toFirstUpper» «clazz.name.toFirstLower», Map<String,String> map) {
			    return map;
			}'''
	}
	
	def validate(MetaClass clazz) {
		'''public Map<String,String> validate(«clazz.name.toFirstUpper» «clazz.name.toFirstLower») {
				return this.validate(«clazz.name.toFirstLower», false);
			}
			
			public Map<String,String> validate(«clazz.name.toFirstUpper» «clazz.name.toFirstLower», boolean isInsert) {
				Map<String,String> validationMap = this.validateAttributes(«clazz.name.toFirstLower»);
				if (isInsert) {
					if (validationMap.containsKey("id")) {
						validationMap.remove("id");
					}
				}
				validationMap.putAll(this.validateAssociations(«clazz.name.toFirstLower»));
				return validationMap;
			}
		'''
	}
	
	def validateAssociations(MetaClass clazz) {
		'''private Map<String,String> validateAssociations(«clazz.name.toFirstUpper» «clazz.name.toFirstLower») {
				Map<String,String> map = new LinkedHashMap<String,String>();
				«FOR a : clazz.selectAssociations»
					//«a.name» - «a.to.range»
					«IF a.minRange == 1 && a.maxRange == 1»
						if(«clazz.name.toFirstLower».get«a.name.toFirstUpper»() == null) {
							map.put("«a.name»", String.format("%s for %s is missing.","«a.name.toFirstUpper»","«a.from.metaClass.name.toFirstUpper»"));
						}
					«ELSEIF a.minRange >= 0 && a.maxRange == 1»
					«ELSE»
						«IF a.minRange > 0»
							if(«clazz.name.toFirstLower».get«a.name.toFirstUpper»().size() < «a.minRange») {
								map.put("«a.name»", String.format("A minimum of %s %s's is required.","«a.minRange»", "«a.to.metaClass.name.toFirstLower»"));
							}
						«ENDIF»
						«IF a.maxRange > 0»
							if(«clazz.name.toFirstLower».get«a.name.toFirstUpper»().size() > «a.maxRange») {
								map.put("«a.name»", String.format("A maximum of %s %s's is possible.","«a.maxRange»", "«a.to.metaClass.name.toFirstLower»"));
							}
						«ENDIF»
						«IF a.from.range.size > 2»
							boolean accepted = false;
							«FOR x : a.from.range»
								«IF(x.matches("[0-9]+"))»
									if(«clazz.name.toFirstLower».get«a.name.toFirstUpper»().size() == Integer::parseInt(x)) {
										accepted = true;
									}
								«ELSEIF(x.equals("*") || x.trim.empty)»
									accepted = true;
								«ENDIF»
							«ENDFOR»				
							if(!accepted) {
								map.put("«a.name»", String.format("Only a range of %s is possible for %s's.","«a.from.range»", "«a.to.metaClass.name.toFirstLower»"));
							}				
						«ENDIF»
					«ENDIF»
				«ENDFOR»
				return map;
			}'''
	}
		 
	def merge(MetaClass mc, boolean mergeAttributes) {
		'''public boolean merge(«mc.name.toFirstUpper» newBean, «mc.name.toFirstUpper» oldBean) 
		throws MDDException {
			return this.merge(newBean, oldBean, false);
		}
		
		public boolean merge(«mc.name.toFirstUpper» newBean, «mc.name.toFirstUpper» oldBean, boolean addExistingDataAttributes) 
		throws MDDException {
			boolean changed = false;
			«IF mergeAttributes»
				«FOR MetaAttribute at : mc.selectAttributes»
					«IF !at.multiple»
						«IF at.range.toLowerCase.equals("datetime")»
							if(newBean.get«at.originalName.toFirstUpper»() == null 
							|| oldBean.get«at.originalName.toFirstUpper»() == null) {
								if (newBean.get«at.originalName.toFirstUpper»() == null
								&& oldBean.get«at.originalName.toFirstUpper»() != null) {
									changed = true;
									if (addExistingDataAttributes) {
										newBean.set«at.originalName.toFirstUpper»(oldBean.get«at.originalName.toFirstUpper»());
									}
								}	
							} else {
								if(!((newBean.get«at.originalName.toFirstUpper»() != null) 
								&& newBean.get«at.originalName.toFirstUpper»()«at.range.getCompareOperator("oldBean.get" + at.originalName.toFirstUpper + "()")»)
								&& !(oldBean.get«at.originalName.toFirstUpper»()«at.range.getCompareOperator(at.range.defaultValue.toString)»)) {
									if(addExistingDataAttributes) {
										newBean.set«at.originalName.toFirstUpper»(oldBean.get«at.originalName.toFirstUpper»());
									}
									changed = true;
								} 
							}
						«ELSE»
							if(!(newBean.get«at.originalName.toFirstUpper»()«at.range.getCompareOperator("oldBean.get" + at.originalName.toFirstUpper + "()")»)
							&& !(oldBean.get«at.originalName.toFirstUpper»()«at.range.getCompareOperator(at.range.defaultValue.toString)»)) {
								if(addExistingDataAttributes) {
									newBean.set«at.originalName.toFirstUpper»(oldBean.get«at.originalName.toFirstUpper»());
								}
								changed = true;
							} 
						«ENDIF»
					«ELSE»
						if(!((newBean.get«at.originalName.toFirstUpper»() != null) 
							&& newBean.get«at.originalName.toFirstUpper»().size() == oldBean.get«at.originalName.toFirstUpper»().size())
							&& !(oldBean.get«at.originalName.toFirstUpper»().size() == 0)) {
								changed = true;
								if (addExistingDataAttributes) {
									newBean.get«at.originalName.toFirstUpper»().clear();
									newBean.get«at.originalName.toFirstUpper»().addAll(oldBean.get«at.originalName.toFirstUpper»());
								}
								//comparison could be more refined though, but current approach is sufficient
						} 
					«ENDIF»
				«ENDFOR»
			«ENDIF»
			
			«FOR MetaAssociation ass : mc.selectAssociations»
				«IF ass.getMaxRange() > 1 || ass.getMaxRange() < 0»
					if(newBean.get«ass.name.toFirstUpper»().size() != oldBean.get«ass.name.toFirstUpper»().size()) {
						changed = true;
					} else {
						for(«ass.to.metaClass.name.toFirstUpper» newRef : newBean.get«ass.name.toFirstUpper»()) {
							boolean found = false;
							for(«ass.to.metaClass.name.toFirstUpper» oldRef : oldBean.get«ass.name.toFirstUpper»()) {
								if(newRef.«ass.to.metaClass.detectId»«ass.to.metaClass.detectIdRange.toString.getCompareOperator("oldRef." + ass.to.metaClass.detectId)») {
									found = true;
									break;
								}
							}
							if(!found) {
								changed = true;
								break;
							}
						}
					}
					newBean.get«ass.name.toFirstUpper»().addAll(oldBean.get«ass.name.toFirstUpper»());
				«ELSE»
					if((newBean.get«ass.name.toFirstUpper»() == null && oldBean.get«ass.name.toFirstUpper»() != null) 
					|| (newBean.get«ass.name.toFirstUpper»() != null && oldBean.get«ass.name.toFirstUpper»() == null) 
					|| ((newBean.get«ass.name.toFirstUpper»() != null && oldBean.get«ass.name.toFirstUpper»() != null) 
					   && !(newBean.get«ass.name.toFirstUpper»().toString().equals(oldBean.get«ass.name.toFirstUpper»().toString())))) {
					//test not in use any more || !(newBean.get«ass.name.toFirstUpper»().«ass.to.metaClass.detectId»«ass.to.metaClass.detectIdRange.toString.getCompareOperator("oldBean.get" + ass.name.toFirstUpper + "()." + ass.to.metaClass.detectId)»)) {
						changed = true;
					}
					if((newBean.get«ass.name.toFirstUpper»() == null && oldBean.get«ass.name.toFirstUpper»() != null)) {
						newBean.set«ass.name.toFirstUpper»(oldBean.get«ass.name.toFirstUpper»()); //what if one wants to set association to zero? should not be relevant though
						changed = true;
					}
				«ENDIF»
			«ENDFOR»
			return changed;
		}'''
	}
	
	def filter(MetaClass clazz) {
		'''	private boolean filter(String search, «clazz.name.toFirstUpper» «clazz.name.toFirstLower») {
				«FOR a : clazz.selectAttributes»
					«IF a.multiple»
						«IF !a.range.castToGeneric.equals("DateTime") && !a.range.castToGeneric.equals("Date")»
							for («a.range.castToGeneric» attr : «clazz.name.toFirstLower».get«a.originalName.toFirstUpper»()) {
								«IF a.range.equals("String")»
									if(attr.toLowerCase().contains(search.toLowerCase())) {
										return true;
									}
								«ELSEIF a.range.equals("Integer") || a.range.equals("Double") || a.range.equalsIgnoreCase("int")»
									if(String.valueOf(attr).startsWith(search)) {
										return true;
									}
								«ELSEIF a.range.equals("DateTime")»
									//do not look for dates
								«ENDIF»
							}
						«ENDIF»
					«ELSE»
						«IF a.range.equals("String")»
							if(«clazz.name.toFirstLower».get«a.originalName.toFirstUpper»().toLowerCase().contains(search.toLowerCase())) {
								return true;
							}
						«ELSEIF a.range.equals("Integer") || a.range.equals("Double") || a.range.equalsIgnoreCase("int")»
							if(String.valueOf(«clazz.name.toFirstLower».get«a.originalName.toFirstUpper»()).startsWith(search)) {
								return true;
							}
						«ELSEIF a.range.equals("DateTime")»
							//do not look for dates
						«ENDIF»
					«ENDIF»
				«ENDFOR»
				return false;
			}
		'''
		}
		
	def filterByParameters(MetaClass clazz) {	
		'''private boolean filterByParameters(Map<String,String> parameters, «clazz.name.toFirstUpper» «clazz.name.toFirstLower») {
				«FOR a : clazz.selectAttributes»
				if(parameters.containsKey("«a.originalName.toFirstLower»")) {
					«IF !a.multiple»
						«IF a.range.equals("String")»
						if(!(«clazz.name.toFirstLower».get«a.originalName.toFirstUpper»().toLowerCase().equals(parameters.get("«a.originalName.toFirstLower»").toLowerCase()))) {
							return false;
						}
						«ELSEIF a.range.equals("Integer") || a.range.equals("Double") || a.range.equalsIgnoreCase("int")»
							if(!(String.valueOf(«clazz.name.toFirstLower».get«a.originalName.toFirstUpper»()).equals(parameters.get("«a.originalName.toFirstLower»")))) {
								return false;
							}
						«ELSEIF a.range.equals("DateTime")»
							//do not compare dates
	                    «ENDIF»
	                «ELSE»
						«IF !a.range.castToGeneric.equals("DateTime") && !a.range.castToGeneric.equals("Date")»
							for («a.range.castToGeneric» attr : «clazz.name.toFirstLower».get«a.originalName.toFirstUpper»()) {
								«IF a.range.equals("String")»
									if(attr.toLowerCase().equals(parameters.get("«a.originalName.toFirstLower»").toLowerCase())) {
										return true;
									}
								«ELSEIF a.range.equals("Integer") || a.range.equals("Double") || a.range.equalsIgnoreCase("int")»
									if(String.valueOf(attr).equals(parameters.get("«a.originalName.toFirstLower»"))) {
										return true;
									}
								«ELSEIF a.range.equals("DateTime")»
									//do not look for dates
								«ENDIF»
							}
						«ENDIF»
	               	«ENDIF»
				}
	            «ENDFOR»
	        	return true;
	    	}'''
		}
	
	def getRegexConstraints(MetaAttribute a) {
		var res = new LinkedList();
		var constraints = a.getConstraints();
		for (c : constraints) {
			if(c.text.startsWith("pattern[") && c.text.endsWith("]")) {
				var constraint = c.text.substring("[pattern".length, c.text.length-1).trim();
				res.add(constraint);
			} else {
				throw new MDDException(String::format("Cannot parse constraint %s of attribute %s.", c.text, a.name));
			}
		}
		return res;
	}

}