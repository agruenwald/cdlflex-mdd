package org.cdlflex.mdd.umltumany.generators

import org.cdlflex.mdd.umltumany.generators.BasicGenerator
import org.cdlflex.mdd.umltumany.filegenerator.FileGenerator
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel
import java.util.List

/**
 * collects the generators for the generation of all 
 * technologies (OWL + JenaAPI + ...)
 */
class AllGenerator extends BasicGenerator {
	private List<BasicGenerator> generatorList;
	
    new (FileGenerator fsa, MetaModel metaModel) {
        this.metaModel = metaModel;
        this.fsa = fsa;  
        this.generatorList = newArrayList();
    }
    
	override boolean generateSpecificsImpl() {
		generate();
		return true
	}
	
	private def generate(BasicGenerator[] generators) {
		for(g : generators) {
			g.generateCode()
		}
	}
	
	public def generate() {
		generate(this.generatorList);
	}
	
	public def getAllGenerators() {
		return this.generatorList;
	}
	
	public def setGeneratorsAll() {
		var cgOwlapi = new ClassGeneratorOWLAPI(fsa, metaModel);
		var cgJena   = new ClassGeneratorJena(fsa, metaModel);
		var dgOwlapi = new DAOGeneratorOWLAPI(fsa, cgOwlapi, metaModel);
		var dgJena   = new DAOGeneratorJena(fsa, cgJena, metaModel);
		this.generatorList = newArrayList(
				cgOwlapi,
				cgJena, 
				dgOwlapi,
				new JUnitGeneratorOWLAPI(fsa, metaModel, cgOwlapi, dgOwlapi),
				dgJena, 
				new JUnitGeneratorJena(fsa, metaModel, cgJena, dgJena)
		);
	}
	
	public def setOnlyJenaAPI() {
		var cgJena   = new ClassGeneratorJena(fsa, metaModel);
		var dgJena   = new DAOGeneratorJena(fsa, cgJena, metaModel);
		this.generatorList = newArrayList(
			cgJena, 
			dgJena, 
			new JUnitGeneratorJena(fsa, metaModel, cgJena, dgJena)
		);
	}
	
	public def setOnlyOWLAPI() {
		var cgOwlapi = new ClassGeneratorOWLAPI(fsa, metaModel);
		var dgOwlapi = new DAOGeneratorOWLAPI(fsa, cgOwlapi, metaModel);
		this.generatorList = newArrayList(
			cgOwlapi,
			dgOwlapi,
			new JUnitGeneratorOWLAPI(fsa, metaModel, cgOwlapi, dgOwlapi)
		);
	}
	
	override getTechnology() {
		throw new UnsupportedOperationException("AllGenerator does not support the getTechnology() method.")
	}
	
	override getGeneratorSuperType() {
		throw new UnsupportedOperationException("AllGenerator does not support the getGeneratorSuperType() method.")
	}
    
}