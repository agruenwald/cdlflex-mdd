package org.cdlflex.mdd.umltumany.main;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.cdlflex.mdd.modelrepo.InstallConfiguration;
import org.cdlflex.mdd.modelrepo.Installer;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.umltumany.CodeGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main class to trigger code generation from command line, or via the maven plugin.
 */
public final class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    public static final String UML_CONFIG = "umltuowl-config.properties";

    private Main() {
        
    }
    
    /**
     * Method to trigger code generation from command line, or via maven.
     * @param args [0]: --install: install models from a specific directory into the central model repository.
     *                             args [1] the path to the directory, in which the umlTuowl configuration file
     *                                      is contained (file contains information about input etc.). 
     *                  --reset: reset the model repository (remove all models).
     *                              args [1] the path to the directory, in which the umlTuowl configuration file
     *                                      is contained (file contains information about input etc.). 
     *                  --info: get info about all models in the central model store.
     *                  --generate: generate code
     *                              args[1] either jena, owlapi, or both. optional, default: both 
     *                              args[2] cleaning mode if true then only the directories of the generated 
     *                                      technology, will be cleaned. For instance, if only jena is 
     *                                      generated, and the parameter is set to true, then the existing 
     *                                      DAOs and unit tests of Jena will be removed before they are 
     *                                      re-generated, but the DAOs for the OWLAPI (possibly in another.
     *                              args[3] configuration directory (same as args [1] in --reset and --install mode). 
     *                              args[4] if true then the configuration is located in the central model.
     *                  --generate-single generate code for a single model. The parameters are the same as in the
     *                              --generate mode, but parameter 4 must provide the ontology prefix (eg. itpm) to
     *                              identify the model. 
     * @throws IOException is thrown if processing fails.
     * @throws MDDException is thrown if processing fails.
     */
    public static void main(String[] args) throws IOException, MDDException {
        ConfigLoader configLoader = null;
        String mode = args.length > 0 ? args[0].toLowerCase().trim() : "";
        if (mode.equals("--install")) {
            doInstall(args);
            return;

        } else if (mode.equals("--reset")) {
            doReset(args);
            return;

        } else if (mode.equals("--info")) {
            if (args.length != 2) {
                LOGGER.error("Please specify: --reset <abs path to base-config.properties directory>");
                return;
            }
            String configDir = args[1];
            LOGGER.info("Directory for configuration files: {}", configDir);
            configLoader = new ConfigLoader(false, configDir);

            LOGGER.info("Curent model store: ");
            LOGGER.info(Installer.getSummary(configLoader));
            return;

        } else if (mode.equals("--generate")) {
            performGeneration(configLoader, args);
        } else if (mode.equals("--generate-single")) {
            performGenerationSingle(configLoader, args);
        } else {
            LOGGER.error("Invalid mode: use --install/--reset/--generate/--generate-single/--info");
            return;
        }
    }
    
    private static void doInstall(String[] args) throws MDDException {
        if (args.length != 2) {
            LOGGER.error("Please specify: --install <abs path to base-config.properties dir>");
            return;
        }
        ConfigLoader configLoader = null;
        String configDir = args[1];
        // String umlTUowlConfigPath = args[2];
        String umlTUowlConfigPath = Main.class.getProtectionDomain().getCodeSource().getLocation().getFile();

        LOGGER.info("Directory for configuration files: {}", configDir);
        configLoader = new ConfigLoader(false, configDir);
        Properties prop =
            configLoader.findPropertyFile(umlTUowlConfigPath + (umlTUowlConfigPath.endsWith("/") ? "" : "/")
                + UML_CONFIG);
        if (prop == null) {
            throw new MDDException(String.format("UmlConfigStream %s not found.", "{projectPath}" + UML_CONFIG));
        }

        InstallConfiguration ic = new InstallConfiguration();
        ic.setConfigLoader(configLoader);
        ic.setConverterName(configLoader.extractFilePath(prop.getProperty("converter")));
        ic.setInput(configLoader.extractFilePath(prop.getProperty("input")));
        ic.setOptionalIRI(configLoader.extractFilePath(prop.getProperty("IRI")));
        ic.setMerge(true);
        Installer.install(ic);
        LOGGER.info("Installed. Curent model store: ");
        LOGGER.info(Installer.getSummary(configLoader));
        return;
    }
    
    private static void doReset(String[] args) throws MDDException {
        ConfigLoader configLoader = null;
        if (args.length != 2) {
            LOGGER.error("Please specify: --reset <abs path to base-config.properties directory>");
            return;
        }
        String configDir = args[1];
        LOGGER.info("Directory for configuration files: {}", configDir);
        configLoader = new ConfigLoader(false, configDir);

        Installer.reset(configLoader);
        LOGGER.info("Reset executed. Curent model store: ");
        LOGGER.info(Installer.getSummary(configLoader));
        return;
    }

    private static void performGeneration(ConfigLoader configLoader, String[] args) throws MDDException,
        IOException {
        // generation
        String genMode = args.length > 1 ? args[1] : CodeGenerator.BOTH;
        boolean cleanOnlyMode = args.length > 2 ? Boolean.parseBoolean(args[2]) : false;
        String configDir = args.length > 3 ? args[3] : "";
        // false because the relative path is a different one ("../../") than that specified
        // in the central config file.
        boolean configInModelRepo = args.length > 4 ? Boolean.parseBoolean(args[4]) : false;

        LOGGER.info("Generation mode: {}", genMode);
        LOGGER.info("cleanOnlyMode: {}", cleanOnlyMode);
        LOGGER.info("Absolute path in which base-config.properties resides: {}", configDir);
        LOGGER.info("Use generator-config from central model-repo instead from umlTumany/resources: {}",
                configInModelRepo);
        if (args.length < 4 || args.length > 5) {
            LOGGER.error("Please specify: --generate <mode(jena/owlapi/both)> <cleanOnlyMode (default=false)> " 
                   + "<abs path to base-config.properties dir> [useCentralModelStore=false]");
            return;
        }

        LOGGER.info("Auto-determined parameter come next:");
        String generatorConfigPath = Main.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        LOGGER.info("Project (generator) path {}", generatorConfigPath);

        CodeGenerator codeGenerator = null;
        List<String> srcDirs = null;
        if (configInModelRepo) {
            configLoader = new ConfigLoader(false, configDir);
            codeGenerator = new CodeGenerator(configLoader);
            LOGGER.info("Universal home dir is : {} ", configLoader.getHomeDir());
            srcDirs = codeGenerator.generateCode();
        } else {
            configLoader = new ConfigLoader(false, configDir, generatorConfigPath);
            LOGGER.info("Universal home dir is : {} ", configLoader.getHomeDir());
            codeGenerator = new CodeGenerator(configLoader);
            srcDirs = codeGenerator.generateCode(genMode, cleanOnlyMode);
        }
        printSrcDirs(srcDirs);
        LOGGER.info(String.format("Quit code generator successfully."));
    }

    private static void performGenerationSingle(ConfigLoader configLoader, String[] args) throws MDDException,
        IOException {
        // generation
        String genMode = args.length > 1 ? args[1] : CodeGenerator.BOTH;
        boolean cleanOnlyMode = args.length > 2 ? Boolean.parseBoolean(args[2]) : false;
        String configDir = args.length > 3 ? args[3] : "";
        boolean configInModelRepo = true; 
        String modelNamespacePrefix = args.length > 4 ? args[4] : "";

        LOGGER.info("Generation mode: {}", genMode);
        LOGGER.info("cleanOnlyMode: {}", cleanOnlyMode);
        LOGGER.info("Absolute path in which base-config.properties resides: {}", configDir);
        LOGGER.info("Model prefix of the single model: {}", modelNamespacePrefix);
        LOGGER.info("Use generator-config from central model-repo instead from umlTumany/resources: {}",
                configInModelRepo);
        if (args.length < 4 || args.length > 5) {
            LOGGER.error("Please specify: --generate-single <mode(jena/owlapi/both)> " 
                    + "<clean only jena/owlapi/both (default=false)> <abs path to base-config.properties dir>");
            return;
        }

        LOGGER.info("Auto-determined parameter come next:");
        String generatorConfigPath = Main.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        LOGGER.info("Project (generator) path {}", generatorConfigPath);

        List<String> srcDirs = null;
        if (configInModelRepo) {
            configLoader = new ConfigLoader(false, configDir);
            CodeGenerator codeGenerator = new CodeGenerator(configLoader);
            LOGGER.info("Universal home dir is : {} ", configLoader.getHomeDir());
            srcDirs = codeGenerator.generateCodeSingleModel(modelNamespacePrefix);
            printSrcDirs(srcDirs);
        }
        LOGGER.info(String.format("Quit code generator successfully."));
    }

    private static void printSrcDirs(List<String> srcDirs) {
        for (String srcDir : srcDirs) {
            LOGGER.info("Used code generation directory: {}", srcDir);
        }
    }

}
