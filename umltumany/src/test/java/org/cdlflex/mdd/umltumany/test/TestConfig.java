package org.cdlflex.mdd.umltumany.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Properties;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Simple access tests to check whether the access to the central model repository is working.
 */
public class TestConfig {

    /**
     * Test if a wrong file name results in finding nothing.
     * @throws MDDException is thrown if the model repository is not configured properly.
     */
    @Test
    public void testFileAccessNeg() throws MDDException {
        Properties prop = new ConfigLoader(true).findPropertyFile("{projectPath}brabl.properties");
        assertTrue(prop == null);
    }

    /**
     * Test if an existing file is accessed properly.
     * @throws MDDException is thrown if the model repository is not configured properly.
     * @throws IOException is thrown in cases where the file cannot be accessed (e.g. wrong
     * file permissions).
     */
    @Test
    @Ignore
    // does not work from command line (configtest.properties of sembase not in class path)
    public void testFileAccessPos() throws IOException, MDDException {
        Properties prop = new ConfigLoader(true).findPropertyFile("{projectPath}configtest.properties");
        assertTrue(prop != null);
        assertEquals("sembase", prop.getProperty("bundle"));
    }

    /**
     * Test the order of the file access.
     * @throws MDDException is thrown if the model repository is not configured properly.
     * @throws IOException is thrown in cases where the file cannot be accessed (e.g. wrong
     * file permissions).
     */
    @Test
    public void testFileAccessPreferOwnBundle() throws IOException, MDDException {
        Properties prop = new ConfigLoader(true).findPropertyFile("{projectPath}configtest2.properties");
        assertEquals("umltumany", prop.getProperty("bundle"));
    }
}
