package org.cdlflex.mdd.umltumany.test;

import static org.junit.Assert.assertEquals;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

/**
 * Basic tests to test regular expression patterns etc (created during implementation).
 */
public class TestBasicStuff {
    /**
     * Test regular expression to replace system properties.
     */
    @Test
    public void testSystemPropertyReplacer() {
        String testX = "/Users/{system.property.karaf.home}/abc/def/geh";
        testX = testX.replaceAll("\\{system\\.property\\.([a-z\\.]{1,25})\\}", "userhome/$1/andreas");
        assertEquals("/Users/userhome/karaf.home/andreas/abc/def/geh", testX);
    }

    /**
     * Test regular expression to replace system properties.
     */
    @Test
    public void testSystemPropertyReplacer2() {
        String testX = "/Users/{system.property.karaf.home}/abc/def/geh";
        testX = testX.replaceAll("\\{system\\.property\\.([a-z\\.]{1,25})\\}", this.getHello("$1"));
        assertEquals("/Users/hello!karaf.homestop/abc/def/geh", testX);
    }

    private String getHello(String s) {
        return "hello!" + s + "stop";
    }

    /**
     * Test extracting content between two parentheses.
     */
    @Test
    public void testExtractContentBetweenParentheses() {
        String matched = "";
        Pattern pattern = Pattern.compile("\\((.*?)\\)");
        Matcher matcher = pattern.matcher("(20)");
        if (matcher.find()) {
            matched = matcher.group(1);
        }
        assertEquals("20", matched);
    }

    /**
     * Test splitting a string with multiple split characters.
     */
    @Test
    public void testSplit() {
        String str = "originLink=http://www.issue.com/?issue=id&a=b";
        String url = str.split("=", 2)[1];
        assertEquals("http://www.issue.com/?issue=id&a=b", url);
    }
}
