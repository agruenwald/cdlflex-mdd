# CDL FLEX :: MDD :: umlTUmany
Code Generation for Semantic Technologies.

## Summary
Transformation of UML diagrams (Visual Paradigm, Visio and others) into any piece of code, 
including access patterns (DAOs) and automatically generated unit tests.

## Execution
The simplest way to execute and maintain models is to setup the 'Semantic Model Editor'
(`custom-itpm-standalone/standalone-webui`). The model editor provides a graphical interface and the ability 
to generate code directly from the WebUI.

To execute the code generation from the command line please refer to the documentation
which is linked below.

## Maintenance
Install Eclipse IDE with support for modeling tools enabled and download the xtend-plugin
to edit and extend the xtend2 templates.

## Notes
If you have problems with the document encodings of the xtend generators, then
please use UTF-8 format! Regex-command for replacing wrong encoding automatically: 
Replace �([^�]+)� by «$1» (should replace at least most of the wrong characters).

## Further Information
More documentation can be found at Confluence under [Custom ITPM](http://cdl-ind.ifs.tuwien.ac.at/confluence/display/UC/Custom+ITPM 
"Custom ITPM").
