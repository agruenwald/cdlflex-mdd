# CDL FLEX :: MDD :: umlTUowl
umlTUowl enables the transformation of vendor-specific UML class diagrams into OWL2 ontologies.
The transformation process includes the conversion and harmonization of concepts into a
flexible meta-model. Metamodels can be persisted into json files which can be re-imported and reused.

# Further Information
+ `documentation/getting_started.pdf` 
+ [http://sourceforge.net/apps/mediawiki/uml2owl/index.php]
+ [http://www.ifs.tuwien.ac.at/node/16259]
+ [Custom ITPM](http://cdl-ind.ifs.tuwien.ac.at/confluence/display/UC/Custom+ITPM 
"Custom ITPM")

# Notes
umlTUowl has been designed as a standalone application and is used as such by several researchers.
Hence it is not optimized as an OSGI module for the Automated Engineering Service Bus.