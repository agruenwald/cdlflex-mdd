package org.cdlflex.mdd.umltuowl.harmonizer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.cdlflex.mdd.umltuowl.main.ParameterHandling;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.cdlflex.mdd.umltuowl.utils.SettingsReader;
import org.cdlflex.mdd.umltuowl.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Gruenwald Realizes a strategy for merging meta model packages into a single package.
 */
public class MergeStrategy extends Strategy {
    private static final Logger logger = LoggerFactory.getLogger(MergeStrategy.class);

    @Override
    public MetaModel applyStrategy(MetaModel metamodel) {
        MetaModel mergedModel = this.mergePackages(metamodel);
        metamodel = mergedModel;
        metamodel = this.handleAttributesMergeStrategy(metamodel);
        return metamodel;
    }

    /**
     * Merge all packages into a single package and return the new model. Classes are prefixed and post fixed depending
     * on settings for merge class settings.
     * 
     * @param model original model
     * @return new model consisting of one package that contains all merged classes.
     */
    protected MetaModel mergePackages(MetaModel model) {
        logger.debug("Apply merge strategy.");
        Set<String> heap = new HashSet<String>();
        MetaModel mergedModel = new MetaModel(model.getName(), SettingsReader.readProperty(ParameterHandling.IRI));
        mergedModel.setDescription(model.getDescription());
        mergedModel.setNamespace(model.getNamespace());
        mergedModel.setComment(model.getComment());
        Iterator<MetaPackage> it = model.getPackages().iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            Iterator<MetaClass> itClass = p.getClasses().iterator();
            while (itClass.hasNext()) {
                MetaClass c = itClass.next();
                String transformedLocalName = transformLocalClassName(c);
                // pre- and postfixing can be deactivated so that classes are only
                // fixed, if they are duplicates.
                if (!Boolean.parseBoolean(SettingsReader.readProperty("merge-disable-fixing"))
                    || heap.contains(transformedLocalName)) {
                    String packageName = Util.metaNameConvertion(p.getName(), "", "", true);
                    String packagePrefix =
                        SettingsReader.readProperty("merge-class-prefix").replace("{package}", packageName);
                    String packagePostfix =
                        SettingsReader.readProperty("merge-class-postfix").replace("{package}", packageName);
                    transformedLocalName = packagePrefix + transformedLocalName + packagePostfix;
                }
                heap.add(transformedLocalName);
                c.setTransformedName(transformedLocalName);
                // create a new (merged-package)
                MetaPackage mergedPackage =
                    mergedModel.managePackageByName(Util.metaNameConvertion(mergedModel.getName(), "", "", true));
                // be aware of the existing package comments !
                mergedPackage = this.mergeComments(mergedPackage, p);
                mergedPackage.getClasses().add(c);
            }
        }
        return mergedModel;
    }

    private MetaPackage mergeComments(MetaPackage mergedPackage, MetaPackage p) {
        for (MetaComment c : p.getComment()) {
            boolean existing = false;
            for (MetaComment x : mergedPackage.getComment()) {
                if (x.getText().equals(c.getText())) {
                    existing = true;
                    break;
                }
            }

            if (!existing) {
                mergedPackage.getComment().add(c);
            }
        }
        return mergedPackage;
    }

    /**
     * Handle uniqueness of attributes (for merge mode). prefix/postfix attribute names and make them unique (inside a
     * package).
     * 
     * @param metamodel the metamodel
     * @return changed metamodel.
     */
    protected MetaModel handleAttributesMergeStrategy(MetaModel metamodel) {
        String strategy = SettingsReader.readProperty("merge-attribute-strategy");
        logger.info(String.format("Apply merge strategy with parameter '%s'.", strategy));
        for (MetaPackage pckg : metamodel.getPackages()) {
            Set<String> attrHeap = new HashSet<String>();
            List<MetaClass> pckgClasses = pckg.findAllClassesSortedByAbstract();
            for (MetaClass metaClass : pckgClasses) {
                // transform class names if not done yet.

                for (MetaAttribute ma : metaClass.getAttributes()) {
                    String aName = Util.metaNameConvertion(ma.getName(), "", "", true);
                    if (strategy.equalsIgnoreCase("all")) {

                        // first take default attribute prefix (e.g. contains {class}) and afterwards
                        // take merge-attribute prefix if still not unique.
                        aName =
                            this.transformLocalAttributeName(aName, "default-attribute-prefix",
                                    "default-attribute-postfix", metaClass, pckg, true);
                        if (attrHeap.contains(aName)) {
                            aName =
                                this.transformLocalAttributeName(aName, "merge-attribute-prefix",
                                        "merge-attribute-postfix", metaClass, pckg, true);
                        }

                    } else if (strategy.equalsIgnoreCase("none")) {

                        // to nothing

                    } else if (strategy.equalsIgnoreCase("duplicates") || strategy.isEmpty()) {

                        if (attrHeap.contains(aName)) {
                            // first take default attribute prefix (e.g. contains {class}) and afterwards
                            // take merge-attribute prefix if still not unique.
                            aName =
                                this.transformLocalAttributeName(aName, "default-attribute-prefix",
                                        "default-attribute-postfix", metaClass, pckg, true);
                            if (attrHeap.contains(aName)) {
                                aName =
                                    this.transformLocalAttributeName(aName, "merge-attribute-prefix",
                                            "merge-attribute-postfix", metaClass, pckg, true);
                            }
                            logger.info(String.format("Replaced attribute %s by %s for class %s.", ma.getName(),
                                    aName, metaClass));
                        }

                    } else {
                        logger.error(String.format("Invali configuration parameter %s.", "merge-attribute-strategy"));
                        break;
                    }
                    attrHeap.add(aName);
                    ma.setName(aName);
                }
            }
        }
        return metamodel;
    }
}
