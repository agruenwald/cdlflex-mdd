package org.cdlflex.mdd.umltuowl.utils;

import java.util.Comparator;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaElement;

public class MetaModelComparator implements Comparator<MetaElement> {

    @Override
    public int compare(MetaElement m1, MetaElement m2) {
        if (m1.getName().isEmpty() && m2.getName().isEmpty()) {
            // check id
            if (m1 instanceof MetaClass) {
                String id1 = ((MetaClass) m1).getId();
                String id2 = ((MetaClass) m2).getId();
                if (!(id1.isEmpty() && id2.isEmpty())) {
                    return id1.compareTo(id2);
                }
            } else if (m1 instanceof MetaAssociation) {
                String id1 = ((MetaAssociation) m1).getId();
                String id2 = ((MetaAssociation) m2).getId();
                if (!(id1.isEmpty() && id2.isEmpty())) {
                    return id1.compareTo(id2);
                }
            }

            // if ids also are not available or empty, then take hashcode
            if (m1.hashCode() == m2.hashCode()) {
                return 0;
            } else {
                return m1.hashCode() > m2.hashCode() ? 1 : -1;
            }
        } else {
            return m1.getName().compareToIgnoreCase(m2.getName());
        }
    }

}
