package org.cdlflex.mdd.umltuowl.utils;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute;

public class MetaAttributeComparator implements Comparator<MetaAttribute> {
    private static final Map<String, Integer> RATING;
    static {
        RATING = new HashMap<String, Integer>();
        RATING.put("id", 100);
        RATING.put("key", 80);
        RATING.put("no", 25);
        RATING.put("identification", 75);
        RATING.put("uid", 99);
        RATING.put("uuid", 95);
        RATING.put("elementId", 60);
        RATING.put("name", 20); // name is also one of the first elements
    }

    @Override
    public int compare(MetaAttribute a1, MetaAttribute a2) {
        String a1Name = a1.getName().toLowerCase();
        String a2Name = a2.getName().toLowerCase();
        int r1 = RATING.get(a1Name) == null ? 0 : RATING.get(a1Name);
        int r2 = RATING.get(a2Name) == null ? 0 : RATING.get(a2Name);

        if ((r1 - r2) != 0) {
            if ((r1 - r2) > 0) {
                return -1;
            } else {
                return 1;
            }
        } else {
            // consider data type (date at the end)
            boolean a1IsDate = a1.getRange().toLowerCase().contains("date");
            boolean a2IsDate = a2.getRange().toLowerCase().contains("date");
            if (a1IsDate && !a2IsDate) {
                return 1;
            } else if (a2IsDate && !a1IsDate) {
                return -1;
            } else {
                return a1.getName().compareToIgnoreCase(a2.getName());
            }
        }
    }

}
