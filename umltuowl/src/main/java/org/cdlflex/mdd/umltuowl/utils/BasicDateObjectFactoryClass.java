package org.cdlflex.mdd.umltuowl.utils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import flexjson.ObjectBinder;
import flexjson.factories.DateObjectFactory;

public class BasicDateObjectFactoryClass extends DateObjectFactory {
    private static final DateFormat DF1 = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat DF2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
    private static final String DF3_TMPL = "yyyy-MM-dd'T'HH:mm:ss";
    private static final DateFormat DF3 = new SimpleDateFormat(DF3_TMPL);

    @Override
    public Object instantiate(ObjectBinder context, Object value, Type targetType, Class targetClass) {
        if (value instanceof String) {
            String s = (String) value;
            if (s.matches("^\\d{8,15}$")) {
                long lv = Long.parseLong(s);
                return super.instantiate(context, lv, targetType, targetClass);
            } else {
                try {
                    long lv = DF2.parse(s).getTime();
                    return super.instantiate(context, lv, targetType, targetClass);
                } catch (ParseException e) {
                }
                try {
                    String s2 = s.substring(0, Math.min(s.length() - 1, DF3_TMPL.length() - 1));
                    long lv = DF3.parse(s2).getTime();
                    return super.instantiate(context, lv, targetType, targetClass);
                } catch (ParseException e) {

                }
                try {
                    long lv = DF1.parse(s).getTime();
                    return super.instantiate(context, lv, targetType, targetClass);
                } catch (ParseException e) {
                }
            }
        } else if (value instanceof Integer) {
            return super.instantiate(context, ((Integer) value).longValue(), targetType, targetClass);
        }
        return super.instantiate(context, value, targetType, targetClass);
    }
}
