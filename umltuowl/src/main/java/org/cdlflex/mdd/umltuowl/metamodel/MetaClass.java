package org.cdlflex.mdd.umltuowl.metamodel;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.cdlflex.mdd.umltuowl.utils.AssociationMergeSorter;
import org.cdlflex.mdd.umltuowl.utils.MetaAttributeComparator;
import org.cdlflex.mdd.umltuowl.utils.MetaModelComparator;

/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com> Represents a metaclass that can be transfered between UML and OWL.
 *         Abstract classes and interfaces are also handled as meta classes.
 */
public class MetaClass extends MetaElement {
    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String originalName;

    private boolean abstractClass;
    private boolean interfaceClass;
    private boolean enumClass;
    private Set<MetaClass> superclasses;
    private Set<MetaClass> subclasses;

    private List<MetaAttribute> attributes;
    private List<MetaAssociation> associations;

    /**
     * Create a new meta class, interface or abstract class.
     * 
     * @param name the diagram-wide, unique, class name.
     */
    public MetaClass(String name) {
        super();
        this.name = name;
        this.originalName = name;
        this.abstractClass = false;
        this.interfaceClass = false;
        this.enumClass = false;
        this.superclasses = this.initClassSet();
        this.subclasses = this.initClassSet();
        this.attributes = this.initAttributeList();
        this.associations = this.initAssociationList();
    }

    /**
     * Creates a new meta class without name. This constructor must be avoided and should only be used for automated
     * processing, such as serialization.
     */
    public MetaClass() {
        this("");
    }

    /**
     * @return name of meta class, as named in modeling language.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name set the name of the meta class with the name of the class, abstract class or interface name of the
     *        model. Also {@code originalName} is changed.
     */
    public void setName(String name) {
        this.name = name;
        this.originalName = name;
    }

    /**
     * @return {@code true}, if the class has been identified as abstract.
     */
    public boolean isAbstractClass() {
        return abstractClass;
    }

    /**
     * @param abstractClass set to {@code true}, if class has been specified as abstract.
     */
    public void setAbstractClass(boolean abstractClass) {
        this.abstractClass = abstractClass;
    }

    /**
     * @return a set of the class's direct super classes is returned.
     */
    public Set<MetaClass> getSuperclasses() {
        return superclasses;
    }

    /**
     * @param superclasses set or add new super classes to {@code this} class. E.g. parent is a (direct) class of child,
     *        while grandparent is no direct super class.
     */
    public void setSuperclasses(Set<MetaClass> superclasses) {
        this.superclasses = superclasses;
    }

    /**
     * @return a set of the class's direct sub classes is returned.
     */
    public Set<MetaClass> getSubclasses() {
        return subclasses;
    }

    /**
     * @param subclasses set or add new super classes to {@code this} class. I.e. plant is a class and tree is a
     *        subclass of it. fir tree is a subclass of tree, hence fir tree is no (direct) subclass of plant.
     */
    public void setSubclasses(Set<MetaClass> subclasses) {
        this.subclasses = subclasses;
    }

    /**
     * @return a temporary, unique identifier is returned for the class, if set during parsing process of (UML)diagram.
     *         Most diagrams use an internal id that may be useful to associate classes to several elements during meta
     *         model creation process.
     */
    public String getId() {
        return id;
    }

    /**
     * @param id set field with unique identifier for the class. In most cases, the class name is sufficient to identify
     *        a meta class, but during parsing process it may be useful, to store a (UML) diagram's internal identifier
     *        to ease association process. Therefore, this field is optional and the developer of the UML parser has to
     *        decide, whether to use it or leave it empty. The field does not influence the final ontology.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return name of the Class.
     */
    @Override
    public String toString() {
        return this.name;
    }

    /**
     * @param transformedName the name transformed into OWL compatible format. Note, that {@code originalName} still
     *        stays the same.
     */
    public void setTransformedName(String transformedName) {
        this.name = transformedName;
    }

    /**
     * @return original name of the class. Even after transformation of name into OWL compatible characters, this
     *         returns the original model's name.
     */
    public String getOriginalName() {
        return originalName;
    }

    /**
     * @return {@code true}, if meta class represents an interface.
     */
    public boolean isInterfaceClass() {
        return interfaceClass;
    }

    /**
     * @param interfaceClass set to {@code true}, if meta class represents an interface.
     */
    public void setInterfaceClass(boolean interfaceClass) {
        this.interfaceClass = interfaceClass;
    }

    /**
     * @return {@code true}, if meta class represents an enum value (or class).
     */
    public boolean isEnumClass() {
        return enumClass;
    }

    /**
     * @param enumClass set to {@code true}, if meta class represents an enum value (or class).
     */
    public void setEnumClass(boolean enumClass) {
        this.enumClass = enumClass;
    }

    /**
     * @return a list of class attributes is returned. No methods.
     */
    public List<MetaAttribute> getAttributes() {
        return attributes;
    }

    /**
     * @param attributes set a list of attributes (e.g. age, first name, surname).
     */
    public void setAttributes(List<MetaAttribute> attributes) {
        this.attributes = attributes;
    }

    /**
     * @return a list of associations (links to other classes).
     */
    public List<MetaAssociation> getAssociations() {
        return associations;
    }

    /**
     * set or add new associations to a class - either bidirectional or unidirectional. Unidirectional associations are
     * only reached from one meta class.
     * 
     * @param associations list of associations
     */
    public void setAssociations(List<MetaAssociation> associations) {
        this.associations = associations;
    }

    /**
     * find an attribute based on its name and return it.
     * 
     * @param attributeName the name of the attribute (case sensitive)
     * @return the attribute or {@code null}, if it does not exist.
     */
    public MetaAttribute findAttributeByName(String attributeName) {
        for (MetaAttribute a : this.attributes) {
            if (a.getName().equalsIgnoreCase(attributeName)) {
                return a;
            }
        }
        return null;
    }

    /**
     * Same as {@code findAttributeByName(String attributeName)}, but includes linked superclasses within the search.
     * The first attribute is returned (it is assumed that there is typically only one attribute with the same name and
     * the same datatype).
     * 
     * @param attributeName the name of the attribute (case sensitive)
     * @return the attribute or {@code null}, if it does not exist.
     */
    public MetaAttribute findAttributeByNameIncludeSuperClasses(String attributeName) {
        MetaAttribute attr = this.findAttributeByName(attributeName);
        if (attr == null) {
            for (MetaClass superClass : this.getSuperclasses()) {
                attr = superClass.findAttributeByNameIncludeSuperClasses(attributeName); // recursive call
                if (attr != null) {
                    return attr;
                }
            }
            return null;
        } else {
            return attr;
        }
    }

    /**
     * Same as {@code findAttributeByNameIncludeSuperClasses(String attributeName)}, but returns the metaClass element
     * instead of the attribute.
     * 
     * @param attributeName the name of the attribute (case sensitive)
     * @return the metaClass (either this or a superclass of this) to which the attribute belongs. {@code null}, if it
     *         does not exist.
     */
    public MetaClass findSuperclassViaAttributeName(String attributeName) {
        MetaAttribute attr = this.findAttributeByName(attributeName);
        if (attr == null) {
            for (MetaClass superClass : this.getSuperclasses()) {
                attr = superClass.findAttributeByNameIncludeSuperClasses(attributeName); // recursive call
                if (attr != null) {
                    return superClass;
                }
            }
            return null;
        } else {
            return this;
        }
    }

    /**
     * find an association based on the name of the class it links to and return it.
     * 
     * @param className the name of the class linked by the association (case sensitive)
     * @return the association or {@code null}, if it does not exist.
     */
    public MetaAssociation findAssociationByName(String className) {
        List<MetaAssociation> result = this.findAssociationListByName(className);
        if (result.size() >= 1) {
            return result.get(0);
        } else {
            return null;
        }
    }

    /**
     * find an association based on the name of the class it links to and return it.
     * 
     * @param className the name of the class linked by the association (case sensitive)
     * @return the association or {@code null}, if it does not exist.
     */
    public List<MetaAssociation> findAssociationListByName(String className) {
        List<MetaAssociation> result = new LinkedList<MetaAssociation>();
        for (MetaAssociation a : this.associations) {
            if (a.getTo().getMetaClass().getName().equals(className)) {
                result.add(a);
            }
        }
        return result;
    }

    public List<MetaAssociation> findAssociationByNameAndAssociationNameSorted(String className,
        String associationName) {
        List<MetaAssociation> result = new LinkedList<MetaAssociation>();
        List<MetaAssociation> sameToName = this.findAssociationListByName(className);
        for (MetaAssociation a : sameToName) {
            if (a.getName().equals(associationName)) {
                result.add(a);
            }
        }
        Collections.sort(result, new AssociationMergeSorter());
        return result;
    }

    /**
     * find an association based on the name of the association.
     * 
     * @param associationName the name of the association (case sensitive)
     * @return the association or {@code null}, if it does not exist.
     */
    public MetaAssociation findAssociationByAssociationName(String associationName) {
        for (MetaAssociation a : this.associations) {
            if (a.getName().equals(associationName)) {
                return a;
            }
        }
        return null;
    }

    /**
     * find an association based on the id of the association (field id, not associationId).
     * 
     * @param id the id of the association looked for, not empty.
     * @return the association or {@code null}, if it does not exist.
     */
    public MetaAssociation findAssociationViaId(String id) {
        for (MetaAssociation a : this.associations) {
            if (a.getId().equals(id)) {
                return a;
            }
        }
        return null;
    }

    /**
     * removes an association from the class.
     * 
     * @param association the association to be removed.
     */
    public void removeAssociation(MetaAssociation association) {
        for (int i = 0; i < this.associations.size(); i++) {
            MetaAssociation a = this.associations.get(i);
            if (a == association) {
                this.associations.remove(i);
                return;
            }
        }
    }

    /**
     * add an association, but only if the same association (id) does not already exist.
     * 
     * @param na the new association
     * @return true if the association did not exist before.
     */
    public boolean addAssociationConsistent(MetaAssociation na) {
        for (int i = 0; i < this.associations.size(); i++) {
            MetaAssociation a = this.associations.get(i);
            if (a.getId().equals(na.getId())) {
                return false;
            }
        }
        this.associations.add(na);
        return true;
    }

    /**
     * add an attribute, but only if the attribute name does not already exist.
     * 
     * @param na the new association
     * @return true if the attribute did not exist before.
     */
    public boolean addAttributeConsistent(MetaAttribute attr) {
        for (int i = 0; i < this.attributes.size(); i++) {
            MetaAttribute a = this.attributes.get(i);
            if (a.getName().toLowerCase().equals(attr.getName().toLowerCase())) {
                if ((a.getRange() == null || a.getRange().isEmpty()) && attr.getRange() != null) {
                    a.setRange(attr.getRange());
                }
                a.addCommentsConsistent(attr.getComment());
                return false;
            }
        }
        this.attributes.add(attr);
        return true;
    }

    /**
     * Add subclass if it does not exist already in the set of subclasses.
     * 
     * @param clazz the subclass to add.
     * @return true if the class has been added and did not exist previously.
     */
    public boolean addSubclassConsistent(MetaClass clazz) {
        return this.addClassConsistent(this.subclasses, clazz);
    }

    /**
     * Add superclass if it does not exist already in the set of superclasses.
     * 
     * @param clazz the super classes to add.
     * @return true if the class has been added and did not exist previously.
     */
    public boolean addSuperclassConsistent(MetaClass clazz) {
        return this.addClassConsistent(this.superclasses, clazz);
    }

    private boolean addClassConsistent(Set<MetaClass> existingClasses, MetaClass newClass) {
        for (MetaClass eC : existingClasses) {
            if (newClass.getId().equals(eC.getId())) {
                return false;
            }
        }
        existingClasses.add(newClass);
        return true;
    }

    public void sort() {
        Set<MetaClass> superclasses = initClassSet();
        superclasses.addAll(this.superclasses);
        this.superclasses = superclasses;

        Set<MetaClass> subclasses = initClassSet();
        subclasses.addAll(this.subclasses);
        this.subclasses = subclasses;

        // try new model: id and key attributes first, the rest is ordered alphabetically
        // do not sort - order is important!
        Collections.sort(this.attributes, new MetaAttributeComparator());
        Collections.sort(this.associations, new MetaModelComparator());
    }

    private Set<MetaClass> initClassSet() {
        return new LinkedHashSet<>();
        // return new TreeSet<MetaClass>(); //change again?
        // return new TreeSet<MetaClass>(new MetaModelComparator());
    }

    private List<MetaAttribute> initAttributeList() {
        return new LinkedList<MetaAttribute>();
    }

    private List<MetaAssociation> initAssociationList() {
        return new LinkedList<MetaAssociation>();
    }
}
