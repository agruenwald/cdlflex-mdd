package org.cdlflex.mdd.umltuowl.metamodel;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com> An abstract class that contains default properties for classes,
 *         attributes, associations and other meta model objects.
 */
public abstract class MetaElement implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<MetaComment> comment;

    public MetaElement() {
        this.comment = new LinkedList<MetaComment>();
    }

    /**
     * @return a list of comments is returned. Comments either are notes that have been created by the user (e.g. in
     *         UML) or they have been auto generated during transformation process to remain information. E.g. comments
     *         are created, if an abstract class has been transformed, because this informations cannot be stored in OWL
     *         otherwise.
     */
    public List<MetaComment> getComment() {
        return comment;
    }

    /**
     * Get comments/annotations of a specific type.
     * 
     * @param type must be one of the public static type variables specified in {@code MetaComment}.
     * @return a list of comments of the specific type. For instance constraints, internal annotations and so on.
     */
    public List<MetaComment> getCommentByType(String type) {
        List<MetaComment> res = new LinkedList<MetaComment>();
        for (MetaComment c : this.comment) {
            if (c.getType().equals(type)) {
                res.add(c);
            }
        }
        return res;
    }

    /**
     * Shortcut method for retrieving all constraints of a certain element.
     * 
     * @return a list of constraints with size >= 0.
     */
    public List<MetaComment> getConstraints() {
        return this.getCommentByType(MetaComment.TYPE_CONSTRAINT);
    }

    /**
     * see {@link findComments(String type, String annotationSubtype, List<String> valuesLookingFor)}
     * 
     * @param type use MetaComment.TYPE*
     * @param annotationSubtype for instance @status, @example, ...
     * @return
     */
    public List<MetaComment> findComments(String type, String annotationSubtype) {
        return this.findComments(type, annotationSubtype, new LinkedList<String>());
    }

    /**
     * Looks for specific comments.
     * 
     * @param type the type of the comments based on MetaComment.Type*
     * @param annotationSubtype the subtype of the comment, typically starting with @, such as @status
     * @param valuesLookingFor a list of values that should be compared against the subtype value of the comment. For
     *        instance, if the comment contains the text @status:ready, then the value would by "ready". if the list is
     *        empty, then it is ignored (all values are returned).
     * @return a list of comments which match the given type, the annotationSubtype, and which match one of the values
     *         in the list for the annotation subtype property.
     */
    public List<MetaComment> findComments(String type, String annotationSubtype, List<String> valuesLookingFor) {
        List<MetaComment> comments = this.getCommentByType(type);
        List<MetaComment> filtered = new LinkedList<MetaComment>();
        for (MetaComment c : comments) {
            List<String> x = c.getAnnotationSubtype();
            if (x.size() == 2) {
                String annotationType = x.get(0);
                String value = x.get(1);
                if (annotationType.equals(annotationSubtype)
                    && (valuesLookingFor.contains(value) || valuesLookingFor.isEmpty())) {
                    filtered.add(c);
                }
            }
        }
        return filtered;
    }

    /**
     * @param comment set a list of comments or add a comment to a list. Comments either are notes that have been
     *        created by the user (e.g. in UML) or they have been auto generated during transformation process to remain
     *        information. E.g. comments are created, if an abstract class has been transformed, because this
     *        informations cannot be stored in OWL otherwise.
     */
    public void setComment(List<MetaComment> comment) {
        this.comment = comment;
    }

    public void addCommentsConsistent(List<MetaComment> newComments) {
        for (MetaComment newComment : newComments) {
            boolean found = false;
            for (int i = 0; i < this.comment.size(); i++) {
                MetaComment c = this.comment.get(i);
                if (c.toString().equals(newComment.toString())) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                this.comment.add(newComment);
            }

        }

    }

    /**
     * @return name of meta element in human readable form but with as less information as possible (only the name, no
     *         other information).
     */
    public abstract String getName();

}
