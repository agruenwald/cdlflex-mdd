package org.cdlflex.mdd.umltuowl.harmonizer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClassPoint;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Gruenwald This strategy merges classes with same names from different packages into a single class
 *         before applying further merge strategies.
 */
public class MergeStrategyMergeClasses extends MergeStrategy {
    private static final Logger logger = LoggerFactory.getLogger(MergeStrategyMergeClasses.class);
    private boolean applySuperMergeStrategy = true;

    /**
     * @param applySuperMergeStrategy if true then the parent merge strategy will be applied merging attributes,
     *        association names etc. Default = true
     */
    public MergeStrategyMergeClasses(boolean applySuperMergeStrategy) {
        this.applySuperMergeStrategy = applySuperMergeStrategy;
    }

    @Override
    public MetaModel applyStrategy(MetaModel metamodel) {
        metamodel = this.mergeDuplicateClasses(metamodel);
        if (applySuperMergeStrategy) {
            metamodel = super.applyStrategy(metamodel);
        }
        return metamodel;
    }

    /**
     * @param model original model
     * @return new model consisting of one package that contains all merged classes.
     */
    private MetaModel mergeDuplicateClasses(MetaModel oldModel) {
        logger.info("Merge reused/duplicate classes ( " + MergeStrategyMergeClasses.class.getName()
            + " sub - strategy).");

        MetaModel mm = new MetaModel();
        mm.setName(oldModel.getName());
        mm.setNamespace(oldModel.getNamespace());
        mm.setDescription(oldModel.getDescription());
        mm.setComment(oldModel.getComment());
        Iterator<MetaPackage> pit = oldModel.getPackages().iterator();
        while (pit.hasNext()) {
            MetaPackage p = pit.next();
            MetaPackage np = new MetaPackage();
            np.setName(p.getName());
            np.setComment(p.getComment());
            Iterator<MetaClass> cit = p.getClasses().iterator();
            while (cit.hasNext()) {
                MetaClass mc = cit.next();
                MetaClass nmc = new MetaClass(mc.getName());
                boolean existingClass = false;
                if (mm.findClassByName(nmc.getName()) != null) {
                    nmc = mm.findClassByName(nmc.getName());
                    existingClass = true;
                }

                nmc.setId(mc.getId());
                nmc.setAbstractClass(mc.isAbstractClass());
                nmc.setEnumClass(mc.isEnumClass());
                nmc.setInterfaceClass(mc.isInterfaceClass());
                nmc.addCommentsConsistent(mc.getComment());

                for (MetaAttribute ma : mc.getAttributes()) {
                    MetaAttribute nma = new MetaAttribute();
                    nma.addCommentsConsistent(ma.getComment());
                    nma.setName(ma.getName());
                    nma.setRange(ma.getRange());
                    nma.setMultMin(ma.getMultMin());
                    nma.setMultMax(ma.getMultMax());
                    nma.setRangeIsEnum(ma.isRangeIsEnum());
                    nma.setVisibility(ma.getVisibility());
                    nmc.addAttributeConsistent(nma);
                }

                if (!existingClass) {
                    np.getClasses().add(nmc);
                }
            }
            mm.getPackages().add(np);
        }

        pit = oldModel.getPackages().iterator();
        while (pit.hasNext()) {
            MetaPackage p = pit.next();
            List<MetaClass> oldP = p.findAllClassesSortedByAbstract();

            for (int i = 0; i < oldP.size(); i++) {
                MetaClass mc = oldP.get(i);
                MetaClass nmc = mm.findClassByName(mc.getName());
                if (nmc == null) {
                    throw new RuntimeException(String.format("Could not find class with name %s in new model.",
                            mc.getName()));
                }

                for (MetaClass s : mc.getSubclasses()) {
                    if (mm.findClassByName(s.getName()) == null) {
                        throw new RuntimeException(
                                String.format("Sub-MetaClass with name=%s not found.", s.getName()));
                    }
                    nmc.addSubclassConsistent(mm.findClassByName(s.getName()));
                }

                for (MetaClass s : mc.getSuperclasses()) {
                    if (mm.findClassByName(s.getName()) == null) {
                        throw new RuntimeException(String.format("Super-MetaClass with name=%s not found.",
                                s.getName()));
                    }
                    nmc.addSuperclassConsistent(mm.findClassByName(s.getName()));
                }

                for (MetaAssociation a : mc.getAssociations()) {
                    MetaAssociation na = createAssociation(mm, a);
                    MetaAssociation maInv = a.getInverseAssociation();
                    if (maInv != null) {
                        na.setInverseAssociation(createAssociation(mm, maInv));
                        na.getInverseAssociation().setInverseAssociation(na);

                        // add inverse association to the counterpart.
                        MetaClass nmcCounterPart =
                            mm.findClassByName(na.getInverseAssociation().getFrom().getMetaClass().getName());
                        if (nmcCounterPart == null) {
                            throw new RuntimeException(String.format(
                                    "Could not find counterpart class with name %s in new model.", na
                                            .getInverseAssociation().getFrom().getMetaClass().getName()));
                        }
                        nmc.addAssociationConsistent(na);
                        nmcCounterPart.addAssociationConsistent(na.getInverseAssociation()); // TODO check
                        // nmc.addAssociationConsistent(na.getInverseAssociation());
                        /*
                         * //test if(!nmc.getName().equals(nmcCounterPart.getName())) {
                         * nmcCounterPart.addAssociationConsistent(na.getInverseAssociation()); } else {
                         * nmc.addAssociationConsistent(na.getInverseAssociation()); }
                         */
                    } else {
                        nmc.addAssociationConsistent(na);
                    }
                }
            }
        }

        // finally, walk through all associations and remove those which are identified as being the same, but which
        // carry less information than the "master" association.
        boolean restart = true;
        while (restart) {
            restart = false;
            List<MetaAssociation> assocs = mm.getAllAssociations(); // all associations, but each association
                                                                    // (association-id) is only included once
            for (MetaAssociation assoc : assocs) {
                MetaClass fromClass = mm.findClassByName(assoc.getFrom().getMetaClass().getName());
                if (fromClass == null) {
                    throw new RuntimeException(String.format("Could not find class with name %s in new model.", assoc
                            .getFrom().getMetaClass().getName()));
                }

                MetaClass toClass = mm.findClassByName(assoc.getTo().getMetaClass().getName());
                if (toClass == null) {
                    throw new RuntimeException(String.format("Could not find class with name %s in new model.", assoc
                            .getTo().getMetaClass().getName()));
                }

                // possible candidates (from-to-classes match)
                List<MetaAssociation> relevantAssocs =
                    fromClass.findAssociationByNameAndAssociationNameSorted(toClass.getName(), assoc.getName());
                if (relevantAssocs.size() == 0) {
                    relevantAssocs =
                        toClass.findAssociationByNameAndAssociationNameSorted(fromClass.getName(), assoc.getName());
                }

                MetaAssociation mostInformativeAssoc = relevantAssocs.get(0);
                Set<String> idsToRemove = new HashSet<String>(); // set for duplicate self-references
                for (int i = 1; i < relevantAssocs.size(); i++) {
                    MetaAssociation duplAssoc = relevantAssocs.get(i);
                    // TODO if from and to-class are the same then the condition
                    // to remove the class is that the id of the duplicate classes
                    // differ (??) - or the id is not inverse (??)
                    if (duplAssoc.getFrom().getMetaClass().getName()
                            .equals(duplAssoc.getTo().getMetaClass().getName())) {
                        // self-reference
                        if (duplAssoc.getInverseAssociation() != null) {
                            if (duplAssoc.getInverseAssociation().getId().equals(mostInformativeAssoc.getId())) {
                                // inverse of association to maintain
                                continue;
                            } else {
                                // remove - different
                            }
                        } else {
                            // remove - unidirectional duplicate (?)
                        }
                    }
                    idsToRemove.add(duplAssoc.getId());
                    mostInformativeAssoc.addCommentsConsistent(duplAssoc.getComment());
                    if (duplAssoc.getInverseAssociation() != null) {
                        idsToRemove.add(duplAssoc.getInverseAssociation().getId());
                        if (mostInformativeAssoc.getInverseAssociation() != null) {
                            mostInformativeAssoc.getInverseAssociation().addCommentsConsistent(
                                    duplAssoc.getInverseAssociation().getComment());
                        }
                    }
                }

                if (idsToRemove.size() > 0) {
                    restart = true;
                    for (String id : idsToRemove) {
                        /*
                         * if (logger.isInfoEnabled()) { List<MetaAssociation> as = mm.findAssociationById(id); for
                         * (MetaAssociation a : as) {
                         * logger.info(String.format("Going to remove association %s (id=%s) (belongs to package %s).",
                         * a, a.getId(), mm.findParentPackage(a.getFrom().getMetaClass()).getName())); } }
                         */
                        int removed = mm.removeAssociationById(id);
                        if (removed <= 0) {
                            List<MetaAssociation> as = mm.findAssociationById(id);
                            for (MetaAssociation a : as) {
                                logger.info(String.format(
                                        "Tried to remove association %s (id=%s) (belongs to package %s).", a,
                                        a.getId(), mm.findParentPackage(a.getFrom().getMetaClass()).getName()));
                            }
                            throw new RuntimeException(String.format(
                                    "Did not remove any associations with id %s (intended ids to remove where %s).",
                                    id, idsToRemove));
                        } else {
                            logger.info(String.format("Removed association with id %s %d times.", id, removed));
                        }
                    }
                }

                if (restart) {
                    break;
                }

            }
        }

        return mm;
    }

    private static MetaAssociation createAssociation(MetaModel nmm, MetaAssociation a) {
        MetaAssociation na = new MetaAssociation();
        na.setId(a.getId() == null ? "" : a.getId());
        na.setAssociationId(a.getAssociationId() == null ? "" : a.getAssociationId());
        na.setName(a.getName() == null ? "" : a.getName());
        na.setComment(a.getComment());
        na.setAggregation(a.isAggregation());
        na.setComposition(a.isComposition());
        na.addCommentsConsistent(a.getComment());

        MetaClassPoint nfrom = new MetaClassPoint();
        if (nmm.findClassByName(a.getFrom().getMetaClass().getName()) == null) {
            throw new RuntimeException(String.format(
                    "Association: From-MetaClass with name=%s not found in new meta-model.", a.getFrom()
                            .getMetaClass().getName()));
        }

        MetaClass from = nmm.findClassByName(a.getFrom().getMetaClass().getName());
        nfrom.setMetaClass(from);
        nfrom.setMetaClassId(from.getId());
        nfrom.setRange(a.getFrom().getRange());

        MetaClassPoint nto = new MetaClassPoint();
        if (nmm.findClassByName(a.getTo().getMetaClass().getName()) == null) {
            throw new RuntimeException(String.format(
                    "Association: To-MetaClass with name=%s not found in new meta-model.", a.getTo().getMetaClass()
                            .getName()));
        }

        MetaClass to = nmm.findClassByName(a.getTo().getMetaClass().getName());
        nto.setMetaClass(to);
        nto.setMetaClassId(to.getId());
        nto.setRange(a.getTo().getRange());

        na.setFrom(nfrom);
        na.setTo(nto);
        return na;
    }

}
