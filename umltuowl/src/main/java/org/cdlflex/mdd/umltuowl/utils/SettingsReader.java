package org.cdlflex.mdd.umltuowl.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com> Utility class for reading properties from default properties file to
 *         specify different settings.
 */
public class SettingsReader {
    private static final String SETTINGS = "settings.properties";
    private static Properties settings = new Properties(); // accept as default behavior from outside
    private static Properties userSettings = new Properties();
    private static Map<String, String> userInput = new HashMap<String, String>();

    /**
     * @param key the property key. The key will be tested against this value.
     * @return the value for the given property or empty value, if property does not exist.
     */
    public static String readProperty(String key) {
        String propValue = "";
        if (userInput.get(key) != null && !userInput.get(key).isEmpty()) {
            propValue = userInput.get(key);
        } else {
            propValue = userSettings.isEmpty() ? settings.getProperty(key, "") : userSettings.getProperty(key, "");
        }
        return propValue;
    }

    /**
     * enables to read multiple values of a key. the key must be prefixed with "-1", "-2", ... starting at 1 in the
     * properties file. At the moment a maximum of 100 values is supported, which should be sufficient. If multiple
     * values are used then these values must exist with suffixes in strict order, i.e., -1, -2 and not -1, -5, ...
     * 
     * @param key the original key requested from the properties file, e.g. "output" when output=file://.....
     * @return a list of properties or an empty array if nothing available.
     */
    public static List<String> readPropertyArray(String key) {
        List<String> result = new LinkedList<String>();
        List<String> variants = new LinkedList<String>();
        variants.add(key);
        for (int i = 1; i <= 100; i++) {
            variants.add(key + "-" + i);
        }

        int i = 0;
        for (String keyVariant : variants) {
            String value = "";
            if (userInput.get(keyVariant) != null && !userInput.get(keyVariant).isEmpty()) {
                value = userInput.get(keyVariant);
            } else {
                value =
                    userSettings.isEmpty() ? settings.getProperty(keyVariant, "") : userSettings.getProperty(
                            keyVariant, "");
            }

            if (!value.isEmpty()) {
                result.add(value);
            } else {
                if (i >= 1) { // break if no continuous values exist.
                    break;
                }
            }
            i++;
        }

        return result;
    }

    /**
     * @param key name of the property.
     * @return {@code true}, if property is existing (that means, that name must be known in default properties file).
     */
    public static boolean containsProperty(String key) {
        return settings.containsKey(key);
    }

    /**
     * Overwrite property or set a new value.
     * 
     * @param key name of the property
     * @param value the value of the property
     */
    public static void overwriteProperty(String key, String value) {
        userInput.put(key, value);
    }

    /**
     * @param mySettings if not empty, the user's properties file is used for overriding the default settings.
     * @throws IOException if file could not be found.
     */
    public static void loadUserSettings(String mySettings) throws IOException {
        userSettings = new Properties();
        if (!mySettings.isEmpty()) {
            String path = mySettings;
            InputStream in = null;
            try {
                URL url = null;
                try {
                    // hack for Karaf service bus (if resource given as stream)
                    url = new SettingsReader().getClass().getClassLoader().getResource(mySettings);
                } catch (Exception e) {
                }
                if (url != null) {
                    in = url.openStream();
                } else {
                    in = new FileInputStream(new File(path));
                }
            } catch (IOException e) {
                throw new IOException(String.format("%s not found. Make shure, the file is located at %s.",
                        mySettings, path));
            }
            // ClassLoader.getSystemResourceAsStream(path);
            /*
             * if(in == null) {
             * 
             * }
             */
            userSettings.load(in);
            in.close();
        }
    }

    /**
     * load the settings from a given file.
     * 
     * @param file
     * @throws IOException
     */
    public static void loadSettingsFromExternalFile(Properties properties) throws IOException {
        settings = properties;
    }

    /**
     * Loads global settings from properties file.
     * 
     * @throws IOException if file could not be found.
     */
    public static void loadSettings() throws IOException {
        settings = new Properties();
        String path = SETTINGS;
        URL url = new SettingsReader().getClass().getClassLoader().getResource(path);
        if (url == null) {
            throw new IOException(String.format("%s not found. Make sure, the file is located in your classpath.",
                    SETTINGS));
        }
        InputStream in = url.openStream();
        settings.load(in);
        in.close();
    }

}
