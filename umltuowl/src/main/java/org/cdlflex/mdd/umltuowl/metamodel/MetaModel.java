package org.cdlflex.mdd.umltuowl.metamodel;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.cdlflex.mdd.umltuowl.metamodeltoowl.JsonMaker;
import org.cdlflex.mdd.umltuowl.utils.MetaModelComparator;

/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com> Represents the global model that contains all packages and hence all
 *         classes and constructs of a UML class model or an Ontology and is used to transfer between UML and OWL (or
 *         reverse).
 */
public class MetaModel extends MetaElement implements Comparable<MetaModel>, Serializable {
    private static final long serialVersionUID = 8650410999355739012L;

    private String name;
    private String namespace;
    private String description;
    private Set<MetaPackage> packages;

    /**
     * Creates a new meta model without name. This constructor must be avoided and should only be used for automated
     * processing, such as serialization.
     */
    public MetaModel() {
        this("", "");
    }

    /**
     * Create the meta model. Usually, for each meta model one OWL ontology is created (into a separate file). One meta
     * model is designed to contain one UML package.
     * 
     * @param name the name of the meta model (descriptive), if existing.
     */
    public MetaModel(String name, String namespace) {
        this.packages = this.initSet();
        this.name = name;
        this.namespace = namespace;
        this.description = "";
    }

    public MetaModel(String name) {
        this(name, "");
    }

    /**
     * @return name of the meta model or empty if none was existing.
     */
    public String getName() {
        return name;
    }

    /**
     * set the name of the meta model (descriptive). avoid special characters. Blanks are allowed.
     * 
     * @param name the name of the meta model.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * set the namespace of the metamodel, e.g. http://www.tuwien.ac.at/itpm.owl
     * 
     * @param namespace e.g. http://www.tuwien.ac.at/itpm.owl
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /**
     * @return the namespace of the model is returned. However, is optinal, although it should be used.
     */
    public String getNamespace() {
        return this.namespace;
    }

    /**
     * @return namespace - prefix if getNamespace() is not empty. example: itpm
     */
    public String getNamespacePrefix() {
        if (this.namespace.isEmpty()) {
            return "";
        } else {
            String prefix = this.namespace; // http://xyz.abc/itpm.owl
            prefix = prefix.replaceAll(".owl", ""); // http://xyz.abc/itpm
            String reverseString = new StringBuilder(prefix).reverse().toString(); // mpti/cba.zyx//:pttp
            int pos = reverseString.indexOf("/"); // mpti/cba.zyx//:pttp
            prefix = reverseString.substring(0, pos); // mpti
            prefix = new StringBuffer(prefix).reverse().toString(); // itpm
            return prefix;
        }
    }

    /**
     * @return additional description if available.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description set the description, if available. Anything is allowed.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return returns a set of all packages inside the meta model.
     */
    public Set<MetaPackage> getPackages() {
        return packages;
    }

    /**
     * find a meta class by its identifier (internal) an return it.
     * 
     * @param classId unique class id (!= class name).
     * @return the meta class with the given class id or {@code null}, if no meta class matched.
     */
    public MetaClass findClassById(String classId) {
        MetaClass res = null;
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            MetaClass metaClass = p.findClassById(classId);
            if (metaClass != null) {
                if (res != null) {
                    throw new RuntimeException(String.format("ClassId %s exists several times!!!!", classId));
                }
                res = metaClass;
            }
        }
        return res;
    }

    /**
     * find a meta class by its name an return it.
     * 
     * @param name unique class name.
     * @return the meta class with the given class id or {@code null}, if no meta class matched.
     */
    public MetaClass findClassByName(String name) {
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            MetaClass metaClass = p.findClassByName(name);
            if (metaClass != null) {
                return metaClass;
            }
        }
        return null;
    }

    /**
     * finds all classes of the entire model with a certain name.
     * 
     * @param name the class name, does not need to be necessarily unique. For instance in package 1 and package 5 a
     *        class could exist with the same name. Whether it represents the same entity or not is up to the
     *        harmonizing strategy. The currently constraint is that each package contains only unique classes though
     *        (e.g. package 1 cannot contain 2 classes named "House").
     * @return a list of equally named meta classes.
     */
    public List<MetaClass> findAllClassesByName(String name) {
        Iterator<MetaPackage> it = this.packages.iterator();
        List<MetaClass> list = new LinkedList<MetaClass>();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            MetaClass metaClass = p.findClassByName(name);
            if (metaClass != null) {
                list.add(metaClass);
            }
        }

        return list;
    }

    /**
     * retrieve all classes in the entire meta model.
     * 
     * @return all classes available in the entire meta model.
     */
    public List<MetaClass> getAllClasses() {
        Iterator<MetaPackage> it = this.packages.iterator();
        List<MetaClass> list = new LinkedList<MetaClass>();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            list.addAll(p.getClasses());
        }
        return list;
    }

    /**
     * @return number of class elements that are contained in the whole meta model.
     */
    public int getSize() {
        int size = 0;
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            size = size + p.getClasses().size();
        }
        return size;
    }

    /**
     * find a meta package by its name and return it.
     * 
     * @param packageName name of the package, case sensitive.
     * @return meta package or {@code null}, if package does not exist.
     */
    public MetaPackage findPackageByName(String packageName) {
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            if (p.getName().equals(packageName)) {
                return p;
            }
        }
        return null;
    }

    /**
     * find an association based on its ID. Should not be used to often, because this method is computational expensive.
     * 
     * @param associationId name of the association ID
     * @return list of found meta associations. In case of unidirectional relationship, two associations are found. If
     *         ID is unknown, an empty list is returned.
     */
    public List<MetaAssociation> findAssociationById(String associationId) {
        List<MetaAssociation> list = new LinkedList<MetaAssociation>();
        for (MetaAssociation ass : this.getAllAssociations()) {
            if (ass.getAssociationId().equals(associationId)) {
                list.add(ass);
            }
        }
        return list;
    }

    /**
     * Find all associations which link to or from a specific meta class. Usually in a class only the "known"
     * associations are stored. However, if there are unidirectional associations then they are not known within the
     * metaclass. This method finds ALL associations related to a class.
     * 
     * @param metaClass the metaClass, filled with correct id, name.
     * @return a list of associations which contain the metaClass either as a from, or as a to-class.
     */
    public List<MetaAssociation> findAssociationsAssociatedWithClass(MetaClass metaClass) {
        List<MetaAssociation> list = new LinkedList<MetaAssociation>();
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            for (MetaClass packageMetaClass : p.getClasses()) {
                for (MetaAssociation ass : packageMetaClass.getAssociations()) {
                    if (ass.getFrom().getMetaClass().equals(metaClass)
                        || ass.getTo().getMetaClass().equals(metaClass)) {
                        list.add(ass);
                    }
                }
            }
        }
        return list;
    }

    /**
     * Detect to which class associations belong
     * 
     * @param associationId unique association id
     * @return list of all classes matching the association. Dupliates possible.
     */
    public List<MetaClass> findClassCarryingAssociationId(String associationId) {
        List<MetaClass> list = new LinkedList<MetaClass>();
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            for (MetaClass metaClass : p.getClasses()) {
                for (MetaAssociation ass : metaClass.getAssociations()) {
                    if (ass.getAssociationId().equals(associationId)) {
                        list.add(ass.getFrom().getMetaClass());
                        if (ass.getInverseAssociation() != null
                            && ass.getInverseAssociation().getAssociationId().equals(associationId)) {
                            list.add(ass.getTo().getMetaClass());
                            return list;
                        }
                    }
                }
            }
        }
        return list;
    }

    public List<MetaAssociation> findAssociationsAssociatedWithClassName(String className) {
        List<MetaAssociation> list = new LinkedList<MetaAssociation>();
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            for (MetaClass packageMetaClass : p.getClasses()) {
                for (MetaAssociation ass : packageMetaClass.getAssociations()) {
                    if (ass.getFrom().getMetaClass().getName().equals(className)
                        || ass.getTo().getMetaClass().getName().equals(className)) {
                        if (!dullExists(list, ass)) {
                            list.add(ass);
                        }
                    }
                }
            }
        }
        return list;
    }

    private boolean dullExists(List<MetaAssociation> list, MetaAssociation assoc) {
        boolean exists = false;

        for (MetaAssociation a : list) {
            if (a.getId().equals(assoc.getId())) {
                return true;
            }
        }

        return exists;
    }

    /**
     * @return a list of all associations of the entire meta model, including inverse associations.
     */
    public List<MetaAssociation> getAllAssociations() {
        List<MetaAssociation> list = new LinkedList<MetaAssociation>();
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            for (MetaClass metaClass : p.getClasses()) {
                for (MetaAssociation ass : metaClass.getAssociations()) {
                    if (!dullExists(list, ass)) {
                        list.add(ass);
                    }

                    if (ass.getInverseAssociation() != null) {
                        if (!dullExists(list, ass.getInverseAssociation())) {
                            list.add(ass.getInverseAssociation());
                        }
                    }
                }
            }
        }

        return list;
    }

    public int removeAssociationById(String id) {
        int removed = 0;
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            for (MetaClass packageMetaClass : p.getClasses()) {
                boolean restart = true;
                while (restart) {
                    restart = false;
                    for (MetaAssociation ass : packageMetaClass.getAssociations()) {
                        if (ass.getId().equals(id)) {
                            packageMetaClass.removeAssociation(ass);
                            removed++;
                            restart = true;
                            break;
                        }
                    }
                }
            }
        }
        return removed;
    }

    /**
     * same as {@code findPackagByName}, but creates a new package and returns it, if package is not already existing.
     * 
     * @param packageName name of the package, case sensitive.
     * @return meta package; either existing one or a new one with empty class set.
     */
    public MetaPackage managePackageByName(String packageName) {
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            if (p.getName().equals(packageName)) {
                return p;
            }
        }
        MetaPackage newPackage = new MetaPackage(packageName);
        this.packages.add(newPackage);
        return newPackage;
    }

    /**
     * find the package a class belongs to.
     * 
     * @param clazz the metaclass.
     * @return the package the class belongs to or null if the clazz does not belong to any package. (the id is
     *         compared).
     */
    public MetaPackage findParentPackage(MetaClass clazz) {
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            MetaClass mc = p.findClassById(clazz.getId());
            if (mc != null) {
                return p;
            }
        }
        return null;
    }

    /**
     * @param clazz a class to be removed.
     * @return true if the class was found and has been deleted.
     */
    public boolean removeClassById(String classId) {
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage p = it.next();
            for (MetaClass mc : p.getClasses()) {
                if (mc.getId().equals(classId)) {
                    p.getClasses().remove(mc);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * resort the entire metamodel after modifications have been performed on names, ids, etc. of sub-elements. The set
     * itself does not sync its order when attributes change dynamically.
     */
    public void sort() {
        Set<MetaPackage> packages = this.initSet();
        packages.addAll(this.packages);
        this.packages = packages;
        for (MetaPackage p : this.packages) {
            p.sort();
        }
    }

    private Set<MetaPackage> initSet() {
        return new TreeSet<MetaPackage>(new MetaModelComparator());
    }

    /**
     * @return whole model is returned as a string containing package, class and attribute information.
     */
    @Override
    public String toString() {
        String s = "";
        Iterator<MetaComment> itComments = this.getComment().iterator();
        while (itComments.hasNext()) {
            MetaComment c = itComments.next();
            s += "         Comment: " + c.getText() + "\n";
        }
        Iterator<MetaPackage> it = this.packages.iterator();
        while (it.hasNext()) {
            MetaPackage pack = it.next();
            s += "================= Package: " + pack.getName() + "=================\n";
            s += pack.toString();
        }
        return s;
    }

    @Override
    public int compareTo(MetaModel o) {
        return this.getName().compareTo(o.getName());
    }

    @Override
    public MetaModel clone() {
        return JsonMaker.toMetaModel(JsonMaker.toJson(this)); // could still be optimized, but works well
    }
}
