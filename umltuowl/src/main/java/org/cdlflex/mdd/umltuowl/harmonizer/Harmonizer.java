package org.cdlflex.mdd.umltuowl.harmonizer;

import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.utils.SettingsReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Gruenwald Guarantee uniqueness of class, attribute names. Merge all classes into a single metamodel
 *         or separate them into different metamodels depending on settings.
 */
public class Harmonizer {
    private static final Logger logger = LoggerFactory.getLogger(Harmonizer.class);

    /**
     * Harmonize components of meta model by merging, proof name uniqueness, etc. depending on parameter settings an
     * return harmonized metamodel.
     * 
     * @param metamodel original model gained from UML file (or other source).
     * @return harmonized meta model, compatible for OWL.
     */
    public MetaModel harmonize(MetaModel metamodel) {
        // 1) Merge packages (also handles class names)
        boolean mergePackages = Boolean.parseBoolean(SettingsReader.readProperty("merge-packages"));
        boolean mergeEquallyNamedClasses =
            Boolean.parseBoolean(SettingsReader.readProperty("merge-equally-named-classes"));
        Strategy strategy = null;
        if (mergePackages) {
            if (!mergeEquallyNamedClasses) {
                strategy = new MergeStrategy();
            } else {
                strategy = new MergeStrategyMergeClasses(true);
            }
        } else {
            strategy = new DefaultStrategy();
        }
        metamodel = strategy.applyStrategy(metamodel);
        metamodel = strategy.transformLocalRelationNames(metamodel);

        logger.debug(metamodel.toString());
        return metamodel;
    }

    /**
     * Apply a merge strategy explicitly thus passing a specific strategy. Additional harmonizing routines such as
     * transformation of local relation name concepts are skipped in favour of providing a predefined strategy.
     * 
     * @param metamodel the meta model
     * @param strategy the injected strategy which should be part of umltuowl.
     * @return the metamodel after applying the transformation strategy.
     */
    public MetaModel harmonizeExplicitly(MetaModel metamodel, Strategy strategy) {
        metamodel = strategy.applyStrategy(metamodel);
        return metamodel;
    }
}
