package org.cdlflex.mdd.umltuowl.utils;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.semanticweb.owlapi.vocab.OWL2Datatype;

/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com> Utility class that provides methods that may be useful for the
 *         project, in general.
 */
public class Util {
    private static final String WHITESPACE_REPLACEMENT_PARAMETER = "whitespace-replacement-char";

    /**
     * Appends prefix and postfix to a name with respect to text patterns, that occur twice. Example: prefix=Interface_
     * classname=Interface_Animal => result = Interface_Animal and not Interface_Interface_Animal. Auto converts first
     * character to upper case if prefix is appended and ends with a character.
     * 
     * @param name the name of a meta element. length must be greater than zero.
     * @param prefix the prefix that will be included in the beginning.
     * @param postfix the postfix that will be appended to the end.
     * @param convertSpecialChars cleans special characters that may not be supported by OWL (e.g. " " into "_") if
     *        {@code true}.
     * @return converted name depending on input attributes.
     */
    public static String metaNameConvertion(String name, String prefix, String postfix, boolean convertSpecialChars) {
        name = name.trim();
        String replaceString = SettingsReader.readProperty(WHITESPACE_REPLACEMENT_PARAMETER).trim();
        if (replaceString.contains("{empty}")) {
            replaceString = "";
        } else if (replaceString.isEmpty()) {
            replaceString = "_";
        }

        name = convertSpecialChars ? name.replace(" ", replaceString) : name;
        if (!name.startsWith(prefix) && !prefix.isEmpty()) {
            name = name.substring(0, 1).toUpperCase() + name.substring(1, name.length());
        }
        name = name.startsWith(prefix) ? name : prefix + name;
        name = name.endsWith(postfix) ? name : name + postfix;
        return name;
    }

    /**
     * Transforms two values (min and max) of association ranges into a list of string values. Input examples are (min;
     * max): (,*); (1,1); (0,1); (1,2,3,4,5;1,2,3,4,5); (;) Allowed values are empty, * (=max) an numeric ranges.
     * 
     * @param minValue the minimum value of a range, e.g. blank, 1 or a vector like 1,2,3 or 1..2
     * @param maxValue the maximum value of a range, e.g. blank, *, 0, 1, *.
     * @throws TransformationException
     * @return an ordered list that contains the range. Contains only blank, "*" or numeric values.
     */
    public static List<String> normalizeRange(String minValue, String maxValue) throws TransformationException {
        List<String> range = new LinkedList<String>();
        if (minValue.equals(maxValue)) {
            if (minValue.isEmpty()) {
                minValue = "*";
                maxValue = "*";
            }

            String[] values = minValue.split(",");
            for (String v : values) {
                v = v.trim();
                String[] rangeSplit = v.split("\\.\\.");
                if (rangeSplit.length == 2 && !rangeSplit[1].equals("*")) {
                    Integer start = Integer.parseInt(rangeSplit[0].trim());
                    Integer end = Integer.parseInt(rangeSplit[1].trim());
                    for (int i = start; i <= end; i++) {
                        range.add(String.valueOf(i));
                    }
                } else if (rangeSplit.length == 2 && rangeSplit[1].equals("*")) {
                    range.add(rangeSplit[0].trim());
                    range.add(rangeSplit[1].trim());
                } else if (v.equals("*")) {
                    range.add(v);
                } else {
                    try {
                        if (v.equals("")) {
                            range.add(v);
                        } else {
                            Integer.parseInt(v);
                            range.add(v);
                        }
                    } catch (NumberFormatException e) {
                        throw new TransformationException(String.format(
                                "Cannot parse range %s. Problem with value %s occured.", minValue, v));
                    }
                }
            }
        } else {
            range.add(minValue);
            range.add(maxValue);
        }
        Collections.sort(range, new RangeComparator());
        return range;
    }

    /**
     * @param range list of ranges as converted in {@code Util.normalizeRange}.
     * @return {@code true}, if all range values form a chain, e.g. 1,2,3,4,5 or 5,6,7 while 5,8,10 would not be a
     *         chain.
     */
    public static boolean isChain(List<String> range) {
        if (Util.isMinLimit(range) && Util.isMaxLimit(range) && range.size() >= 2) {
            int oldValue = -1;
            for (int i = 0; i < range.size(); i++) {
                int value = Integer.parseInt(range.get(i));
                if (oldValue >= 0 && oldValue != value - 1) {
                    return false;
                }
                oldValue = value;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param range list of ranges as converted in {@code Util.normalizeRange}.
     * @return {@code true}, if list has a minimum limit (e.g. 0; 1 or another fixed value).
     */
    public static boolean isMinLimit(List<String> range) {
        boolean isMinLimit = true;
        if (range.isEmpty() || range.get(0).equals("*") || range.get(0).equals("0") || range.get(0).isEmpty()) {
            isMinLimit = false;
        }
        return isMinLimit;
    }

    /**
     * @param range list of ranges as converted in {@code Util.normalizeRange}.
     * @return {@code true}, if list has a maximum limit (e.g. 10; 1000 or another fixed value).
     */
    public static boolean isMaxLimit(List<String> range) {
        boolean isMaxLimit = true;
        if (range.isEmpty() || range.get(range.size() - 1).equals("*")) {
            isMaxLimit = false;
        }
        return isMaxLimit;
    }

    /**
     * @return {@code true}, if datatype is a well known primitive datatype, e.g. string, integer, boolean, ... The OWL
     *         API is used for evaluation.
     */
    public static boolean isSupportedPrimitiveDatatype(String datatype) {
        if (datatype.equalsIgnoreCase("bool")) {
            datatype = "boolean";
        }

        // Use OWL library supported types
        String[] owl2prefixes = new String[] { "XSD", "RDF", "OWL" };
        for (String owl2Prefix : owl2prefixes) {
            try {
                OWL2Datatype owl2Datatype =
                    (OWL2Datatype) OWL2Datatype.class.getDeclaredField(owl2Prefix + "_" + datatype.toUpperCase())
                            .get(null);
                if (owl2Datatype != null) {
                    return true;
                }
            } catch (Exception e) {
                // error =
                // (String.format("Could not convert to datatype that is supported by OWL2: %s. \nTake a look at XSD (XML Schema Definition) data types and rename your attribute type according to XSD supported types (e.g. string, token, ...).",owl2Prefix+datatype));
            }
        }
        return false;
    }

    /**
     * Compares a number with a string value and checks if the number is greater than or equal the value in the string.
     * therefore the string is parsed into a numeric value. If the string contains invalid characters, they
     * {@code false} will be returned.
     * 
     * @param val the numeric value
     * @param strVal the numeric string value or some character data
     * @return {@code true} if {@code strVal} can be parsed into an Integer and is greater than {@code val}.
     */
    static public boolean isGreatherOrEqualThan(String strVal, Integer val) {
        Integer val2 = -1;
        try {
            val2 = Integer.parseInt(strVal);
        } catch (Exception e) {
        }

        if (val2 >= val && val2 >= 0) {
            return true;
        } else {
            return false;
        }
    }
}
