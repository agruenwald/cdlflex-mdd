package org.cdlflex.mdd.umltuowl.uml2metamodel;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClassPoint;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.cdlflex.mdd.umltuowl.utils.Util;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Gruenwald Converter for Argo UML XMI2.0 syntax version 0.34. Note: Does not work for version 0.32!!!
 */
public class ArgoUMLConverterXMIV034 extends UMLConverter {
    private MetaModel model;
    private Document xmi;
    private static final Logger logger = LoggerFactory.getLogger(ArgoUMLConverterXMIV034.class);

    /**
     * Creation of a new Argo UML XMI 2.0 metamodel (done automatically by factory).
     */
    public ArgoUMLConverterXMIV034() {
        this.model = new MetaModel("ARGOUML_XMI_2_0_MODEL_V0_34");
    }

    @Override
    public MetaModel convertModel(Document xmi) {
        logger.info(String.format("Convert model using %s XMI converter.",
                ArgoUMLConverterXMIV034.class.getSimpleName()));
        this.xmi = xmi;
        this.model.setName(xmi.select("uml|Model").attr("name"));
        Elements classes = xmi.select("UML|Class[name]");
        Elements interfaces = xmi.select("UML|Interface[name]");
        buildClasses(classes);
        buildInterfaces(interfaces);
        addGeneralization(classes);
        addGeneralization(interfaces);
        addInterfaceRealization(interfaces);
        addAssociations();
        addComments(classes, interfaces);
        return model;
    }

    /**
     * Bind UML comments to classes.
     */
    private void addComments(Elements classes, Elements interfaces) {
        logger.trace("Called addComments(classes,interfaces).");

        for (Element c : classes) {
            Elements comments = c.getElementsByTag("UML:ModelElement.comment").select("UML|Comment");
            for (Element com : comments) {
                String commentId = com.attr("xmi.idref");
                String comment = xmi.select(String.format("UML|Comment[xmi.id=%s]", commentId)).attr("body");
                MetaClass metaClass = model.findClassById(c.attr("xmi.id"));
                metaClass.getComment().add(new MetaComment(comment));
                logger.trace(String.format("Added comment %s to %s.", comment, metaClass));
            }
        }

        for (Element c : interfaces) {
            Elements comments = c.select("UML|ModelElement[comment]").select("UML|Comment");
            for (Element com : comments) {
                String commentId = com.attr("idref");
                String comment = xmi.select(String.format("UML|Comment[id=%s]", commentId)).attr("body");
                MetaClass metaClass = model.findClassById(c.attr("id"));
                metaClass.getComment().add(new MetaComment(comment));
                logger.trace(String.format("Added comment %s to %s.", comment, metaClass));
            }
        }
    }

    /**
     * Add sub- and super class relations.
     * 
     * @param classes identified classes (and interfaces).
     */
    private void addGeneralization(Elements classes) {
        logger.trace("Called addGeneralization(classes).");
        for (Element c : classes) {
            String classId = c.attr("xmi.id");
            MetaClass clazz = model.findClassById(classId);
            logger.trace(String.format("Inspect class %s (generalization).", clazz));
            Elements x = xmi.getElementsByTag("UML:Generalization.child");// <Element> x = getComplicatedNamespace(xmi,
                                                                          // "UML|Generalization", "child");
            for (Element e : x) {
                Elements es = e.select(String.format("UML|Class[xmi.idref=%s]", classId));
                if (es.size() != 0) {
                    String superClassId =
                        e.parent().getElementsByTag("UML:Generalization.parent").select("UML|Class").first()
                                .attr("xmi.idref");
                    MetaClass superClazz = model.findClassById(superClassId);
                    if (superClazz == null) {
                        logger.warn("========>" + superClassId + " (Interface?) ");
                    } else {
                        logger.debug(String.format("Class %s has superclass %s.", clazz, superClazz));
                        superClazz.getSubclasses().add(clazz);
                        clazz.getSuperclasses().add(superClazz);
                    }
                }
            }
        }
    }

    /**
     * Add sub- and super class relations for interfaces.
     */
    public void addInterfaceRealization(Elements interfaces) {
        logger.trace("Called addInterfaceRealization(interfaces).");
        for (Element c : interfaces) {
            String classId = c.attr("xmi.id");
            logger.trace(String.format("Inspect class %s (abstraction).", classId));
            Elements x = xmi.select("UML|Abstraction[xmi.id]");
            for (Element e : x) {
                Elements es = e.select(String.format("UML|Interface[xmi.idref=%s]", classId));
                if (es.size() != 0) {
                    String subClassId =
                        e.getElementsByTag("UML:Dependency.client").first().child(0).attr("xmi.idref");
                    String superClassId =
                        e.getElementsByTag("UML:Dependency.supplier").first().child(0).attr("xmi.idref");
                    MetaClass superClazz = model.findClassById(superClassId);
                    MetaClass clazz = model.findClassById(subClassId);
                    if (superClazz == null || clazz == null) {
                        logger.warn("========>" + superClassId + "/" + clazz + " couldn't be realized.");
                    } else {
                        logger.debug(String.format("Class %s has superclass %s.", clazz, superClazz));
                        superClazz.getSubclasses().add(clazz);
                        clazz.getSuperclasses().add(superClazz);
                    }
                }
            }
        }
    }

    /**
     * Add fine grained information (attributes) to meta classes.
     * 
     * @param classes identified classes and interfaces.
     */
    private void buildClasses(Elements classes) {
        logger.trace("Called buildClasses(classes).");
        for (Element c : classes) {
            String name = c.attr("name");
            if (Util.isSupportedPrimitiveDatatype(name)) {
                logger.warn(String
                        .format("Class %s has been detected as a datatype primitive and therefore was removed from class list.",
                                name));
            } else if (name.isEmpty()) {

            } else {
                boolean isAbstract = false;
                if (name.contains("{abstract}") || c.attr("isAbstract").equalsIgnoreCase("true")) {
                    isAbstract = true;
                    name = name.replace("{abstract}", "").trim();
                    logger.info(String.format("Class %s was classified as abstract.", name));
                }
                MetaClass umlClass = new MetaClass(name);
                umlClass.setId(c.attr("xmi.id"));
                umlClass.setAbstractClass(isAbstract);
                umlClass = buildAttributes(umlClass, c);
                MetaPackage metaPackage = model.managePackageByName("");
                metaPackage.getClasses().add(umlClass);
            }
        }
    }

    /**
     * Extract interfaces from XML model and add it to meta model as classes.
     * 
     * @param interfaces identified interfaces (XMI).
     */
    private void buildInterfaces(Elements interfaces) {
        logger.trace("Called buildInterfaces(interfaces).");
        for (Element c : interfaces) {
            String name = c.attr("name");
            MetaClass umlClass = new MetaClass(name);
            umlClass.setId(c.attr("xmi.id"));
            umlClass.setInterfaceClass(true);
            MetaPackage metaPackage = model.managePackageByName("");
            metaPackage.getClasses().add(umlClass);
        }
    }

    /**
     * find data type by its relating {@code rangeId}, which is part of the XMI file.
     * 
     * @param metaClass the meta class the attribute range belongs to.
     * @param rangeId the internal data type id (provided by VParadigm XMI).
     * @return data type name (e.g. string) or empty if void.
     */
    private String findRangeType(MetaClass metaClass, String rangeId) {
        logger.trace(String.format("Called findRangeType(xmi-document,%s", rangeId));
        Elements e =
            xmi.select("packagedElement[xmi:type=uml:DataType]").select(String.format("[xmi:id=%s]", rangeId));

        if (e.first() == null) {
            e = xmi.select(String.format("[xmi:id=%s]", rangeId));
            if (e.first() != null) {
                if (Util.isSupportedPrimitiveDatatype(e.first().attr("name"))) {
                    logger.warn(String
                            .format("Did not find data type \"%s\" for class %s (range id %s). "
                                + "However, attribute name has been detected as an primitive datatype, so the problem has been solved automatically by the converter. ",
                                    e.first().attr("name"), metaClass.getName(), rangeId));
                } else {
                    logger.warn(String.format("Did not find data type for class %s (range id %s). "
                        + "The data range seems not to be a primitive data type, so the datatype was removed.",
                            metaClass.getName(), rangeId, e.first().attr("name")));
                    return "void";
                }
            }
        }
        return e.first().attr("name");
    }

    /**
     * Add all attributes to meta class. Methods are ignored.
     * 
     * @param umlClass a single meta class.
     * @param xmiClass corresponding class from Visual Paradigm XMI file.
     * @return meta class that contains all related attributes.
     */
    private MetaClass buildAttributes(MetaClass umlClass, Element xmiClass) {
        logger.trace(String.format("Called buildAttributes(%s,jsoupXMIClass)", umlClass.getName()));
        Elements attrs = xmiClass.select("uml|Attribute");
        for (Element a : attrs) {
            if (!a.attr("association").isEmpty()) {
                logger.trace(String.format("Association %s found as attribute (skipped).", a.attr("association")));
                continue;
            }
            MetaAttribute metaAttribute = new MetaAttribute(a.attr("name"));
            metaAttribute.setVisibility(a.attr("visibility"));
            String rangeId = a.attr("type");
            String range = "void";
            if (!rangeId.isEmpty()) {
                logger.debug("Range id of " + umlClass + " is " + rangeId);
                range = findRangeType(umlClass, rangeId);
            } else {
                String idref = a.select("UML|DataType[xmi.idref]").attr("xmi.idref");
                if (!idref.isEmpty()) {
                    logger.debug("Range id of " + umlClass + " is " + idref);
                    range = this.xmi.select("uml|DataType[xmi.id=" + idref + "]").attr("name");
                }
            }
            metaAttribute.setRange(range);
            umlClass.getAttributes().add(metaAttribute);
        }
        return umlClass;
    }

    /**
     * Add associations between classes.
     */
    private void addAssociations() {
        Elements associations = xmi.select("uml|Association");
        for (Element ass : associations) {
            Elements elements = ass.select("uml|AssociationEnd[xmi.id]");
            logger.debug("Found new association between classes.");
            String name = ass.attr("name");
            String id = ass.attr("xmi.id"); // ass.attr("memberEnd"); //if name exists
            if (id.isEmpty() && name.isEmpty()) {
                continue;
            }
            logger.debug("id and name: " + id + "-" + name);
            if (elements.size() != 2) {
                logger.error(String
                        .format("Wrong file format. Association %s contains more (or less) than 2 (i.e. %d) endpoints. Please check your XMI file or send us an e-mail.",
                                name, elements.size()));
            } else {
                this.createAndLinkAssociation(id, elements, ass, name);
            }
        }
    }

    /**
     * Creates new associations and links them to the meta classes.
     * 
     * @param associationId the ID of the association (same for both link directions). Is used to identify the
     *        association globally (e.g. find comments).
     * @param elements a list of XMI elements containing end points (=XMI code).
     * @param ass the association element (XMI) that contains the two endpoints and navigation information.
     * @param name name of the association if user named it, otherwise empty.
     */
    private void createAndLinkAssociation(String associationId, Elements elements, Element ass, String name) {
        String lowerValueId = elements.get(0).attr("xmi.id");
        String upperValueId = elements.get(1).attr("xmi.id");
        String lowerValueFrom = elements.get(0).select("UML|MultiplicityRange").attr("lower");
        String upperValueFrom = elements.get(0).select("UML|MultiplicityRange").attr("upper");
        String lowerValueTo = elements.get(1).select("UML|MultiplicityRange").attr("lower");
        String upperValueTo = elements.get(1).select("UML|MultiplicityRange").attr("upper");

        lowerValueFrom = lowerValueFrom.equals("-1") ? "*" : lowerValueFrom;
        upperValueFrom = upperValueFrom.equals("-1") ? "*" : upperValueFrom;
        lowerValueTo = lowerValueTo.equals("-1") ? "*" : lowerValueTo;
        upperValueTo = upperValueTo.equals("-1") ? "*" : upperValueTo;

        String aggregationFrom = elements.get(0).attr("aggregation").equals("aggregate") ? "true" : "false";
        String aggregationTo = elements.get(1).attr("aggregation").equals("aggregate") ? "true" : "false";
        String compositionFrom = elements.get(0).attr("aggregation").equals("composite") ? "true" : "false";
        String compositionTo = elements.get(1).attr("aggregation").equals("composite") ? "true" : "false";

        MetaClass metaClassFrom = null;
        Elements fromElements = elements.get(0).select("UML|AssociationEnd").first().children();
        for (Element e : fromElements) {
            if (e.tagName().equals("uml:associationend.participant")) {
                String idref = e.child(0).attr("xmi.idref");
                metaClassFrom = this.model.findClassById(idref);
                break;
            }
        }

        MetaClass metaClassTo = null;
        Elements toElements = elements.get(1).select("UML|AssociationEnd").first().children();
        for (Element e : toElements) {
            if (e.tagName().equals("uml:associationend.participant")) {
                String idref = e.child(0).attr("xmi.idref");
                metaClassTo = this.model.findClassById(idref);
                break;
            }
        }

        // break if class has not been found
        if (metaClassFrom == null || metaClassTo == null) {
            logger.warn(String
                    .format("One of the association endpoins contains an empty class so we skipped the association (association ID %s - name %s).",
                            associationId, name));
            return;
        }

        boolean isBidirectional = true;
        boolean fromToNavigable = elements.get(0).attr("isNavigable").equals("true") ? true : false;
        boolean toFromNavigable = elements.get(1).attr("isNavigable").equals("true") ? true : false;

        if (fromToNavigable && toFromNavigable) {

        } else {
            isBidirectional = false;
        }

        logger.debug(String.format("association (%s) %s<->%s (%s;%s) found. Navigation is %s.", name.isEmpty()
            ? "noname" : name, metaClassFrom.getName(), metaClassTo.getName(), lowerValueFrom, upperValueFrom,
                isBidirectional ? "bidirectional" : "unidirectional"));

        MetaAssociation fromAssociation = new MetaAssociation(associationId, lowerValueId, name);
        MetaAssociation toAssociation = new MetaAssociation(associationId, upperValueId, name);

        fromAssociation.setFrom(new MetaClassPoint(metaClassFrom));
        fromAssociation.setTo(new MetaClassPoint(metaClassTo));
        fromAssociation.getFrom().setRangeSmart(lowerValueFrom, upperValueFrom);
        fromAssociation.getTo().setRangeSmart(lowerValueTo, upperValueTo);
        fromAssociation.setAggregation(Boolean.parseBoolean(aggregationFrom));
        fromAssociation.setComposition(Boolean.parseBoolean(compositionFrom));

        toAssociation.setTo(fromAssociation.getFrom());
        toAssociation.setFrom(fromAssociation.getTo());
        toAssociation.setAggregation(Boolean.parseBoolean(aggregationTo));
        toAssociation.setComposition(Boolean.parseBoolean(compositionTo));

        if (isBidirectional) {
            fromAssociation.setInverseAssociation(toAssociation);
            toAssociation.setInverseAssociation(fromAssociation);
            metaClassFrom.getAssociations().add(fromAssociation);
            metaClassTo.getAssociations().add(toAssociation);
        } else if (toFromNavigable) {
            metaClassFrom.getAssociations().add(fromAssociation);
        } else {
            metaClassTo.getAssociations().add(toAssociation);
        }
    }
}
