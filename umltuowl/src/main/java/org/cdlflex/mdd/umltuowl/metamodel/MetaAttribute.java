package org.cdlflex.mdd.umltuowl.metamodel;

/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com> Represents an attribute of a class. An attribute consists of a name, a
 *         datatype (=range; even, if range might be "UNKNOWN"), a visibility status, and some optional information.
 */
public class MetaAttribute extends MetaElement {
    private static final long serialVersionUID = 1L;
    public static final String VOID = "void";
    public static final String DATETIME = "datetime";
    public static final String BOOLEAN = "boolean";
    public static final String TIMESTAMP = "timestamp";
    public static final String TIME = "time";

    public static final String PUBLIC = "public";
    public static final String PROTECTED = "protected";
    public static final String PRIVATE = "private";
    public static final String PACKAGE = "package";

    private String name;
    private String originalName;

    private String visibility;
    private String range;
    private boolean rangeIsEnum;

    private int multMin;
    private int multMax;

    public MetaAttribute(String name) {
        super();
        this.name = name;
        this.originalName = name;
        this.visibility = PUBLIC;
        this.range = VOID;
        this.rangeIsEnum = false;
        this.multMin = 1;
        this.multMax = 1;
    }

    /**
     * Creates a new meta attribute without name. This constructor must be avoided and should only be used for automated
     * processing, such as serialization.
     */
    public MetaAttribute() {
        this("");
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if (this.originalName.isEmpty()) {
            this.originalName = name;
        }
    }

    /**
     * set the original name (usually not used) before a a transformation occured.
     * 
     * @param originalName the original name. not empty.
     */
    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    /**
     * @return the original name before transformations happened. Without prefixes etc.
     */
    public String getOriginalName() {
        if (this.originalName.isEmpty()) {
            return this.name;
        } else {
            return this.originalName;
        }
    }

    /**
     * @return visibility of attribute (public, private or protected)
     */
    public String getVisibility() {
        return visibility;
    }

    /**
     * set visiblity of attribute.
     * 
     * @param visibility public, private or protected are allowed.
     */
    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    /**
     * @return range of attribute. Values see {@code setRange(String range)}.
     */
    public String getRange() {
        return range;
    }

    /**
     * Set range of attribute.
     * 
     * @param range of attribute. Allowed are
     */
    public void setRange(String range) {
        if (range.equalsIgnoreCase("bool") || range.equalsIgnoreCase("boolean")) {
            range = BOOLEAN;
        }
        this.range = range;
    }

    public boolean isRangeIsEnum() {
        return rangeIsEnum;
    }

    public void setRangeIsEnum(boolean rangeIsEnum) {
        this.rangeIsEnum = rangeIsEnum;
    }

    /**
     * By default, each attribute only allows one value of a primitive or linked concept. In that case, multMin and
     * multMax are both set to one. In case you want to allow a range of values (e.g. list of strings) then you need to
     * reset the min and max values.
     * 
     * @param multMin a value greather than zero. By default, the value is set to one.
     */
    public void setMultMin(int multMin) {
        this.multMin = multMin;
    }

    /**
     * @return the minimum range value, by default this is one.
     */
    public int getMultMin() {
        return multMin;
    }

    /**
     * the maximum range value. by default this is set to one.
     * 
     * @return a value greater than zero. One will be returned in the default case. -1 will be returned in case that
     *         there is no limit (ininite list).
     */
    public int getMultMax() {
        return multMax;
    }

    /**
     * By default, each attribute only allows one value of a primitive or linked concept. In that case, multMin and
     * multMax are both set to one. In case you want to allow a range of values (e.g. list of strings) then you need to
     * reset the min and max values.
     * 
     * @param multMax set multMin and multMax to one if there is no range. Set the parameter to -1 if the range is
     *        infinite. Otherwise, set it to any positive value as long as it is greater or equas than the min value.
     */
    public void setMultMax(int multMax) {
        this.multMax = multMax;
    }

    /**
     * a helper method.
     * 
     * @return true if the attribute accounts for ranges. otherwise false (=default);
     */
    public boolean isMultiple() {
        if ((this.multMax == this.multMin) && this.multMin == 1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return attribute in UML notation is returned (with leading sign for visibility).
     */
    @Override
    public String toString() {
        String v = "?";
        if (visibility.equals(PRIVATE)) {
            v = "-";
        } else if (visibility.equals(PUBLIC)) {
            v = "+";
        } else if (visibility.equals(PROTECTED)) {
            v = "#";
        } else if (visibility.equals(PACKAGE)) {
            v = "~";
        }
        return v
            + name
            + ":"
            + getRange()
            + (this.multMax == 1 && this.multMin == 1 ? "" : String.format(" [%s..%s]", this.multMin,
                    (this.multMax == -1 ? "*" : this.multMax))) + "\n";
    }
}
