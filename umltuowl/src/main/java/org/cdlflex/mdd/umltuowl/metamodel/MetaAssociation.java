package org.cdlflex.mdd.umltuowl.metamodel;

import java.util.Collections;

import org.cdlflex.mdd.umltuowl.utils.RangeComparator;
import org.cdlflex.mdd.umltuowl.utils.Util;

/**
 * @author Andreas Gruenwald <a.gruenw@gmail.com> Represents an association between classes. In OWL, an association is
 *         represented by an object property.
 */
public class MetaAssociation extends MetaElement {
    private static final long serialVersionUID = 1L;
    private String id;
    private String associationId;
    private String name;
    private MetaClassPoint from;
    private MetaClassPoint to;
    private boolean composition;
    private boolean aggregation;

    private MetaAssociation inverseAssociation;

    /**
     * Create a new association (unidirectional), composition or aggregation.
     * 
     * @param id internal id can be used for parsing XMI - unique for each unidirectional relation.
     * @param associationId id of association. if two associations relate to each other (e.g. bidirectional
     *        associations) both association have an unique id, but the same association id. Is used primarily for
     *        parsing purposes, e.g. linking comments, so can be used for internal purposes and does not influence OWL
     *        creation.
     * @param name the name of the association if empty or otherwise blank.
     */
    public MetaAssociation(String associationId, String id, String name) {
        super();
        this.from = null;
        this.to = null;
        this.name = name;
        this.id = id;
        this.associationId = associationId;
        this.inverseAssociation = null;
        this.composition = false;
        this.aggregation = false;
    }

    /**
     * Creates a new meta association without name. This constructor must be avoided and should only be used for
     * automated processing, such as serialization.
     */
    public MetaAssociation() {
        this("", "", "");
    }

    public String getAssociationId() {
        return associationId;
    }

    public String getId() {
        return id;
    }

    /**
     * Only use for automated processing!
     * 
     * @param id
     */
    public void setAssociationId(String id) {
        this.associationId = id;
    }

    /**
     * Only use for automated processing!
     * 
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public MetaClassPoint getFrom() {
        return from;
    }

    public void setFrom(MetaClassPoint from) {
        this.from = from;
    }

    public MetaClassPoint getTo() {
        return to;
    }

    public void setTo(MetaClassPoint to) {
        this.to = to;
    }

    public boolean isComposition() {
        return composition;
    }

    public void setComposition(boolean composition) {
        this.composition = composition;
    }

    public boolean isAggregation() {
        return aggregation;
    }

    public void setAggregation(boolean aggregation) {
        this.aggregation = aggregation;
    }

    public MetaAssociation getInverseAssociation() {
        return inverseAssociation;
    }

    public void setInverseAssociation(MetaAssociation inverseAssociation) {
        this.inverseAssociation = inverseAssociation;
    }

    /**
     * Compare if the maximum range of a meta association (to) is greater than a specific limit.
     * 
     * @param maxLimit the maximum limit >= 0. e.g. 1
     * @return true if the maximum range is greater than the limit. e.g. maxLimit = 1: true will be returned if the
     *         maximum range has been specified to be zero or 1.
     */
    public boolean isMaximumRangeGreaterThan(int maxLimit) {
        Collections.sort(this.getTo().getRange(), new RangeComparator());
        if (Util.isMaxLimit(this.getTo().getRange())) {
            int maxValue = Integer.parseInt(this.getTo().getRange().get(this.getTo().getRange().size() - 1).trim());
            if (maxValue > maxLimit) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        String s;
        s =
            String.format(" %s%s%s%s-->%s(%s%s%s).", from.getMetaClass() != null ? from.getMetaClass().getName()
                : "----ERROR----UNKNOWN---FROM.getMetaClass----", from.getRange().toString(),
                    this.inverseAssociation == null ? "---" : "<--", name.isEmpty() ? "noname" : name, to
                            .getMetaClass() != null ? to.getMetaClass().getName()
                        : "----ERROR----UNKNOWN---TO.getMetaClass----", to.getRange().toString(), this.aggregation
                        ? " - aggregation" : "", this.composition ? " - composition" : "");

        for (MetaComment c : this.getComment()) {
            s += String.format("\n       Comment: %s", c.getText());
        }
        return s;
    }

}
