package org.cdlflex.mdd.umltuowl.uml2metamodel;

import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Gruenwald Factory that selects a specific converter implementation for UML to meta model convertion.
 */
public class TransferFactory {
    private static final Logger logger = LoggerFactory.getLogger(TransferFactory.class);
    private static final String PACKAGE_NAMESPACE = "org.cdlflex.mdd.umltuowl";

    public static final String FORMAT_XML = "xml";
    public static final String FORMAT_JSON = "json";

    /**
     * Each converter that is supported must be added here.
     */
    public class ConverterInfo {
        private final String name;
        private final String description;
        private final String encoding;
        private final String format;

        private ConverterInfo(String name, String description, String encoding, String format) {
            this.name = name;
            this.description = description;
            this.encoding = encoding;
            this.format = format;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public String getEncoding() {
            return encoding;
        }

        @Override
        public String toString() {
            return name + ":" + description;
        }

        public String getFormat() {
            return format;
        }
    }

    public static final List<ConverterInfo> CONVERTERS = new LinkedList<ConverterInfo>();
    static {
        CONVERTERS
                .add(new TransferFactory().new ConverterInfo(
                        "VisualParadigmConverterXMI2",
                        "Convertion of Visual Paradigm XMI 2.1 UML class diagrams (modeling of data objects). Compatible with version 10.0 SE. Please enable UML2 support when exporting otherwise there may be some problems regarding associations and range types.",
                        "UTF-8", FORMAT_XML));
        CONVERTERS
                .add(new TransferFactory().new ConverterInfo(
                        "MSVisio2010ConverterXMI",
                        "Convertion of Microsoft Visio 2010 class diagrams (modeling of data objects). To create XMI out of MS Visio you can install a macro inside MS Visio (you may google for it).",
                        "UTF-16", FORMAT_XML));

        CONVERTERS
                .add(new TransferFactory().new ConverterInfo(
                        "ArgoUMLConverterXMI2",
                        "Convertion of ArgoUML (version 0.32.2) XMI 2.1. The format is slightly different than the Visual Paradigm syntax.",
                        "UTF-8", FORMAT_XML));

        CONVERTERS
                .add(new TransferFactory().new ConverterInfo(
                        "ArgoUMLConverterXMIV034",
                        "Convertion of ArgoUML (version 0.34) XMI 2.1. The format is not compatible with previous ArgoUML versions!!!",
                        "UTF-8", FORMAT_XML));

        CONVERTERS.add(new TransferFactory().new ConverterInfo("JsonConverter",
                "Convertion of a (umlTUowl)-based JSON meta-model.", "UTF-8", FORMAT_JSON));
        /*
         * CONVERTERS.add(new TransferFactory(). new ConverterInfo( "MSVisio2010EER", "Just for testing purpose.",
         * "UTF-8"));
         */
    }

    /**
     * Return proposed encoding for a special converter, e.g. UTF-16 or UTF-8.
     * 
     * @param converterName name of the converter.
     * @return the proposed encoding or an empty result if converter does not exist.
     */
    public static String getEncoding(String converterName) {
        for (ConverterInfo c : CONVERTERS) {
            if (c.getName().equals(converterName)) {
                return c.getEncoding();
            }
        }
        return "";
    }

    public static ConverterInfo findConverterInfo(String converterName) {
        for (ConverterInfo c : CONVERTERS) {
            if (c.getName().equals(converterName)) {
                return c;
            }
        }
        return null;
    }

    /**
     * Dynamically selects the converter for UML to meta model transfer.
     * 
     * @param umlFormat name of the converter. E.g. "VisualParadigmConverterXMI2".
     * @return specific converter.
     * @throws ConverterNotExistsException if converter does not exist or umlFormat has been spelled wrong.
     */
    public static UMLConverter createConverter(String umlFormat) throws ConverterNotExistsException {
        UMLConverter umlConverter = null;
        try {
            umlConverter =
                (UMLConverter) Class.forName(PACKAGE_NAMESPACE + ".uml2metamodel." + umlFormat).newInstance();
        } catch (ClassNotFoundException e) {
            throw new ConverterNotExistsException(String.format("Converter %s does not exist.", umlFormat));
        } catch (InstantiationException e) {
            logger.error(String.format("%s", e.getMessage()));
        } catch (IllegalAccessException e) {
            logger.error(String.format("%s", e.getMessage()));
        }
        return umlConverter;
    }
}
