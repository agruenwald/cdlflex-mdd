package org.cdlflex.mdd.umltuowl.main;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.cdlflex.mdd.umltuowl.harmonizer.Harmonizer;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.JsonMaker;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.OWLMaker;
import org.cdlflex.mdd.umltuowl.uml2metamodel.ConverterNotExistsException;
import org.cdlflex.mdd.umltuowl.uml2metamodel.TransferFactory;
import org.cdlflex.mdd.umltuowl.uml2metamodel.TransferFactory.ConverterInfo;
import org.cdlflex.mdd.umltuowl.uml2metamodel.UMLConverter;
import org.cdlflex.mdd.umltuowl.utils.SettingsReader;
import org.cdlflex.mdd.umltuowl.utils.Util;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.semanticweb.owlapi.model.OWLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com> Main class for OWL transformation. Requires an input file (Visual
 *         Paradigm XMI 2.0) and transforms it into an output file (OWL).
 */
public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    public static final String VERSION = "3.0";

    /**
     * Default main method, which expects the mandatory parameters and creates output files based on the given input
     * file.
     * 
     * @param args
     */
    public static void main(String[] args) {
        mainWithoutModel(args);
    }

    /**
     * Same as the main-method, but does not extract the metamModel from an input file. Instead, the metamodel is
     * already provided as an input parameter.
     * 
     * @param metaModel
     * @param args
     * @return
     */
    public static MetaModel mainWithModel(MetaModel metaModel, String[] args) {
        return performMain(metaModel, args);
    }

    private static MetaModel mainWithoutModel(String[] args) {
        MetaModel metaModel = loadMetaModel(args);
        if (metaModel == null) {
            return metaModel;
        }
        return performMain(metaModel, args);
    }

    private static MetaModel performMain(MetaModel metaModel, String[] args) {
        try {
            // Harmonize, e.g. create IRI's.
            metaModel = harmonizeModel(metaModel);

            // Create Output
            if (createOutput(metaModel)) {
                logger.info(String.format(
                        "Finished transformation process (%s class elements contained in resulting ontology).",
                        metaModel.getSize()));
                for (MetaPackage pkg : metaModel.getPackages()) {
                    logger.info(String.format(" *Package %s:", pkg.getName()));
                    logger.info(String.format("  ==> %s classes", pkg.getClasses().size()));
                    logger.info(String.format("  ==> %s associations", pkg.getSizeAssociations()));
                    logger.info(String.format("  ==> %s attributes", pkg.getSizeAttributes()));
                }
            }
            return metaModel;
        } catch (Exception e) {
            logger.error(String
                    .format("Transformation process failed. "
                        + "Please make shure that you used the correct input format (e.g. Visual Paradigm XMI 2.1 export files and not the vpp - project files)."
                        + "Exception: %s. If you have further problems, send a mail to <a.gruenw@gmail.com> and describe/attach your input file.",
                            e.getMessage()));
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Validate input parameters and load meta model. But do not create output.
     * 
     * @param args the input arguments (e.g. from console by user).
     * @return created meta model or {@code null}, if parameters are missing or transformation failed. Errors are logged
     *         automatically.
     */
    public static MetaModel loadMetaModel(String[] argsx) {
        try {
            SettingsReader.loadSettings(); // load settings
        } catch (IOException e) {
            logger.error(String.format("Failed to load default settings (%s).", e.getMessage()));
            return null;
        }

        // add output parameter because it is optional, but not used here...
        List<String> lArgsx = Arrays.asList(argsx);
        List<String> lArgs = new LinkedList<String>();
        lArgs.addAll(lArgsx); // cannot add to lArgsx directly, because abstract
        lArgs.add(ParameterHandling.OUTPUT + "=irrelevant");
        String[] args2 = lArgs.toArray(new String[lArgs.size()]);

        // Handle input parameters and settings
        if (new ParameterHandling().validateParameters(args2)) {
            return null;
        }

        String inputFilename = SettingsReader.readProperty(ParameterHandling.INPUT);
        String converterName = SettingsReader.readProperty(ParameterHandling.CONVERTER);

        MetaModel metaModel = null;
        ConverterInfo converterInfo = TransferFactory.findConverterInfo(converterName);
        if (converterInfo == null) {
            logger.error(String.format("Converter %s does not exist. Please specify an existing one.", converterName));
            return null;
        } else if (converterInfo.getFormat().equals(TransferFactory.FORMAT_JSON)) {
            String jsonFileContent;
            try {
                jsonFileContent = XMLPreprocessor.readFileContent(inputFilename, converterInfo.getEncoding());
            } catch (IOException e) {
                logger.error(String.format("Cannot not open input file %s.", inputFilename));
                return null;
            }
            metaModel = JsonMaker.toMetaModel(jsonFileContent);
        } else if (converterInfo.getFormat().equals(TransferFactory.FORMAT_XML)) {
            Document umlDocument = null;
            try {
                String charset =
                    SettingsReader.readProperty("encoding").isEmpty() ? TransferFactory.getEncoding(converterName)
                        : SettingsReader.readProperty("encoding");
                logger.info(String.format("Try to read %s using %s charset.", inputFilename, charset));
                String fileContent = XMLPreprocessor.readPreprocessedFileContent(inputFilename, charset);
                logger.info(String.format("Preprocessed %s. Parse XML...", inputFilename));
                umlDocument = Jsoup.parse(fileContent);
            } catch (IOException e) {
                logger.error(String.format("Cannot not open input file %s.", inputFilename));
                return null;
            }

            logger.info(String.format("Looking for converter %s...", converterName));
            UMLConverter converter = null;
            try {
                converter = TransferFactory.createConverter(converterName);
            } catch (ConverterNotExistsException e) {
                logger.error(String.format("Converter %s does not exist. Please specify an existing one.",
                        converterName));
                return null;
            }

            logger.info(String.format("Convert XML into meta model..."));
            metaModel = converter.convertModel(umlDocument);
        } else {
            logger.error(String.format("Unknown format \"%s\" for converter %s.", converterInfo.getFormat(),
                    converterInfo.getName()));
            return null;
        }

        if (metaModel != null) {
            metaModel.setNamespace(SettingsReader.readProperty(ParameterHandling.IRI));
        }

        if (SettingsReader.readProperty(ParameterHandling.VERBOSE).equalsIgnoreCase("true")) {
            // logger.info(String.format("Built metamodel: \n %s",metaModel));
        } else {
            logger.debug(String.format("Built metamodel: \n %s", metaModel));
        }

        return metaModel;
    }

    /**
     * Harmonize the meta model, e.g. create IRI's and prefix associations. Do not do anything else.
     * 
     * @param metaModel the meta model.
     * @return harmonized meta model.
     */
    public static MetaModel harmonizeModel(MetaModel metaModel) {
        Harmonizer harmonizer = new Harmonizer();
        MetaModel metaModelNew = harmonizer.harmonize(metaModel);
        if (SettingsReader.readProperty(ParameterHandling.VERBOSE).equalsIgnoreCase("true")) {
            logger.info(String.format("Built metamodel (after harmonizing): \n %s", metaModelNew));
        }
        return metaModelNew;
    }

    /**
     * Create OWL/JSON meta model(s) and output it to file(s). If errors occur, they will be logged.The output format
     * will depend on the file ending given in the output parameter(s): json/owl
     * 
     * @param metaModel the meta model.
     * @return {@code true}, if successfully created output OWL/JSON file(s).
     */
    public static boolean createOutput(MetaModel metaModel) {
        List<String> outputNames = SettingsReader.readPropertyArray(ParameterHandling.OUTPUT);
        String iriName = SettingsReader.readProperty(ParameterHandling.IRI);

        for (String outputName : outputNames) {
            String outputFormat = outputName.endsWith(".json") ? "json" : "owl";
            if (outputFormat.equals("json")) {
                logger.info(String.format("Convert metamodel into JSON using FLEXJSON (IRI name is %s)...", iriName));
            } else {
                logger.info(String.format("Convert metamodel into OWL using OWL API (IRI name is %s)...", iriName));
            }

            if (outputFormat.equals("json")) {
                try {
                    JsonMaker.toJsonFile(metaModel, outputName);
                } catch (IOException e) {
                    logger.error(String.format("Failed streaming JSON into file %s. Details: %s.", outputName,
                            e.getMessage()));
                    return false;
                }
                logger.info(String.format("Created file %s.", outputName));
                continue;
            }

            Iterator<MetaPackage> it = metaModel.getPackages().iterator();
            try {
                if (metaModel.getPackages().size() > 1) {
                    int i = 1;
                    while (it.hasNext()) {
                        MetaPackage metaPackage = it.next();
                        String append = "";
                        if (metaPackage.getName().isEmpty()) {
                            append = String.valueOf(i);
                            i++;
                        } else {
                            append = Util.metaNameConvertion(metaPackage.getName(), "", "", true);
                        }

                        int splitIndex = outputName.lastIndexOf(".");
                        String numberedOutputName =
                            outputName.substring(0, splitIndex) + "_" + append
                                + outputName.substring(splitIndex, outputName.length());

                        OWLMaker.toOwl(numberedOutputName, iriName, metaPackage);
                        logger.info(String.format("Created file %s.", numberedOutputName));
                    }
                } else {
                    while (it.hasNext()) {
                        MetaPackage metaPackage = it.next();
                        OWLMaker.toOwl(outputName, iriName, metaPackage);
                        logger.info(String.format("Created file %s.", outputName));
                    }
                }
            } catch (OWLException e) {
                // System.err.println(e.getMessage());
                logger.error(String.format("Failed building OWL ontology into file %s. Details: %s.", outputName,
                        e.getMessage()));
                return false;
            }
        }
        return true;
    }

    /**
     * Same as main method, but all parameters are passed as arguments, not as a list of assignments.
     */
    public static MetaModel transformAndReadMetaModel(InputStream input, InputStream settings, String converterType,
        String IRI, boolean verbose, List<String> outputFormats) {

        List<String> params = new LinkedList<String>();
        params.add(String.format("%s=%s", ParameterHandling.INPUT));
        params.add(String.format("%s=%s", ParameterHandling.IRI, IRI));
        params.add(String.format("%s=%s", ParameterHandling.OUTPUT));
        params.add(String.format("%s=%s", ParameterHandling.CONVERTER, converterType));
        params.add(String.format("%s=%s", ParameterHandling.SETTINGS));
        params.add(String.format("%s=%s", ParameterHandling.VERBOSE, verbose));

        String[] args = new String[params.size()];
        return Main.mainWithoutModel(params.toArray(args));
    }

    /**
     * Creates a JSON file by using the standard settings for the output of the OWL file.
     * 
     * @param metaModel the (already prepared) meta model.
     * @throws IOException is thrown if the file cannot be created correctly.
     */
    public static void createJSON(MetaModel metaModel) throws IOException {
        String outputNameJson = SettingsReader.readProperty(ParameterHandling.OUTPUT).replace(".owl", ".json");
        JsonMaker.toJsonFile(metaModel, outputNameJson);
        logger.info(String.format("Created file %s.", outputNameJson));
    }
}
