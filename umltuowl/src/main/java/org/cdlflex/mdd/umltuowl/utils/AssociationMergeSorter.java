package org.cdlflex.mdd.umltuowl.utils;

import java.util.Comparator;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;

/**
 * Compares associations which are connected between two equal entities and have got the same association name assigned.
 * 
 */
public class AssociationMergeSorter implements Comparator<MetaAssociation> {
    private static final int A1_GREATER = 1;
    private static final int A2_GREATER = -1;
    private static final int TIE = 0;

    @Override
    public int compare(MetaAssociation a1, MetaAssociation a2) {
        int compSum = 0;
        // return > 0 if a1 is greater
        if (a1.getInverseAssociation() == null && a2.getInverseAssociation() != null) {
            return A1_GREATER;
        } else if (a1.getInverseAssociation() != null && a2.getInverseAssociation() == null) {
            return A2_GREATER;
        } else if (a1.getInverseAssociation() == null && a2.getInverseAssociation() == null) {
            compSum += compareType(a1, a2) * 10;
            compSum += compareRange(a1, a2);

        } else {
            // both are bidirectional
            compSum += compareType(a1, a2) * 10;
            compSum += compareRange(a1, a2);
            return compSum;
        }

        if (compSum > 0) {
            return A1_GREATER;
        } else if (compSum < 0) {
            return A2_GREATER;
        } else {
            return TIE;
        }
    }

    private int compareType(MetaAssociation a1, MetaAssociation a2) {
        boolean a1Typed = a1.isAggregation() || a1.isComposition();
        boolean a2Typed = a2.isAggregation() || a2.isComposition();

        if (a1Typed && !a2Typed) {
            return A1_GREATER;
        } else if (!a1Typed && a2Typed) {
            return A2_GREATER;
        } else {
            return TIE;
        }
    }

    private int compareRange(MetaAssociation a1, MetaAssociation a2) {
        if (a1.getFrom().getRange().size() != 1 && a2.getFrom().getRange().size() == 1) {
            return A1_GREATER;
        } else if (a2.getFrom().getRange().size() != 1 && a2.getFrom().getRange().size() == 1) {
            return A2_GREATER;
        } else if (a1.getTo().getRange().size() != 1 && a2.getTo().getRange().size() == 1) {
            return A1_GREATER;
        } else if (a2.getTo().getRange().size() != 1 && a1.getTo().getRange().size() == 1) {
            return A2_GREATER;
        } else {
            return TIE;
        }
    }

}
