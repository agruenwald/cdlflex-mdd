package org.cdlflex.mdd.umltuowl.uml2metamodel;

import java.util.LinkedList;
import java.util.List;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClassPoint;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaElement;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.cdlflex.mdd.umltuowl.utils.SettingsReader;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Gruenwald Converter for Microsoft Visio 2010's XMI 1.0 syntax.
 */
public class MSVisio2010ConverterXMI extends UMLConverter {
    private MetaModel model;
    private Document xmi;
    private static final Logger logger = LoggerFactory.getLogger(MSVisio2010ConverterXMI.class);
    private static final String COMPOSITION = "composition";
    private static final String AGGREGATION = "aggregation";

    /**
     * Creation of a new Microsoft Visio 2010 XMI meta model (done automatically by factory).
     */
    public MSVisio2010ConverterXMI() {
        this.model = new MetaModel("UML_MICROSOFT_VISIO_2010_XMI_MODEL");
    }

    private String getFoundationName(Element e) {
        Element x = e.getElementsByTag("Foundation.Core.ModelElement.name").first();
        if (x == null) {
            return "";
        } else {
            return x.text();
        }
    }

    @Override
    public MetaModel convertModel(Document xmi) {
        logger.info(String.format("Convert model using %s XMI converter.",
                MSVisio2010ConverterXMI.class.getSimpleName()));
        this.xmi = xmi;

        Element xmiModel = this.selectXMIModel();
        model = new MetaModel(xmiModel.getElementsByTag("Foundation.core.modelelement.name").first().text());

        buildClasses(xmiModel, false);
        buildClasses(xmiModel, true);
        addGeneralization();
        addAssociations();
        return model;
    }

    /**
     * Select a model in the UML diagram if more than one are existing.
     * 
     * @return the model (XMI-Element) that has been selected depending on number of models existing in the Visio XMI
     *         file and the user's input parameters.
     */
    private Element selectXMIModel() {
        String modelNo = SettingsReader.readProperty("visio-model");
        Elements models = xmi.getElementsByTag("Model_Management.Model");

        Element model = null;
        int size = 0;
        int i = 1;
        for (Element m : models) {
            Elements modelName = m.getElementsByTag("Foundation.Core.ModelElement.name");
            if (modelName.size() > 0) {
                String name = modelName.first().text();
                logger.debug("Found model " + name);
                if (modelNo.isEmpty()) {
                    if (size < modelName.size()) {
                        size = modelName.size();
                        model = m;
                    }
                } else {
                    if (i == Integer.parseInt(modelNo)) {
                        model = m;
                        logger.info(String.format("Picked model %s (no %s).", name, i));
                        break;
                    }
                }
                i++;
            }
        }

        if (modelNo.isEmpty()) {
            logger.info(String
                    .format("Picked model that contains the most elements (%s). %n"
                        + "If you want to use another model you can do this by specifying input parameter \"viso-model\" "
                        + "for the Visio 2010 converter. %nE.g.: visio-model=1 or visio-model=2 (start with values greater than zero).",
                            model.getElementsByTag("Foundation.Core.ModelElement.name").first().text()));
        }
        return model;
    }

    /**
     * Add sub- and super class relations.
     */
    private void addGeneralization() {
        logger.trace("Called addGeneralization().");

        for (MetaPackage p : model.getPackages()) {
            for (MetaClass c : p.getClasses()) {
                String selector = String.format("[xmi.id=%s]", c.getId());
                // logger.debug("Selector is " + selector);
                Element xmiClass = xmi.getElementsByTag("Foundation.Core.Class").select(selector).first();
                if (xmiClass != null) { // dirty hack
                    handleGeneralization(xmiClass, p, c);
                }
            }
        }
        return;

    }

    private void handleGeneralization(Element xmiClass, MetaPackage metaPackage, MetaClass metaClass) {
        logger.debug(String.format("Handle generalization for class/interface %s.", metaClass));

        Elements superclasses = xmiClass.getElementsByTag("Foundation.Core.GeneralizableElement.generalization");
        List<Element> sc = new LinkedList<Element>();
        for (Element s : superclasses) {
            sc.addAll(s.getElementsByTag("Foundation.Core.Generalization"));
        }
        for (Element s : sc) {
            generalizationLinkage(metaPackage, metaClass, s.attr("xmi.idref"), "Generalization");
        }

        sc = new LinkedList<Element>();
        Elements subclasses = xmiClass.getElementsByTag("Foundation.Core.GeneralizableElement.specialization");
        for (Element s : subclasses) {
            sc.addAll(s.getElementsByTag("Foundation.Core.Generalization"));
        }
        for (Element s : sc) {
            generalizationLinkage(metaPackage, metaClass, s.attr("xmi.idref"), "Specialization");
        }

    }

    /**
     * Linkage of classes with their sub- and superclasses. The links are added in {@code this.model} for the specified
     * meta class (parameter metaClass).
     * 
     * @param metaPackage the actual package.
     * @param metaClass the class to link.
     * @param ref the reference id in XMI that is linked in the generalization/specialization section of the XMI class.
     * @param type Either the reference id should be proofed on {@code "Generalization"} or {@code "Specialization"}.
     */
    private void generalizationLinkage(MetaPackage metaPackage, MetaClass metaClass, String ref, String type) {
        if (ref.isEmpty()) {
            return;
        }

        Elements linkedElements =
            xmi.getElementsByTag("Foundation.Core.Generalization").select("[xmi.idref=" + ref + "]");
        for (Element l : linkedElements) {
            if (type.equals("Generalization")) {
                if (l.parent().tagName().endsWith("specialization")) {
                    Element p = l.parent().parent();
                    String pId = p.attr("xmi.id");
                    MetaClass superclass = metaPackage.findClassById(pId);
                    metaClass.getSuperclasses().add(superclass);
                    superclass.getSubclasses().add(metaClass);
                    logger.debug("superclass is : " + superclass);
                }
            } else {
                if (l.parent().tagName().endsWith("generalization")) {
                    Element p = l.parent().parent();
                    String pId = p.attr("xmi.id");
                    MetaClass subclass = metaPackage.findClassById(pId);
                    metaClass.getSubclasses().add(subclass);
                    subclass.getSuperclasses().add(metaClass);
                    logger.debug("subclass is : " + subclass);
                }
            }

        }

    }

    /**
     * Add fine grained information (attributes) to meta classes.
     * 
     * @param xmiModel XMI code of selected model in MS Visio's file.
     * @param isInterface true if interfaces should be built.
     */
    private void buildClasses(Element xmiModel, boolean isInterface) {
        logger.trace("Called buildClasses(classes).");
        Elements umlClasses = null;
        if (isInterface) {
            umlClasses = this.xmi.getElementsByTag("Foundation.Core.Interface");
        } else {
            umlClasses = this.xmi.getElementsByTag("Foundation.Core.Class");
        }
        for (Element c : umlClasses) {
            String name = getFoundationName(c);
            if (!name.isEmpty()) {
                logger.debug("Found class " + name);
                Elements pkgs = c.getElementsByTag("Model_Management.Package");
                if (pkgs.isEmpty()) {
                    continue;
                }
                Element pckgRef = pkgs.first();
                if (pckgRef != null) {
                    String refId = pckgRef.attr("xmi.idref");
                    // logger.info("Package : " + pckgRef.attr("idref"));
                    Element oPckg =
                        xmi.getElementsByTag("Model_Management.package").select(String.format("[xmi.id=%s]", refId))
                                .first();
                    // Element oPckg = xmi.select(String.format("Model_Management[Package.id=%s]",refId)).first();
                    logger.trace("Package name is " + getFoundationName(oPckg));
                    MetaPackage metaPackage = model.managePackageByName(getFoundationName(oPckg));
                    MetaClass metaClass = new MetaClass(name);
                    metaClass.setId(c.attr("xmi.id"));
                    metaClass.setAbstractClass(isAbstract(c));
                    metaClass.setInterfaceClass(isInterface);
                    buildAttributes(metaClass, c);
                    addComment(metaClass, c);
                    metaPackage.getClasses().add(metaClass);
                }
            }
        }
    }

    /**
     * test if a class/an interface is abstract.
     * 
     * @param c the XMI element of the class or the interface
     * @return {@code true}, if interface has been detected as abstract.
     */
    private boolean isAbstract(Element c) {
        Element isAbstract = c.getElementsByTag("Foundation.Core.GeneralizableElement.isAbstract").first();
        if (isAbstract != null) {
            String isA = isAbstract.attr("xmi.value");
            if (isA.equalsIgnoreCase("true")) {
                return true;
            }
        }
        return false;
    }

    /**
     * find data type by its relating {@code rangeId}, which is part of the XMI file.
     * 
     * @param rangeId the internal data type id (provided by VParadigm XMI).
     * @return data type name (e.g. string) or empty if void.
     */
    private String findRangeType(String rangeId) {
        logger.trace(String.format("Called findRangeType(xmi-document,%s", rangeId));
        Element dtype = xmi.getElementsByTag("Foundation.Core.DataType").select("[xmi.id=" + rangeId + "]").first();
        String range = getFoundationName(dtype);
        return range;
    }

    /**
     * Add all attributes to meta class. Methods are ignored.
     * 
     * @param umlClass a single meta class.
     * @param xmiClass corresponding class from Visual Paradigm XMI file.
     * @return meta class that contains all related attributes.
     */
    private MetaClass buildAttributes(MetaClass umlClass, Element xmiClass) {
        logger.trace(String.format("Called buildAttributes(%s,jsoupXMIClass)", umlClass.getName()));
        Elements attrs = xmiClass.getElementsByTag("Foundation.Core.Attribute");
        for (Element a : attrs) {
            String name = getFoundationName(a);
            if (name.isEmpty()) {
                continue;
            }
            String visibility =
                a.getElementsByTag("Foundation.Core.ModelElement.visibility").first().attr("xmi.value");
            MetaAttribute metaAttribute = new MetaAttribute(name);
            metaAttribute.setVisibility(visibility);
            Elements structural = a.getElementsByTag("Foundation.Core.StructuralFeature.type");
            String datatypeId = "";
            for (Element e : structural) {
                Elements datatype = e.getElementsByTag("Foundation.Core.DataType");
                if (datatype.size() == 1) {
                    datatypeId = datatype.first().attr("xmi.idref");
                    break;
                }
            }
            /*
             * String datatypeId = a.getElementsByTag("Foundation.Core.StructuralFeature.type")
             * .getElementsByTag("Foundation.Core.DataType").attr("xmi.idref");
             */
            String range = "void";
            if (!datatypeId.isEmpty()) {
                logger.trace("Range id of " + umlClass + " is " + datatypeId);
                range = findRangeType(datatypeId);
                logger.debug("Range for " + metaAttribute.getName() + " is " + range);

            }
            metaAttribute.setRange(range);
            umlClass.getAttributes().add(metaAttribute);
        }
        return umlClass;
    }

    /**
     * Add associations between classes.
     */
    private void addAssociations() {
        Elements associationsAll = xmi.getElementsByTag("Foundation.Core.Association");
        List<Element> associations = new LinkedList<Element>();
        for (Element e : associationsAll) {
            if (e.hasAttr("xmi.id")) {
                associations.add(e);
            }
        }
        for (Element ass : associations) {
            Elements allEnds = ass.getElementsByTag("Foundation.Core.AssociationEnd"); // .select(":has([xmi.id])");
            List<Element> ends = new LinkedList<Element>();
            for (Element e : allEnds) {
                if (e.hasAttr("xmi.id")) {
                    ends.add(e);
                }
            }
            String assId = ass.attr("xmi.id");
            String aName1 = "";
            String aName2 = "";

            // select all elements that are not empty and do not end with a number, because those
            // entries are autogenerated by MS Visio (e.g. Association6, Association7,...).
            Elements assName =
                ass.getElementsByTag("Foundation.Core.ModelElement.name").select(":matchesOwn(\\S*[^\\d]$)");
            if (assName.size() == 1) {
                aName1 = aName2 = assName.get(0).text();
            } else if (assName.size() == 2) {
                aName1 = assName.get(0).text();
                aName2 = assName.get(1).text();
            } else if (assName.size() > 2) {
                logger.info(String.format("Association with id %s contains more than 2 name tags.", assId));
            }

            Element endpoint1 = ends.get(0);
            Element endpoint2 = ends.get(1);
            this.createAndLinkAssociation(ass.attr("xmi.id"), ass, aName1, aName2, endpoint1, endpoint2);
        }
    }

    /**
     * Creates new associations and links them to the meta classes.
     * 
     * @param associationId the ID of the association (same for both link directions). Is used to identify the
     *        association globally (e.g. find comments).
     * @param association the association XMI element containing containing all information about an association.
     * @param name1 the name of the association, if entered by the user. If no association name is given, this value
     *        (and {@code name2}) will be empty. If the user entered an association name or only used one name for the
     *        association, {@code name1} and {@code name2} are identically. If the user tagged the association in both
     *        direction, then {@code name1} contains the name of the first endpoint and {@code name2} contains the name
     *        of the second one.
     * @param name2 see parameter {@code name1}.
     * @param endpoint1 XMI element that contains endpoint 1 of the association.
     * @param endpoint2 XMI element that contains endpoin2 1 of the association.
     * @param isBidirectional navigation allowed in both directions.
     */
    private void createAndLinkAssociation(String associationId, Element association, String name1, String name2,
        Element endpoint1, Element endpoint2) {

        MetaAssociation fromAssociation =
            new MetaAssociation(association.attr("xmi.id"), endpoint1.attr("xmi.id"), name1);
        MetaAssociation toAssociation =
            new MetaAssociation(association.attr("xmi.id"), endpoint2.attr("xmi.id"), name2);

        List<Element> es = new LinkedList<Element>();
        Element class1 = null;
        es.addAll(endpoint1.getElementsByTag("Foundation.Core.AssociationEnd.type"));
        for (Element e : es) {
            Elements x = e.getElementsByTag("Foundation.Core.Class");
            if (x.size() > 0) {
                class1 = x.first();
                break;
            }
        }

        Element class2 = null;
        es = new LinkedList<Element>();
        es.addAll(endpoint2.getElementsByTag("Foundation.Core.AssociationEnd.type"));
        for (Element e : es) {
            Elements x = e.getElementsByTag("Foundation.Core.Class");
            if (x.size() > 0) {
                class2 = x.first();
                break;
            }
        }

        MetaClass metaClassFrom = model.findClassById(class1.attr("xmi.idref"));
        MetaClass metaClassTo = model.findClassById(class2.attr("xmi.idref"));

        String range1 = endpoint1.getElementsByTag("Foundation.Core.AssociationEnd.multiplicity").text();
        String range2 = endpoint2.getElementsByTag("Foundation.Core.AssociationEnd.multiplicity").text();

        boolean aggregationFrom =
            isAggregation(endpoint1.getElementsByTag("Foundation.Core.AssociationEnd.aggregation").first()
                    .attr("xmi.value"));
        boolean aggregationTo =
            isAggregation(endpoint2.getElementsByTag("Foundation.Core.AssociationEnd.aggregation").first()
                    .attr("xmi.value"));
        boolean compositionFrom =
            isComposition(endpoint1.getElementsByTag("Foundation.Core.AssociationEnd.aggregation").first()
                    .attr("xmi.value"));
        boolean compositionTo =
            isComposition(endpoint2.getElementsByTag("Foundation.Core.AssociationEnd.aggregation").first()
                    .attr("xmi.value"));

        fromAssociation.setFrom(new MetaClassPoint(metaClassFrom));
        fromAssociation.setTo(new MetaClassPoint(metaClassTo));

        fromAssociation.getFrom().setRangeSmart(range1, range1);
        fromAssociation.setFrom(new MetaClassPoint(metaClassFrom));
        fromAssociation.setTo(new MetaClassPoint(metaClassTo));
        fromAssociation.getFrom().setRangeSmart(range1, range1);
        fromAssociation.getTo().setRangeSmart(range2, range2);
        fromAssociation.setAggregation(aggregationFrom);
        fromAssociation.setComposition(compositionFrom);

        toAssociation.setTo(fromAssociation.getFrom());
        toAssociation.setFrom(fromAssociation.getTo());
        toAssociation.setAggregation(aggregationTo);
        toAssociation.setComposition(compositionTo);

        boolean navigable1 =
            Boolean.parseBoolean(endpoint1.getElementsByTag("Foundation.Core.AssociationEnd.isNavigable").first()
                    .attr("xmi.value"));
        boolean navigable2 =
            Boolean.parseBoolean(endpoint2.getElementsByTag("Foundation.Core.AssociationEnd.isNavigable").first()
                    .attr("xmi.value"));

        boolean isBidirectional = true;
        if (!navigable1 && !navigable2) {
            // best practice
            isBidirectional = true;
        } else if (!navigable1 || !navigable2) {
            isBidirectional = false;
        }

        addComment(fromAssociation, association);
        addComment(toAssociation, association);
        if (isBidirectional) {
            fromAssociation.setInverseAssociation(toAssociation);
            toAssociation.setInverseAssociation(fromAssociation);
            metaClassFrom.getAssociations().add(fromAssociation);
            metaClassTo.getAssociations().add(toAssociation);
        } else {
            if (!navigable1) {
                metaClassFrom.getAssociations().add(fromAssociation);
            } else {
                metaClassTo.getAssociations().add(toAssociation);
            }
        }

        logger.debug(String.format("association (%s) %s<->%s (%s) found. Navigation is %s.", name1.isEmpty()
            ? "noname" : name1, metaClassFrom.getName(), metaClassTo.getName(), range1, isBidirectional
            ? "bidirectional" : "unidirectional"));

    }

    /**
     * MS Visio 2010 creates aggregation/composition information language dependet. So we only know the German value and
     * can only guess for English values.
     * 
     * @param value the value do be interpreted.
     * @return {@code COMPOSITION}, if value was evaluated as a composition, {@code AGGREGATION}, if value was evaluated
     *         as an aggregation and otherwise empty.
     */
    private String evaluateMSVisioAggregationInfo(String value) {
        if (value.contains("keine") || value.contains("none") || value.contains("no one") || value.contains("empty")) {
            return "";
        } else if (value.contains("zusammengesetzt") || value.contains("composition") || value.contains("compos")) {
            return COMPOSITION;
        } else if (value.contains("gemeinsam") || value.contains("aggregation") || value.contains("aggregated")
            || value.contains("aggreg")) {
            return AGGREGATION;
        } else {
            return "";
        }
    }

    /**
     * @param value the aggregation value from MS Visio XMI (language dependent).
     * @return {@code true}, if value was interpreted as an aggregation.
     */
    private boolean isAggregation(String value) {
        String normalizedValue = this.evaluateMSVisioAggregationInfo(value);
        if (normalizedValue.equals(AGGREGATION)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param value the aggregation value from MS Visio XMI (language dependent).
     * @return {@code true}, if value was interpreted as a composition.
     */
    private boolean isComposition(String value) {
        String normalizedValue = this.evaluateMSVisioAggregationInfo(value);
        if (normalizedValue.equals(COMPOSITION)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * MS Visio 2010 XMI 1.0 does not support comments. But one can use the "documentation" field for classes,
     * interfaces and attributes to add comments.
     * 
     * @param element the meta element.
     * @param xmi the xmi element that contains all tags that are necessary for grabbing the documentation field.
     */
    public void addComment(MetaElement element, Element xmiElement) {
        Elements comments =
            xmiElement.getElementsByTag("Foundation.Extension_Mechanisms.TaggedValue.value").select(
                    ":matchesOwn(\\S+)");
        for (Element c : comments) {
            element.getComment().add(new MetaComment(c.text()));
        }
    }
}
