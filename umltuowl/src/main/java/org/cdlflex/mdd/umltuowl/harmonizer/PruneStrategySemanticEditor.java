package org.cdlflex.mdd.umltuowl.harmonizer;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaElement;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Gruenwald This strategy prunes associations, classes and packages whom's status has been set to
 *         "irrelevant" by the user of the Semantic Web Editor (CDL-Flex-editor). These concepts are filtered out by
 *         this strategy. For instance, concepts which have been marked as tool-internal info classes will be
 *         disregarded.
 */
public class PruneStrategySemanticEditor extends MergeStrategy {
    private static final Logger logger = LoggerFactory.getLogger(PruneStrategySemanticEditor.class);

    public static final String ANNOTATION_LOCAL = "local";
    public static final String ANNOTATION_LOCAL_AVAILABLE = "local_available";
    public static final String ANNOTATION_INFO = "info";
    public static final String ANNOTATION_SUGGESTION = "suggestion";
    public static final String ANNOTATION_AVAILABLE = "available";

    public static final String STATUS_ANNOTATION = "@status";

    private static final List<String> prunedStati = new LinkedList<String>();
    static {
        // prunedStati.add(ANNOTATION_LOCAL_AVAILABLE);
        // prunedStati.add(ANNOTATION_LOCAL);
        prunedStati.add(ANNOTATION_INFO);
        prunedStati.add(ANNOTATION_SUGGESTION);
        // prunedStati.add(ANNOTATION_AVAILABLE);
    }

    @Override
    public MetaModel applyStrategy(MetaModel metamodel) {
        metamodel = this.pruneElementsByStatus(metamodel);
        return metamodel;
    }

    /**
     * @param model original model
     * @return new model consisting of one package that contains the pruned meta-model.
     */
    private MetaModel pruneElementsByStatus(MetaModel mm) {
        logger.trace("Prune elements via status ( " + PruneStrategySemanticEditor.class.getName() + ").");
        this.prune(mm.getPackages());

        for (MetaPackage p : mm.getPackages()) {
            this.prune(p.getClasses());
            for (MetaClass c : p.getClasses()) {
                this.prune(c.getAttributes());
            }
        }

        // it might be that links are available which link to non-existing entries
        for (MetaPackage p : mm.getPackages()) {
            for (MetaClass c : p.getClasses()) {
                this.clean(mm, c, c.getSubclasses());
                this.clean(mm, c, c.getSuperclasses());
                this.clean(mm, c, c.getAssociations());
                // enum is currently not cleaned. the reason is that
                // this should be managed directly in the diagram and
                // will be recognized by the model generator properly
            }
        }
        return mm;
    }

    private void prune(Set<? extends MetaElement> elems) {
        LinkedList<MetaElement> lRem = new LinkedList<MetaElement>();
        int i = 0;
        for (MetaElement p : elems) {
            String relevantStatus = this.isRelevant(p);
            if (!relevantStatus.isEmpty()) {
                lRem.add(p);
                logger.info(String.format("Pruned %s %s (%s).", lRem.get(i).getClass().getSimpleName(), lRem.get(i)
                        .getName(), relevantStatus));
                i++;
            }
        }

        for (i = 0; i < lRem.size(); i++) {
            elems.remove(lRem.get(i));
        }
    }

    private void prune(List<? extends MetaElement> elems) {
        LinkedList<MetaElement> lRem = new LinkedList<MetaElement>();
        int i = 0;
        for (MetaElement p : elems) {
            String relevantStatus = this.isRelevant(p);
            if (!relevantStatus.isEmpty()) {
                lRem.add(p);
                logger.info(String.format("Pruned %s %s (%s).", lRem.get(i).getClass().getSimpleName(), lRem.get(i)
                        .getName(), relevantStatus));
                i++;
            }
        }

        for (i = 0; i < lRem.size(); i++) {
            elems.remove(lRem.get(i));
        }
    }

    private String isRelevant(MetaElement e) {
        List<MetaComment> relevantAnnotations =
            e.findComments(MetaComment.TYPE_INTERNAL_ANNOTATION, STATUS_ANNOTATION, prunedStati);
        if (relevantAnnotations.size() > 0) {
            return relevantAnnotations.get(0).getText();
        } else {
            return "";
        }
    }

    private void clean(MetaModel mm, MetaClass p, Set<MetaClass> set) {
        List<MetaClass> notAvailable = new LinkedList<MetaClass>();
        for (MetaClass c : set) {
            if (mm.findClassById(c.getId()) == null) {
                notAvailable.add(c);
            }
        }

        for (MetaClass c : notAvailable) {
            set.remove(c);
            logger.info(String.format("Remove generalization info between %s and %s.", c, p));
        }
    }

    private void clean(MetaModel mm, MetaClass p, List<MetaAssociation> set) {
        List<MetaAssociation> notAvailable = new LinkedList<MetaAssociation>();
        for (MetaAssociation a : set) {
            // String fromId = a.getFrom().getMetaClassId();
            String toId = a.getTo().getMetaClassId();
            if (mm.findClassById(toId) == null) {
                notAvailable.add(a);
            }
        }

        for (MetaAssociation a : notAvailable) {
            set.remove(a);
            logger.info(String.format("Remove association %s in %s.", a, p));
        }
    }

}
