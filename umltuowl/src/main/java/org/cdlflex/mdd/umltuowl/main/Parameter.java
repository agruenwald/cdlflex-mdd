package org.cdlflex.mdd.umltuowl.main;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com> Specification of an input parameter (internal)
 * 
 */
public class Parameter {
    private final String name;
    private String description;
    private final boolean isMandatory;
    private final boolean print;
    private final boolean allowMultiple;

    /**
     * Creation of a new input parameter.
     * 
     * @param name the name of the parameter as specified by the user.
     * @param description the description for the user.
     * @param isMandatory if {@code true}, the parameter is mandatory.
     * @param allowMultiple if true then several parameters can be passed in the form param-1, param-2, ...
     */
    public Parameter(String name, String description, boolean isMandatory, boolean print, boolean allowMultiple) {
        this.name = name;
        this.description = description;
        this.isMandatory = isMandatory;
        this.print = print;
        this.allowMultiple = allowMultiple;
    }

    /**
     * Creation of a new input parameter.
     * 
     * @param name the name of the parameter as specified by the user.
     * @param description the description for the user.
     * @param isMandatory if {@code true}, the parameter is mandatory.
     */
    public Parameter(String name, String description, boolean isMandatory) {
        this(name, description, isMandatory, true, false);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public boolean allowMultiple() {
        return this.allowMultiple;
    }

    public boolean isPrint() {
        return print;
    }

    @Override
    public String toString() {
        return name;
    }

    public String toConsole() {
        String res = String.format("%s: %s%s", name, description, isMandatory ? " (mandatory)" : "(optional)");
        return res;
    }
}
