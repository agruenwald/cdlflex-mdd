package org.cdlflex.mdd.umltuowl.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.cdlflex.mdd.umltuowl.uml2metamodel.TransferFactory;
import org.cdlflex.mdd.umltuowl.uml2metamodel.TransferFactory.ConverterInfo;
import org.cdlflex.mdd.umltuowl.utils.SettingsReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Input parameter handling (for console).
 */
public class ParameterHandling {
    private static final Logger logger = LoggerFactory.getLogger(ParameterHandling.class);
    public static final String SETTINGS = "settings";
    public static final String INPUT = "input";
    public static final String IRI = "IRI";
    public static final String OUTPUT = "output";
    public static final String CONVERTER = "converter";
    public static final String VERBOSE = "verbose";

    private List<Parameter> knownParams;

    protected String getConverterInfo() {
        String res = "";
        for (TransferFactory.ConverterInfo conv : TransferFactory.CONVERTERS) {
            res += String.format("  - %s%n", conv);
        }
        return res;
    }

    public ParameterHandling() {
        this.knownParams = new ArrayList<Parameter>();
        knownParams
                .add(new Parameter(
                        IRI,
                        "The IRI for prefixing entities in the OWL ontology. E.g: http://www.tuwien.ac.at/out.owl."
                            + "Note, that an invalid IRI can lead to problems in Protege. Therefore, always use the above described syntax (http://.../xyz.owl)!",
                        true));
        knownParams.add(new Parameter(INPUT,
                "The path of the input file that contains the model. E.g: /home/andi/powerstationmodel.xmi", true));

        knownParams
                .add(new Parameter(
                        OUTPUT,
                        "The path of the resulting file(s). E.g: /home/andi/out.owl. "
                            + "If more than one files are resulting (depends on settings), then a number is assigned automatically. "
                            + "Multiple outputs are prefixed with numbers, e.g., "
                            + OUTPUT
                            + "-1, so that several files can be created "
                            + "within one program run. The output format is detected based on the file ending. Currently json and owl are supported.",
                        true, true, true));

        for (int i = 1; i <= 100; i++) {
            knownParams
                    .add(new Parameter(
                            OUTPUT + "-" + i,
                            "The path of the resulting file(s). E.g: /home/andi/out.owl. "
                                + "If more than one files are resulting (depends on settings), then a number is assigned automatically. "
                                + "Multiple outputs are prefixed with numbers, e.g., "
                                + OUTPUT
                                + "-1, so that several files can be created "
                                + "within one program run. The output format is detected based on the file ending. Currently json and owl are supported.",
                            false, false, false));
        }

        knownParams
                .add(new Parameter(CONVERTER,
                        "The name of the converter that translates the model into meta syntax: \n"
                            + getConverterInfo(), true));

        knownParams
                .add(new Parameter(
                        SETTINGS,
                        "Specify a path to your own settings.properties file. You can copy the default settings file and modify "
                            + "the default values by overwritting the values in the settings.properties file. Furthermore, you can "
                            + "overwrite each of the property keys by specifying values from the command line tool. \n"
                            + "For a description of the parameters take a look inside the settings.properties file.\n"
                            + "Please enter the path to your settings.properties file like this: /home/andi/settings.properties.",
                        false));

        knownParams.add(new Parameter(VERBOSE, "Use to output additional information about the model (" + VERBOSE
            + "=true). \n"
            + "For further logging information checkout the source, change logging.properties and build "
            + "the project using ant.", false));

        knownParams.add(new Parameter("visio-model",
                "Specific parameter for MS Visio converter (numeric). E.g. visio-model=1, visio-model=2.", false));
    }

    /**
     * Validate input parameters and check if they are ok.
     * 
     * @param args input parameters in the format: param=value.
     * @return {@code true}, if error occured or input is incomplete.
     */
    public boolean validateParameters(String[] args) {
        boolean isError = false;
        for (String a : args) {
            String[] part = a.split("=");
            if (part.length != 2) {
                logger.error(String.format("Invalid input parameter combination \"%s\".", a));
                isError = true;
            } else {
                boolean paramOk = false;
                for (Parameter knownP : this.knownParams) {
                    if (knownP.getName().toLowerCase().equalsIgnoreCase(part[0])) {
                        paramOk = true;
                        SettingsReader.overwriteProperty(part[0], part[1]);
                        break;
                    }
                }

                // parameter still unknown
                if (!paramOk) {
                    logger.debug("No default input parameter. Looking up in settings...");
                    // if it is not an default input parameter it might be a settings parameter
                    if (!SettingsReader.containsProperty(part[0])) {
                        isError = true;
                        logger.error(String.format("Unknown input parameter name \"%s\".", part[0]));
                    }
                } else {
                    SettingsReader.overwriteProperty(part[0], part[1]);
                }
            }
        }
        // load individual settings if given by the user
        try {
            SettingsReader.loadUserSettings(SettingsReader.readProperty(SETTINGS));
        } catch (IOException e) {
            logger.error(String.format("Invalid path for your settings file (%s) specified.",
                    SettingsReader.readProperty(SETTINGS)));
            isError = true;
        }

        // now check missing parameters
        if (!SettingsReader.readProperty("-h").isEmpty() || !SettingsReader.readProperty("-help").isEmpty()
            || args.length == 0) {
            logger.info(String
                    .format("UML2OWL is a tool by Vienna Technology University that transforms XMI syntax into Protege OWL syntax."));
            logger.info(String.format("Version: %s", Main.VERSION));
            logger.info(String.format("Please specify the following parameters:"));
            for (Parameter p : this.knownParams) {
                if (p.isPrint()) {
                    logger.info(p.toConsole());
                }
            }
            isError = true;
        } else if (!isError) {
            for (Parameter p : this.knownParams) {
                if (p.isMandatory() && SettingsReader.readProperty(p.getName()).isEmpty()) {
                    // multiple parameters and mandatory: param-1 must exist at least.
                    if (!(p.allowMultiple() && !SettingsReader.readProperty(p.getName() + "-1").isEmpty())) {
                        logger.error(String.format("Missing input parameter: %s.", p.getName()));
                        logger.info(p.toConsole());
                        isError = true;
                    }
                }
            }
        }

        if (!isError) {
            String converter = SettingsReader.readProperty(CONVERTER);
            isError = true;
            for (ConverterInfo ci : TransferFactory.CONVERTERS) {
                if (ci.getName().equals(converter)) {
                    isError = false;
                    break;
                }
            }
            if (isError) {
                logger.error(String.format("Invalid converter name : %s.", converter));
            }
        }
        return isError;
    }

}
