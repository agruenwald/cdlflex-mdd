package org.cdlflex.mdd.umltuowl.metamodel;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * This class handles commments, respectively annotations, which are either created manually, or automated to store
 * information during the transformation processes. In the simplest case a comment is just a String. In more advanced
 * cases, this String contains specific additional information about the type of the comment. However, every
 * annotation/comment can be transformed into a single String easily.
 * 
 * @author andreas_gruenwald
 * 
 */
public class MetaComment implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String TYPE_COMMENT = "";
    /** default, plain comment **/
    /** used to store internal transformation and positioning info, which is mainly useful for machines, not for human */
    public static final String TYPE_INTERNAL_ANNOTATION = "@internal";

    /**
     * might be used to save additional model constraints, e.g. a regex for an attribute or a more complicated
     * constraint for a class, or a package.
     */
    public static final String TYPE_CONSTRAINT = "@constraint";

    private String text;
    private String type;
    private Date createdAt;
    private String creatorName;

    /**
     * Non-argument constructor, useful for flexjson.
     */
    public MetaComment() {
        this("");
    }

    /**
     * Creation of a plain comment.
     * 
     * @param text the comment text without anything else specified (recommended constructor).
     */
    public MetaComment(String text) {
        this.setText(text);
        this.type = TYPE_COMMENT;
        this.creatorName = "";
        this.createdAt = null;
    }

    /**
     * Creation of a plain comment with additional date information.
     * 
     * @param text the comment without anything else specified
     * @param addDate if true then the date is set to the current date.
     */
    public MetaComment(String text, boolean addDate) {
        this.setText(text);
        this.type = TYPE_COMMENT;
        this.creatorName = "";
        this.createdAt = new Date();
    }

    /**
     * @return the type of the comment, one of the static public variables of MetaComment. By default the getType() will
     *         be empty, which means it is a simple comment.
     */
    public String getType() {
        return type;
    }

    /**
     * Annotations (type {@code TYPE_INTERNAL_ANNOTATION}) can consist of additional sub-types, such as positioning
     * information, state information, or information regarding the type or the code generation behavior. This sub-types
     * are provided in the form "@top:250px", where the part before the ":" is the subtype.
     * 
     * @return a list, where the first element contains the subtype (e.g., "@top") and the second parameter contains the
     *         value of the subtype (e.g., 250px). If no subtype is provided, or the subtype is provided in an illegal
     *         manner then an empty list is returned. Note that annotiations do not necessarily need to provide a
     *         subtype hence an empty return value is a legitimate result.
     */
    public List<String> getAnnotationSubtype() {
        List<String> res = new LinkedList<String>();
        if (this.getType().equals(TYPE_INTERNAL_ANNOTATION)) {
            String pattern = "(^@[a-zA-Z]{2,20}):(.*)$";
            if (this.text.matches(pattern)) {
                res.add(this.text.replaceFirst(pattern, "$1"));
                res.add(this.text.replaceFirst(pattern, "$2"));
            }
        }
        return res;
    }

    /**
     * @param type see static variables of this class. default="".
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return will return {@code null} in the default case, or if available the date of the creation of the comment.
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the date and time the comment was created. on default the createdAt date may be set to
     *        {@code null}.
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return if available the name (or any identifying information) about the creator of the comment. By default will
     *         be null.
     */
    public String getCreatorName() {
        return creatorName;
    }

    /**
     * @param creatorName optionally set the name of the creator of the comment. Decide yourself which identifier to
     *        use, but it is recommended to use a human-readable form of the creator's name.
     */
    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    /**
     * Set the comment text, the annotation information, etc.
     * 
     * @param text the comment information, which is mandatory and must not be empty.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the text of the comment without additional annotation information.
     */
    public String getText() {
        return text;
    }

    /**
     * Summarizes the whole comment information in a string which could be saved without loosing important information
     * (the creation information is not included).
     */
    @Override
    public String toString() {
        String type = this.type.isEmpty() ? this.type : this.type + ":";
        return String.format("%s%s", type, text);
    }

}
