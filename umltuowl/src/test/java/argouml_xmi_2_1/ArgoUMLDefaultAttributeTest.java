package argouml_xmi_2_1;

import java.util.List;

import metamodel.DefaultAttributeTest;

import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import testhelper.TestHelper;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>. Test cases for reference example models.
 * 
 */
public class ArgoUMLDefaultAttributeTest extends DefaultAttributeTest {
    /**
     * ArgoUML does not support different packages (for XMI export).
     */
    @Override
    protected MetaClass readClass(String classname) {
        List<MetaClass> classes = metaModel.findPackageByName("").findMultipleClassesByName(classname);
        if (classes.isEmpty()) {
            return null;
        } else if (classes.size() == 1) {
            return classes.get(0);
        } else {
            // prefer classes with associations because in Argo UML it can be that
            // more than a single class exist with same class name.
            for (MetaClass c : classes) {
                if (!c.getAttributes().isEmpty()) {
                    return c;
                }
            }
            return classes.get(0);
        }
    }

    /**
     * Load the meta model at the beginning.
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void loadMetaModel() throws Exception {
        metaModel =
            TestHelper.loadMetaModel(ArgoUMLDefaultAbstractClass.CONVERTER_NAME,
                    ArgoUMLDefaultAbstractClass.ARGOUML_SAMPLES);
    }

    /** ArgoUML does not convert primitive datatype information into XMI files. **/
    @Test
    @Ignore
    @Override
    public void studentAgeDatatype() {
    }

    /** ArgoUML does not convert primitive datatype information into XMI files. **/
    @Test
    @Ignore
    @Override
    public void studentNameDatatype() {
    }

    /** ArgoUML does not convert primitive datatype information into XMI files. **/
    @Test
    @Ignore
    @Override
    public void studentMaleDatatype() {
    }

    /** ArgoUML does not convert primitive datatype information into XMI files. **/
    @Test
    @Ignore
    @Override
    public void buildingNameDatatype() {
    }

    /** ArgoUML does not convert primitive datatype information into XMI files. **/
    @Test
    @Ignore
    @Override
    public void buildingAgeDatatype() {
    }
}
