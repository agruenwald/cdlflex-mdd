package owlapi;

import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.OWLMaker;
import org.junit.Test;

import testhelper.TestHelper;
import v_paradigm_xmi_2_1.VParadigmDefaultAbstractClass;

public class TestOWLAPI {

    @Test
    public void testOWLAPICreation() throws Exception {
        MetaModel metaModel =
            TestHelper.loadMetaModel(VParadigmDefaultAbstractClass.CONVERTER_NAME,
                    VParadigmDefaultAbstractClass.VISUAL_PARADIGM_SAMPLES);

        OWLMaker.toOwl("owlapi/output_test_tmp.owl", "http://www.tuwien.ac.at/test.owl", metaModel.getPackages()
                .iterator().next());
    }
}
