package metamodel;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.junit.Test;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>. Test cases for reference example models. Same for all different
 *         converters. Tests if generalizations works (are converted correctly) for default reference models. Should be
 *         used for every converter. See {@code VParadigmDefaultAttributeTest}.
 * 
 */
public abstract class DefaultGeneralizationTest {
    protected static MetaModel metaModel;
    private static final String PACKAGE_2 = "Generalization";

    protected MetaClass readClass(String classname) {
        return metaModel.findPackageByName(PACKAGE_2).findClassByName(classname);
    }

    @Test
    public void animalClass() {
        assertNotNull(readClass("Animal"));
    }

    public MetaClass findClassName(Set<MetaClass> set, String classname) {
        Iterator<MetaClass> it = set.iterator();
        while (it.hasNext()) {
            MetaClass mc = it.next();
            if (mc.getName().equals(classname)) {
                return mc;
            }
        }
        return null;
    }

    @Test
    public void pigClass() {
        assertNotNull(readClass("Pig"));
    }

    @Test
    public void wildPigClass() {
        assertNotNull(readClass("Wild Pig"));
    }

    @Test
    public void artiodactylClass() {
        assertNotNull(readClass("Artiodactyl"));
    }

    @Test
    public void razorbackClass() {
        assertNotNull(readClass("Razorback"));
    }

    @Test
    public void dogClass() {
        assertNotNull(readClass("Dog"));
    }

    @Test
    public void catClass() {
        assertNotNull(readClass("Cat"));
    }

    @Test
    public void birdClass() {
        assertNotNull(readClass("Bird"));
    }

    @Test
    public void petClass() {
        assertNotNull(readClass("Pet"));
    }

    @Test
    public void animalIsInterfaceClass() {
        assertTrue(readClass("Animal").isInterfaceClass());
    }

    @Test
    public void notAnimalIsAbstractClass() {
        assertFalse(readClass("Animal").isAbstractClass());
    }

    @Test
    public void notAnimalHasSuperclass() {
        assertThat(readClass("Animal").getSuperclasses().size(), is(equalTo(0)));
    }

    @Test
    public void animalHasSubclasses() {
        assertThat(readClass("Animal").getSubclasses().size(), is(equalTo(2)));
    }

    @Test
    public void pigHasSuperclassOne() {
        assertThat(readClass("Pig").getSuperclasses().size(), is(equalTo(1)));
    }

    @Test
    public void pigHasSubclassesTwo() {
        assertThat(readClass("Pig").getSubclasses().size(), is(equalTo(2)));
    }

    @Test
    public void pigHasSuperclassArtiodactyl() {
        assertNotNull(findClassName(readClass("Pig").getSuperclasses(), "Artiodactyl"));
    }

    @Test
    public void pigHasSubclassRazorback() {
        assertNotNull(findClassName(readClass("Pig").getSubclasses(), "Razorback"));
    }

    @Test
    public void pigHasSubclassWildpig() {
        assertNotNull(findClassName(readClass("Pig").getSubclasses(), "Wild Pig"));
    }

    @Test
    public void notpigHasSubclassArtiodactyl() {
        assertNull(findClassName(readClass("Pig").getSubclasses(), "Artiodactyl"));
    }

    @Test
    public void notPigIsAbstractClass() {
        assertFalse(readClass("Pig").isAbstractClass());
    }

    @Test
    public void notPigIsInterface() {
        assertFalse(readClass("Pig").isInterfaceClass());
    }

    @Test
    public void petHasSuperclassOne() {
        assertThat(readClass("Pet").getSuperclasses().size(), is(equalTo(1)));
    }

    @Test
    public void petHasSubclassesTwo() {
        assertThat(readClass("Pet").getSubclasses().size(), is(equalTo(2)));
    }

    @Test
    public void petHasSuperclassAnimal() {
        assertNotNull(findClassName(readClass("Pet").getSuperclasses(), "Animal"));
    }

    @Test
    public void petHasSubclassCat() {
        assertNotNull(findClassName(readClass("Pet").getSubclasses(), "Cat"));
    }

    @Test
    public void petHasSubclassDog() {
        assertNotNull(findClassName(readClass("Pet").getSubclasses(), "Dog"));
    }

    @Test
    public void notPetIsAbstractClass() {
        assertFalse(readClass("Pet").isAbstractClass());
    }

    @Test
    public void notPetIsInterface() {
        assertFalse(readClass("Pet").isInterfaceClass());
    }

    @Test
    public void notCatIsAbstractClass() {
        assertFalse(readClass("Cat").isAbstractClass());
    }

    @Test
    public void notCatIsInterface() {
        assertFalse(readClass("Cat").isInterfaceClass());
    }

    @Test
    public void notCatHasSubclasses() {
        assertThat(readClass("Cat").getSubclasses().size(), is(equalTo(0)));
    }

    @Test
    public void catHasSuperclassPet() {
        assertNotNull(findClassName(readClass("Cat").getSuperclasses(), "Pet"));
    }

    @Test
    public void notBirdIsAbstractClass() {
        assertFalse(readClass("Bird").isAbstractClass());
    }

    @Test
    public void notBirdIsInterface() {
        assertFalse(readClass("Bird").isInterfaceClass());
    }

    @Test
    public void notBirdHasSubclasses() {
        assertThat(readClass("Bird").getSubclasses().size(), is(equalTo(0)));
    }

    @Test
    public void notBirdHasSuperclasses() {
        assertThat(readClass("Bird").getSuperclasses().size(), is(equalTo(0)));
    }

    @Test
    public void notDogIsAbstractClass() {
        assertFalse(readClass("Dog").isAbstractClass());
    }

    @Test
    public void notDogIsInterface() {
        assertFalse(readClass("Dog").isInterfaceClass());
    }

    @Test
    public void notDogHasSubclasses() {
        assertThat(readClass("Dog").getSubclasses().size(), is(equalTo(0)));
    }

    @Test
    public void dogHasSuperclassPet() {
        assertNotNull(findClassName(readClass("Dog").getSuperclasses(), "Pet"));
    }

    @Test
    public void notWildpigIsAbstractClass() {
        assertFalse(readClass("Wild Pig").isAbstractClass());
    }

    @Test
    public void notWildpigIsInterface() {
        assertFalse(readClass("Wild Pig").isInterfaceClass());
    }

    @Test
    public void notWildpigHasSubclasses() {
        assertThat(readClass("Wild Pig").getSubclasses().size(), is(equalTo(0)));
    }

    @Test
    public void wildpigHasSuperclassPig() {
        assertNotNull(findClassName(readClass("Wild Pig").getSuperclasses(), "Pig"));
    }

    @Test
    public void notRazorbackIsAbstractClass() {
        assertFalse(readClass("Razorback").isAbstractClass());
    }

    @Test
    public void notRazorbackIsInterface() {
        assertFalse(readClass("Razorback").isInterfaceClass());
    }

    @Test
    public void notRazorbackHasSubclasses() {
        assertThat(readClass("Razorback").getSubclasses().size(), is(equalTo(0)));
    }

    @Test
    public void razorbackHasSuperclassPig() {
        assertNotNull(findClassName(readClass("Razorback").getSuperclasses(), "Pig"));
    }

    //
    @Test
    public void artiodactylIsAbstractClass() {
        assertTrue(readClass("Artiodactyl").isAbstractClass());
    }

    @Test
    public void notArtiodactylIsInterface() {
        assertFalse(readClass("Artiodactyl").isInterfaceClass());
    }

    @Test
    public void artiodactylHasSuperclassAnimal() {
        assertNotNull(findClassName(readClass("Artiodactyl").getSuperclasses(), "Animal"));
    }

    @Test
    public void artiodactylHasSubclassPig() {
        assertNotNull(findClassName(readClass("Artiodactyl").getSubclasses(), "Pig"));
    }

    @Test
    public void artiodactylHasSubclassOne() {
        assertThat(readClass("Artiodactyl").getSubclasses().size(), is(equalTo(1)));
    }

    @Test
    public void artiodactylHasSuperclassOne() {
        assertThat(readClass("Artiodactyl").getSuperclasses().size(), is(equalTo(1)));
    }

    @Test
    public void artiodactylHasComment() {
        List<MetaComment> comments = readClass("Artiodactyl").getComment();
        boolean x = false;
        for (MetaComment comment : comments) {
            if (comment.getText().contains("Cloven hoofed animals.")) {
                x = true;
                break;
            }
        }
        assertTrue(x);
    }

}
