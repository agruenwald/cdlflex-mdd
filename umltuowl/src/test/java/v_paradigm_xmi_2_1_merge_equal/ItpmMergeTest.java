package v_paradigm_xmi_2_1_merge_equal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.cdlflex.mdd.umltuowl.harmonizer.Harmonizer;
import org.cdlflex.mdd.umltuowl.harmonizer.MergeStrategyMergeClasses;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.junit.BeforeClass;
import org.junit.Test;

import testhelper.TestHelper;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>. Test cases for reference example models.
 * 
 */
public class ItpmMergeTest {
    private static MetaModel metaModel;

    /**
     * Load the meta model at the beginning.
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void loadMetaModel() throws Exception {
        metaModel =
            TestHelper.loadMetaModel(ITPMDefaultAbstractClass.CONVERTER_NAME, ITPMDefaultAbstractClass.ITPM_V6,
                    "itpm-example/settings-itpm.properties");
    }

    @Test
    public void testMergeStrategyMergeClasses() throws Exception {
        Harmonizer harmonizer = new Harmonizer();
        MetaModel hModel = harmonizer.harmonizeExplicitly(metaModel, new MergeStrategyMergeClasses(false));

        // MetaModel hModel = Main.harmonizeModel(metaModel);
        assertTrue(hModel.findClassByName("Report") != null);
        assertTrue(hModel.findClassByName("Report").findAttributeByName("loc") != null);
        assertTrue(hModel.findClassByName("Report").findAttributeByName("codeQuality") != null);
        assertTrue(hModel.findClassByName("Report").findAttributeByName("description") != null);
        assertTrue(hModel.findClassByName("Report").getAssociations().size() > 0);
        MetaAssociation reports = hModel.findClassByName("Report").getAssociations().iterator().next();
        assertTrue(hModel.findClassById(reports.getFrom().getMetaClassId()) != null);
        assertTrue(hModel.findClassById(reports.getTo().getMetaClassId()) != null);
        assertEquals(reports.getFrom().getMetaClassId(), reports.getFrom().getMetaClass().getId());
        assertEquals(reports.getTo().getMetaClassId(), reports.getTo().getMetaClass().getId());
        assertEquals(1, hModel.findAllClassesByName("Report").size());
        assertEquals(1, hModel.findAllClassesByName("Product").size());
        assertEquals(1, hModel.findAllClassesByName("SoftwareProduct").size());
        assertEquals(1, hModel.findAllClassesByName("Product").get(0).getSubclasses().size());
        assertEquals(1, hModel.findAllClassesByName("Product").get(0).getSuperclasses().size());
        assertTrue(hModel.findClassByName("Product").getSubclasses().iterator().next().getName()
                .equals("SoftwareProduct"));
        assertTrue(hModel.findClassByName("Product").getSuperclasses().iterator().next().getName().equals("Artifact"));

        assertEquals("Product", hModel.findClassByName("SoftwareProduct").getSuperclasses().iterator().next()
                .getName());

        assertEquals(hModel.findClassByName("Product").getId(), hModel.findClassByName("SoftwareProduct")
                .getSuperclasses().iterator().next().getId());
    }

    @Test
    public void testEnum() throws Exception {
        Harmonizer harmonizer = new Harmonizer();
        MetaModel hModel = harmonizer.harmonizeExplicitly(metaModel, new MergeStrategyMergeClasses(false));
        assertTrue(hModel.findClassByName("OutputFormat").isEnumClass());
    }

}
