package v_paradigm_xmi_2_1_merge_equal;

import v_paradigm_xmi_2_1.VParadigmDefaultAbstractClass;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>. Test cases for reference example models.
 * 
 */
public abstract class ITPMDefaultAbstractClass {
    protected static final String ITPM_V6 = "itpm-example/itpm-v6.uml";
    public static final String CONVERTER_NAME = VParadigmDefaultAbstractClass.CONVERTER_NAME;

}
