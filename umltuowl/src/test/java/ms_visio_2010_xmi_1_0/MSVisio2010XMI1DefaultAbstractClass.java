package ms_visio_2010_xmi_1_0;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>. Test cases for reference example models.
 * 
 */
public abstract class MSVisio2010XMI1DefaultAbstractClass {
    protected static final String SAMPLES_FILE = "ms_visio_2010_xmi_1_0/input-file.xmi";
    protected static final String CONVERTER_NAME = "MSVisio2010ConverterXMI";

}
