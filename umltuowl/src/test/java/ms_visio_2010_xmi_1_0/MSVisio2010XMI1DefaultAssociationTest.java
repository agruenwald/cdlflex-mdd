package ms_visio_2010_xmi_1_0;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import metamodel.DefaultAssociationTest;

import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.junit.BeforeClass;
import org.junit.Test;

import testhelper.TestHelper;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>. Test cases for reference example models.
 * 
 */
public class MSVisio2010XMI1DefaultAssociationTest extends DefaultAssociationTest {
    /**
     * Load the meta model at the beginning.
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void loadMetaModel() throws Exception {
        metaModel =
            TestHelper.loadMetaModel(MSVisio2010XMI1DefaultAbstractClass.CONVERTER_NAME,
                    MSVisio2010XMI1DefaultAbstractClass.SAMPLES_FILE);
    }

    @Test
    public void motherChildrenAssociation() {
        MetaAssociation a =
            metaModel.findPackageByName(PACKAGE_3).findClassByName("Mother").findAssociationByName("Children");
        assertThat(a.getTo().getRange().get(0), is(equalTo("1")));
        assertThat(a.getTo().getRange().get(1), is(equalTo("*")));
    }

    @Test
    public void childrenMotherAssociation() {
        MetaAssociation a =
            metaModel.findPackageByName(PACKAGE_3).findClassByName("Children").findAssociationByName("Mother");
        assertThat(a.getTo().getRange().get(0), is(equalTo("1")));
    }

    @Test
    public void childrenMotherAssociationName() {
        MetaAssociation a =
            metaModel.findPackageByName(PACKAGE_3).findClassByName("Children").findAssociationByName("Mother");
        assertThat(a.getName(), is(equalTo("hasMother")));
    }

    @Test
    public void motherChildrenAssociationName() {
        MetaAssociation a =
            metaModel.findPackageByName(PACKAGE_3).findClassByName("Mother").findAssociationByName("Children");
        assertThat(a.getName(), is(equalTo("isMotherOf")));
    }

}
