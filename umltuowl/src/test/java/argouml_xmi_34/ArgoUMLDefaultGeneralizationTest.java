package argouml_xmi_34;

import metamodel.DefaultGeneralizationTest;

import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.junit.BeforeClass;

import testhelper.TestHelper;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>. Test cases for reference example models.
 * 
 */
public class ArgoUMLDefaultGeneralizationTest extends DefaultGeneralizationTest {

    /**
     * ArgoUML does not support different packages (for XMI export).
     */
    @Override
    protected MetaClass readClass(String classname) {
        return metaModel.findPackageByName("").findClassByName(classname);
    }

    /**
     * Load the meta model at the beginning.
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void loadMetaModel() throws Exception {
        metaModel =
            TestHelper.loadMetaModel(ArgoUMLDefaultAbstractClass.CONVERTER_NAME,
                    ArgoUMLDefaultAbstractClass.ARGOUML_SAMPLES);
    }

}
