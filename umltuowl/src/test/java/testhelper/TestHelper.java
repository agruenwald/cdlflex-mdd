package testhelper;

import java.net.URL;
import org.cdlflex.mdd.umltuowl.main.Main;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>. Contains helper classes to ease testing.
 * 
 */
public class TestHelper {
    private static final Logger logger = LoggerFactory.getLogger(TestHelper.class);

    public static MetaModel loadMetaModel(String converter, String modelFilename) throws Exception {
        return loadMetaModel(converter, modelFilename, "");
    }

    /**
     * Load a meta model of a specific file and return it.
     * 
     * @param converter name of the XMI/XML converter.
     * @param modelFilename the name of the filename that contains the model. That file must be located in the
     *        reference-models folder. Example for parameter: visual-paradigm-samples.xmi
     * @param optional path to settings file (within reference-models/)
     * @return meta model if creation succeed.
     * @throws Exception is thrown if creation failed somehow.
     */
    public static MetaModel loadMetaModel(String converter, String modelFilename, String settingsPath)
        throws Exception {
        logger.info("Called loadMetaModel()");
        URL inputUrl = ClassLoader.getSystemResource("reference-models/" + modelFilename);
        if (inputUrl == null) {
            logger.error(String.format("File %s not found.", modelFilename));
            return null;
        }
        logger.debug(String.format("Located input file at %s.", inputUrl.getPath()));
        if (settingsPath.isEmpty()) {
            MetaModel metaModel =
                Main.loadMetaModel(new String[] { "input=" + inputUrl.getPath(),
                    "IRI=http://www.tuwien.ac.at/owltest/", "converter=" + converter, "output=dummy" });
            return metaModel;
        } else {
            URL settingsUrl = ClassLoader.getSystemResource("reference-models/" + settingsPath);
            if (settingsUrl == null) {
                logger.error(String.format("File %s not found.", settingsPath));
                return null;
            }

            MetaModel metaModel =
                Main.loadMetaModel(new String[] { "input=" + inputUrl.getPath(),
                    "IRI=http://www.tuwien.ac.at/owltest/", "converter=" + converter,
                    "settings=" + settingsUrl.getPath(), "output=dummy" });
            return metaModel;
        }
    }
}
