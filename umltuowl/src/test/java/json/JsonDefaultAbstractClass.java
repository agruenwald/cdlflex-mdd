package json;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>. Test cases for reference example models.
 * 
 */
public abstract class JsonDefaultAbstractClass {
    protected static final String VISUAL_PARADIGM_SAMPLES = "v_paradigm_xmi_2_1/input-file.xmi";
    public static final String CONVERTER_NAME = v_paradigm_xmi_2_1.VParadigmDefaultAbstractClass.CONVERTER_NAME;

}
