package json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.cdlflex.mdd.umltuowl.harmonizer.Harmonizer;
import org.cdlflex.mdd.umltuowl.harmonizer.MergeStrategyMergeClasses;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAssociation;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.JsonMaker;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.OWLMaker;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.semanticweb.owlapi.model.OWLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import testhelper.TestHelper;

public class JsonTest {
    private static final Logger logger = LoggerFactory.getLogger(JsonTest.class);

    private static MetaModel metamodel = null;

    private String getTestFolder() {
        String url = ClassLoader.getSystemResource("reference-models/").getPath();
        url = url.replace("/bin/reference-models/", "/test/reference-models/json/");
        if (!url.contains("json")) {
            url += "json/";
        }
        return url;
    }

    private String readFileContent(String filePath) throws IOException {
        FileInputStream stream = new FileInputStream(new File(filePath));
        try {
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            /* Instead of using default, pass in a decoder. */
            return Charset.defaultCharset().decode(bb).toString();
        } finally {
            stream.close();
        }
    }

    @Before
    public void setUp() throws Exception {
        metamodel =
            TestHelper.loadMetaModel(v_paradigm_xmi_2_1.VParadigmDefaultAbstractClass.CONVERTER_NAME,
                    "itpm-example/itpm.uml", "itpm-example/settings-itpm.properties");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testToJsonRun() {
        String res = JsonMaker.toJson(metamodel);
        assertFalse(res.isEmpty());
    }

    @Test
    public void testToMetaModelRun() {
        String json = JsonMaker.toJson(metamodel);
        MetaModel mm = JsonMaker.toMetaModel(json);
        assertTrue(mm.getPackages().size() > 0);
    }

    @Test
    public void testToFile() throws IOException {
        String url = getTestFolder() + "output.json";
        JsonMaker.toJsonFile(metamodel, url);
        assertTrue(true);
    }

    @Test
    public void testRoundTrip() {
        String json = JsonMaker.toJson(metamodel);
        MetaModel m2 = JsonMaker.toMetaModel(json);
        assertTrue(m2 != null);
    }

    @Test
    public void testBackToMetaModelWeb1() throws IOException {
        String url = getTestFolder() + "web_diagram_itpm1.json";
        String json = this.readFileContent(url);
        MetaModel mm = JsonMaker.toMetaModel(json);
        assertTrue(mm.getPackages().size() > 0);
        assertTrue(mm.getPackages().iterator().next().getClasses().size() > 5);
    }

    @Test
    public void testBackToMetaModelWeb2() throws IOException {
        String url = getTestFolder() + "web_diagram_itpm2.json";
        String json = this.readFileContent(url);
        MetaModel mm = JsonMaker.toMetaModel(json);
        assertTrue(mm.getPackages().size() > 0);
        assertTrue(mm.getAllClasses().size() > 20);
    }

    @Test
    public void testHarmonizingModelWeb2() throws IOException {
        String url = getTestFolder() + "web_diagram_itpm2.json";
        String json = this.readFileContent(url);
        MetaModel mm = JsonMaker.toMetaModel(json);
        mm = new Harmonizer().harmonizeExplicitly(mm, new MergeStrategyMergeClasses(false));
        assertTrue(mm.getPackages().size() > 0);
        // assertTrue(mm.getPackages().iterator().next().getClasses().size()>5);
        assertTrue(mm.getAllClasses().size() > 20);
        assertTrue(mm.findClassByName("JiraIssue") != null);

        // test modification of name with forward propagation
        MetaClass sm = mm.findClassByName("Scope Management");
        assertTrue(sm != null);

        MetaClass ka = mm.findClassByName("KnowledgeArea");
        assertTrue(ka != null);

        sm.setName("ScopeManagementXXX");
        Iterator<MetaClass> it = ka.getSubclasses().iterator();
        boolean found = false;
        while (it.hasNext()) {
            MetaClass sub = it.next();
            if (sub.getId().equals(sm.getId())) {
                found = true;
                assertEquals(sub.getName(), (sm.getName()));
                assertEquals("ScopeManagementXXX", sub.getName());
                break;
            }
        }
        assertTrue(found);
    }

    @Test
    public void testBasicSort() throws IOException {
        MetaModel mm = new MetaModel();
        mm.getPackages().add(new MetaPackage("ZZZ"));
        mm.getPackages().add(new MetaPackage("AAA"));
        mm.getPackages().add(new MetaPackage("GGG"));
        List<MetaPackage> packageList = new LinkedList<MetaPackage>();
        packageList.addAll(mm.getPackages());
        assertEquals("AAA", packageList.get(0).getName());
        assertEquals("GGG", packageList.get(1).getName());
        assertEquals("ZZZ", packageList.get(2).getName());
    }

    @Test
    public void testBasicSort2() throws IOException {
        MetaModel mm = new MetaModel();
        mm.getPackages().add(new MetaPackage("IT-PM V5"));
        mm.getPackages().add(new MetaPackage("Issue Domain"));
        mm.getPackages().add(new MetaPackage("Artifact Domain"));
        mm.getPackages().add(new MetaPackage("DataSourceConfiguration"));
        mm.getPackages().add(new MetaPackage("Jira Issues"));
        mm.getPackages().add(new MetaPackage("Rule Structure"));
        mm.getPackages().add(new MetaPackage("Knowledge Base"));

        List<MetaPackage> packageList = new LinkedList<MetaPackage>();
        packageList.addAll(mm.getPackages());
        assertEquals("Artifact Domain", packageList.get(0).getName());
        assertEquals("DataSourceConfiguration", packageList.get(1).getName());
        assertEquals("Issue Domain", packageList.get(2).getName());
        assertEquals("IT-PM V5", packageList.get(3).getName());
        assertEquals("Jira Issues", packageList.get(4).getName());
        assertEquals("Knowledge Base", packageList.get(5).getName());
        assertEquals("Rule Structure", packageList.get(6).getName());
    }

    @Test
    public void testBasicSort2NameSetAfterwards() throws IOException {
        MetaModel mm = new MetaModel();

        MetaPackage itpm = new MetaPackage("IT-PM V5");
        MetaPackage issueDomain = new MetaPackage();
        MetaPackage artifactDomain = new MetaPackage();
        MetaPackage dataSourceConfiguration = new MetaPackage();
        MetaPackage jiraIssues = new MetaPackage();
        MetaPackage ruleStructure = new MetaPackage();
        MetaPackage knowledgeBase = new MetaPackage();

        mm.getPackages().add(itpm);
        mm.getPackages().add(issueDomain);
        mm.getPackages().add(artifactDomain);
        mm.getPackages().add(dataSourceConfiguration);
        mm.getPackages().add(jiraIssues);
        mm.getPackages().add(ruleStructure);
        mm.getPackages().add(knowledgeBase);

        issueDomain.setName("Issue Domain");
        dataSourceConfiguration.setName("DataSourceConfiguration");
        artifactDomain.setName("Artifact Domain");
        dataSourceConfiguration.setName("DataSourceConfiguration");
        jiraIssues.setName("Jira Issues");
        knowledgeBase.setName("Knowledge Base");
        ruleStructure.setName("Rule Structure");

        mm.sort();

        List<MetaPackage> packageList = new LinkedList<MetaPackage>();
        packageList.addAll(mm.getPackages());

        assertEquals("Artifact Domain", packageList.get(0).getName());
        assertEquals("DataSourceConfiguration", packageList.get(1).getName());
        assertEquals("Issue Domain", packageList.get(2).getName());
        assertEquals("IT-PM V5", packageList.get(3).getName());
        assertEquals("Jira Issues", packageList.get(4).getName());
        assertEquals("Knowledge Base", packageList.get(5).getName());
        assertEquals("Rule Structure", packageList.get(6).getName());
    }

    @Test
    public void testCorrectlySortedMetaModel() throws IOException {
        String url = getTestFolder() + "web_diagram_itpm3.json";
        String json = this.readFileContent(url);
        MetaModel mm = JsonMaker.toMetaModel(json);
        List<MetaPackage> packageList = new LinkedList<MetaPackage>();

        packageList.addAll(mm.getPackages());

        assertEquals("Artifact Domain", packageList.get(0).getName());
        assertEquals("DataSourceConfiguration", packageList.get(1).getName());
        assertEquals("Issue Domain", packageList.get(2).getName());
        assertEquals("IT-PM V5", packageList.get(3).getName());
        assertEquals("Jira Issues", packageList.get(4).getName());
        assertEquals("Knowledge Base", packageList.get(5).getName());
        assertEquals("Rule Structure", packageList.get(6).getName());
    }

    @Test
    public void testJsonReferenceClassInAssociation() throws IOException {
        String url = getTestFolder() + "web_diagram_itpm3.json";
        String json = this.readFileContent(url);
        MetaModel mm = JsonMaker.toMetaModel(json);

        MetaClass jiraIssueClass = mm.findClassById("box-55191");
        List<MetaAssociation> aList = mm.findAssociationsAssociatedWithClass(jiraIssueClass);

        MetaAssociation reporter = null;
        for (MetaAssociation ma : aList) {
            if (ma.getTo().getMetaClassId().equals(jiraIssueClass)) {
                reporter = ma;
                break;
            }
        }

        if (reporter != null) {
            int prevSize = reporter.getTo().getMetaClass().getAssociations().size();
            assertEquals(jiraIssueClass.getAssociations().size(), prevSize);
            reporter.getTo().getMetaClass().removeAssociation(reporter);
            assertEquals(reporter.getTo().getMetaClass().getAssociations().size(), prevSize - 1);
            assertEquals(reporter.getTo().getMetaClass().getAssociations().size(), jiraIssueClass.getAssociations()
                    .size());
        }
    }

    @Test
    public void testHarmonizeMerge3AssociationTestWithEnums() throws IOException {
        String url = getTestFolder() + "web_diagram_itpm3.json";
        String json = this.readFileContent(url);
        MetaModel mm = JsonMaker.toMetaModel(json);
        mm = new Harmonizer().harmonizeExplicitly(mm, new MergeStrategyMergeClasses(false));
        List<MetaAssociation> mas = mm.getAllAssociations();
        for (MetaAssociation ma : mas) {
            String fromId = ma.getFrom().getMetaClassId();
            String toId = ma.getTo().getMetaClassId();
            MetaClass fromC = mm.findClassById(fromId);
            MetaClass toC = mm.findClassById(toId);
            if (fromC == null || toC == null) {
                logger.info(String.format("Invalid association: %s (fromId=%s, toId=%s). ", ma.getName(), fromId,
                        toId));
                logger.info(String.format("fromC=%s", fromC == null ? "null" : fromC));
                logger.info(String.format("toC=%s", toC == null ? "null" : toC));
            }
            assertTrue(fromC != null);
            assertTrue(toC != null);
        }

        MetaClass dsf = mm.findClassByName("DataSourceField");
        assertEquals(3, dsf.getSubclasses().size());
        assertEquals(1, mm.findClassByName("SimpleDataField").getSuperclasses().size());
        assertEquals(1, mm.findClassByName("RowNumberField").getSuperclasses().size());
        assertEquals(1, mm.findClassByName("ReferenceField").getSuperclasses().size());
    }

    @Test
    public void testJsonAndOwlMergeStrategyRangeCastException() throws IOException, OWLException {
        String url = getTestFolder() + "web_diagram_itpm4.json";
        String json = this.readFileContent(url);
        MetaModel mm = JsonMaker.toMetaModel(json);

        // test ranges as in OWLMaker
        List<MetaAssociation> mas = mm.getAllAssociations();
        for (MetaAssociation ma : mas) {
            List<String> range = ma.getFrom().getRange();
            if (range.size() == 0 || range.get(0).contains("*")
                || (range.size() == 2 && range.get(0).isEmpty() && range.get(1).equals("*"))) {

            }

            range = ma.getTo().getRange();
            if (range.size() == 0 || range.get(0).contains("*")
                || (range.size() == 2 && range.get(0).isEmpty() && range.get(1).equals("*"))) {

            }
        }

        // ensure that every association only exists once
        mas = mm.getAllAssociations();
        for (MetaAssociation ma : mas) {
            for (MetaAssociation dupl : mm.findAssociationById(ma.getId())) {
                logger.info(String.format(
                        "Dupl association: %s-%s (from metaClassId=%s, to metaClassId=%s, hashCode=%s).",
                        dupl.getId(), dupl, dupl.getFrom().getMetaClassId(), dupl.getTo().getMetaClassId(),
                        dupl.hashCode()));
            }
            assertEquals(1, (mm.findAssociationById(ma.getId())).size());
        }

        mm = new Harmonizer().harmonizeExplicitly(mm, new MergeStrategyMergeClasses(true));
        assertEquals(1, mm.getPackages().size());
        File temp = File.createTempFile("owl-tmp", ".tmp");
        logger.info(String.format("Move OWL file to %s.", temp));
        OWLMaker.toOwl(temp.getAbsolutePath(), "http://unknown.iri/", mm.getPackages().iterator().next());
    }

    @Test
    public void testValidPackageName() throws IOException, OWLException {
        String url = getTestFolder() + "web_diagram_itpm4.json";
        String json = this.readFileContent(url);
        MetaModel mm = JsonMaker.toMetaModel(json);

        mm = new Harmonizer().harmonizeExplicitly(mm, new MergeStrategyMergeClasses(true));

        assertEquals(1, mm.getPackages().size());
        assertTrue(mm.getPackages().iterator().next().getName().length() > 1);
    }

    @Test
    public void testSortedAttributes() throws IOException, OWLException {
        String url = getTestFolder() + "web_diagram_itpm5.json";
        String json = this.readFileContent(url);
        MetaModel mm = JsonMaker.toMetaModel(json);

        MetaClass backlogItem = mm.findClassByName("BacklogItem");
        MetaClass sprintBacklog = mm.findClassByName("sprintBacklog");
        assertTrue(backlogItem != null);
        assertTrue(sprintBacklog != null);
        assertEquals("id", backlogItem.getAttributes().get(0).getName());
        assertEquals("name", backlogItem.getAttributes().get(1).getName());

        assertEquals("id", sprintBacklog.getAttributes().get(0).getName());
        assertEquals("name", sprintBacklog.getAttributes().get(1).getName());
        assertEquals("dueDate", sprintBacklog.getAttributes().get(2).getName());
        assertEquals("startDate", sprintBacklog.getAttributes().get(3).getName());
    }

    @Test
    public void testJsonNullPointers() throws IOException, OWLException {
        String url = getTestFolder() + "web_diagram_itpm4.json";
        String json = this.readFileContent(url);
        MetaModel mm = JsonMaker.toMetaModel(json);

        mm = new Harmonizer().harmonizeExplicitly(mm, new MergeStrategyMergeClasses(true));

        // test ranges as in OWLMaker
        List<MetaAssociation> mas = mm.getAllAssociations();
        for (MetaAssociation ma : mas) {
            assertTrue(ma.getFrom().getRange() != null);
            assertTrue(ma.getTo().getRange() != null);
            assertTrue(ma.getFrom().getMetaClass() != null);
            assertTrue(ma.getTo().getMetaClass() != null);
            assertTrue(ma.getTo().getMetaClass().getName() != null);
        }

        List<MetaClass> classes = mm.getAllClasses();
        for (MetaClass m : classes) {
            assertTrue(m.getSubclasses() != null);
            assertTrue(m.getSuperclasses() != null);
            for (MetaClass sub : m.getSubclasses()) {
                assertTrue(sub.getName().length() > 0);
            }
            for (MetaClass sup : m.getSuperclasses()) {
                assertTrue(sup.getName().length() > 0);
            }
        }

        MetaClass issue = mm.findClassByName("Issue");

        List<MetaClass> issueClasses = mm.findAllClassesByName("Issue");
        assertEquals(1, issueClasses.size());

        for (MetaAssociation mai : issue.getAssociations()) {
            logger.info(String.format("Issue: %s", mai + "- " + mai.getId()));
        }

        assertEquals(5, issue.getAssociations().size());
        List<String> anames = new LinkedList<String>();
        for (MetaAssociation a : issue.getAssociations()) {
            anames.add(a.getName());
        }
        assertTrue(anames.contains("issueType"));
        assertTrue(anames.contains("approvedInto"));

        assertTrue(anames.contains("epicOrStory"));

        MetaAssociation issueType = issue.getAssociations().get(anames.indexOf("issueType"));
        assertTrue(issueType != null);
        assertEquals(0, issueType.getFrom().getRange().size());
        assertEquals(1, issueType.getTo().getRange().size());
        assertTrue(mm.findClassById(issueType.getFrom().getMetaClass().getId()) != null);
        assertTrue(mm.findClassById(issueType.getTo().getMetaClass().getId()) != null);

        assertEquals(0, issue.getSubclasses().size());
        assertEquals(1, issue.getSuperclasses().size());
        assertEquals("BacklogItem", issue.getSuperclasses().iterator().next().getName());

        MetaClass artifact = mm.findClassByName("Artifact");
        anames = new LinkedList<String>();
        for (MetaAssociation a : artifact.getAssociations()) {
            anames.add(a.getName());
        }
        MetaAssociation produceArtifact = artifact.getAssociations().get(anames.indexOf("produceArtifact"));
        assertTrue(produceArtifact != null);
        assertEquals("Requirement", produceArtifact.getTo().getMetaClass().getName());
        MetaClass req = produceArtifact.getTo().getMetaClass();
        for (MetaClass sub : req.getSubclasses()) {
            assertTrue(sub != null);
        }
    }
}
