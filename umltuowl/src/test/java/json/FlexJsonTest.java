package json;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.JsonMaker;
import org.junit.Test;

public class FlexJsonTest {
    public static final String JAVASCRIPT_JSON_EXAMPLE = "2013-09-13T05:31:52.305Z";
    public static final String DATE_ANOTHER_EXAMPLE = "2013-09-05";

    @Test
    public void testRegex() {
        String test = "fsafsfsdfs df \"createdAt\":23423423423423434, \"next\": sfsdf";
        test = test.replaceFirst("(\"createdAt\":)\\d{5,15}", "$1" + JAVASCRIPT_JSON_EXAMPLE);
        assertTrue(test.contains("createdAt"));
        assertTrue(test.contains(JAVASCRIPT_JSON_EXAMPLE));
        assertTrue(test.contains("\"createdAt\":" + JAVASCRIPT_JSON_EXAMPLE));
    }

    @Test
    public void testDateSerialization1() {
        MetaModel mm = new MetaModel("test");
        MetaPackage p = new MetaPackage("");
        MetaComment comment1 = new MetaComment("comment1", true);
        p.getComment().add(comment1);
        mm.getPackages().add(p);

        String json = JsonMaker.toJson(mm);
        json = json.replaceFirst("(\"createdAt\":)\\d{5,15}", "$1" + String.format("\"%s\"", DATE_ANOTHER_EXAMPLE));
        mm = JsonMaker.toMetaModel(json);
        assertTrue(mm.getPackages().iterator().next().getComment().get(0).getCreatedAt() != null);
    }

    @Test
    public void testDateSerializationJS() {
        MetaModel mm = new MetaModel("test");
        MetaPackage p = new MetaPackage("");
        MetaComment comment1 = new MetaComment("comment1", true);
        p.getComment().add(comment1);
        mm.getPackages().add(p);

        String json = JsonMaker.toJson(mm);
        json =
            json.replaceFirst("(\"createdAt\":)\\d{5,15}", "$1" + String.format("\"%s\"", JAVASCRIPT_JSON_EXAMPLE));
        mm = JsonMaker.toMetaModel(json);
        assertTrue(mm.getPackages().iterator().next().getComment().get(0).getCreatedAt() != null);
    }

    @Test
    public void testDateSerializationJSReal() {
        Date d = new Date();
        MetaModel mm = new MetaModel("test");
        MetaPackage p = new MetaPackage("");
        MetaComment comment1 = new MetaComment("comment1", true);
        p.getComment().add(comment1);
        mm.getPackages().add(p);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
        String json = JsonMaker.toJson(mm);
        json = json.replaceFirst("(\"createdAt\":)\\d{5,15}", "$1" + String.format("\"%s\"", df.format(d))); // 2013-09-13T05:31:52.305Z
        mm = JsonMaker.toMetaModel(json);

        Date dNew = mm.getPackages().iterator().next().getComment().get(0).getCreatedAt();
        assertTrue(Math.floor(dNew.getTime() / 1000) == Math.floor(d.getTime() / 1000));
    }

    @Test
    public void testDateSerializationDateMS() {
        Date d = new Date();
        MetaModel mm = new MetaModel("test");
        MetaPackage p = new MetaPackage("");
        MetaComment comment1 = new MetaComment("comment1", true);
        p.getComment().add(comment1);
        mm.getPackages().add(p);

        String json = JsonMaker.toJson(mm);
        json = json.replaceFirst("(\"createdAt\":)\\d{5,15}", "$1" + String.format("\"%s\"", d.getTime()));
        mm = JsonMaker.toMetaModel(json);

        Date dx = mm.getPackages().iterator().next().getComment().get(0).getCreatedAt();
        assertTrue(dx.equals(d));
    }

    @Test
    public void testJS2() {
        String s = JAVASCRIPT_JSON_EXAMPLE + "dd";

        MetaModel mm = new MetaModel("test");
        MetaPackage p = new MetaPackage("");
        MetaComment comment1 = new MetaComment("comment1", true);
        p.getComment().add(comment1);
        mm.getPackages().add(p);

        String DF3_TMPL = "yyyy-MM-dd'T'HH:mm:ss";
        String s2 = s.substring(0, Math.min(s.length() - 1, DF3_TMPL.length() - 1));
        String json = JsonMaker.toJson(mm);
        json = json.replaceFirst("(\"createdAt\":)\\d{5,15}", "$1" + String.format("\"%s\"", s2));
        mm = JsonMaker.toMetaModel(json);

        Date dx = mm.getPackages().iterator().next().getComment().get(0).getCreatedAt();
        Calendar cal = new GregorianCalendar();
        cal.setTime(dx);
        assertTrue(cal.get(Calendar.SECOND) > 0);
    }
}
