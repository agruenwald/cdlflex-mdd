package json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import org.cdlflex.mdd.umltuowl.harmonizer.PruneStrategySemanticEditor;
import org.cdlflex.mdd.umltuowl.metamodel.MetaAttribute;
import org.cdlflex.mdd.umltuowl.metamodel.MetaClass;
import org.cdlflex.mdd.umltuowl.metamodel.MetaComment;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodel.MetaPackage;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.JsonMaker;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PrunningTest {
    private static MetaModel mm = null;

    private String getTestFolder() {
        String url = ClassLoader.getSystemResource("reference-models/").getPath();
        url = url.replace("/bin/reference-models/", "/test/reference-models/json/");
        if (!url.contains("json")) {
            url += "json/";
        }
        return url;
    }

    private String readFileContent(String filePath) throws IOException {
        FileInputStream stream = new FileInputStream(new File(filePath));
        try {
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            /* Instead of using default, pass in a decoder. */
            return Charset.defaultCharset().decode(bb).toString();
        } finally {
            stream.close();
        }
    }

    @Before
    public void setUp() throws Exception {
        String url = getTestFolder() + "pruningtest.json";
        String json = this.readFileContent(url);
        mm = JsonMaker.toMetaModel(json);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testPruneAttribute() throws IOException {
        assertEquals(3, mm.getPackages().size());
        MetaPackage p = mm.findPackageByName("Diagram #1");
        assertEquals(5, p.getClasses().size());
        MetaClass class3 = p.findClassByName("Class3");
        assertTrue(class3 != null);
        assertTrue(class3.findAttributeByName("pruneMe") != null);

        mm = new PruneStrategySemanticEditor().applyStrategy(mm);

        p = mm.findPackageByName("Diagram #1");
        class3 = p.findClassByName("Class3");
        assertTrue(class3 != null);
        assertTrue(class3.findAttributeByName("pruneMe") == null);
    }

    @Test
    public void testPruneClass() throws IOException {
        assertEquals(3, mm.getPackages().size());
        MetaPackage p = mm.findPackageByName("Diagram #1");
        assertEquals(5, p.getClasses().size());
        MetaClass classToPrune = p.findClassByName("ClassToPrune");
        assertTrue(classToPrune != null);

        mm = new PruneStrategySemanticEditor().applyStrategy(mm);

        p = mm.findPackageByName("Diagram #1");
        classToPrune = p.findClassByName("ClassToPrune");
        assertTrue(classToPrune == null);
        assertEquals(0, p.findClassByName("Class").getSuperclasses().size());
        assertEquals(0, p.findClassByName("Class3").getSubclasses().size());
        assertEquals(0, p.findClassByName("Class").getAssociations().size());
    }

    @Test
    public void testPrunePackage() throws IOException {
        assertEquals(3, mm.getPackages().size());
        assertTrue(mm.findPackageByName("s2") != null);
        mm = new PruneStrategySemanticEditor().applyStrategy(mm);
        assertEquals(2, mm.getPackages().size());
        assertTrue(mm.findPackageByName("s2") == null);
    }

    @Test
    public void testCheckConstraintAvailable() throws IOException {
        assertEquals(3, mm.getPackages().size());
        MetaPackage p = mm.findPackageByName("Constraints and Mulitplicity");
        assertTrue(p != null);
        MetaAttribute name = p.findClassByName("Person").findAttributeByName("name");
        MetaComment c = name.getCommentByType(MetaComment.TYPE_CONSTRAINT).get(0);
        assertTrue(c.getText().startsWith("pattern"));
    }

    @Test
    public void testSerializeMetaModelComment() {
        MetaModel mm = new MetaModel();
        mm.getComment().add(new MetaComment("this is awesome."));
        String json = JsonMaker.toJson(mm);
        assertTrue(json.contains("this is awesome."));
    }
}
