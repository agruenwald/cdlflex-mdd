package json;

import metamodel.DefaultGeneralizationTest;

import org.junit.BeforeClass;

import testhelper.TestHelper;

/**
 * 
 * @author Andreas Gruenwald <a.gruenw@gmail.com>. Test cases for reference example models.
 * 
 */
public class JsonDefaultGeneralizationTest extends DefaultGeneralizationTest {
    /**
     * Load the meta model at the beginning.
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void loadMetaModel() throws Exception {
        metaModel =
            TestHelper.loadMetaModel(JsonDefaultAbstractClass.CONVERTER_NAME,
                    JsonDefaultAbstractClass.VISUAL_PARADIGM_SAMPLES);
    }

}
