package json;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.JsonMaker;
import org.junit.After;
import org.junit.Test;

/**
 * umlTUowl enables the re-usage of classes throughout multiple packages. For instance, a concept named "Project" may
 * defined in the first package, containing all relevant attribute names, such as "id", "name", "startDate". The same
 * concept may be reused in another package. The id is different, the concepts are merged automatically via their name.
 * Because the merging process must consider the proper merging of associations, generalizations, etc. it is not as
 * simple as one may think. Hence some additional tests are provided within this class. These tests are applied on a
 * model with the maximum redundancy (two packages with identical content).
 */
public class JsonDupModelTest {
    private String getTestFolder() {
        String url = ClassLoader.getSystemResource("reference-models/").getPath();
        url = url.replace("/bin/reference-models/", "/test/reference-models/json/");
        if (!url.contains("json")) {
            url += "json/";
        }
        return url;
    }

    private String readFileContent(String filePath) throws IOException {
        FileInputStream stream = new FileInputStream(new File(filePath));
        try {
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            /* Instead of using default, pass in a decoder. */
            return Charset.defaultCharset().decode(bb).toString();
        } finally {
            stream.close();
        }
    }

    @Test
    public void testDuplJsonModelToModel() throws IOException {
        String url = getTestFolder() + "itpm_dupl.json";
        MetaModel mm = JsonMaker.toMetaModel(readFileContent(url));
        assertTrue(mm != null);
    }

    @After
    public void tearDown() throws Exception {
    }

}
