//CHECKSTYLE:OFF The complexity of this class slightly exceeds the checkstyle limits.
//However, splitting the class into more (smaller) classes would result in inappropriate manual
//coding and testing efforts.
package org.cdlflex.mdd.sembase.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.cdlflex.mdd.sembase.MDDException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The config loader eases the access to global project files based on a common home directory. The home-directory is
 * determined from a single file called "base-config.properties". This file contains a number of property values and
 * patterns how to determine the correct home directory. Within the home-directory, all configuration files are located.
 * Because in some environments no access is available to the sembase-config-file, also the karaf.home Java system
 * variable as well as SEMBASE_HOME (OS environment variable) are accessed when reading the home directory (fallback).
 * The access and the home-directory may vary depending on the environment (e.g. live or home). Patterns are supported
 * which enable the use of system variables. For instance {system.property.karaf.home} retrieves the karaf home path in
 * an Apache Karaf environment. {system.property.user.home} retrieves the users OS home directory (e.g.
 * /User/andreas_gruenwald/ on a Mac OS).
 */

public class ConfigLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigLoader.class);
    private static final String KARAF_HOME = "karaf.home"; // Java system variable (however also env will be tested
                                                           // against this name)
    private static final String SEMBASE_HOME = "SEMBASE_HOME"; // environment variable (however also Java sys will be
                                                               // tested against this name)

    private static final Pattern SYSTEM_PROPERTY_REGEX = Pattern.compile(
            "\\{system\\.property\\.([a-z\\_\\.]{1,100})\\}", Pattern.CASE_INSENSITIVE);
    private static final Pattern SYSTEM_ENV_REGEX = Pattern.compile("\\{system\\.env\\.([a-z\\_\\.]{1,100})\\}",
            Pattern.CASE_INSENSITIVE);
    private static final Pattern SEMBASE_HOME_DIR_REGEX = Pattern.compile("(\\{home\\.dir\\})",
            Pattern.CASE_INSENSITIVE);
    private static final Pattern TESTHOME_DIR_REGEX = Pattern.compile("(\\{test\\.dir\\})", Pattern.CASE_INSENSITIVE);

    private static final String CLASS_PATH = "{projectPath}";
    public static final String CONFIG_PATH = "{configPath}"; /*
                                                              * prefix a filename with this to ensure that first the
                                                              * manual passed constructor path is checked before
                                                              * continuing with "filename without {configPath}"
                                                              */
    public static final String BASE_CONFIG_SIMPLE = "base-config.properties";
    public static final String BASE_CONFIG = CLASS_PATH + BASE_CONFIG_SIMPLE; // the one and only base configuration: i
    public static final String HOME_DIR_KEY = "home-dir";
    public static final String ENV_BASE_CONFIG = "sembase/" + BASE_CONFIG_SIMPLE; // for Karaf and sembase variables
    public static final String TESTHOME_DIR_KEY = "test-dir";

    private static String curHomeDir = ""; // global accessible should be ok. Makes switching between environments
                                           // impossible within one run but that should not be intended anyway.
    private final boolean test;

    private final String configurationDir;
    private String baseConfigHomeDir; /*
                                       * alternative location of base-config home directory (sometimes base-config
                                       * location is unknown)
                                       */

    public ConfigLoader(boolean isTest) {
        this(isTest, "");
    }

    public ConfigLoader(boolean isTest, String baseConfigHomeDir) {
        this(isTest, baseConfigHomeDir, baseConfigHomeDir);
        this.baseConfigHomeDir = configurationDir;
    }

    /**
     * More complex constructor for test environment.
     * 
     * @param configurationDir either empty or set to the projectPath, where the configuration resides.
     */
    public ConfigLoader(boolean isTest, String baseConfigHomeDir, String configurationDir) {
        this.baseConfigHomeDir = baseConfigHomeDir;
        this.configurationDir =
            configurationDir.endsWith(File.separator) ? configurationDir : configurationDir + File.separator;
        this.test = isTest;
    }

    private String addProtocol(String s) {
        if (s.startsWith("file://")) {
            return s;
        } else {
            return "file://" + s;
        }
    }

    /**
     * get the home directory, which is the base for all the other configuration files and is provided as a path in the
     * file system which can be used and refered to reliably. Depending on the environment the home directory will be
     * identified differently. In any case it will be tried to locate the configuration file either in (not strictly
     * ordered) a) one of the given configuration paths (optional) b) in the classpath of the projects c) in the
     * karaf.home directory.
     * 
     * @return the name of the home directory (absolute path).
     * @throws MDDException is thrown if the access to the home directory failed.
     */
    public String getHomeDir() throws MDDException {
        if (!curHomeDir.isEmpty()) {
            return curHomeDir; // cache
        }
        List<String> lookedPaths = new ArrayList<String>();
        Properties prop = null;
        if (!this.baseConfigHomeDir.equals(File.separator)) {
            String propPath = trailSlash(this.regexReplacements(baseConfigHomeDir, false)) + BASE_CONFIG_SIMPLE;
            LOGGER.info(String.format("Look for file in baseConfigHomeDir %s.", baseConfigHomeDir));
            lookedPaths.add(propPath);
            prop = new Properties();
            try {
                prop.load(new URL(addProtocol(propPath)).openStream());
            } catch (IOException | NullPointerException e) {
                throw new MDDException(String.format(
                        "Could not find %s in defined configuration path %s (looked in %s; details: %s). "
                            + "Please ensure that the relevant environment and Java system variables are set.",
                        BASE_CONFIG_SIMPLE, baseConfigHomeDir, propPath, e.getMessage()));
            }
        }

        String primLookingPath = BASE_CONFIG;
        boolean envVarChosen = false; // used to output more debug information on remote servers.
        if (prop == null) {
            prop = this.findPropertyFile(BASE_CONFIG, false, lookedPaths);
            lookedPaths.add(BASE_CONFIG);
            String[] environments = new String[] {KARAF_HOME, SEMBASE_HOME};
            for (String env : environments) {
                // try if Karaf or sembase environment is on (first Java system property, then OS environment variable)
                String envHome = System.getProperty(env);
                envHome = envHome == null ? "" : envHome;
                envHome = envHome.isEmpty() ? System.getenv(env) : envHome;
                envHome = envHome == null ? "" : envHome;
                if (!envHome.isEmpty()) {
                    LOGGER.info(String.format("Environment/System variable %s = %s.", env, envHome));
                    primLookingPath = String.format("%s%s", trailSlash(envHome), ENV_BASE_CONFIG);
                    envVarChosen = true;
                    try {
                        prop = new Properties();
                        lookedPaths.add(primLookingPath);
                        LOGGER.info("Open stream to {}.", addProtocol(primLookingPath));
                        prop.load(new URL(addProtocol(primLookingPath)).openStream());
                    } catch (NullPointerException | IOException e) {
                        LOGGER.error(e.getMessage());
                        throw new MDDException(
                                String.format("Cannot read properties of %s: %s.", env, e.getMessage()));
                    }
                } else {
                    LOGGER.info(String.format("Environment variable %s not set. ", env));
                }
            }
        }

        if (prop == null) {
            throw new MDDException(String.format("Mandatory entry (BASE) file %s missing (looked in %s).",
                    BASE_CONFIG_SIMPLE, StringUtils.join(lookedPaths, ";")));
        }

        String base = this.test ? TESTHOME_DIR_KEY : HOME_DIR_KEY;
        List<String> entries = this.propertyChainReader(prop, base);
        File homeDir = null;
        for (String iDir : entries) {
            String dir = iDir;
            if (envVarChosen) {
                LOGGER.info("Try to access {}.", dir);
                dir = this.regexReplacements(dir, false);
                LOGGER.info("Replaced system and property variables so that now {} will be accessed.", dir);
            }

            homeDir = this.findWritableFile(dir, false, lookedPaths);
            if (homeDir != null) {
                if (!homeDir.isDirectory()) {
                    throw new MDDException(String.format("%s %s must be a directory.", base, dir));
                } else {
                    break; // found
                }
            } else if (envVarChosen) {
                LOGGER.info(String.format("Looked at %s and skipped it because either not found or not writable.",
                        dir));
                if (this.findFile(dir, false, lookedPaths) != null) {
                    LOGGER.warn(String.format(
                            "%s is existing, but it is not writable! Please change the file permissions!!!", dir));
                }
            }
        }

        if (homeDir != null) {
            curHomeDir = homeDir.getAbsolutePath();
        } else {
            LOGGER.error(String.format("Could not find any home directory."));
            for (String lp : lookedPaths) {
                LOGGER.info("Looked path: {}", lp);
            }
        }
        LOGGER.info(String.format("The current home directory is \"%s\".", curHomeDir));
        return curHomeDir;
    }

    /**
     * reads property values, starting at the key value, and - as long as other values exist these values are readed as
     * well. For instance, if key-val, key-val-1, key-val-2, key-val4 exist then the values of key-val, key-val1, and
     * key-val2 will be returned.
     * 
     * @param prop the properties file, already loaded.
     * @param key the property key of the values that you are looking for.
     * @return a list of values for which the key matched, or an empty list if no results are available.
     */
    public List<String> propertyChainReader(Properties prop, String key) {
        List<String> result = new LinkedList<String>();
        String value = prop.getProperty(key);

        int i = 1;
        while (value != null && !value.isEmpty()) {
            result.add(value);
            value = prop.getProperty(key + "-" + i);
            i++;
        }
        return result;
    }

    /**
     * Simplified method to find a property file via its name.
     * @param s the path to a file, main contain placeholders, which are replaced dynamically: {projectPath} is replaced
     *        by the project path of the Java project if possible. {system.property....} is replaced by a Java system
     *        property. For instance, {system.property.karaf.home} is replaced by the system property {karaf.home}.
     *        {system.env}s are replaced by environment variables. Different OS may vary (e.g. uppercase/lowercase)
     *        hence if using environmental vars simple values should be used and lower/upercase should be avoided if
     *        possible.
     * @return the properties file if existing.
     * @throws MDDException is thrown if the access failed.
     */
    public Properties findPropertyFile(String s) throws MDDException {
        return this.findPropertyFile(s, true, new ArrayList<String>());
    }

    /**
     * Find the value of a property based on property file name and key.
     * 
     * @param propertyFileName the name of the property file. The ending (.properties) does not need to be specified and
     *        will be added automatically on demand.
     * @param key the key within the property file
     * @return the value as a string or an empty string if either the key or the file are missing
     * @throws MDDException is thrown if the access failed.
     */
    public String findPropertySmart(String propertyFileName, String key) throws MDDException {
        propertyFileName =
            propertyFileName.endsWith(".properties") ? propertyFileName : propertyFileName + ".properties";
        Properties p = this.findPropertyFile(propertyFileName, true, new ArrayList<String>());
        if (p == null) {
            return "";
        } else {
            return p.getProperty(key, "");
        }
    }

    private Properties findPropertyFile(String s, boolean considerPatterns, List<String> lookupPaths)
        throws MDDException {
        Properties properties = new Properties();
        InputStream is = null;
        try {
            is = this.findFile(s, considerPatterns, lookupPaths);
            if (is == null) {
                throw new IOException("not found.");
            }
            properties.load(is);
            return properties;
        } catch (IOException e) {
            return null;
        }
    }
    
    /**
     * Find a file and return a stream object to it. 
     * @param s the name of the file.
     * @return an input stream.
     * @throws MDDException is thrown if the file does not exist or any error occurs.
     */
    public InputStream findFile(String s) throws MDDException {
        return this.findFile(s, true, new ArrayList<String>());
    }

    private InputStream findFile(String s, boolean considerPatterns, List<String> lookupPaths) throws MDDException {
        s = regexReplacements(s, considerPatterns);
        // look in classpath only if it is a test (or if it is specified explicitly)
        if (test || s.contains(CLASS_PATH)) {
            String absPath = s.replace(CLASS_PATH, "").trim();
            lookupPaths.add(absPath);
            URL f = ClassLoader.getSystemResource(absPath); // does not access external bundles (in Eclipse it works)
            if (f != null) {
                lookupPaths.add(f.getFile());
                try {
                    return ClassLoader.getSystemResource(absPath).openStream();
                } catch (IOException e) {
                    throw new MDDException(String.format("Could not read file from classpath: %s.", e.getMessage()));
                }
            } else {
                absPath = this.replaceConfigurationDirPlaceholder(s);
                lookupPaths.add(absPath);
                File fx = new File(absPath);
                if (fx.exists()) {
                    try {
                        return getUrl(fx.getAbsolutePath()).openStream();
                    } catch (MalformedURLException e) {
                        throw new MDDException(e.getMessage());
                    } catch (IOException e) {
                        throw new MDDException(String.format("Cannot read: %s (%s).", fx.getAbsoluteFile(),
                                e.getMessage()));
                    }
                } else {
                    InputStream is = this.tryReadingWithPrefix(s, lookupPaths);
                    return is;
                }
            }
        } else {
            log(s, false);
            s = this.replaceConfigurationDirPlaceholder(s);
            File fx = new File(s);
            if (fx.exists()) {
                log(fx.getAbsolutePath(), true);
                try {
                    return getUrl(fx.getAbsolutePath()).openStream();
                } catch (MalformedURLException e) {
                    throw new MDDException(e.getMessage());
                } catch (IOException e) {
                    throw new MDDException(String.format("Could not read file: %s (%s)", fx.getAbsoluteFile(),
                            e.getMessage()));
                }
            } else {
                InputStream is = this.tryReadingWithPrefix(s, lookupPaths);
                return is;
            }
        }
    }

    private String trailSlash(String hd) {
        if (!hd.endsWith(File.separator) && !hd.endsWith("/")) {
            hd += "/";
        }
        return hd;
    }

    private String addHomePrefix(String path) throws MDDException {
        String hd = curHomeDir; // causes infinite loop this.getHomeDir();
        if (!path.startsWith(hd)) {
            hd = trailSlash(hd);
            path = hd + path;
        }
        return path;
    }

    private InputStream tryReadingWithPrefix(String s, List<String> lookupPaths) throws MDDException {
        // try to add home-prefix and try again.
        s = this.addHomePrefix(s);
        File fx = new File(s);
        lookupPaths.add(s);
        if (fx.exists()) {
            log(fx.getAbsolutePath(), true);
            try {
                return getUrl(fx.getAbsolutePath()).openStream();
            } catch (MalformedURLException e) {
                throw new MDDException(e.getMessage());
            } catch (IOException e) {
                throw new MDDException(String.format("Could not read file: %s (%s - %s)", fx.getAbsoluteFile(), e
                        .getClass().getName(), e.getMessage()));
            }
        }
        return null;
    }

    /**
     * Find a file and ensure that the write permission is provided to the user / app.
     * @param s the name of the file to access.
     * @return the file if accessible.
     * @throws MDDException is thrown if file access was denied.
     */
    public File findWritableFile(String s) throws MDDException {
        return this.findWritableFile(s, true, new ArrayList<String>());
    }

    /**
     * read a file which is writable. hence files in bundles are ignored. exception: if executing from eclipse or so,
     * then it is possible.
     * 
     * @param s the file pattern.
     * @return a file or null if not writable or existing.
     * @throws MDDException
     */
    private File findWritableFile(String s, boolean considerPatterns, List<String> lookupPaths) throws MDDException {
        s = regexReplacements(s, considerPatterns);
        // look in classpath only if it is a test (or if it is specified explicitly)
        if (s.contains(CLASS_PATH)) {
            String absPath = s.replace(CLASS_PATH, "").trim();
            lookupPaths.add(absPath);
            URL f = ClassLoader.getSystemResource(absPath); // does not access external bundles (in Eclipse it works)
            if (f != null) {
                File fx = checkFile(f.getFile(), true);
                if (fx == null) {
                    LOGGER.error(String.format("Cannot write %s.", fx));
                } else {
                    lookupPaths.add(fx.getAbsolutePath());
                    return fx;
                }
                return checkFile(absPath, true);
            }
        } else {
            File fx = checkFile(s, true);
            if (fx != null) {
                lookupPaths.add(fx.getAbsolutePath());
                return fx;
            } else {
                s = this.addHomePrefix(s);
                lookupPaths.add(s);
                return checkFile(s, true);
            }

        }
        log(s, false);
        return null;
    }

    private File checkFile(String path, boolean checkWritable) throws MDDException {
        File fx = null;
        try {
            path = this.replaceConfigurationDirPlaceholder(path);
            fx = new File(getUrl(path).getFile());
        } catch (IOException e) {
            throw new MDDException(String.format("URL convertion failed: %s.", e.getMessage()));
        }
        if (fx.exists()) {
            if (!checkWritable) {
                log(fx.getAbsolutePath(), true);
            } else if (fx.canWrite()) {
                log(fx.getAbsolutePath(), true);
            } else {
                LOGGER.error(String.format("Cannot write %s.", fx));
            }
            return fx;
        } else {
            log(fx.getAbsolutePath(), false);
        }
        return null;
    }

    /**
     * takes a file path and replaces the placeholders with known values. Replaced are system properties, the
     * home-directory, etc.
     * 
     * @param s a file path with placeholders (placeholders like {system.properties.user.home}
     * @return the same string with replaced placeholders.
     * @throws MDDException is thrown if a severe error occurs during the path extraction.
     */
    public String extractFilePath(String s) throws MDDException {
        return this.regexReplacements(s, true);
    }

    private String regexReplacements(String s, boolean considerPatterns) throws MDDException {
        if (considerPatterns) {
            getHomeDir(); // initialize home directory if not already done
        }
        s = advancedRegexReplacer(s, SYSTEM_PROPERTY_REGEX, "", true, false);
        s = advancedRegexReplacer(s, SYSTEM_ENV_REGEX, "", false, true);
        s = advancedRegexReplacer(s, SEMBASE_HOME_DIR_REGEX, curHomeDir, false, false);
        s = advancedRegexReplacer(s, TESTHOME_DIR_REGEX, curHomeDir, false, false);
        return s;
    }

    private String advancedRegexReplacer(String s, Pattern pattern, String repl, boolean isSystemProperty,
        boolean isSystemEnv) {

        Matcher matcher = null;
        try {
            matcher = pattern.matcher(s);
        } catch (NullPointerException e) {
            // appeared on server
            LOGGER.error(e.getMessage());
            LOGGER.error(String.format("Failed to extract pattern %s from string %s.", pattern, s));
        }

        while (matcher.find()) {
            String x = repl;
            String var = matcher.group(1);
            if (isSystemProperty) {
                x = System.getProperty(var);
                x = x == null ? "" : x;
            } else if (isSystemEnv) {
                x = System.getenv(var);
                x = x == null ? "" : x;
            }
            x = x.replace("\\", "\\\\"); // for Windows
            s = matcher.replaceFirst(x);
        }
        return s;
    }

    /**
     * use this method internally to replace the placeholder for the configuration directory either by "" if there is no
     * configuration file given, or otherwise by the name of the specified configuration dir with trailing slash.
     * 
     * @param s the path with configuration placeholder (optional placeholder)
     * @return the path with replaced configuration placeholders.
     */
    private String replaceConfigurationDirPlaceholder(String s) {
        if (configurationDir.equals(File.separator)) {
            return s.replace(CONFIG_PATH, "").trim();
        } else {
            return s.replace(CONFIG_PATH, this.configurationDir).trim();
        }
    }

    private void log(String name, boolean found) {
        LOGGER.debug(String.format("Extracted config file name: %s (found=%s).", name, found));
    }

    /**
     * get an URL out of an (unprefixed) absolute file path (filesystem independent; tested for UNIX, MacOS and
     * Windows).
     * 
     * @param s the absolute file path
     * @return e.g. file://User/andreas/...
     * @throws IOException is thrown if the access to the file is denied.
     */
    public static URL getUrl(String s) throws IOException {
        URL url = null;
        if (!s.contains("://") && (s.contains(":/") || s.contains(":\\") || s.contains(":\\"))) {
            url = new URL("file:///" + s);
        } else {
            url = new URL(s.contains("://") ? s : "file://" + s);
        }
        return url;
    }

    /**
     * Whether the config loader is executed in the test environment or not.
     * 
     * @return true if the application is executed in the test environment.
     */
    public boolean isTestEnvironment() {
        return this.test;
    }
}
//CHECKSTYLE:ON
