package org.cdlflex.mdd.sembase.semaccess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.WordUtils;

/**
 * Some serialization/deserialization utils for data access based on DAOs.
 */
public final class DAOUtil {
    private static final String DELIMITER = "::";

    /**
     * Hidden default constructor.
     */
    private DAOUtil() {
    }
    
    /**
     * Deserialize a bean which has been serialized with {@link #MDDBeanInterface.toString()}.
     * 
     * @param bean the serialized bean, e.g. "Issue::2051"
     * @return the ID extracted from the bean, e.g. 2051.
     */
    public static String deserializeIdFromBean(String bean) {
        List<String> parts = split(bean);
        return parts.get(1);
    }

    /**
     * Extract the meta class type from a serialized bean.
     * 
     * @param bean the serialized bean, e.g. "Issue::2051"
     * @return the non-abstract type , e.g. "Issue", "Collaborator" or "Project".
     */
    public static String deserializeMetaClassFromBean(String bean) {
        List<String> parts = split(bean);
        return WordUtils.capitalize(parts.get(0));
    }

    private static List<String> split(String bean) {
        String[] parts = bean.split(DELIMITER);
        List<String> lparts = new ArrayList<String>();
        if (parts.length == 2) {
            lparts = Arrays.asList(parts);
        } else {
            lparts.add(bean.trim());
            lparts.add("");
        }
        return lparts;
    }
}
