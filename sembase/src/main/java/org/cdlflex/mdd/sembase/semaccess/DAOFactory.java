package org.cdlflex.mdd.sembase.semaccess;

import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;

/**
 * The DAO Factory provides method to instantiate beans and DAOs, which were generated via the MDD approach.
 */
public interface DAOFactory {
    /**
     * Retrieve the connection interface to the underlying storage technology.
     * 
     * @param project the parameter is used to specify that some data does not reside within a global data model, but
     *        exists within a given (sub)project. While data models such as configuration, or project management might
     *        be available throughout an entire application, there may be the need for data instance stores, which apply
     *        to the same model, but are separated. For instance, a model may be valid for all projects within an
     *        application, but each project keeps its own project data instances. The parameter is OPTIONAL, and
     *        currently only one (default) project is supported. Nevertheless, for continuing development this parameter
     *        should be specified readily. Initialize the parameter with "" if the data instances should be stored in
     *        the default data model. This is the default is can be used legitimately. If the parameter is used, only
     *        lower case characters (a-z) should be used.
     * @param namespacePrefix The shortcut for the data model (graph) within a model. Must not be empty. Examples are
     *        itpm, config.
     * @return a connection which can be used.
     * @throws MDDException is thrown if no connection could be initialized, probably because some of the input
     *         parameters were wrong, or because of technical issues.
     */
    Connection createConnection(String project, String namespacePrefix) throws MDDException;

    /**
     * Get access to an (auto-generated) data access object via the name of the (also-generated) POJO. The returned DAO
     * might have been extended manually.
     * 
     * @param classNameBean the name of the Java bean. Do not confuse it with the name of the DAO. Examples: "Project",
     *        "Stakeholder", "Person" instead of "ProjectDAO", ...
     * @param con The connection is required for internal purposes (e.g. to maintain access to the data store based on
     *        the OWLAPI) and hence must be provided. The state of the connection is irrelevant.
     * @param <T> The type of the (naive, generated) bean.
     * @return A data access object which is either abstract (based on the generic T parameter), or specific. If
     *         possible the resulting instance should be accessed via the DAO interface.
     * @throws MDDException is thrown if the instantiation failed.
     */
    <T extends Object> DAO<T> create(String classNameBean, Connection con) throws MDDException;

    /**
     * Get access to an (auto-generated) data access object via the Java class of the related POJO. The returned DAO
     * might have been extended manually.
     * 
     * @param beanClass the Java class of the related POJO. Do not confuse it with the Java class of the DAO itself.
     *        Example: Stakeholder.class instead of StakeholderDAO.class.
     * @param con The connection is required for internal purposes (e.g. to maintain access to the data store based on
     *        the OWLAPI) and hence must be provided. The state of the connection is irrelevant.
     * @param <T> The type of the (naive, generated) bean.
     * @return A data access object which is either abstract (based on the generic T parameter), or specific. If
     *         possible the resulting instance should be accessed via the DAO interface.
     * @throws MDDException is thrown if the instantiation failed.
     */
    <T extends Object> DAO<T> create(Class<T> beanClass, Connection con) throws MDDException;

    /**
     * Get access to an (auto-generated) data access object via the Java class of the related POJO. (not true: Opposed
     * to the create methods, this class does access only the auto-generated DAO and does not inject manual extensions.
     * true: The createNativeDAO method is used to guarantee that accesses within the auto-generated code are detected.
     * Currently a call will deliver the same result as a create(...) call. However, it is ensured that the caller was a
     * auto-generated DAO which ensures modifiability for the future).
     * 
     * @param classNameBean the name of the Java bean. Do not confuse it with the name of the DAO. Examples: "Project",
     *        "Stakeholder", "Person" instead of "ProjectDAO", ...
     * @param con The connection is required for internal purposes (e.g. to maintain access to the data store based on
     *        the OWLAPI) and hence must be provided. The state of the connection is irrelevant.
     * @return A data access object which is either abstract (based on the generic T parameter), or specific. If
     *         possible the resulting instance should be accessed via the DAO interface.
     * @throws MDDException is thrown if the instantiation failed.
     */
    DAO<? extends Object> createNativeDAO(String classNameBean, Connection con) throws MDDException;

    /**
     * Get access to the native implementation of an (auto-generated) bean, without injecting manual extensions.
     * 
     * @param beanName the name of the bean which is equally to the simple name of the Java class. Example: Stakeholder,
     *        Project, Issue.
     * @param con The connection is required for internal purposes (to access the correct packages) and hence must be
     *        provided. The state of the connection is irrelevant.
     * @param <T> The type of the (naive, generated) bean.
     * @return the concrete implementation of the Java bean.
     * @throws MDDException is thrown if technical instantiation problems occured or if no class exists with the given
     *         name.
     */
    <T extends Object> T createNativeBean(String beanName, Connection con) throws MDDException;

    /**
     * Get access to the implementation of an (auto-generated) bean, an extend it with manual implementations. For
     * instance, additional methods might have been added, or some methods may have been overridden by the developer.
     * 
     * @param beanClassName the name of the bean which is equally to the simple name of the Java class. Example:
     *        Stakeholder, Project, Issue.
     * @param con The connection is required for internal purposes (to access the correct packages) and hence must be
     *        provided. The state of the connection is irrelevant.
     * @param <T> The type of the (naive, generated) bean.
     * @return the fully-fledged Java bean, including manual extensions.
     * @throws MDDException is thrown if technical instantiation problems occured or if no class exists with the given
     *         name.
     */
    <T extends Object> T createBean(String beanClassName, Connection con) throws MDDException;

    /**
     * Create a specific generated bean based on the beans class information. The factory will not only create a plain
     * bean, but will also look for additional resources which might have been defined manually. For instance, some
     * classes may have been extended manually to extend or to reduce the validation checks.
     * 
     * @param beanClass a class which is available in the custom.itpm.generated project.
     * @param con The connection is required for internal purposes (to access the correct packages) and hence must be
     *        provided. The state of the connection is irrelevant.
     * @param <T> The type of the (naive, generated) bean.
     * @return the class of the given type is returned.
     * @throws MDDException is thrown if the initialization failed.
     */
    <T extends Object> T createBean(Class<T> beanClass, Connection con) throws MDDException;
}
