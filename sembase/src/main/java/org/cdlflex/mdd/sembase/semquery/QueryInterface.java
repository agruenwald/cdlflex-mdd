package org.cdlflex.mdd.sembase.semquery;

import java.util.List;

import org.cdlflex.mdd.sembase.MDDException;

/**
 * This interface facilitates the queries of varies different query languages, such as OWL-DL, SPARQL or SPARQL-DL over
 * one interface.
 */
public interface QueryInterface {
    /**
     * get the query result and include the titles in the first row.
     * 
     * @param query the query, for instance a SPARQL query (any query type is allowed).
     * @return a list. the first entry contains the titles. the rest contains the records.
     * @throws MDDException is thrown if the query fails.
     */
    List<List<String>> query(String query) throws MDDException;

    /**
     * same as {@link QueryInterface#query(String)} but instead of returning each record as a list the records are
     * returned as maps.
     * 
     * @param query the query, typically in short-form without prefixing etc. (e.g. SPARQL: "SELECT ..." instead of
     *        "PREFX 1, PREFIX 2, ... SELECT...."
     * @return a list of records. Each record contains key-value pairs. The key equals the name of the column.
     * @throws MDDException is thrown if the query fails.
     */
    List<QueryMap> queryMap(String query) throws MDDException;

    /**
     * Test execution of an query.
     * @param query a query, analogously to {@link #query(String)}.
     * @throws MDDException is thrown if the query fails.
     */
    void test(String query) throws MDDException;

    /**
     * Extract the names (column names) from a query's parameter names.
     * @param query a query, analogously to {@link #query(String)}.
     * @return a list with all names extracted from the query (column names).
     * @throws MDDException is thrown if the query fails.
     */
    List<String> extractNames(String query) throws MDDException;

    /**
     * gets the SPARQL query default header, containing all the prefixes and the assigned namespaces, so that the user
     * can start with a plain SELECT, e.g. SELECT a,b,c FROM prefix1:concept1, owl:subclass...
     * 
     * @return the default SPARQL header.
     * @throws MDDException is thrown if building the query fails.
     */
    String getSparqlDefaultHeader() throws MDDException;
}
