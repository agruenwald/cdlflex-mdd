package org.cdlflex.mdd.sembase.semaccess;

/**
 * This interface is implemented by any model-generated Java bean. It provides standard information about a Beans
 * origin, its numbers of modifications (an increasing number), and the last update. The origin is typically the source
 * from where a bean comes from, e.g. a certain Google Spreadsheet, an Excel Resource, a tool). The version number
 * starts at 1 for new beans and is increased by one at each update.
 */
public interface MDDBeanInterface {

    /**
     * Set the date of the last change. If the date is null then the current date will be set automatically.
     * 
     * @param lastChange a valid date.
     */
    void setLastChange(java.util.Date lastChange);

    /**
     * get the date of the last record update. If used in conjunction with the MDD DAOs then this value will be always
     * set to a date.
     * 
     * @return the date of the last update or null if not used yet.
     */
    java.util.Date getLastChange();

    /**
     * Set the version of a data record. This method is typically used by automated routines.
     * 
     * @param version version > 0.
     */
    void setVersionInfo(int version);

    /**
     * get the version info of a record. While the version info may be handled depending on the type of application, by
     * default the version is increased by one whenever an update occurs.
     * 
     * @return version > 0.
     */
    int getVersionInfo();

    /**
     * A string containing information about the origin of a record. The origin indicates which application or data
     * source has been used to create the record. Once set, the origin never changes until the record has been removed.
     * 
     * @return a value with length > 0 indicating information about the origin. The origin can be either the name of a
     *         tool or the name of a more general tool domain. It also can be some information in-between such as
     *         "spreadsheet-teamdata".
     */
    String getOrigin();

    /**
     * Set the origin of a record. The origin should always remain the same once a record has been created.
     * 
     * @param origin the origin of the record.
     */
    void setOrigin(String origin);

    /**
     * Set the link to a specific record in the "cloud". In a heterogeneous network of tools each record (or each file)
     * typically origin from a specific source and then are distributed to other tools/sources. This method can be used
     * to indicate where the record is origin from, so that an end-user may use the link to navigate to the instance
     * within the specific tool.
     * 
     * @param originLink empty or the complete link to a tool including the data protocol, such as
     *        http://issues.jira.com/issue/ITPM-1 or (if no record-level access is possible)
     *        file://User/andi/teamdata.xlsx
     */
    void setOriginLink(String originLink);

    /**
     * get a link to the local instance of a record.
     * 
     * @return either empty or a link to the specific record or (if not available) a link to the file where the record
     *         resides. Examples are: "", "file://User/andi/teamdata.xlsx" or "http://issues.jira.com/issue/ITPM-1".
     */
    String getOriginLink();

    /**
     * set the date at which the bean has been created at. The value will only be used if the field is still empty.
     * Otherwise always the first update date will be used. The value is optional.
     * 
     * @param createdAt the date of creation.
     */
    void setCreatedAt(java.util.Date createdAt);

    /**
     * get the date of creation.
     * 
     * @return null if not used yet. A valid date is returned for all data records which have been saved by using the
     *         machine-generated DAO.
     */
    java.util.Date getCreatedAt();

    /**
     * Get a string which represents the bean/instance and is human-readable. Opposed to {@link #toString()} this method
     * does not necessarily return a unique identifier. The method returns some default content but should be overriden
     * whenever you want to refine the output.
     * 
     * @return a String >= zero whenever possible.
     */
    String getDisplayFriendlyName();

    /**
     * Get the most important information of the entity in a serialized form. Typically the String will contain the type
     * (meta class) of the entity followed by its ID (if available). Examples: + "issue::ITPM-25" for an instance of the
     * type "Issue" with the id "ITPM-25". + "issue" for an instance without any ID yet but of the type "Issue".
     * 
     * @return return the stringified version of the bean.
     */
    String toString();
}
