package org.cdlflex.mdd.sembase;

import java.util.List;
import java.util.Map;

/**
 * This exception is used within the entire MDD generation approach to indicate any kind of unexpected behavior during
 * the storage of semantic data. It is also used to transport any initialization or access failures regarding the
 * generated artifacts. Use it whenever there is unexpected behavior which is related to the storage of semantic data or
 * the usage of generated artifacts.
 */
public class MDDException extends Exception {
    private static final long serialVersionUID = 4564196422960311516L;

    public MDDException(String error) {
        super(error);
    }

    public MDDException(List<String> errors) {
        super(errors.get(0));
    }

    public MDDException(Map<String, String> errors) {
        super(errors.values().iterator().next());
    }
}
