package org.cdlflex.mdd.sembase;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This utility class is used within the generated code (umltumany -> itpm-generated). It provides methods which are
 * used frequently for the validation of fields, for the generation of auto-values, etc.
 */
public final class Util {
    private static final java.text.SimpleDateFormat DATETIME_FORMAT = new java.text.SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");
    private static final Logger LOGGER = LoggerFactory.getLogger(Util.class);

    /**
     * Hidden default constructor.
     */
    private Util() {
    }
    
    /**
     * Default check for a string content. Currently the mandatory string length needs to be at least 1. Originally the
     * mandatory string length was 2, however there are sometimes IDs with only one character so the length has been
     * reduced.
     * 
     * @param s the string
     * @param name the name of the string to provide a proper error message.
     * @return the message or empty if the check succeeded.
     */
    public static String checkMandatory(String s, String name) {
        return checkMandatory(s, name, 1);
    }

    /**
     * Same as {@link #checkMandatory(boolean, String)}, but with an additional parameter
     * for minimum length.
     * @param s the string
     * @param name the name of the string to provide a proper error message.
     * @param minLength the minimum length of the string.
     * @return the error message or an empty message if the check was ok.
     */
    public static String checkMandatory(String s, String name, int minLength) {
        if (s == null || s.isEmpty()) {
            return String.format("%s is empty.", name);
        } else if (s.length() < minLength) {
            return String.format("Length of string %s is smaller than " + minLength + ".", name);
        } else {
            return "";
        }
    }

    /**
     * Check whether an integer is "empty" or not (empty means null or lower or
     * equals to zero).
     * @param s the integer
     * @param name the name of the parameter.
     * @return the error message or an empty message if the check was ok.
     */
    public static String checkMandatory(Integer s, String name) {
        if (s == null) {
            return String.format("%s is empty.", name);
        } else if (s <= 0) {
            return String.format("Integer %s must be greater than zero.", name);
        } else {
            return "";
        }
    }

    /**
     * Check whether a double is "empty" or not (empty means null or lower or
     * equals to zero).
     * @param s the number
     * @param name the name of the parameter.
     * @return the error message or an empty message if the check was ok.
     */
    public static String checkMandatory(Double s, String name) {
        if (s == null) {
            return String.format("%s is empty.", name);
        } else if (s <= 0) {
            return String.format("Double %s must be greater than zero.", name);
        } else {
            return "";
        }
    }

    /**
     * Check whether a boolean is "empty" or not (empty means null or lower or
     * equals to zero). This method is mainly used for generic access.
     * @param s the boolean value
     * @param name the name of the parameter.
     * @return will always return an empty message.
     */
    public static String checkMandatory(boolean s, String name) {
        return "";
    }
    
    /**
     * Check whether a date is "empty" or not (empty means null).
     * @param d the date.
     * @param name the name of the parameter.
     * @return the error message or an empty message if the check was ok.
     */
    public static String checkMandatory(Date d, String name) {
        if (d == null) {
            return String.format("Date %s is empty.", name);
        } else {
            return "";
        }
    }

    /**
     * Check whether a specific field value undercuts a defined limit.
     * @param field the name of the field.
     * @param value the value.
     * @param min the minimum limit.
     * @return the error message or an empty message if the check was ok.
     */
    public static String undercutsMsg(String field, int value, int min) {
        return String.format("The number of elements of %s is lower than the minimum %s (value: %s).", field, min,
                value);
    }

    /**
     * Check whether a specific field value exceeds a defined limit. 
     * @param field the name of the field.
     * @param value the value.
     * @param max the maximum limit.
     * @return the error message or an empty message if the check was ok.
     */
    public static String exceedsMsg(String field, int value, int max) {
        return String.format("The number of elements of %s is higher than the maximum %s (value: %s).", field, max,
                value);
    }

    /**
     * Add an error message at the beginning of the map, if the error message is not empty.
     * @param oldMap the existing error messages.
     * @param fieldId the id of the field (technical, or user-communicable).
     * @param error the error message to add.
     * @return a new map (not the same instance than the oldMap).
     */
    public static Map<String, String> addAtBeginningIfError(Map<String, String> oldMap, String fieldId, String error) {
        if (!error.isEmpty()) {
            Map<String, String> newMap = new LinkedHashMap<String, String>();
            newMap.put(fieldId, error);
            newMap.putAll(oldMap);
            return newMap;
        } else {
            return oldMap;
        }
    }

    /**
     * Generate a unique string.
     * @param conceptName the name of a concept / class the ID should belong to.
     * @param s if an identifier (e.g., a manual unique identifier) is already known
     * then this identifier should be passed.
     * @return the newly generated UUID, or the string passed in parameter s.
     */
    public static String generateId(String conceptName, String s) {
        if (s.isEmpty()) {
            s = "" + UUID.randomUUID();
        }
        return s;
    }

    /**
     * Generate a unique number.
     * @param conceptName the name of a concept / class the ID should belong to.
     * @param id if the id is already known and positive (> 0) then the passed
     * id is used as the identifier.
     * @return the newly generated UUID (>0), or the string passed in parameter id.
     */
    public static int generateId(String conceptName, int id) {
        if (id <= 0) {
            UUID idOne = UUID.randomUUID();
            String str = "" + idOne;
            int uid = str.hashCode();
            String filterStr = "" + uid;
            str = filterStr.replaceAll("-", "");
            return Integer.parseInt(str);
        }
        return id;
    }

    /**
     * extracts date and time from a string. Warning: the timezone cannot be parsed properly via default simple date
     * format (sometimes the ending varies). hence the date and the time are parsed separately.
     * 
     * @param literal date as string in format 2013-11-07T10:24:35.303Z
     * @return date and time (previously: without time)
     * @throws MDDException is thrown if the literal is not a correct date.
     */
    public static Date parseDateTime(String literal) throws MDDException {
        try {
            String date = literal.split("T")[0].trim();
            String time = literal.split("T")[1].trim().substring(0, 8);
            Date datetime = DATETIME_FORMAT.parse(String.format("%s %s", date, time));
            return datetime;
        } catch (NullPointerException | ParseException | ArrayIndexOutOfBoundsException e) {
            throw new MDDException(e.getMessage());
        }
    }

    /**
     * Copy a file.
     * @param sourceFile the source file.
     * @param destFile the destination.
     * @throws IOException is thrown if either file name was incorrect, a file did not
     * exist, or any permission or access errors occurs.
     */
    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }

    /**
     * Copy a whole directory, including files.
     * @param sourceDir the source directory.
     * @param destDir the target directory.
     * @throws IOException is thrown if the directory name was wrong, or if 
     * any access / permission problem occurs.
     */
    public static void copyDirectory(File sourceDir, File destDir) throws IOException {
        FileUtils.copyDirectory(sourceDir, destDir);
    }

    /**
     * Get the (absolute) name of a path, excluding the file name itself.
     * @param path a valid absolute file path, e.g. /Volumes/a/x.txt
     * @return the file path without the name of the file at the end (e.g. /Volumes/a/)
     */
    public static String getFilePathWithoutFileName(String path) {
        String filePath = path;
        String filePathReverse = new StringBuffer(filePath).reverse().toString();
        int slashPos = filePathReverse.length() - filePathReverse.indexOf("/"); // last slash
        filePath = filePath.substring(0, slashPos);
        return filePath;
    }

    /**
     * Filter out special characters which should not appear in an ontology's URI (e.g. ":") (currently deactivated).
     * Will return the same string as passed.
     * 
     * @param str the string
     * @return the string in which special characters are replaced now (currently: the same string again).
     */
    public static String filterChars(String str) {
        return Util.filterChars(str, true);
    }

    /**
     * Filter out special characters which should not appear in an ontology's URI (e.g. ":") Take care not to remove the
     * wrong characters!
     * 
     * @param str the string
     * @param considerAsURI if true then an URI is considered. In that case http:// etc. must be preserved.
     * @return the string in which special characters are replaced now.
     */
    public static String filterChars(String str, boolean considerAsURI) {
        boolean replaceActivated = false;
        boolean containsSpecialChars = false;
        if (str.startsWith("http://")) {
            considerAsURI = true; // auto-detection
        } else {
            considerAsURI = false;
        }
        String[] str2 = str.split("#");
        if (str2.length == 2) {
            String url = str2[0];
            String addr = str2[1];
            if (addr.contains(":")) {
                containsSpecialChars = true;
            }
            if (replaceActivated) {
                addr = addr.replaceAll(":", "");
                str = url + "#" + addr;
            }
        } else if (considerAsURI) {
            return str;
        } else {
            if (str.contains(":")) {
                containsSpecialChars = true;
            }
            if (replaceActivated) {
                str = str.replaceAll(":", "");
            }
        }

        if (containsSpecialChars) {
            LOGGER.debug(String.format("The object's id contains illegal characters (value=\"%s\"). "
                + "As a result some object properties within the semantic store may be broken. "
                + "Please consider to redesign the approach how you enter/store the id.", str));
        }

        return str;
    }

    /**
     * the integer version of filter special chars to avoid generation errors.
     * 
     * @param i the input
     * @return the output which == the input.
     */
    public static int filterChars(int i) {
        return i;
    }
}
