package org.cdlflex.mdd.sembase.semquery;

import java.util.HashMap;

/**
 * Interface to get specific fields of result set via their names.
 */
public class QueryMap extends HashMap<String, String> {
    private static final long serialVersionUID = 1L;

    @Override
    public String get(Object key) {
        String value = super.get(key);
        if (value == null) {
            value = "";
        }
        return value;
    }

    /**
     * get a double value with a specific key.
     * @param key the key of a map to refer to a specific value (typically string).
     * @return the double value cast into a string, or 0.0 if the value did not
     * exist or was empty.
     */
    public String getDouble(Object key) {
        String value = super.get(key);
        if (value == null || value.isEmpty()) {
            value = "0.0";
        }
        return value;
    }

}
