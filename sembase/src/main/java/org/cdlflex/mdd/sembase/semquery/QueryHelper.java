package org.cdlflex.mdd.sembase.semquery;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cdlflex.mdd.sembase.MDDException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides methods which are used during the retrieval of query data of any kind (mostly however SPARQL).
 */
public class QueryHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(QueryHelper.class);

    /**
     * Get a list of attribute names which are contained in a SPARQL query.
     * 
     * @param query the query typically starting with select
     * @return a list of parameter names which appeared in the SPARQL query. For instance if the SPARQL query was
     *         "SELECT ?name ?id ?descr WHERE { ...} then [name,id,descr] will be returned.
     * @throws MDDException is thrown in case of extraction problems.
     */
    public List<String> extractNames(String query) throws MDDException {
        query = query.replaceAll("(\\r|\\n)", "");
        List<String> names = new LinkedList<String>();
        Stack<String> s = new Stack<String>();
        Pattern p = Pattern.compile("SELECT(.*)WHERE");
        Matcher m = p.matcher(query);
        if (!m.find()) {
            LOGGER.error("We have a problem here: " + query);
            return names;
        }
        String c = m.group(1).trim();
        Pattern p2 = Pattern.compile("(AS|as)? ?+\\?[a-zA-Z]+");
        Matcher m2 = p2.matcher(c);
        boolean found = false;
        List<String> variables = new LinkedList<String>();
        while (m2.find()) {
            found = true;
            variables.add(m2.group(0));
        }
        if (!found) {
            LOGGER.error("Nothing found in clause c: " + c);
            return names;
        }

        for (int i = 0; i < variables.size(); i++) {
            String term = variables.get(i).trim();
            if (term.toLowerCase().startsWith("as")) {
                s.pop(); // remove previous variable because we use alias //can be several
                term = term.split(" ")[1].trim();
            }
            s.push(term.replace("?", ""));
        }

        for (int i = 0; i < s.size(); i++) {
            names.add(s.get(i));
        }
        return names;
    }

    /**
     * analyze a single cell in the SPARQL result set.
     * 
     * @param v the variable name if available.
     * @param out the SPARQL cell content.
     * @return the extracted, displayable value or null if the empty result was detected.
     */
    public String fillValue(String v, String out) {
        String res = null;
        if (out.equals("http://www.w3.org/2002/07/owl#Nothing")) {
            return res; // empty result set - skip!
        } else if (out.length() > 0 && out.contains("^^")) {
            res = out.split("\\^\\^")[0];
        } else if (out.length() > 0 && out.contains("#")) {
            res = out.split("#")[1];
        } else {
            // at the moment do nothing (links to other entries?)
            LOGGER.trace("Take care about " + v + "---" + out);
            // for example concatenated priority, etc. - at the moment this is considered as the case where no type at
            // all is known
            res = out;
        }
        return res;
    }
}
