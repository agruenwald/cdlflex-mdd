package org.cdlflex.mdd.sembase.semquery;

/**
 * Access query facilities via the name of the qurey language.
 */
public interface QueryInterfaceFactory {
    /**
     * get the default SPARQL prefix which is relevant for any SPARQL query
     * (e.g. namespace prefix definitions), but which is cumbersome to enter by users.
     * @return the default query interface, which provides SPARQL access. returns null if no query interface is
     *         available (should not be the case!) or if the initialization of the interface failed.
     */
    QueryInterface getDefaultSparql();

    /**
     * retrieve a specific query interface, which has been registered under a specific name to the factory. For instance
     * SPARQLDL, OWLDL, FASTQUERY would be possible. The available values will however depend on the used technology
     * (e.g. jena or owlapi).
     * 
     * @param name the short name of the (known) query interface.
     * @return the query interface or null if not found.
     */
    QueryInterface getQueryInterface(String name);
}
