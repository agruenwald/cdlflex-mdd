package org.cdlflex.mdd.sembase.semaccess;

import java.util.List;
import java.util.Map;

import org.cdlflex.mdd.sembase.MDDException;

/**
 * The interface is the default interface for data access objects, which are created via the MDD code generator.
 * Nevertheless the interface can als be used otherwise.
 * 
 * @param <T> is the name of the bean which is managed by the data access object.
 */
public interface DAO<T extends Object> {
    /**
     * Insert a new record, including validation.
     * 
     * @param t the bean filled with data. The id may be auto-generated depending on the settings.
     * @throws MDDException is thrown in case that the validation fails, or that there are problems with the insertion
     *         of the data (for instance if the underlying model schema does not match etc.). Depending on the
     *         underlying technology, some specific exception may also be thrown which are not caught via MDDException
     *         handling.
     */
    void insert(T t) throws MDDException;

    /**
     * Update an existing record, including validation.
     * 
     * @param t the bean filled with data. The id must not be empty and must exist.
     * @throws MDDException is thrown in case that the validation fails, or that there are problems with the update of
     *         the data (for instance the record does not exist yet, or the underlying model schema does not match
     *         etc.). Depending on the underlying technology, some specific exception may also be thrown which are not
     *         caught via MDDException handling.
     */
    void update(T t) throws MDDException;

    /**
     * Remove an existing record, based on its key values (id).
     * 
     * @param t the bean filled with the most essential data about the record (typically the id).
     * @throws MDDException is thrown in case that the record does not exist, or there are other problems depending on
     *         the underlying (semantic?) technology. Depending on the underlying technology, some specific exception
     *         may also be thrown which are not caught via MDDException handling.
     */
    void remove(T t) throws MDDException;

    /**
     * Read a record from the underlying data store, based on its id.
     * 
     * @param id the unique id of the record. Beans without a String-id are also supported. The id will be cast
     *        automatically into the appropriate primitive in that case.
     * @return the record or null if the record does not exist.
     * @throws MDDException is thrown if problems occur during the reading. This may depend on the underlying
     *         technology. For instance the model scheme may be out of date, not available, etc. Some specific exception
     *         may also be thrown which are not caught via MDDException handling.
     */
    T readRecord(String id) throws MDDException;

    /**
     * To make the retrieval of existing beans more convenient, especially in combination with reflections, this methods
     * enables the retrieval of the full record, based on a given bean. The method behaves identical to
     * {@link #readRecord(String id) readRecord}
     * 
     * @param bean the bean filled with the unique id of the record. Beans without a String-id are also supported. The
     *        id will be cast automatically into the appropriate primitive in that case.
     * @return the record or null if the record does not exist.
     * @throws MDDException is thrown if problems occur during the reading. This may depend on the underlying
     *         technology. For instance the model scheme may be out of date, not available, etc. Some specific exception
     *         may also be thrown which are not caught via MDDException handling.
     */
    T readRecordViaBean(T bean) throws MDDException;

    /**
     * Retrieve a record based on a set of attributes, which are compared against a set of search values.
     * 
     * @param comparisonAttributes a map containing the names of the attributes as keys and search patterns as values.
     *        For instance a map could contain the values ("name", "peter"), ("description", "student"). In that case
     *        all records are returned which contain the value "peter" in the field "name", and the value "student" in
     *        the field description. The comparision is case insensitive.
     * @return a list of beans which match the given attributes.
     * @throws MDDException is thrown if problems occur during the reading. 
     *         This may depend on the underlying technology. For
     *         instance the model scheme may be out of date, not available, etc. Some specific exception may also be
     *         thrown which are not caught via MDDException handling.
     */
    List<T> readRecordByAttributeValues(Map<String, String> comparisonAttributes) throws MDDException;

    /**
     * Retrieve all records matching a certain string. The comparision is case insensitive.
     * 
     * @param s the comparision string. Empty means that all records are ok.
     * @return a list of matching entries.
     * @throws MDDException is thrown if problems occur during the reading. 
     *         This may depend on the underlying technology. For
     *         instance the model scheme may be out of date, not available, etc. Some specific exception may also be
     *         thrown which are not caught via MDDException handling.
     */
    List<T> readFulltext(String s) throws MDDException;

    /**
     * Merges two beans, i.e. their attributes and associations.
     * 
     * @param newBean the new bean which will be merged into.
     * @param oldBean the previous version of the bean
     * @return true if the new bean differs from the old bean (if changes in the attributes or the associations have
     *         been detected), otherwise false. Note: attribute values will be compared. For associations the number of
     *         linked entities will be compared and the value of the respective ID's.
     * @throws MDDException is reserved for very unlikely situations in which the underlying data store may be accessed.
     *         Is not expected to appear.
     */
    boolean merge(T newBean, T oldBean) throws MDDException;

    /**
     * Merges two beans, i.e. their attributes and associations.
     * 
     * @param newBean the new bean which will be merged into.
     * @param oldBean the previous version of the bean
     * @param addExistingDataAttributes if true, then the attributes of the old bean are written into the new bean if
     *        not empty or null.
     * @return true if the new bean differs from the old bean (if changes in the attributes or the associations have
     *         been detected), otherwise false. Note: attribute values will be compared. For associations the number of
     *         linked entities will be compared and the value of the respective ID's.
     * @throws MDDException is reserved for very unlikely situations in which the underlying data store may be accessed.
     *         Is not expected to appear.
     */
    boolean merge(T newBean, T oldBean, boolean addExistingDataAttributes) throws MDDException;

    /**
     * This method provides the means to delete all associations (in semantic terms: object properties), considering
     * their origin and associated classes. Object properties represent links between two (named) individuals
     * (=objects). The synchronization apps and the MDD connector of the ITPM project supports the synchronization of
     * data, including links to other data concepts (these links are the object properties). For instance, Requirement
     * -> isDefinedBy -> Stakeholder, Requirement -> hasCategory -> RequirementCategory. In these synchronization
     * applications all object properties are removed before synchronization and then they are refreshed (recreated)
     * during the synchronization process.
     * 
     * @param origin the origin.
     * @param ignoreClass if true then all object properties will be removed regardless of their from-class.
     * @param ignoreOrigin the association/object property is deleted even if both entities originate from another
     *        location/origin. Makes sense in many cases, but must be chosen with care because really all properties
     *        might be removed; even those of other classes which are not related.
     * @return the number of removed object properties >= 0.
     * @throws MDDException is reserved for very unlikely situations in which the underlying data store may be accessed.
     *         Is not expected to appear.
     **/
    int deleteAllObjectProperties(String origin, boolean ignoreClass, boolean ignoreOrigin)
        throws MDDException;

    /**
     * Validate the attributes of a class.
     * 
     * @param clazz the bean, which has been created by the MDD generator.
     * @return a map which contains a number of entries in the form (fieldName, errorMessage), containing all
     *         violations. If the map is empty then the validation succeeded.
     */
    Map<String, String> validateAttributes(T clazz);

    /**
     * Validate the content of a Java bean, including its attributes and associations. The attributes of the associated
     * (linked) entries are not considered.
     * 
     * @param clazz the bean, which has been created by the MDD generator.
     * @param isInsert if true then the id will not be compared.
     * @return a map which contains a number of entries in the form (fieldName, errorMessage), containing all
     *         violations. If the map is empty then the validation succeeded.
     */
    Map<String, String> validate(T clazz, boolean isInsert);

    /**
     * Validate the content of a Java bean, including its attributes and associations. The attributes of the associated
     * (linked) entries are not considered.
     * 
     * @param clazz the bean, which has been created by the MDD generator.
     * @return a map which contains a number of entries in the form (fieldName, errorMessage), containing all
     *         violations. If the map is empty then the validation succeeded.
     */
    Map<String, String> validate(T clazz);

    /**
     * When set to true, not only the main data record with its linked attributes and associations is retrieved when
     * reading data, but the attributes of the linked associations are also filled with data (at the first level). In
     * the default mode this is not the case (reason: performance).
     * 
     * @param r set to true and for linked objects their attributes will be retrieved from the model store as well.
     */
    void setReadLinkedObjectAttributesLevel1(boolean r);

    /**
     * Similar to {@link setReadLinkedObjectAttributesLevel1(boolean r)}. The difference is that not only the attributes
     * of linked records are read, but the entire record including its associations as well is retrieved (at the first
     * level).
     * 
     * @param r If the associations of linked entries are irrelevant then this parameter should not be set to true. Set
     *        the parameter to true if not only the attributes of linked associations at level 1 are relevant, but the
     *        entire record (which may be used for further processing).
     */
    void setReadLinkedObjectsLevel1(boolean r);

    /**
     * get the parameter value for telling the DAO whether to retrieve attribute values at level 1 or not.
     * 
     * @return the value. The method is usually not relevant for developers.
     */
    boolean isReadLinkedObjectAttributesLevel1();

    /**
     * get the parameter value for telling the DAO whether to retrieve attributes AND associations at level 1 or not
     * (the entire object is read not only the link).
     * 
     * @return the value. The method is usually not relevant for developers.
     */
    boolean isReadLinkedObjectsLevel1();

    /**
     * If set to true then manual constraints (those constraints which can be defined via the Semantic Editor and are
     * stored in the Ontology in form of annotations/comments and/or so-called OWL-Facets) are ignored. This might be
     * useful for machine-tests.
     * 
     * @param disable set to true to disable manual constraints. By default manual constraints are active.
     */
    void setDisableManualConstraints(boolean disable);

    /**
     * get the value of the manual constraint setting.
     * 
     * @return true if manual constraint checking has been disabled.
     */
    boolean isDisableManualConstraints();

    /**
     * Assign a generated value to the bean's ID field (the first field is assumed to be the ID). If the field is not
     * empty then no value will be generated.
     * 
     * @param bean the bean
     * @throws MDDException does only appear when technical problems occur.
     */
    void assignGeneratedId(MDDBeanInterface bean) throws MDDException;
}
