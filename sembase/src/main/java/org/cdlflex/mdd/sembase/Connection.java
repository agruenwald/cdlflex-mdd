package org.cdlflex.mdd.sembase;

/**
 * Abstract interface for ontology and triple store connections, such as used by Jena or the OWLAPI.
 */
public interface Connection {
    /**
     * Establish a connection to the ontology store. This method should be called once at the beginning of an
     * application. For each data store/ontology only one connection per application is allowed.
     * 
     * @throws MDDException is thrown if something went wrong with the underlying data store or if the connection
     *         already existed.
     */
    void connect() throws MDDException;

    /**
     * Same as {@link #connect()} but reestablish a connection only when there does not exist any. So opposed to
     * {@link #connect()} there is no exception thrown if the connection already had been established.
     * 
     * @throws MDDException is thrown if the connection cannot be established.
     */
    void connectOnDemand() throws MDDException;

    /**
     * Get whether a connection is established or not.
     * 
     * @return true if the connection has been established or false otherwise.
     * 
     * @throws MDDException is thrown if there were problems with checking the connection.
     */
    boolean isConnected() throws MDDException;

    /**
     * Close the connection to the ontology store properly. This method should be called whenever an application is
     * closed or a timeout happens.
     * 
     * @throws MDDException is thrown if the connection cannot be closed.
     */
    void close() throws MDDException;

    /**
     * Begin a new transaction. This method should be called if several data operations (possibly with different DAOs)
     * are performed, and during the access modifications take place. For several technologies (e.g. Jena) it is not
     * allowed to mix or nest different transactions. Hence, only one transaction can be active at a time in a single
     * thread. Thus, only begin a transaction when necessary and commit it immediately after the data modifications have
     * been performed. Only afterwards you may execute transactions on other data stores.
     * 
     * @throws MDDException is thrown if a user tries to establish a connection, but no connection took place before
     *         (via {@code connect}. Is also thrown if other unexpected problems occur.
     */
    void begin() throws MDDException;

    /**
     * When commit is executed, the data is finally persisted to the ontology store. This operation cannot be undone.
     * Whenever commit is called a connection must have been opened and began before. Note that when a commit is
     * triggered, then the connection will go back to its previous mode. The default behavior is that after a commit has
     * been performed the autocommit mode is enabled again if not defined differently before the execution of the
     * begin-command.
     * 
     * @throws MDDException is thrown if the commit fails.
     */
    void commit() throws MDDException;

    /**
     * Execute this method to tell the ontology store that the data which has been modified since the last call of
     * {@code begin} should not be persist, but should be removed. Not all technologies may support proper rollbacks, so
     * for some technologies in addition a trigger may needed to be set in order to support rollbacks.
     * 
     * @throws MDDException is thrown if the rollback fails.
     */
    void rollback() throws MDDException;

    /**
     * This method has been introduced since some technologies (e.g. Jena) distinguish between a readable and a writable
     * access. While for instance for the OWLAPI there is no necessity to handle transactions after an autoBegin()
     * event, for other technologies such as Jena TDB, the event must be detected so that a commit (or reset) can take
     * place at the autocommit event. Otherwise nested transactions will result which will be result in a blocked
     * application state.
     * 
     * @throws MDDException is thrown if starting a new transaction fails (if detectable).
     */
    void autoBeginReadOnly() throws MDDException;

    /**
     * Perform this method after reading data (e.g. via SPARQL) and after an {@code begin() or autoBegin()} statement.
     * 
     * @throws MDDException is thrown if a read only commit fails.
     */
    void commitReadOnly() throws MDDException;

    /**
     * this method is used for internal purpopses (e.g. in the MDD code generator) typically. A transaction is started,
     * but only if it is not yet open and/or has not been started manually with the call of {@code begin}.
     * 
     * @throws MDDException is thrown if during an (automatic) transaction start an error occurs.
     */
    void autoBegin() throws MDDException;

    /**
     * Counterpart of {@code autoBegin()} method, used typically for internal purposes, not by the end user.
     * 
     * @throws MDDException is thrown if an (auto) commit fails (if detected).
     */
    void autoCommit() throws MDDException;

    /**
     * In read-only transactions commits typically are easier to handle than in CRUD operations. Therefor it is useful
     * to distinguish between a write/update and a read-only operation to improve performance etc.
     * 
     * @throws MDDException is thrown if an (auto) commit (readonly) fails (if detected).
     */
    void autoCommitReadOnly() throws MDDException;

    /**
     * Remove all individuals (A-Box) from a graph/file/ontology. The concepts (T-Box) however remain untouched. Before
     * resetting data a connection should be established. If no connection has been established, then the method will
     * try to connect itself and will close the connection at the end again.
     * 
     * @throws MDDException is thrown if the data reset fails.
     */
    void resetData() throws MDDException;

    /**
     * If this method is called then not only the individuals are removed (A-Box), but also the T-Box is reseted to its
     * default state.
     * 
     * @throws MDDException is thrown if the ontology reset fails.
     */
    void resetOntology() throws MDDException;

    /**
     * Enables or disables the inferring mechanism.
     * 
     * @param inferring false if it should be enabled, otherwise set to false.
     */
    void setInferring(boolean inferring);

    /**
     * get whether inferrence is activated or not.
     * @return the inference state is returned.
     */
    boolean isInferring();

    /**
     * get a namespace prefix from an ontology (as in OWL).
     * @return a unique namespace prefix for a certain type of connetion. Typical examples are "itpm", "config", "jira",
     *         ...
     */
    String getNamespacePrefix();

}
