package org.cdlflex.mdd.sembase.test;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains test for the access handling to the central sembase configuration directory.
 */
public class TestConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestConfig.class);

    /**
     * Test to check if base-config properties file is accessible (cannot be tested without a fixed path).
     * 
     * @throws MDDException is thrown in case of error. 
     */
    @Test
    @Ignore
    public void testxx() throws MDDException {
        String path =
            "/Users/andreas_gruenwald/git/custom-itpm/assembly/target/cdlflex-itpm-1.0.0-SNAPSHOT"
                + "/sembase/base-config.properties";
        Properties prop = new ConfigLoader(false).findPropertyFile(path);
        assertTrue(prop != null);
    }

    /**
     * Negative test to access properties file in central config directory.
     * 
     * @throws MDDException is thrown in case of error.
     */
    @Test
    public void testFileAccessNeg() throws MDDException {
        Properties prop = new ConfigLoader(true).findPropertyFile("{projectPath}brabl.properties");
        assertTrue(prop == null);
    }

    /**
     * Test to check standardized access to configuration file.
     * 
     * @throws MDDException is thrown in case of error.
     */
    @Test
    public void testFileAccessPos() throws MDDException {
        Properties prop = new ConfigLoader(true).findPropertyFile("{projectPath}configtest.properties");
        assertEquals("sembase", prop.getProperty("bundle"));
    }

    /**
     * Test to check access to a folder.
     * 
     * @throws MDDException is thrown in case of error.
     */
    @Test
    public void testHomeDir() throws MDDException {
        String homeDir = new ConfigLoader(false).getHomeDir();
        LOGGER.info("Home dir: " + homeDir);
        assertTrue(!homeDir.isEmpty());
    }

    /**
     * Test to check access to a specific folder.
     * 
     * @throws MDDException is thrown in case of error.
     */
    @Test
    public void testTestDirTestMode() throws MDDException {
        String homeDir = new ConfigLoader(true).getHomeDir();
        LOGGER.info("Test Home dir: " + homeDir);
        assertTrue(!homeDir.isEmpty());
        assertTrue(homeDir.endsWith("test"));
    }

    /**
     * Test to check access to a read-only file.
     * 
     * @throws MDDException is thrown in case of error.
     */
    @Test
    public void testReadGlobalFile() throws MDDException {
        File f = new ConfigLoader(true).findWritableFile("jena-config-sdb.properties");
        if (f == null) {
            LOGGER.warn("Please move the sembase configuration directory within custom-itpm/assembly "
                + "into one of the directories specified in base-config.properties or add the "
                + "path to the configuration directory. Alternatively, try to set "
                + "either karaf.home, sembase.home (Java system properties) or SEMBASE_HOME (environment variable).");
        }
        assertTrue(f != null);
        LOGGER.info("global file: " + f.getAbsolutePath());
    }

    /**
     * Test to check a commonly used regular expression pattern.
     */
    @Test
    public void testPatternExtraction() {
        String pattern = "\\{system\\.property\\.([a-z\\.]{1,50})\\}";
        Pattern compiledPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        String s = "{system.property.user.home}/git/custom-itpm/assembly/src/main/resources/sembase";
        Matcher matcher = compiledPattern.matcher(s);
        while (matcher.find()) {
            String var = matcher.group(1);
            String x = System.getProperty(var);
            x = x == null ? "" : x;
            s = s.replaceFirst(compiledPattern.pattern(), x);
        }
        // comparison must consider that test may be executed on different servers.
        assertTrue(s.indexOf("/git/custom-itpm/assembly/src/main/resources/sembase") > 0);
        assertFalse(s.contains("{"));
    }

    /**
     * Test to check environment-variable - based access to paths.
     */
    @Test
    public void testPatternExtractionEnv() {
        String pattern = "\\{system\\.env\\.([a-zA-Z\\_\\.]{1,50})\\}";
        Pattern compiledPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        String s = "{system.env.SEMBASE_HOME}/git/custom-itpm/assembly/src/main/resources/sembase";
        Matcher matcher = compiledPattern.matcher(s);
        while (matcher.find()) {
            String var = matcher.group(1);
            String x = System.getProperty(var);
            x = x == null ? "" : x;
            s = s.replaceFirst(compiledPattern.pattern(), x);
            assertTrue(true);
            return;
        }
        assertTrue(false);
    }

    /**
     * Test to check a commonly used, complex regex pattern for replacement.
     */
    @Test
    public void testReplaceSystemEnv() {
        String varContent = "{system.env.SEMBASE_HOME}/sembase/live";
        String pattern = "\\{system\\.env\\.([a-zA-Z\\_\\.]{1,50})\\}";
        varContent = varContent.replaceFirst(pattern, "hurray");
        assertTrue(varContent.contains("hurray"));
    }

    /**
     * Test to check a commonly used, complex regex pattern for replacement (with a directory).
     * 
     * @throws MDDException is thrown in case of error.
     */
    @Test
    public void testReplaceHomeDir() throws MDDException {
        String path = "{home.dir}/pm-data/requirements-manual-synctest-excel.xlsx";
        String result = new ConfigLoader(true).extractFilePath(path);
        assertTrue(result != path);
    }

}
