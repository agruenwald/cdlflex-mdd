package org.cdlflex.mdd.sembase.test;

import static org.junit.Assert.*;

import org.cdlflex.mdd.sembase.Util;
import org.junit.Test;

/**
 * This test is used to compare special characters.
 */
public class TestSpecialChars {
    /**
     * Test special character replacement in URIs.
     */
    @Test
    public void testReplaceSpecialCharsKeepHttp() {
        String iri =
            "http://www.cdlflex.org/config.owl#AggregatedReferenceField-915b4fbf-d6ea-4bf0-81c2-41b399e1ab33";
        String iri2 = Util.filterChars(iri);
        assertEquals(iri, iri2);
    }
}
