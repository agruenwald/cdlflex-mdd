# CDL FLEX :: MDD :: Sembase

This module carries the basic interfaces which are implemented by the 
MDD generator approach umlTUmany.

The main type of interfaces are 
+ connectivity interfaces (in sembase)
+ query interfaces for SPARQL and other languages (semquery)
+ Abstract interfaces for DAOs which are heavily used by the sources generated with umlTUmany.

Additionally, a class named '''ConfigLoader is provided. The config loader eases the access 
to global project files based on mechanisms for determing a common home directory.

## Test Environment
Sembase (and the modules which use it) must be configured to access to the common
(sembase) configuration directory. The configuration for the
sembase directory is available in base-config.properties. Some appliations, such as
CI-Servers (e.g. Jenkins), Tomcat or the OpenEngSb will however require a system or
environment variable to be configured, because the base-config.properties file does
not appear within the classpath. If required, please set either
+ karaf.home (Java system variable), or
+ SEMBASE_HOME 
and point it to the configuration directory of sembase, which is typically contained
within the custom-itpm project (e.g. SEMBASE_HOME=/home/jenkins/custom-itpm/assembly/src/main/resources 
when the sembase directory is located in /home/jenkins/custom-itpm/assembly/src/main/resources/sembase).

Also ensure that the user permissions are set correctly (r+w permissions for the user
should be provided for the SEMBASE_HOME directory).
