package org.cdlflex.mdd.modelrepo.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.cdlflex.mdd.modelrepo.AdvancedModelStore;
import org.cdlflex.mdd.modelrepo.AdvancedModelStoreImpl;
import org.cdlflex.mdd.modelrepo.ModelStoreImpl;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.Util;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.JsonMaker;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Check persisting a model with redundant information 
 * (two packages which are duplicates).
 */
public class DuplModlTest {
    private static final String MODEL_REPO_BACKUP = "modelrepo-backup";

    private void resetFiles() throws IOException, MDDException {
        ConfigLoader configLoader = new ConfigLoader(true);
        Util.copyDirectory(configLoader.findWritableFile(MODEL_REPO_BACKUP),
                configLoader.findWritableFile(ModelStoreImpl.MODEL_REPO_PATH));
    }

    /**
     * Setup test environment.
     * @throws MDDException is thrown if setup fails.
     * @throws IOException is thrown if access to files fails.
     */
    @Before
    public void setUp() throws MDDException, IOException {
        resetFiles();
    }

    /**
     * Backup test environment after execution.
     * @throws MDDException is thrown if reseting models fails.
     * @throws IOException is thrown if access to files fails.
     */
    @After
    public void backUp() throws MDDException, IOException {
        resetFiles();
    }

    /**
     * Test the persistence of a meta model with multiple / identical packages.
     * @throws MDDException is thrown if the model is invalid or the processing of
     * the model failed.
     * @throws IOException the exception is thrown if the model (or parts of it) 
     * cannot be persisted.
     */
    @Test
    public void testMergeTwoIdenticalPackages() throws MDDException, IOException {
        AdvancedModelStore store = new AdvancedModelStoreImpl(new ConfigLoader(true));
        ConfigLoader configLoader = new ConfigLoader(true);
        InputStream is = configLoader.findFile("{projectPath}itpm_dupl.json");
        StringWriter writer = new StringWriter();
        IOUtils.copy(is, writer);
        MetaModel mm = JsonMaker.toMetaModel(writer.toString());
        store.persistOWLMetaModel(mm, false);
        assertTrue(true);
    }

}
