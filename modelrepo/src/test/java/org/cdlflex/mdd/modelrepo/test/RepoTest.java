package org.cdlflex.mdd.modelrepo.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.cdlflex.mdd.modelrepo.AdvancedModelStore;
import org.cdlflex.mdd.modelrepo.AdvancedModelStoreImpl;
import org.cdlflex.mdd.modelrepo.ModelStore;
import org.cdlflex.mdd.modelrepo.ModelStoreImpl;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.Util;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Basic tests for the model repository access.
 */
public class RepoTest {
    private static final String MODEL_REPO_BACKUP = "modelrepo-backup";
    private static final Integer SLEEP_TIME = 1000;
    private static final Logger LOGGER = LoggerFactory.getLogger(RepoTest.class);
    
    private void resetFiles() throws IOException, MDDException {
        ConfigLoader configLoader = new ConfigLoader(true);
        Util.copyDirectory(configLoader.findWritableFile(MODEL_REPO_BACKUP),
                configLoader.findWritableFile(ModelStoreImpl.MODEL_REPO_PATH));
    }

    /**
     * Setup test environment.
     * @throws MDDException is thrown if setup fails.
     * @throws IOException is thrown if access to files fails.
     */
    @Before
    public void setUp() throws MDDException, IOException {
        resetFiles();
    }

    /**
     * Backup test environment after execution.
     * @throws MDDException is thrown if reseting models fails.
     * @throws IOException is thrown if access to files fails.
     */
    @After
    public void backUp() throws MDDException, IOException {
        resetFiles();
    }

    /**
     * Test the initialization of the model repository.
     * @throws MDDException is thrown if initialization fails.
     */
    @Test
    public void testInit() throws MDDException {
        new ModelStoreImpl(new ConfigLoader(true));
        // if initialization ok, then no exception has been thrown
        assertTrue(true);
    }

    /**
     * Test the retrieval of all models in the store.
     * @throws MDDException is thrown if the access to the model repository fails.
     */
    @Test
    public void testFindAll() throws MDDException {
        ModelStore store = new ModelStoreImpl(new ConfigLoader(true));
        store.findAllMetaModels();
    }

    /**
     * Test the removal of the entire model store (all models).
     * @throws MDDException is thrown if the removal fails.
     */
    @Test
    public void testRemoveAll() throws MDDException {
        ModelStore store = new ModelStoreImpl(new ConfigLoader(true));
        store.removeAllMetaModels();
        assertEquals(0, store.findAllMetaModels().size());
    }
    
    /**
     * Test the retrieval of a specific meta model.
     * @throws MDDException is thrown if the model access fails.
     */
    @Test
    public void testFindMetaModel() throws MDDException {
        ModelStore store = new ModelStoreImpl(new ConfigLoader(true));
        MetaModel itpm = store.findMetaModel("itpm");
        assertTrue(itpm != null);
    }

    /**
     * Test to persist a specific model.
     * @throws MDDException is thrown if the access to persist the model fails.
     */
    @Test
    public void testPersistModel() throws MDDException {
        ModelStore store = new ModelStoreImpl(new ConfigLoader(true));
        MetaModel itpm = store.findMetaModel("itpm");
        assertTrue(itpm != null);
        store.persistMetaModel(itpm);
        try {
            Thread.sleep(SLEEP_TIME);
        } catch (InterruptedException e) {
            LOGGER.info(e.getMessage());
        }
        store.persistMetaModel(itpm);
        try {
            Thread.sleep(SLEEP_TIME);
        } catch (InterruptedException e) {
            LOGGER.info(e.getMessage());
        }
        store.persistMetaModel(itpm);
    }

    /**
     * Test the persistence of the model as an OWL ontology.
     * @throws MDDException is thrown if the persistence of the OWL ontology fails.
     */
    @Test
    public void testPersistOWL() throws MDDException {
        AdvancedModelStore store = new AdvancedModelStoreImpl(new ConfigLoader(true));
        MetaModel itpm = store.findMetaModel("itpm");
        store.persistOWLMetaModel(itpm, true);
    }

    /**
     * Test the removal of all models, including all redundant ontologies.
     * @throws MDDException is thrown if the persistence of the ontologies fails.
     */
    @Test
    public void testRemoveAllAdvanced() throws MDDException {
        AdvancedModelStore store = new AdvancedModelStoreImpl(new ConfigLoader(true));
        store.removeAllMetaModels();
    }

    /**
     * Test the removal of the itpm meta model.
     * @throws MDDException is thrown if the persistence of the ontology fails.
     */
    @Test
    public void testRemoveAdvanced() throws MDDException {
        AdvancedModelStore store = new AdvancedModelStoreImpl(new ConfigLoader(true));
        store.removeMetaModel("itpm");
    }
}
