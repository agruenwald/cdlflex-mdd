package org.cdlflex.mdd.modelrepo;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.Util;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.umltuowl.main.Main;
import org.cdlflex.mdd.umltuowl.main.ParameterHandling;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.JsonMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Install and reset models in the the meta-model repository which is the central register for all umlTUowl-based
 * models.
 */
public final class Installer {
    private static final String MODEL_REPO_BACKUP = "modelrepo-backup"; /*
                                                                         * the default backup/deploy directory for
                                                                         * models
                                                                         */
    private static final Logger LOGGER = LoggerFactory.getLogger(Installer.class);

    private Installer() {

    }

    /*
     * Install a new model based on a given file path into the central meta-model repo.
     * 
     * @param configLoader the configuration loader containing information about the environment.
     * @param input the (absolute) path to the file which contains the meta-model in serial form.
     * @param converterName the name of the converter according to the umlTUowl XMI input format.
     * @param optionalIRI the optional IRI for the OWL ontology IRI.
     * @param merge if true then all packages within the model will be merged.
     * @throws MDDException is thrown if operation fails.
     
    public static void install(ConfigLoader configLoader, String input, String converterName, String optionalIRI,
        boolean merge) throws MDDException {

        AdvancedModelStore store = new AdvancedModelStoreImpl(configLoader);
        List<String> argsL = new LinkedList<String>();
        argsL.add(ParameterHandling.INPUT + "=" + input);
        argsL.add(ParameterHandling.CONVERTER + "=" + converterName);
        if (!optionalIRI.isEmpty()) {
            argsL.add(ParameterHandling.IRI + "=" + optionalIRI);
        }
        MetaModel mm = Main.loadMetaModel(argsL.toArray(new String[argsL.size()]));
        if (mm == null) {
            throw new MDDException(String.format("Meta model not loaded correctly!"));
        }
        store.persistOWLMetaModel(mm, merge);
    }
    */
    
    /**
     * Install a new model based on a given file path into the central meta-model repo.
     * @param installConfig the installation configuration.
     * @throws MDDException is thrown if operation fails.
     */
    public static void install(InstallConfiguration installConfig) throws MDDException {
        AdvancedModelStore store = new AdvancedModelStoreImpl(installConfig.getConfigLoader());
        List<String> argsL = new LinkedList<String>();
        argsL.add(ParameterHandling.INPUT + "=" + installConfig.getInput());
        argsL.add(ParameterHandling.CONVERTER + "=" + installConfig.getConverterName());
        if (!installConfig.getOptionalIRI().isEmpty()) {
            argsL.add(ParameterHandling.IRI + "=" + installConfig.getOptionalIRI());
        }
        MetaModel mm = Main.loadMetaModel(argsL.toArray(new String[argsL.size()]));
        if (mm == null) {
            throw new MDDException(String.format("Meta model not loaded correctly!"));
        }
        store.persistOWLMetaModel(mm, installConfig.isMerge());
    }
    
    

    /**
     * Install meta models which are provided in the internal (minimum) format. This method is a simplification of the
     * {@code install(...)} method, because the installation file is already assumed to be in the minimum format (the
     * appropriate converter will be picked automatically) and name of the file has already been set to the namespace of
     * the model. This method should be only used on purpose and is typically not recommended for use.
     * 
     * @param configLoader the configuration loader.
     * @param namespacePrefixModel The namespace of the model, e.g. itpm
     * @param modelPath the directory in which the (minimum) meta-models reside. This path is equivalent with the
     *        home-path of the model-store.
     * @throws MDDException is thrown if operation fails.
     */
    public static void installInternalModel(ConfigLoader configLoader, String namespacePrefixModel, String modelPath)
        throws MDDException {
        AdvancedModelStore store = new AdvancedModelStoreImpl(configLoader);
        String modelContent;
        File modelFile = new File(modelPath + (modelPath.endsWith("/") ? "" : "/") + namespacePrefixModel + ".json");
        try {

            modelContent = FileUtils.readFileToString(modelFile);
        } catch (IOException e) {
            throw new MDDException(String.format("Cannot load/install file %s.", modelFile));
        }
        MetaModel mm = JsonMaker.toMetaModel(modelContent);
        store.persistOWLMetaModel(mm, true);
    }

    /**
     * This method resets the current model store and removes all of its models. The method takes the environment
     * (live/test) into consideration. By default, if the live configuration is active, all models are stored in the
     * backup-directory before removal. Deactivate this feature by using the reset-method with backup=false.
     * 
     * @param configLoader The configuration loader with the information about the environment etc.
     * @throws MDDException is thrown if the removal of any model failed.
     */
    public static void reset(ConfigLoader configLoader) throws MDDException {
        reset(configLoader, true);
    }

    /**
     * This method resets the current model store and removes all of its models. The method takes the environment
     * (live/test) into consideration.
     * 
     * @param configLoader The configuration loader with the information about the environment etc.
     * @param backup set to true if a backup of the models should be taken in the live-environment. In the
     *        test-environment no backups are stored at all. All previously existing models in the backup-dir are
     *        removed.
     * @throws MDDException is thrown if the removal of any model failed.
     */
    public static void reset(ConfigLoader configLoader, boolean backup) throws MDDException {
        AdvancedModelStoreImpl store = new AdvancedModelStoreImpl(configLoader);
        if (backup && !configLoader.isTestEnvironment()) {
            List<MetaModel> list = store.findAllMetaModels();
            File backupDir = configLoader.findWritableFile(MODEL_REPO_BACKUP);
            LOGGER.info(String.format("Clean backup-dir %s and paste all models from the live-environment.",
                    backupDir));
            try {
                FileUtils.cleanDirectory(backupDir);
            } catch (IOException e) {
                throw new MDDException(
                        String.format("Could not clean backup-dir in live environment: %s.", backupDir));
            }
            for (MetaModel mm : list) {
                NamingManager nm = new NamingManager(mm);
                File jsonFile = nm.getPyhsicalInternalFile(configLoader);
                try {
                    FileUtils.copyFileToDirectory(jsonFile, backupDir);
                    LOGGER.info(String.format("Copy file %s and paste it in %s.", jsonFile, backupDir));
                } catch (IOException e) {
                    throw new MDDException(String.format("Could not move %s to backup-dir %s.", jsonFile, backupDir));
                }
            }
        }
        store.removeAllMetaModels();
        // clean(configLoader); //always appropriate
    }

    /**
     * Clean the entire model store.
     * 
     * @param configLoader the configuration loader with the environment containing.
     * @throws MDDException is thrown if cleaning failed.
     */
    public static void clean(ConfigLoader configLoader) throws MDDException {
        Installer.clean(configLoader, true);
    }

    /**
     * Clean all data instances from the DBs (if possible) but do not touch the models themselves.
     * 
     * @param configLoader the configuration loader with the environment containing.
     * @param onlyTDB if true then only the Jena TDB data is cleaned.
     * @throws MDDException is thrown if cleaning failed.
     */
    public static void clean(ConfigLoader configLoader, boolean onlyTDB) throws MDDException {
        AdvancedModelStoreImpl store = new AdvancedModelStoreImpl(configLoader);
        store.cleanAllPhysicalInstances(onlyTDB);
    }

    /**
     * Install all models from the default backup-directory (including complete OWL model generation). The existing
     * models are removed.
     * 
     * @param configLoader the configuration loader
     * @throws MDDException is thrown if the installation fails.
     */
    public static void installModelsFromBackupDir(ConfigLoader configLoader) throws MDDException {
        installModelsFromBackupDir(configLoader, MODEL_REPO_BACKUP);
    }

    /**
     * Install all models from a backup-directory. Please use {@code installModelsFromBackupDir(configLoader)} in favor
     * of this method if possible.
     * 
     * @param configLoader the configuration loader. Depending on the configuration loader environment the
     *        home-directory and the model-path will be determined.
     * @param hotDeployDir the directory which contains a set of serialized meta-models in the internal (json) format.
     *        These models are copied into the model-repo path and there the complete installation of the (advanced) OWL
     *        models take place.
     * @throws MDDException is thrown if the installation fails.
     */
    public static void installModelsFromBackupDir(ConfigLoader configLoader, String hotDeployDir) throws MDDException {
        pasteModels(configLoader, hotDeployDir);
        installInternalModels(configLoader);
    }

    /**
     * Install all available internal models, which reside in the main model-repo directory (either live or test
     * environment). Opposed to {@code installModelsFromBackupDir} nothing is loaded from the backup dir before.
     * 
     * @param configLoader the configuration loader. Depending on the configuration loader environment the
     *        home-directory and the model-path will be determined.
     * @throws MDDException is thrown if the installation fails.
     */
    public static void installInternalModels(ConfigLoader configLoader) throws MDDException {
        ModelStore store = new ModelStoreImpl(configLoader);
        // clean(configLoader); //clean initially
        List<MetaModel> mList = store.findAllMetaModels();
        for (MetaModel m : mList) {
            String modelPrefixName = m.getNamespacePrefix();
            Installer.installInternalModel(configLoader, modelPrefixName,
                    configLoader.findWritableFile(ModelStoreImpl.MODEL_REPO_PATH).getAbsolutePath());
        }
    }

    /**
     * Copies a set of models from one directory into another.
     * 
     * @param configLoader the configuration loader providing the environment information.
     * @param hotDeployDir the name of the directory where the models reside (from-location).
     * @throws MDDException is thrown if the installation fails.
     */
    private static void pasteModels(ConfigLoader configLoader, String hotDeployDir) throws MDDException {
        try {
            Util.copyDirectory(configLoader.findWritableFile(hotDeployDir),
                    configLoader.findWritableFile(ModelStoreImpl.MODEL_REPO_PATH));
        } catch (IOException e) {
            throw new MDDException(String.format("Could not copy models from backup-directory %s to %s.",
                    hotDeployDir, ModelStoreImpl.MODEL_REPO_PATH));
        }
    }

    /**
     * Copies a set of models from the default backup directory ({@code MODEL_REPO_BACKUP} into the model repository
     * path. The model repository path {@code ModelStoreImpl.MODEL_REPO_PATH} depends on the environment. Existing
     * models are overwritten.
     * 
     * @param configLoader the configuration loader which provides the environment information (e.g. test env.)
     * @throws MDDException is thrown if copying failed.
     */
    public static void pasteBackupModels(ConfigLoader configLoader) throws MDDException {
        pasteModels(configLoader, MODEL_REPO_BACKUP);
    }

    /**
     * This method provides information about the installed models in the central repo-store. The information is very
     * condensed and provided as a printable string.
     * 
     * @param configLoader the configuration loader providing the environment information.
     * @return a short summary about the current model repo store and its contained models.
     * @throws MDDException is thrown if the installation fails.
     */
    public static String getSummary(ConfigLoader configLoader) throws MDDException {
        String res = "";
        List<MetaModel> models = new AdvancedModelStoreImpl(configLoader).findAllMetaModels();
        for (MetaModel mm : models) {
            res += String.format("%40s <%s>\n", mm.getName(), mm.getNamespace());
        }
        return res;
    }
}
