package org.cdlflex.mdd.modelrepo;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.umltuowl.harmonizer.PruneStrategySemanticEditor;
import org.cdlflex.mdd.umltuowl.main.Main;
import org.cdlflex.mdd.umltuowl.main.ParameterHandling;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.utils.SettingsReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of advanced model repository access methods.
 */
public class AdvancedModelStoreImpl extends ModelStoreImpl implements AdvancedModelStore {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdvancedModelStoreImpl.class);
    private static final String UMLTUOWL_HOME = "umltuowl/";

    private static final String SETTINGS_BARE = "settings-bare.properties";
    private static final String SETTINGS_MERGE = "settings-merge.properties";

    protected static final String JENA_CONFIG = "jena-config.properties";

    private static final String EXT_TEMPLATE = "extended_template.owl";
    private static final String EXT_TEMPL_PLACEHOLDER_EXT = "__NAMESPACE_EXT__";
    private static final String EXT_TEMPL_PLACEHOLDER_BSE = "__NAMESPACE_BASE__";

    public AdvancedModelStoreImpl(ConfigLoader configLoader) throws MDDException {
        super(configLoader);
    }

    private void persistOWLMetaModel(MetaModel mm, boolean merge, String filePath) throws MDDException {
        initTransformation(merge, mm, filePath);
        Main.mainWithModel(mm, new String[] {});
    }

    private void initTransformation(boolean merge, MetaModel mm, String optionalOutput) throws MDDException {
        String settingsFile = merge ? SETTINGS_MERGE : SETTINGS_BARE;
        Properties settings = this.configLoader.findPropertyFile(UMLTUOWL_HOME + settingsFile);
        NamingManager namingManager = new NamingManager(mm);
        if (settings == null) {
            throw new MDDException(String.format("Could not find file %s.", UMLTUOWL_HOME + settingsFile));
        }
        try {
            SettingsReader.loadSettingsFromExternalFile(settings);
            SettingsReader.overwriteProperty(ParameterHandling.IRI, namingManager.getNamespace());
            SettingsReader.overwriteProperty(ParameterHandling.OUTPUT, optionalOutput);
        } catch (IOException e) {
            throw new MDDException(e.getMessage());
        }
    }

    @Override
    public void persistOWLMetaModel(MetaModel mm, boolean merge) throws MDDException {
        NamingManager namingManager = new NamingManager(mm);

        this.cleanOwlModels(namingManager.getNamspacePrefix()); // clean directory from previous models.
        this.persistMetaModel(mm); // persist meta-model in internal format (json).

        // persist reconstructible model
        String path = NamingManager.getOWLHome(configLoader) + namingManager.getNamspacePrefix() + ".owl";

        // clone required because otherwise modified metamodel is returned (may cause double prefixes).
        this.persistOWLMetaModel(mm.clone(), false, path);
        // the prefixes have already been added to the meta model. be aware that the prefixing
        // does not happen twice

        path = namingManager.getPhysicalOWL(configLoader);
        this.persistOWLMetaModel(mm.clone(), true, path);

        path = namingManager.getPhysicalOWLExtended(configLoader);
        File extended = new File(path);
        if (!extended.exists()) {
            // new
            String tmplContent = this.provideExtTemplate(namingManager);
            File newFile = new File(path);
            try {
                newFile.createNewFile();
                FileUtils.write(newFile, tmplContent);
            } catch (IOException e) {
                throw new MDDException(String.format("Failed: %s.", e.getMessage()));
            }

        }
    }

    @Override
    public void removeMetaModel(String name) throws MDDException {
        MetaModel mm = super.findMetaModel(name);
        super.removeMetaModel(name);
        this.cleanOwlModels(name);
        this.removePhysicalOWLModels(mm);
    }

    @Override
    public void removeAllMetaModels() throws MDDException {
        super.removeAllMetaModels();
        try {
            FileUtils
                    .deleteDirectory(new File(ConfigLoader.getUrl(NamingManager.getOWLHome(configLoader)).getPath()));
            try {
                FileUtils.cleanDirectory(new File(ConfigLoader.getUrl(NamingManager.getDBHome(configLoader))
                        .getPath()));
            } catch (IOException | RuntimeException ex) {
                LOGGER.debug(ex.getMessage());
            }
            this.removePhysicalJenaTDBModels();
        } catch (IOException e) {
            throw new MDDException(e.getMessage());
        }
    }

    @Override
    public void cleanAllPhysicalInstances(boolean onlyTDB) throws MDDException {
        try {
            if (!onlyTDB) {
                FileUtils.cleanDirectory(new File(NamingManager.getDBHome(configLoader)));
            }
        } catch (IOException | RuntimeException ex) {
            LOGGER.debug(ex.getMessage());
        }
        this.removePhysicalJenaTDBModels();
    }

    private String provideExtTemplate(NamingManager namingManager) throws MDDException {
        String tmplHome = NamingManager.getTemplatesHome(configLoader);
        String extTempl = tmplHome + EXT_TEMPLATE;
        File extTemplFile = new File(extTempl);
        try {
            String tmpl = FileUtils.readFileToString(extTemplFile);
            tmpl = tmpl.replaceAll(EXT_TEMPL_PLACEHOLDER_EXT, namingManager.getExtendedNamespace());
            tmpl = tmpl.replaceAll(EXT_TEMPL_PLACEHOLDER_BSE, namingManager.getNamespace());
            return tmpl;
        } catch (IOException e) {
            throw new MDDException(String.format("Cannot find template %s.", extTempl));
        }
    }

    @Override
    public MetaModel retrieveMetaModelCleaned(MetaModel mm) throws MDDException {
        this.initTransformation(false, mm, "");
        mm = Main.loadMetaModel(new String[] {});
        return mm;
    }

    @Override
    public MetaModel retrieveMetaModelMerged(MetaModel mm) throws MDDException {
        this.initTransformation(true, mm, "");
        mm = new PruneStrategySemanticEditor().applyStrategy(mm); // first prune to avoid several static
        mm = Main.harmonizeModel(mm);
        return mm;
    }

    /**
     * clean the OWL models which are used to persist the data models, but do not store real individual data.
     * 
     * @param name
     * @throws MDDException
     */
    private void cleanOwlModels(String name) throws MDDException {
        File fileDir = new File(NamingManager.getOWLHome(configLoader));
        if (fileDir.isDirectory()) {
            File[] files = fileDir.listFiles();
            for (File file : files) {
                if (file.getName().toLowerCase().equals(name.toLowerCase() + ".owl")) {
                    file.delete();
                } else if (file.getName().toLowerCase().startsWith(name.toLowerCase() + "_")) {
                    file.delete();
                }
            }
        }
    }

    /**
     * remove the OWL models that actually contain real individuals (data-store).
     * 
     * @param mm
     * @throws MDDException
     */
    private void removePhysicalOWLModels(MetaModel mm) throws MDDException {
        NamingManager namingManager = new NamingManager(mm);

        File ont = new File(namingManager.getPhysicalOWL(configLoader));
        ont.delete();

        File extOnt = new File(namingManager.getPhysicalOWLExtended(configLoader));
        extOnt.delete();

    }

    /**
     * try to remove the entire Jena TDB files if the config file is available and if Jena TDB is used.
     * @param jena configuration properties for jena.
     * @throws MDDException
     */
    private void removePhysicalJenaTDBModels(Properties jena) throws MDDException {
        String tech = jena.getProperty("jenaStorageTechnology");
        if (tech != null && tech.toLowerCase().trim().equals("tdb")) {
            String jenaTDBDir = configLoader.extractFilePath(jena.getProperty("host"));
            try {
                jenaTDBDir = ConfigLoader.getUrl(jenaTDBDir).getFile();
                File jenaTDB = new File(jenaTDBDir);
                if (jenaTDB.exists()) {
                    FileUtils.cleanDirectory(jenaTDB);
                }
            } catch (IOException e) {
                String msg =
                    String.format("Could not clean Jena TDB database directory at %s (details: %s)", 
                            jenaTDBDir, e.getMessage());
                // Windows: http://stackoverflow.com/questions/4179145/release-java-file-lock-in-windows
                if (System.getProperty("os.name").toLowerCase().contains("win")) {
                    LOGGER.warn(
                            "{}. Because the OS is Windows the lock cannot be removed. Please delete the files "
                                + "manually.", msg);
                } else {
                    throw new MDDException(msg);
                }
            }
        }
    }
    
    /**
     * try to remove the entire Jena TDB files if the config file is available and if Jena TDB is used.
     * @throws MDDException
     */
    private void removePhysicalJenaTDBModels() throws MDDException {
        Properties jena = configLoader.findPropertyFile(JENA_CONFIG);
        if (jena != null) {
            removePhysicalJenaTDBModels(jena);
        }
    }
}
