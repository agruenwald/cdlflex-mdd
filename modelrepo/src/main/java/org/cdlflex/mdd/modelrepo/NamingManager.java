package org.cdlflex.mdd.modelrepo;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.commons.lang.StringUtils;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;

/**
 * Manages the naming of meta models, their namespaces etc. and the access of those properties, based on the available
 * information of models.
 */
public class NamingManager {
    private final MetaModel mm;
    public static final String DEFAULT_NAMESPACE = "http://www.cdlflex.org/";
    private static final String EXT_NAMESPACE = "ext";

    private static final String OWL_PATH = "owl_models";
    private static final String DB_STORE = "db_store";
    private static final String TEMPLATES = "templates";

    public NamingManager(MetaModel mm) {
        this.mm = mm;
    }

    /**
     * Return the full path of the OWL ontology home (internal model repository
     * directory where the models, persisted in OWL format reside).
     * @param configLoader the configuration loader which includes information about the 
     * type of environment and the specific locations.
     * @return the absolute path to the OWL model home directory. 
     * @throws MDDException is thrown if the access fails or some variables were not
     * initialized properly.
     */
    protected static String getOWLHome(ConfigLoader configLoader) throws MDDException {
        return configLoader.getHomeDir() + "/" + ModelStoreImpl.MODEL_REPO_PATH + "/" + OWL_PATH + "/";
    }

    /**
     * Return the full path of the path where the (semantic) data bases reside.
     * @param configLoader the configuration loader which includes information about the 
     * type of environment and the specific locations.
     * @return the absolute path to the directory where the data bases reside. 
     * @throws MDDException is thrown if the access fails or some variables were not
     * initialized properly.
     */
    protected static String getDBHome(ConfigLoader configLoader) throws MDDException {
        return configLoader.getHomeDir() + "/" + ModelStoreImpl.MODEL_REPO_PATH + "/" + DB_STORE + "/";
    }

    /**
     * Get the template directory within the model repository. For instance,
     * for the extended OWL ontology files a template exists, which can be reused
     * for the creation of new ontologies with base and extended OWL files 
     * (to separate T-Box and A-Box).
     * @param configLoader the configuration loader which includes information about the 
     * type of environment and the specific locations.
     * @return the absolute path to the template directory.
     * @throws MDDException is thrown if the access fails or some variables were not
     * initialized properly.
     */
    protected static String getTemplatesHome(ConfigLoader configLoader) throws MDDException {
        return configLoader.getHomeDir() + "/" + ModelStoreImpl.MODEL_REPO_PATH + "/" + TEMPLATES + "/";
    }

    /**
     * Check whether a path is correct and accessible.
     * @param path the path to a URL or a file / directory.
     * @return the absolute path (complete)
     * @throws MDDException is thrown if the path is incorrect.
     */
    protected static String toStr(String path) throws MDDException {
        try {
            return new File(ConfigLoader.getUrl(path).toURI().getPath()).getAbsolutePath();
        } catch (IOException | URISyntaxException e) {
            throw new MDDException(e.getMessage());
        }
    }

    /**
     * get the namespace of a model.
     * 
     * @return the namespace, either derived from the name, or taken from the explicitly stated namespace.
     * @throws MDDException is thrown if the path is incorrect.
     */
    public String getNamespace() throws MDDException {
        return this.getNamespace(false);
    }

    /**
     * get the extended namespace, which typically provides additional model constraints (default by experts) and/or
     * individuals.
     * @return the entire address of the extended namespace.
     * @throws MDDException is thrown if the path is incorrect.
     */
    public String getExtendedNamespace() throws MDDException {
        return this.getNamespace(true);
    }

    private String getNamespace(boolean isExtendedNamespace) throws MDDException {
        if (mm.getNamespace() == null || mm.getNamespace().isEmpty()) {
            String name = mm.getName();
            name = org.cdlflex.mdd.umltuowl.utils.Util.metaNameConvertion(name, "", "", true);
            return DEFAULT_NAMESPACE + (isExtendedNamespace ? EXT_NAMESPACE : "") + name + ".owl";
        } else {
            if (isExtendedNamespace) {
                String namespace = mm.getNamespace();
                String prefix = this.getNamspacePrefix();
                namespace =
                    StringUtils.replaceOnce(namespace, "/" + prefix + ".owl", "/" + EXT_NAMESPACE + prefix + ".owl");
                return namespace;
            } else {
                return mm.getNamespace();
            }
        }
    }

    /**
     * get the namespace prefix of a model in accordance with {@code getNamespace()}.
     * @return the namespace prefix.
     * @throws MDDException is thrown if the path is incorrect.
     */
    public String getNamspacePrefix() throws MDDException {
        String namespace = this.getNamespace();
        return this.getNamespacePrefix(namespace);
    }

    private String getNamespacePrefix(String namespace) throws MDDException {
        String prefix = "";
        int start = StringUtils.lastIndexOf(namespace, "/");
        int end = StringUtils.lastIndexOf(namespace, ".");
        if (start < 0 || end < 0 || (end <= start)) {
            throw new MDDException(String.format("Could not extract namespace prefix from %s.", namespace));
        } else {
            prefix = StringUtils.substring(namespace, start + 1, end);
        }
        return prefix;
    }

    /**
     * Get the extended namespace prefix of a model.
     * @return the namespace prefix as a string (only the extended prefix)
     * @throws MDDException is thrown if the path is incorrect.
     */
    public String getExtendedNamespacePrefix() throws MDDException {
        String extnamespace = this.getExtendedNamespace();
        return this.getNamespacePrefix(extnamespace);
    }

    /**
     * Get access to the physical OWL file.
     * @param configLoader the configuration loader.
     * @return the file path.
     * @throws MDDException is thrown if the path is incorrect.
     */
    public String getPhysicalOWL(ConfigLoader configLoader) throws MDDException {
        String filePath = getDBHome(configLoader) + this.getNamspacePrefix() + ".owl";
        return filePath;
    }

    /**
     * Get access to the extended physical OWL file.
     * @param configLoader the configuration loader.
     * @return the file path to the extended OWL.
     * @throws MDDException is thrown if the path is incorrect.
     */
    public String getPhysicalOWLExtended(ConfigLoader configLoader) throws MDDException {
        String filePath = getDBHome(configLoader) + this.getExtendedNamespacePrefix() + ".owl";
        return filePath;
    }

    /**
     * get the file where the internal (json) model is stored.
     * 
     * @param configLoader the configuration loader.
     * @return absolute path to the internal model file.
     * @throws MDDException is thrown if the path is incorrect.
     */
    public File getPyhsicalInternalFile(ConfigLoader configLoader) throws MDDException {
        return new ModelStoreImpl(configLoader).getInternalModelFile(mm.getNamespacePrefix());
    }
}
