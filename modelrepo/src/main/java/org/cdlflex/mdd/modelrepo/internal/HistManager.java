package org.cdlflex.mdd.modelrepo.internal;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manages the history of stored meta models. The main purpose is to backup models to prevent unintended data loss. In
 * future it can also be used to implement model versioning.
 */
public class HistManager {
    private static final int HISTORY_MAX = 10;
    private static final String BACKUP = "history";
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
    private static final Logger LOGGER = LoggerFactory.getLogger(HistManager.class);

    /**
     * Clean the history of changes (for each manual change in a model, a history
     * version is maintained).
     * @param modelFile The model file (internal).
     * @param coreName the namespace prefix of the model (e.g., itpm).
     * @throws MDDException is throw if the history cannot be cleaned.
     */
    public void cleanHistory(File modelFile, String coreName) throws MDDException {
        File sembase = modelFile.getParentFile();
        File backupDir = new File(String.format("%s/%s_%s", sembase.getAbsolutePath(), BACKUP, coreName));
        if (backupDir.exists()) {
            try {
                FileUtils.deleteDirectory(backupDir);
            } catch (IOException e) {
                throw new MDDException(String.format("Could not delete dir %s.", backupDir));
            }
        }
    }

    /**
     * Save the previous version of the internal model in history. Only a specific
     * number of history files is maintained (internal logic).
     * @param actJsonFile the current version of the internal model.
     * @throws IOException is thrown if the file was not persisted in history
     * correctly.
     */
    public void saveHistory(File actJsonFile) throws IOException {
        checkActFileValidity(actJsonFile);
        String simpleFileName = actJsonFile.getName();
        File dir = actJsonFile.getParentFile();
        String coreName = simpleFileName.replace(".json", ""); // itpm
        String dateStr = sdf.format(new Date());
        String parentDir = String.format("%s/%s_%s", dir.getAbsoluteFile(), BACKUP, coreName);
        File historyDir = new File(parentDir);
        if (actJsonFile.exists()) { // only copy if existing (for the first time there may exist no file)
            String target = String.format("%s/%s%s.json", parentDir, coreName, dateStr);
            if (!historyDir.exists()) {
                historyDir.mkdir();
            }
            LOGGER.trace(String.format("target: %s.", target));
            Util.copyFile(actJsonFile, new File(target));
        }

        List<File> histFiles = new LinkedList<File>();
        for (File f : historyDir.listFiles()) {
            if (!f.getName().endsWith(".json") || !f.getName().contains(coreName)) {
                continue; // skip
            }
            histFiles.add(f);
        }
        
        Collections.sort(histFiles, new Comparator<File>() {
            public int compare(File f1, File f2) {
                return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified()) * (-1); // newest files are first
            }
        });

        // remove the eldest files
        while (histFiles.size() > HISTORY_MAX) {
            File histFile = histFiles.get(histFiles.size() - 1);
            histFile.delete();
            LOGGER.info(String.format("Removed history file %s.", histFile));
            histFiles.remove(histFiles.size() - 1); // remove the last file as long as there are more than HISTORY_MAX
                                                    // files available.
        }
    }
    
    private void checkActFileValidity(File actJsonFile) throws IOException {
        if (actJsonFile.exists() && !actJsonFile.isFile()) {
            throw new IOException(String.format("Could not save json history: %s is not a file.", actJsonFile));
        }
        File dir = actJsonFile.getParentFile();
        if (!dir.isDirectory()) {
            throw new IOException(String.format(
                    "Could not detect parent directory of json file for saving history (%s, %s)", actJsonFile,
                    actJsonFile.getParent()));
        }

        String simpleFileName = actJsonFile.getName();
        if (!simpleFileName.endsWith(".json")) {
            throw new IOException(String.format("Output file must be a JSON file (ending .json!): %s.",
                    simpleFileName));
        }


    }
}
