package org.cdlflex.mdd.modelrepo.internal;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Simple internal utility class.
 */
public final class Util {
    private Util() {
        
    }
    
    /**
     * Read a file content as a string.
     * @param file the file to read.
     * @return the content of the file.
     * @throws FileNotFoundException is thrown if during the access of the file
     * an error occurs.
     */
    public static String readFileContent(File file) throws FileNotFoundException {
        String content = "";
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()) {
            content += sc.nextLine();
        }
        sc.close();
        return content;
    }
}
