package org.cdlflex.mdd.modelrepo;

import org.cdlflex.mdd.sembase.config.ConfigLoader;

/**
 * A class which encapsulates the installation configuration.
 */
public class InstallConfiguration {
    private ConfigLoader configLoader;
    private String input;
    private String converterName;
    private String optionalIRI;
    private boolean merge;
    
    /**
     * Get the configuration loader.
     * @return the configLoader object.
     */
    public final ConfigLoader getConfigLoader() {
        return configLoader;
    }
    /**
     * Set the configuration loader environment.
     * @param configLoader the configLoader to set
     */
    public final void setConfigLoader(ConfigLoader configLoader) {
        this.configLoader = configLoader;
    }
    /**
     * get the input of the file.
     * @return the input
     */
    public final String getInput() {
        return input;
    }
    /**
     * Set the input XMI file (UML format supported by umlTUowl).
     * @param input the input to set
     */
    public final void setInput(String input) {
        this.input = input;
    }
    
    /** get the converter name according to umlTUowl.
     * @return the converterName.
     */
    public final String getConverterName() {
        return converterName;
    }
    
    /**
     * Set the converter name according to umlTUowl.
     * @param converterName the converterName to set
     */
    public final void setConverterName(String converterName) {
        this.converterName = converterName;
    }
    
    /** get the optional IRI.
     * @return the optionalIRI.
     */
    public final String getOptionalIRI() {
        return optionalIRI;
    }
    
    /**
     * Set the optional IRI.
     * @param optionalIRI the optionalIRI to set
     */
    public final void setOptionalIRI(String optionalIRI) {
        this.optionalIRI = optionalIRI;
    }
    
    /**
     * Get the merge parameter.
     * @return the merge parameter (true if merge activated).
     */
    public final boolean isMerge() {
        return merge;
    }
    
    /**
     * Set the merge parameter.
     * @param merge the merge to set
     */
    public final void setMerge(boolean merge) {
        this.merge = merge;
    }
 
}
