package org.cdlflex.mdd.modelrepo;

import java.util.List;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
/**
 * Basic operations to access the model store, which includes all
 * meta models (umlTUowl format and OWL ontology format).
 */
public interface ModelStore {
    /**
     * Find a specific meta model.
     * @param name the name of the meta model, eg. itpm
     * @return the internal meta model in umlTUowl format.
     * @throws MDDException is thrown if the model does not exist or cannot be accessed.
     */
    MetaModel findMetaModel(String name) throws MDDException;

    /**
     * Find the (human-readable) names of the meta-models and return them in alphabetic order. For instance itpm is
     * returned if the model is stored as itpm.json. Should be used because of performance advantages over
     * {@code findAllMetaModelNames()}.
     * 
     * @return a list of the names of the meta-models.
     * @throws MDDException is thrown if all meta models cannot be accessed.
     */
    List<String> findAllMetaModelNames() throws MDDException;

    /**
     * Retrieve all meta models.
     * @return a list of all existing meta models.
     * @throws MDDException is thrown if all meta models cannot be accessed.
     */
    List<MetaModel> findAllMetaModels() throws MDDException;

    /**
     * Persist a specific meta model.
     * @param mm the meta model in umlTUowl format.
     * @throws MDDException is thrown if the model cannot be persisted.
     */
    void persistMetaModel(MetaModel mm) throws MDDException;

    /**
     * Remove a specific meta model.
     * @param name the name of the meta model, eg. itpm
     * @throws MDDException is thrown if the model cannot be removed from the repository.
     */
    void removeMetaModel(String name) throws MDDException;

    /**
     * remove all meta-models and their databases (if possible). this includes the deletion of the OWLAPI files and the
     * Jena TDB files if they are accessible.
     * @throws MDDException is thrown if the models cannot be removed from the repository.
     */
    void removeAllMetaModels() throws MDDException;
}
