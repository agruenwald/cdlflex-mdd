package org.cdlflex.mdd.modelrepo;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;

/**
 * Interface for extended model repository access.
 */
public interface AdvancedModelStore extends ModelStore {

    /**
     * persist a meta model in OWL format.
     * 
     * @param mm the meta model, containing 0 .. n meta packages.
     * @param merge if true then all the meta packages are merged into a single one. The names are merged/prefixed by
     *        default as well.
     * @throws MDDException is thrown if the operation fails.
     */
    void persistOWLMetaModel(MetaModel mm, boolean merge) throws MDDException;

    /**
     * Retrieve a meta model from the repository, but clean it additionally for code generation (remove spaces, etc.).
     * 
     * @param mm the meta model, containing 0 .. n meta packages.
     * @return the cleaned meta model.
     * @throws MDDException is thrown if the operation fails.
     */
    MetaModel retrieveMetaModelCleaned(MetaModel mm) throws MDDException;

    /**
     * retrieve a meta model from the repository and merge its packages into a single package. Additionally,
     * packages/classes/attributes with irrelevant status (for the code generation and for the ontology store) are
     * pruned.
     * 
     * @param mm the meta model, containing 0 .. n meta packages.
     * @return the merged meta model.
     * @throws MDDException is thrown if the operation fails.
     */
    MetaModel retrieveMetaModelMerged(MetaModel mm) throws MDDException;

    /**
     * Clean data (instances) from DB's if possible but do not touch models themselves.
     * 
     * @param onlyTDB if true (default) then only the DB instances from complex database systems, such as TDB are
     *        cleaned not the serial OWL DB which is basically an extended mirror of the XML models.
     * @throws MDDException is thrown if the operation fails.
     */
    void cleanAllPhysicalInstances(boolean onlyTDB) throws MDDException;
}
