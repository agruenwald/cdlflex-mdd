package org.cdlflex.mdd.modelrepo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.cdlflex.mdd.modelrepo.internal.HistManager;
import org.cdlflex.mdd.modelrepo.internal.Util;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;
import org.cdlflex.mdd.umltuowl.metamodeltoowl.JsonMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The implementation of the abstract interface to access the 
 * model store to persist model repositories in several
 * formats (internal umlTUowl format, OWL ontologies).
 */
public class ModelStoreImpl implements ModelStore {
    protected final ConfigLoader configLoader;
    public static final String MODEL_REPO_PATH = "modelrepo";
    private final File repoDir;
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelStoreImpl.class);

    private static final String FILE_ENDING = ".json";

    public ModelStoreImpl(ConfigLoader configLoader) throws MDDException {
        this.configLoader = configLoader;
        File repoDirI = this.configLoader.findWritableFile(MODEL_REPO_PATH);
        if (repoDirI == null) {
            throw new MDDException(String.format("Model repo %s not found.", MODEL_REPO_PATH));
        }
        this.repoDir = repoDirI;
    }

    private String createName(String name) {
        return MODEL_REPO_PATH + "/" + name + FILE_ENDING;
    }

    /**
     * Get the model in internal (umlTUowl) format.
     * @param name the name of the model, eg. itpm
     * @return the file containing the model.
     * @throws MDDException is thrown if access to the model repository fails.
     */
    public File getInternalModelFile(String name) throws MDDException {
        File fModel = this.configLoader.findWritableFile(createName(name));
        return fModel;
    }

    @Override
    public MetaModel findMetaModel(String name) throws MDDException {
        File fModel = this.configLoader.findWritableFile(createName(name));
        if (fModel == null) {
            return null;
        } else {
            MetaModel model = null;
            try {
                model = JsonMaker.toMetaModel(Util.readFileContent(fModel));
            } catch (FileNotFoundException e) {
                LOGGER.info("do nothing, return empty model.");
            }
            return model;
        }
    }
    
    @Override
    public void persistMetaModel(MetaModel mm) throws MDDException {
        NamingManager nm = new NamingManager(mm);
        String name = nm.getNamspacePrefix();
        File f = new File(this.configLoader.getHomeDir() + "/" + this.createName(name));
        try {
            JsonMaker.toJsonFile(mm, f.getAbsolutePath()); // create file
            new HistManager().saveHistory(f); // manage history
        } catch (IOException e) {
            throw new MDDException(String.format("Cannot save meta model %s. (%s)", name, e.getMessage()));
        }
    }

    @Override
    public void removeMetaModel(String name) throws MDDException {
        File fModel = this.configLoader.findWritableFile(createName(name));
        if (fModel == null) {
            throw new MDDException(String.format("Could not find model %s.", name));
        } else {
            fModel.delete();
            new HistManager().cleanHistory(fModel, name);
        }
    }

    private List<File> filterModels(File[] files) {
        List<File> res = new LinkedList<File>();
        for (File file : files) {
            if (file.isFile() && !file.isHidden()) {
                res.add(file);
            }
        }
        return res;
    }

    @Override
    public List<String> findAllMetaModelNames() throws MDDException {
        List<File> files = filterModels(repoDir.listFiles());
        List<String> result = new LinkedList<String>();
        for (File file : files) {
            String name = extractName(file.getName());
            result.add(name);
        }
        Collections.sort(result);
        return result;
    }

    private String extractName(String wholeName) {
        String name = wholeName.replaceAll(FILE_ENDING + "$", "");
        return name;
    }

    @Override
    public List<MetaModel> findAllMetaModels() throws MDDException {
        List<File> files = filterModels(repoDir.listFiles());
        List<MetaModel> result = new LinkedList<MetaModel>();
        for (File file : files) {
            try {
                MetaModel model = JsonMaker.toMetaModel(Util.readFileContent(file));
                result.add(model);
            } catch (FileNotFoundException e) {
                LOGGER.error(e.getMessage()); // cannot happen
            }
        }
        Collections.sort(result);
        return result;
    }

    @Override
    public void removeAllMetaModels() throws MDDException {
        List<File> files = filterModels(repoDir.listFiles());
        for (File file : files) {
            file.delete();
            String name = extractName(file.getName());
            new HistManager().cleanHistory(file, name);
            LOGGER.debug("Removed model from store: {}.", file);
        }
    }
}
