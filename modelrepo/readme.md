# CDL FLEX :: MDD :: Modelrepo

The model repository maintains the Meta-Models which are created within the semantic
code generation approach. These models are usually located at ${SEMBASE_HOME} or
{karaf.home} (Java environment variable) which typically point to the
`custom-itpm/assembly/src/main/resources` directory. The modelrepo maintains the 
installation, backup, history and retrieval of meta-models based on the umlTUowl-internal
structure (bridge between UML and OWL). The modelrepo contains the following artifacts for
a specific model:

+ examplemodel.json: The internal representation of the meta-model in JSON format, typically
created with the Semantic Meta Editor which is available in `custom-itpm-standalone/standalone-webui`
+ history_examplemodel/: a list of json files representing the latest models (fail-safe).
+ owl_models/examplemodel.owl: The representation of the model in reusable OWL.
+ db_store/*: two OWL files for each model: examplemodel.owl and extexamplemodel.owl. Opposed
to the OWL-file in `owl_models/` the concept names are prefixed and optimized for the
storage and retrieval of instances.

Depending on the environment (test or live) different model repo home directories will
be accessed.

## Further Information
More documentation can be found at Confluence under [Custom ITPM](http://cdl-ind.ifs.tuwien.ac.at/confluence/display/UC/Custom+ITPM 
"Custom ITPM"). 
