# CDL FLEX :: MDD :: Sembase OWLAPI

This module contains classes and tests to access the OWLAPI in a systematic way.
Wrappers to enable query access via 
+ SPARQL
+ SPARQL-DL (simplified SPARQL algorithm)
+ OWLDL
+ SWRL

The pellet reasoner is shipped within the package and enables reasoning and
consistency checks of ontologies.

Note that Apache Jena and the Pellet OWL API reasoner are incompatible. Hence
the OWLAPI (incl. Pellet) and the Jena API are kept separate and can never be 
included together.

## Access mechanism OWL-files
Each ontology model consists of two files: 
+ Basic OWL file which contains the concepts (A-BOX, C-BOX)
+ An extended OWL file which is intended to maintain manual extensions and instances (T-BOX).
When accessing an ontology both files are read. The basic OWL file is imported into the extended OWL
file. While the basic OWL file is never modified the extensions are placed in the extended
file.

## Installation requirements
It is essential that the modelrepo within the sembase configuration
directory (typically included in custom-itpm/assembly/src/main/resources/sembase) is
available. Ensure that all files and sub-files are equipped with write-permissions. 
The test directory is used for (unit) tests, while the live-directory is used in running
environments.