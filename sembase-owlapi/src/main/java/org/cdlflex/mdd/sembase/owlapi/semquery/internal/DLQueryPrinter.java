package org.cdlflex.mdd.sembase.owlapi.semquery.internal;

import java.util.Set;

import org.cdlflex.mdd.sembase.semquery.QueryInterface;
import org.semanticweb.owlapi.expression.ParserException;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.util.ShortFormProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper class for printing DL queries.
 */
class DLQueryPrinter {
    private static final Logger LOGGER = LoggerFactory.getLogger(QueryInterface.class);
    private DLQueryEngine dlQueryEngine;
    private ShortFormProvider shortFormProvider;
    private static final int FIFTY = 50;
    
    /**
     * Instantiate a query object.
     * @param engine the engine
     * @param shortFormProvider the short form provider
     */
    public DLQueryPrinter(DLQueryEngine engine, ShortFormProvider shortFormProvider) {
        this.shortFormProvider = shortFormProvider;
        dlQueryEngine = engine;
    }

    /**
     * Perform a query.
     * @param classExpression the class expression to use for interrogation
     * @param printOn if true then the message is printed (logged).
     */
    public void askQuery(String classExpression, boolean printOn) {
        if (classExpression.length() == 0) {
            LOGGER.info("No class expression specified");
        } else {
            try {
                StringBuilder sb = new StringBuilder();
                if (printOn) {
                    sb.append("\n--------------------------------------------------------------------------------\n");
                    sb.append("QUERY:   ");
                    sb.append(classExpression);
                    sb.append("\n");
                    sb.append("--------------------------------------------------------------------------------\n\n");
                }
                // Ask for the subclasses, superclasses etc. of the specified
                // class expression. Print out the results.
                Set<OWLClass> superClasses = dlQueryEngine.getSuperClasses(classExpression, true);
                if (printOn) {
                    printEntities("SuperClasses", superClasses, sb);
                }

                Set<OWLClass> equivalentClasses = dlQueryEngine.getEquivalentClasses(classExpression);
                if (printOn) {
                    printEntities("EquivalentClasses", equivalentClasses, sb);
                }
                Set<OWLClass> subClasses = dlQueryEngine.getSubClasses(classExpression, true);
                if (printOn) {
                    printEntities("SubClasses", subClasses, sb);
                }
                Set<OWLNamedIndividual> individuals = dlQueryEngine.getInstances(classExpression, true);
                if (printOn) {
                    printEntities("Instances", individuals, sb);
                    LOGGER.info(sb.toString());
                }
            } catch (ParserException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

    private void printEntities(String name, Set<? extends OWLEntity> entities, StringBuilder sb) {
        sb.append(name);
        int length = FIFTY - name.length();
        for (int i = 0; i < length; i++) {
            sb.append(".");
        }
        sb.append("\n\n");
        if (!entities.isEmpty()) {
            for (OWLEntity entity : entities) {
                sb.append("\t");
                sb.append(shortFormProvider.getShortForm(entity));
                sb.append("\n");
            }
        } else {
            sb.append("\t[NONE]\n");
        }
        sb.append("\n");
    }
}
