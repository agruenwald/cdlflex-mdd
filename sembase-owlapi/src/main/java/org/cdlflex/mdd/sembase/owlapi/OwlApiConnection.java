package org.cdlflex.mdd.sembase.owlapi;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;

import org.cdlflex.mdd.modelrepo.AdvancedModelStore;
import org.cdlflex.mdd.modelrepo.AdvancedModelStoreImpl;
import org.cdlflex.mdd.modelrepo.NamingManager;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.Util;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.cdlflex.mdd.umltuowl.metamodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Connector implementation for the OWLAPI.
 */
public class OwlApiConnection implements Connection, Serializable {
    private static final long serialVersionUID = 3748092128260776092L;

    private static final Logger LOGGER = LoggerFactory.getLogger(OwlApiConnection.class);
    private final String ontologySchemaURL;
    private final String extendedOntologyURL;
    private final String ontologySchemaIRI;
    private final String extendedOntologyIRI;
    private final String ontNamespacePrefix;
    private final String extOntNamespacePrefix;

    private final MetaModel metaModel;

    private static final String TMP = "_tmp";
    private static final String ORIGINAL = "_original";

    private SemReasoner semReasoner;
    private OWLDataFactory factory;
    private boolean autoCommitMode;
    private boolean autoCommitTmp;

    private boolean connected;
    private boolean inferring;

    /**
     * Instantiation of a new connector.
     * @param ontologySchemaName the name of the basic ontology, which contains the generated C-Box and T-Box elements.
     *        e.g., itpm (not itpm.owl!)
     * @param extendedOntologyName the name of the extended ontology, which contains additional axioms and the
     *        individuals. E.g., extendeditpm (not extendeditpm.owl!)
     * @param absoluteFilePath the absolute file path to the stored serialized ontologies, e.g.,
     *        /Users/andreas_gruenwald/da-agr/resources/design/
     * @param ontologySchemaIRI the namespace of the base ontology, e.g., http://www.tuwien.ac.at/itpm.owl
     * @param extendedOntologyIRI the namespace of the extended ontology, e.g., http://www.tuwien.ac.at/extendeditpm.owl
     **/
    public OwlApiConnection(String ontologySchemaName, String extendedOntologyName, String absoluteFilePath,
            String ontologySchemaIRI, String extendedOntologyIRI) {

        this.ontologySchemaURL = absoluteFilePath + "/" + ontologySchemaName + ".owl";
        this.extendedOntologyURL = absoluteFilePath + "/" + extendedOntologyName + ".owl";

        this.ontNamespacePrefix = new MetaModel("", ontologySchemaIRI).getNamespacePrefix();
        this.extOntNamespacePrefix = new MetaModel("", extendedOntologyIRI).getNamespacePrefix();
        this.extendedOntologyIRI = extendedOntologyIRI;
        this.ontologySchemaIRI = ontologySchemaIRI;
        this.autoCommitMode = true;
        this.connected = false;
        this.inferring = false;
        metaModel = null;
        autoCommitTmp = true;
    }

    /**
     * Create a new instance of an OWL API connection.
     * @param configLoader the configuration loader to acces the specific environments.
     * @param projectId the projectId (by default empty).
     * @param ontNamespacePrefix the namespace prefix of the ontology (e.g., itpm, or kb)
     * @throws MDDException is thrown if the connection cannot be established correctly.
     */
    public OwlApiConnection(ConfigLoader configLoader, String projectId, 
        String ontNamespacePrefix) throws MDDException {
        AdvancedModelStore store = new AdvancedModelStoreImpl(configLoader);

        metaModel = store.findMetaModel(ontNamespacePrefix);
        if (metaModel == null) {
            throw new MDDException(String.format("Failed to load ontology model \"%s\".", ontNamespacePrefix));
        }

        NamingManager nm = new NamingManager(metaModel);
        this.ontologySchemaURL = nm.getPhysicalOWL(configLoader);
        this.extendedOntologyURL = nm.getPhysicalOWLExtended(configLoader);
        this.ontologySchemaIRI = nm.getNamespace();
        this.extendedOntologyIRI = nm.getExtendedNamespace();

        this.ontNamespacePrefix = nm.getNamspacePrefix();
        this.extOntNamespacePrefix = nm.getExtendedNamespacePrefix();

        this.autoCommitMode = true;
        this.connected = false;
        this.inferring = false;
    }

    /**
     * Get the basic ontology file (T-Box).
     * @return the basic ontology file (OWL).
     * @throws URISyntaxException is thrown if the access fails.
     * @throws IOException is thrown if the access fails.
     */
    public File getBaseOntologyFile() throws URISyntaxException, IOException {
        File baseOntology = new File(ConfigLoader.getUrl(ontologySchemaURL).toURI().getPath());
        return baseOntology;
    }

    /**
     * Get the extended ontology file (A-Box,  additional expert assertions).
     * @return the extended ontology file (OWL).
     * @throws URISyntaxException is thrown if the access fails.
     * @throws IOException is thrown if the access fails.
     */
    public File getExtendedFile() throws IOException, URISyntaxException {
        File extendedOntology = new File(ConfigLoader.getUrl(this.extendedOntologyURL).toURI().getPath());
        return extendedOntology;
    }

    /**
     * get the IRI of the ontology.
     * @return the IRI of the ontology.
     */
    public String getOntologySchemaIRI() {
        return ontologySchemaIRI;
    }
    
    /**
     * get the extended IRI of the ontology. 
     * @return the IRI of th extended ontology.
     */
    public String getExtendedOntologyIRI() {
        return extendedOntologyIRI;
    }

    private File getOriginalFile() throws IOException, URISyntaxException {
        File originalFile =
            new File(ConfigLoader
                    .getUrl(new File(this.extendedOntologyURL).getParent() + "/" + this.extOntNamespacePrefix
                        + ORIGINAL + ".owl").toURI().getPath());
        return originalFile;
    }

    private File getTmpFile() throws IOException, URISyntaxException {
        File tmpFile =
            new File(ConfigLoader
                    .getUrl(new File(this.extendedOntologyURL).getParent() + "/" + this.extOntNamespacePrefix + TMP
                        + ".owl").toURI().getPath());
        return tmpFile;
    }

    @Override
    public void connect() throws MDDException {
        if (this.connected) {
            throw new MDDException("Already connected to OWLAPI.");
        }

        try {
            File extendedOntology = getExtendedFile();
            LOGGER.trace(String.format("Connect to %s...", extendedOntology.getAbsolutePath()));
            File tmpFile = getTmpFile();
            File originalFile = getOriginalFile();
            Util.copyFile(extendedOntology, tmpFile);
            /**
             * the very first time the extended ontology is stored as the original file. This enables resets to the
             * original state.
             **/
            if (!originalFile.exists()) {
                Util.copyFile(extendedOntology, originalFile);
            }
            this.connected = true;
        } catch (IOException | URISyntaxException e) {
            String eMsg = String.format("%s (%s)", e.getMessage(), extendedOntologyURL);
            LOGGER.error(eMsg);
            throw new MDDException(eMsg);
        }
    }

    @Override
    public void close() throws MDDException {
        LOGGER.trace(String.format("Close %s...", ontologySchemaURL));
        File tmpFile;
        try {
            tmpFile = this.getTmpFile();
        } catch (IOException | URISyntaxException e) {
            throw new MDDException(e.getMessage());
        }
        tmpFile.delete();
        this.connected = false;
        LOGGER.trace(String.format("Deleted temporary file %s.", tmpFile.getAbsoluteFile()));
    }

    @Override
    public void resetData() throws MDDException {
        this.reset();
    }

    @Override
    public void resetOntology() throws MDDException {
        this.reset();
    }

    /**
     * In the case of the OWLAPI removing all individuals, and reseting the T-Box to its default state can be performed
     * with the same action. Copying a single file is not costly. In the case of other technologies, e.g. Jena this is
     * not the case.
     * 
     * @throws MDDException
     */
    private void reset() throws MDDException {
        File originalFile;
        File extendedOntology;
        try {
            extendedOntology = getExtendedFile();
            LOGGER.trace(String.format("Reset data in %s.", extendedOntology.getAbsolutePath()));
            originalFile = this.getOriginalFile();
        } catch (IOException | URISyntaxException e1) {
            throw new MDDException(e1.getMessage());
        }
        if (!originalFile.exists()) {
            LOGGER.error(String.format("Cannot reset ontology %s, because original file %s does not exist.",
                    this.ontNamespacePrefix, originalFile.getAbsoluteFile()));
        } else {
            try {
                Util.copyFile(originalFile, extendedOntology);
            } catch (IOException e) {
                throw new MDDException(e.getMessage());
            }
        }
        this.semReasoner = null;
    }

    /**
     * Access the central OWL API access factory.
     * @return an object with the factory.
     */
    public OWLDataFactory getFactory() {
        return factory;
    }

    /**
     * Access the reasoning capabilities for the connection.
     * @return the reasoner.
     */
    public SemReasoner getReasoner() {
        return semReasoner;
    }

    @Override
    public void begin() throws MDDException {
        this.beginInternal(true);
    }

    private void beginInternal(boolean setAutocommitTag) throws MDDException {
        if (!this.connected) {
            throw new MDDException("Cannot begin transaction. Not connected yet.");
        }

        if (setAutocommitTag) {
            this.autoCommitTmp = autoCommitMode;
        }

        this.autoCommitMode = false;
        semReasoner = new SemReasoner();
        try {
            factory = semReasoner.createDataFactory(this);
        } catch (OWLOntologyCreationException e) {
            LOGGER.error(e.getMessage());
            throw new MDDException(e.getMessage());
        } catch (OWLOntologyStorageException e2) {
            LOGGER.error(e2.getMessage());
            throw new MDDException(e2.getMessage());
        }
    }

    @Override
    public void autoBegin() throws MDDException {
        if (this.autoCommitMode) {
            this.beginInternal(false);
            this.autoCommitMode = true;
        }
    }

    @Override
    public void commit() throws MDDException {
        this.internalCommit(false);
    }

    private void internalCommit(boolean isAutoCommit) throws MDDException {
        if (!this.connected) {
            throw new MDDException("Cannot commit. Not connected yet.");
        }
        try {
            semReasoner.inferAndSave();
            if (!isAutoCommit) {
                this.autoCommitMode = autoCommitTmp; // set back to original mode
            }
        } catch (OWLOntologyStorageException e) {
            LOGGER.error(e.getMessage());
            throw new MDDException(e.getMessage());
        }
    }

    @Override
    public void rollback() throws MDDException {
        if (!this.connected) {
            throw new MDDException("Cannot rollback. Not connected yet.");
        }
        semReasoner = new SemReasoner();
        try {
            factory = semReasoner.createDataFactory(this);
            this.autoCommitMode = autoCommitTmp; // set back to original mode (added newly)
        } catch (OWLOntologyCreationException e) {
            LOGGER.error(e.getMessage());
            throw new MDDException(e.getMessage());
        } catch (OWLOntologyStorageException e2) {
            LOGGER.error(e2.getMessage());
            throw new MDDException(e2.getMessage());
        }
    }

    @Override
    public void commitReadOnly() throws MDDException {
        if (!this.connected) {
            throw new MDDException("Cannot commit. Not connected yet.");
        }
        // no inference or saving necessary.
    }

    @Override
    public void autoCommit() throws MDDException {
        if (this.autoCommitMode) {
            this.internalCommit(true);
            // this.autoCommitMode=true;
        }

    }

    public boolean isAutoCommitMode() {
        return autoCommitMode;
    }

    public void setAutoCommitMode(boolean autoCommitMode) {
        this.autoCommitMode = autoCommitMode;
    }

    @Override
    public void autoCommitReadOnly() {
        // nothing to do here yet
    }

    @Override
    public void setInferring(boolean inferring) {
        this.inferring = inferring;
    }

    @Override
    public boolean isInferring() {
        return inferring;
    }

    @Override
    public String getNamespacePrefix() {
        return this.ontNamespacePrefix;
    }

    @Override
    public void autoBeginReadOnly() throws MDDException {
        this.autoBegin();
    }

    @Override
    public void connectOnDemand() throws MDDException {
        if (!this.connected) {
            this.connect();
        }
    }

    @Override
    public boolean isConnected() throws MDDException {
        return this.connected;
    }
}
