package org.cdlflex.mdd.sembase.owlapi.semquery.internal;

import java.util.List;

import org.mindswap.pellet.KnowledgeBase;
import org.mindswap.pellet.jena.PelletInfGraph;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.owlapi.SemReasoner;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.sparqldl.jena.SparqlDLExecutionFactory;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import org.cdlflex.mdd.sembase.semquery.QueryMap;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;

/**
 * Implementation SPARQL (DL) access on top of the OWL-API.
 */
public class SPARQLDLImpl extends SPARQLImpl implements QueryInterface {
    private final Connection con;

    public SPARQLDLImpl(Connection con) {
        super(con);
        this.con = con;
    }

    /* example: http://lists.owldl.com/pipermail/pellet-users/2008-December/003215.html */
    @Override
    public List<List<String>> query(String sparqlQuery) throws MDDException {
        sparqlQuery = addPrefix(sparqlQuery);

        SemReasoner ri = new SemReasoner();
        try {
            ri.createDataFactory(con);
        } catch (OWLOntologyStorageException e) {
            e.printStackTrace();
            throw new MDDException(e.getMessage());
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
            throw new MDDException(e.getMessage());
        }

        PelletReasoner owlapiReasoner = null;
        try {
            owlapiReasoner = ri.createReasoner();
        } catch (OWLOntologyStorageException e) {
            e.printStackTrace();
            throw new MDDException(e.getMessage());
        }
        KnowledgeBase kb = owlapiReasoner.getKB(); // Get the KB from the reasoner
        // Create Pellet-Jena reasoner
        org.mindswap.pellet.jena.PelletReasoner jenaReasoner = new org.mindswap.pellet.jena.PelletReasoner();
        PelletInfGraph graph = jenaReasoner.bind(kb);
        // Wrap the graph in a model
        Model model2 = ModelFactory.createInfModel(graph);
        // Create a query execution over this model
        Query queryx = QueryFactory.create(sparqlQuery);
        QueryExecution qExec = SparqlDLExecutionFactory.create(queryx, model2);

        // run it
        ResultSet results = qExec.execSelect();
        return this.convertResultSetList(results);
    }

    @Override
    public List<QueryMap> queryMap(String sparqlQuery) throws MDDException {
        sparqlQuery = addPrefix(sparqlQuery);

        SemReasoner ri = new SemReasoner();
        try {
            ri.createDataFactory(con);
        } catch (OWLOntologyStorageException e) {
            e.printStackTrace();
            throw new MDDException(e.getMessage());
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
            throw new MDDException(e.getMessage());
        }

        PelletReasoner owlapiReasoner = null;
        try {
            owlapiReasoner = ri.createReasoner();
        } catch (OWLOntologyStorageException e) {
            e.printStackTrace();
            throw new MDDException(e.getMessage());
        }
        KnowledgeBase kb = owlapiReasoner.getKB(); // Get the KB from the reasoner
        // Create Pellet-Jena reasoner
        org.mindswap.pellet.jena.PelletReasoner jenaReasoner = new org.mindswap.pellet.jena.PelletReasoner();
        PelletInfGraph graph = jenaReasoner.bind(kb);
        // Wrap the graph in a model
        Model model2 = ModelFactory.createInfModel(graph);
        // Create a query execution over this model
        Query queryx = QueryFactory.create(sparqlQuery);
        QueryExecution qExec = SparqlDLExecutionFactory.create(queryx, model2);

        // run it
        ResultSet results = qExec.execSelect();
        return this.convertResultSet(results);
    }
}
