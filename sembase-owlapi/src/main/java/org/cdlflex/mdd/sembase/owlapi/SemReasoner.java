package org.cdlflex.mdd.sembase.owlapi;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;

import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.coode.owlapi.turtle.TurtleOntologyFormat;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.io.RDFXMLOntologyFormat;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.util.SimpleIRIMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

/**
 * Facility for the reasoning mechanism of the OWL API. Currently the Pellet reasoner is used for consistency checks.
 * The reasoning mechanism itself is however deactivated by default (performance reasons).
 */
public class SemReasoner implements Serializable {
    private static final long serialVersionUID = -6413020443475583407L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SemReasoner.class);
    private OWLOntologyManager manager;
    private OWLOntology ont;
    private PelletReasoner reasoner;
    private OwlApiConnection ocon;
    
    private static final int FIFTY = 50;

    /**
     * Create a new OWL API data factory which is used to access any reasoning and
     * semantic data maintenance processing.
     * @param c the OWLAPI connection.
     * @return the factory object
     * @throws OWLOntologyCreationException is thrown if the creation of the factory fails.
     * @throws OWLOntologyStorageException is thrown if the ontology cannot be accessed.
     */
    public OWLDataFactory createDataFactory(Connection c) throws OWLOntologyCreationException,
        OWLOntologyStorageException {
        this.ocon = (OwlApiConnection) c;
        return this.createDataFactoryInternal();
    }

    private OWLDataFactory createDataFactoryInternal() throws OWLOntologyCreationException,
        OWLOntologyStorageException {

        manager = OWLManager.createOWLOntologyManager();
        File pmbokOnt;
        File extendedOnt;
        try {
            pmbokOnt = ocon.getBaseOntologyFile();
            extendedOnt = ocon.getExtendedFile();
        } catch (IOException | URISyntaxException e) {
            throw new OWLOntologyStorageException(e.getMessage());
        }
        manager.addIRIMapper(new SimpleIRIMapper(IRI.create(ocon.getOntologySchemaIRI()), IRI.create(pmbokOnt)));
        manager.addIRIMapper(new SimpleIRIMapper(IRI.create(ocon.getExtendedOntologyIRI()), IRI.create(extendedOnt)));
        ont = manager.loadOntologyFromOntologyDocument(pmbokOnt);
        ont = manager.loadOntologyFromOntologyDocument(extendedOnt);
        reasoner = this.createReasoner();
        LOGGER.trace("VerifyQuestion.test() " + ont.getAxiomCount());
        LOGGER.trace("VerifyQuestion.test() " + ont.getImportsClosure());
        OWLDataFactory dataFactory = manager.getOWLDataFactory();
        return dataFactory;
    }

    /**
     * Save the updated ontology and perform reasoning if enabled 
     * for the connection.
     * @throws OWLOntologyStorageException is thrown if saving fails.
     */
    public void inferAndSave() throws OWLOntologyStorageException {
        reasoner = createReasoner();
        LOGGER.trace("Done creating reasoner.");
        if (this.ocon.isInferring()) {
            reasoner.getKB().classify();
            // reasoner.getKB().realize();
            // reasoner.flush(); //
            LOGGER.trace("Realized knowledge base.");
        }

        if (LOGGER.isTraceEnabled()) {
            reasoner.getKB().printClassTree();
        }

        boolean consistent = true;
        if (this.ocon.isInferring()) {
            consistent = reasoner.isConsistent();
        } else {
            // only check consistency from time to time
            int rand = new Random().nextInt(FIFTY);
            if (rand == FIFTY / 2) {
                consistent = reasoner.isConsistent();
            }
        }

        if (!consistent) {
            LOGGER.error("The ontology is unsatisfiable: " + reasoner.getUnsatisfiableClasses());
            throw new OWLOntologyStorageException("The ontology is unsatisfiable.");
        } else {
            LOGGER.trace("There are no unsatisfiable classes so the Ontology will be saved.");
            try {
                manager.saveOntology(ont, IRI.create(ocon.getExtendedFile()));
            } catch (IOException | URISyntaxException e) {
                throw new OWLOntologyStorageException(e.getMessage());
            }
        }
    }

    /**
     * Serialize the ontology in a local file with a specific format.
     * @param filepath the absolute path of the file including the filename.
     * @param format the format, such as turtle, rdf/xml, owl/xml, etc. empty is also allowed.
     * @throws OWLOntologyStorageException is thrown if saving fails.
     * @throws URISyntaxException is thrown if something in the format of the ontology is wrong.
     * @throws IOException is thrown if the ontology cannot be saved correctly.
     */
    public void serializeOntology(String filepath, String format) throws OWLOntologyStorageException,
        URISyntaxException, IOException {
        this.serializeOntology(filepath, null, format);
    }

    /**
     * Serialize an ontology in a specific format.
     * @param filepath the absolute file name. 
     * @param ontSchemaIRI the IRI of the (sub-) ontology to be serialized.
     * @param format turtle or rdf/xml. default = owl/xml
     * @throws OWLOntologyStorageException is thrown if the ontology cannot be stored correctly
     * @throws IOException is thrown if the ontology cannot be saved correctly.
     * @throws URISyntaxException is thrown if the ontology cannot be saved correctly.
     */

    public void serializeOntology(String filepath, String ontSchemaIRI, String format)
        throws OWLOntologyStorageException, URISyntaxException, IOException {
        OWLOntology persistOnt = null;
        if (ontSchemaIRI != null && !ontSchemaIRI.isEmpty()) {
            persistOnt = manager.getOntology(IRI.create(ontSchemaIRI));
        } else {
            persistOnt = ont;
        }
        URI fpIRI = ConfigLoader.getUrl(filepath).toURI();
        if (format.toLowerCase().equals("turtle")) {
            manager.saveOntology(persistOnt, new TurtleOntologyFormat(), IRI.create(fpIRI));
        } else if (format.toLowerCase().equals("rdf/xml")) {
            manager.saveOntology(persistOnt, new RDFXMLOntologyFormat(), IRI.create(fpIRI));
        } else {
            manager.saveOntology(persistOnt, new OWLXMLOntologyFormat(), IRI.create(fpIRI));
        }
    }

    /**
     * retrieve the connected ontology.
     * @return the ontology object from the OWLAPI library.
     */
    public OWLOntology getOntology() {
        return ont;
    }

    /**
     * shortcut to get the ontology manager of the OWLAPI.
     * @return the ontology manager object of the OWLAPI library.
     */
    public OWLOntologyManager getManager() {
        return manager;
    }

    /**
     * Get access to the Pellet reasoner which is used in conjunction with
     * the OWLAPI. Currently no other reasoners are supported.
     * @return the Pellet reasoner object.
     * @throws OWLOntologyStorageException is thrown if the reasoner cannot be accessed.
     */
    public PelletReasoner createReasoner() throws OWLOntologyStorageException {
        try {
            reasoner = PelletReasonerFactory.getInstance().createReasoner(ont);
        } catch (NullPointerException e) {
            LOGGER.error(e.getMessage());
            throw new OWLOntologyStorageException(e.getMessage());
        }
        return reasoner;
    }
    
    /**
     * Same as {@code #createReasoner()}, but the reasoner is created in non 
     * buffering mode, which is more efficient for the processing of larger data.
     * @return the Pellet reasoner object.
     * @throws OWLOntologyStorageException is thrown if the reasoner cannot be accessed.
     */
    public PelletReasoner createNonBufferingReasoner() throws OWLOntologyStorageException {
        try {
            reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
        } catch (NullPointerException e) {
            LOGGER.error(e.getMessage());
            throw new OWLOntologyStorageException(e.getMessage());
        }
        return reasoner;
    }

}
