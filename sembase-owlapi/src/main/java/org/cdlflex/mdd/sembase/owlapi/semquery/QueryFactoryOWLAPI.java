package org.cdlflex.mdd.sembase.owlapi.semquery;

import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.owlapi.semquery.internal.OWLDLImpl;
import org.cdlflex.mdd.sembase.owlapi.semquery.internal.SPARQLDLImpl;
import org.cdlflex.mdd.sembase.owlapi.semquery.internal.SPARQLImpl;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;
import org.cdlflex.mdd.sembase.semquery.QueryInterfaceFactory;

/**
 * Access a specific query technology via the generic query interface.
 */
public class QueryFactoryOWLAPI implements QueryInterfaceFactory {
    private final Connection con;

    public QueryFactoryOWLAPI(Connection con) {
        this.con = con;
    }

    @Override
    public QueryInterface getDefaultSparql() {
        return new SPARQLImpl(con);
    }

    @Override
    public QueryInterface getQueryInterface(String queryImpl) {
        switch (queryImpl.toLowerCase()) {
            case "sparql":
                return new SPARQLImpl(con);
            case "sparqldl":
                return new SPARQLDLImpl(con);
            case "owldl":
                return new OWLDLImpl(con);
            default:
                return null;
        }
    }
}
