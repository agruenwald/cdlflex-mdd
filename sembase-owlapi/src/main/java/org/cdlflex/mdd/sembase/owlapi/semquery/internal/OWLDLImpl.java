package org.cdlflex.mdd.sembase.owlapi.semquery.internal;

//import org.semanticweb.HermiT.Reasoner;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.owlapi.SemReasoner;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;
import org.cdlflex.mdd.sembase.semquery.QueryMap;
import org.semanticweb.owlapi.expression.ParserException;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.util.ShortFormProvider;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Author: Matthew Horridge, The University of Manchester, Bio-Health Informatics Group 
 * and Andreas Gruenwald (a.gruenw@gmail.com). a) The QueryDL interface for the ontology and 
 * b) an interactive interface for browsing the ontology. Only very basic implementation 
 * at the moment.
 **/
public class OWLDLImpl implements QueryInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(OWLDLImpl.class);

    public static final String INDIVIDUALS = "individuals";
    public static final String EQUIVALENT_CLASSES = "equivalent";
    public static final String SUPERCLASSES = "superclasses";
    public static final String SUBCLASSES = "subclasses";
    private final Connection con;
    
    public OWLDLImpl(Connection con) {
        this.con = con;
    }

    @Override
    public List<List<String>> query(String query) throws MDDException {
        return this.query(query, INDIVIDUALS);
    }

    @Override
    public List<QueryMap> queryMap(String query) throws MDDException {
        List<List<String>> list = this.query(query, INDIVIDUALS);

        List<QueryMap> queryList = new LinkedList<QueryMap>();
        for (List<String> l : list) {
            QueryMap m = new QueryMap();
            m.put("0", m.get(l.get(0)));
            queryList.add(m);
        }
        return queryList;
    }
    
    /**
     * Query individuals.
     * @param query the query (in DL format)
     * @return a list of lists of object (attributes)
     * @throws MDDException is thrown if the query cannot be processed correctly.
     */
    public List<List<String>> queryIndividuals(String query) throws MDDException {
        return this.query(query, INDIVIDUALS);
    }

    /**
     * Query individuals.
     * @param query the query (in DL format)
     * @param type the type of the queries (see public variables of the main class).
     * @return a list of lists of object (attributes)
     * @throws MDDException is thrown if the query cannot be processed correctly.
     */
    public List<List<String>> query(String query, String type) throws MDDException {
        try {
            SemReasoner reasonerInterface = new SemReasoner();
            OWLReasoner reasoner = null;
            try {
                reasonerInterface.createDataFactory(con);
                reasoner = reasonerInterface.createReasoner();
            } catch (OWLOntologyStorageException e) {
                e.printStackTrace();
                throw new MDDException(e.getMessage());
            }
            ShortFormProvider shortFormProvider = new SimpleShortFormProvider();
            DLQueryEngine dlQueryEngine = new DLQueryEngine(reasoner, shortFormProvider);
            DLQueryPrinter dlQueryPrinter = new DLQueryPrinter(dlQueryEngine, shortFormProvider);
            dlQueryPrinter.askQuery(query.trim(), false);

            List<List<String>> returnList = new LinkedList<List<String>>();
            if (type.equalsIgnoreCase(SUPERCLASSES)) {
                Set<OWLClass> superClasses = dlQueryEngine.getSuperClasses(query, true);
                addToList(returnList, superClasses, shortFormProvider);
            } else if (type.equalsIgnoreCase(EQUIVALENT_CLASSES)) {
                Set<OWLClass> equivalentClasses = dlQueryEngine.getEquivalentClasses(query);
                addToList(returnList, equivalentClasses, shortFormProvider);
            } else if (type.equalsIgnoreCase(SUBCLASSES)) {
                Set<OWLClass> subClasses = dlQueryEngine.getSubClasses(query, true);
                addToList(returnList, subClasses, shortFormProvider);
            } else {
                Set<OWLNamedIndividual> individuals = dlQueryEngine.getInstances(query, true);
                addIndividualsToList(returnList, individuals, shortFormProvider);
            }
            return returnList;
        } catch (OWLOntologyCreationException e) {
            throw new MDDException("Could not load ontology: " + e.getMessage());
        } catch (ParserException e2) {
            throw new MDDException(String.format("Couldn't parse the query input: %s. (%s)", query, e2.getMessage()));
        }
    }
    
    private void addToList(List<List<String>> returnList, Set<OWLClass> clazzes, ShortFormProvider shortFormProvider) {
        Iterator<OWLClass> it = clazzes.iterator();
        while (it.hasNext()) {
            OWLClass ind = it.next();
            LinkedList<String> l = new LinkedList<String>();
            l.add(shortFormProvider.getShortForm(ind));
            returnList.add(l);
        }
    }
    
    private void addIndividualsToList(List<List<String>> returnList, 
        Set<OWLNamedIndividual> individuals, ShortFormProvider shortFormProvider) {
        Iterator<OWLNamedIndividual> it = individuals.iterator();
        while (it.hasNext()) {
            OWLNamedIndividual ind = it.next();
            LinkedList<String> l = new LinkedList<String>();
            l.add(shortFormProvider.getShortForm(ind));
            returnList.add(l);
        }
    }

    @Override
    public void test(String query) throws MDDException {
        int i = 1;
        for (List<String> l : this.query(query)) {
            i++;
            LOGGER.trace("test query " + i + ": " + l.toString());
        }
    }

    @Override
    public List<String> extractNames(String query) throws MDDException {
        throw new MDDException("Not available/implemented for OWLDL Impl");
    }

    @Override
    public String getSparqlDefaultHeader() throws MDDException {
        throw new MDDException("Not available for OWLDL Impl");
    }
}
