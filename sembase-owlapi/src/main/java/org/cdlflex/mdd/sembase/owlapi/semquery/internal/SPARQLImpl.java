package org.cdlflex.mdd.sembase.owlapi.semquery.internal;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.owlapi.OwlApiConnection;
import org.cdlflex.mdd.sembase.owlapi.SemReasoner;
import org.cdlflex.mdd.sembase.semquery.QueryHelper;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;
import org.cdlflex.mdd.sembase.semquery.QueryMap;
import org.mindswap.pellet.KnowledgeBase;
import org.mindswap.pellet.jena.PelletInfGraph;
import org.semanticweb.owlapi.model.OWLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryParseException;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;

/**
 * This class represents the standard access to the OWL ontology via SPARQL. Jena is wrapped onto the OWLAPI to support
 * SPARQL access.
 */
public class SPARQLImpl implements QueryInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(SPARQLImpl.class);
    private final Connection con;
    private final QueryHelper qh;

    public SPARQLImpl(Connection con) {
        this.con = con;
        qh = new QueryHelper();
    }

    @Override
    public String getSparqlDefaultHeader() {
        String tmp = "PREFIX %s: <%s#> ";
        OwlApiConnection ocon = (OwlApiConnection) con;
        String sparql = this.addPrefix("");
        sparql += String.format(tmp, ocon.getNamespacePrefix(), ocon.getOntologySchemaIRI());
        sparql += String.format(tmp, "ext" + ocon.getNamespacePrefix(), ocon.getExtendedOntologyIRI());
        return sparql;
    }

    /**
     * Add a single prefix to the default SPARQL header.
     * @param query the basic query (excluding headers).
     * @return the entire header.
     */
    protected String addPrefix(String query) {
        String prefix = "PREFIX itpm: <" + "http://www.tuwien.ac.at/itpm.owl" + "#" + "> ";
        prefix += "PREFIX ext: <http://www.semanticweb.org/ontologies/2012/11/extendeditpm.owl#>";
        prefix += "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ";
        prefix += "PREFIX owl: <http://www.w3.org/2002/07/owl#> ";
        prefix += "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> ";
        return prefix + " " + query;
    }

    /* example: http://clarkparsia.com/pellet/faq/owlapi-sparql/ */
    @Override
    public List<List<String>> query(String sparqlQuery) throws MDDException {
        LOGGER.debug(String.format("The SPARQL query: %s", sparqlQuery));
        ResultSet results = this.queryInternal(sparqlQuery);
        return this.convertResultSetList(results);
    }

    @Override
    public List<QueryMap> queryMap(String sparqlQuery) throws MDDException {
        LOGGER.debug(String.format("The SPARQL query: %s", sparqlQuery));
        ResultSet results = this.queryInternal(sparqlQuery);
        return this.convertResultSet(results);
    }

    @Override
    public void test(String sparqlQuery) throws MDDException {
        LOGGER.info(String.format("The SPARQL query: %s", sparqlQuery));
        ResultSet results = this.queryInternal(sparqlQuery);
        ResultSetFormatter.out(System.out, results);
    }

    /**
     * Example: http://clarkparsia.com/pellet/faq/owlapi-sparql.
     * @param sparqlQuery the query.
     * @return an internal result set of the query.
     * @throws MDDException is thrown in case of failures.
     */
    protected ResultSet queryInternal(String sparqlQuery) throws MDDException {
        sparqlQuery = addPrefix(sparqlQuery);
        LOGGER.info("QUERY: " + sparqlQuery);
        SemReasoner ri = new SemReasoner();
        try {
            ri.createDataFactory(con);
        } catch (OWLException e) {
            e.printStackTrace();
            throw new MDDException(e.getMessage());
        } 

        PelletReasoner owlapiReasoner = null;
        try {
            owlapiReasoner = ri.createNonBufferingReasoner(); // (non-buffering mode makes synchronization easier)
        } catch (OWLException e) {
            e.printStackTrace();
            throw new MDDException(e.getMessage());
        }
        KnowledgeBase kb = owlapiReasoner.getKB(); // Get the KB from the reasoner

        PelletInfGraph graph = new org.mindswap.pellet.jena.PelletReasoner().bind(kb); // Create a Pellet graph using
                                                                                       // the KB from OWLAPI
        InfModel model = ModelFactory.createInfModel(graph); // Wrap the graph in a model
        QueryExecution qExec = null;
        try {
            qExec = QueryExecutionFactory.create(sparqlQuery, model);
        } catch (QueryParseException e) {
            throw new MDDException("Could not parse SPARQL: " + e.getMessage());
        }
        ResultSet results = qExec.execSelect();
        return results;
    }

    /**
     * Convert a result set into a list of list of strings.
     * @param results the result set (internal).
     * @return a list of lists of strings of the records.
     */
    protected List<List<String>> convertResultSetList(ResultSet results) {
        List<List<String>> resultList = new LinkedList<List<String>>();
        while (results.hasNext()) {
            QuerySolution solution = results.next();
            Iterator<String> it = solution.varNames();
            List<String> row = new LinkedList<String>();
            int noVarNames = 0;
            Iterator<String> itVarNames = solution.varNames();
            while (itVarNames.hasNext()) {
                itVarNames.next();
                noVarNames++;
            }

            boolean skip = false;
            while (it.hasNext()) {
                String v = it.next();
                String out = solution.get(v).toString();
                String content = qh.fillValue(v, out);
                if (content == null) {
                    skip = true;
                    break;
                } else {
                    row.add(content);
                }
            }
            if (!skip) {
                skip = addIfNotNull(row, noVarNames, resultList);
            }
        }
        return resultList;
    }
    
    private boolean addIfNotNull(List<String> row, int noVarNames, List<List<String>> resultList) {
        boolean skip = false;
        if (row.size() < noVarNames) { // no of varies is not equals to the number in the sparql statement
            skip = true;
        }
        /* hack: for instance the case if no collaborators are found in a group-by */
        if (skip || row.size() == 0) {
            skip = true;
        } else if (row.size() == 1 && (row.get(0).equals("0.0") || row.get(0).equals("0"))) { 
            skip = true;
        } else {
            resultList.add(row);
        }
        return skip;
    }

    /**
     * Convert a result set into a map object.
     * @param results the internal result set object.
     * @return a list of maps. They key corresponds to the key variables.
     */
    protected List<QueryMap> convertResultSet(ResultSet results) {
        List<QueryMap> resultList = new LinkedList<QueryMap>();
        while (results.hasNext()) {
            QuerySolution solution = results.next();
            Iterator<String> it = solution.varNames();
            QueryMap row = new QueryMap();
            boolean skip = false;
            while (it.hasNext()) {
                String v = it.next();
                String out = solution.get(v).toString();
                String content = qh.fillValue(v, out);
                if (content == null) {
                    skip = true;
                    break;
                } else {
                    row.put(v, content);
                }
            }
            if (!skip) {
                resultList.add(row);
            }
        }
        return resultList;
    }

    @Override
    public List<String> extractNames(String query) throws MDDException {
        return qh.extractNames(query);
    }
}
