package org.cdlflex.mdd.sembase.test;

import static org.junit.Assert.assertTrue;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.owlapi.OwlApiConnection;
import org.junit.Test;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
/**
 * Test the auto commit mode and the OWLAPI connector.
 *
 */
public class TestOwlAPIBasics {
    /**
     * Create a connection and try to connect / commit/ reason, etc.
     * @throws MDDException is thrown if the test fails.
     * @throws OWLOntologyStorageException is thrown if the OWLAPI connection or reasoning fails.
     */
    @Test
    public void testImportResolvement() throws MDDException, OWLOntologyStorageException {
        OwlApiConnection connection = ConnectionUtil.createOwlApiConnection();
        connection.connect();
        connection.begin();
        connection.getReasoner().inferAndSave();
        connection.commit();
        connection.close();
        assertTrue(true);
    }

    /**
     * Test the reset of the entire data base.
     * @throws MDDException is thrown if the test fails.
     */
    @Test
    public void testResetDataInEmptyModelAuto() throws MDDException {
        OwlApiConnection c = ConnectionUtil.createOwlApiConnection();
        c.resetOntology();
        c.close();
        c.connect();
        c.resetData();
        c.close();
    }

    /**
     * Test if the auto commit mode is maintained correctly.
     * @throws MDDException is thrown if the test fails.
     */
    @Test
    public void testBackToAutocommitMode() throws MDDException {
        OwlApiConnection c = ConnectionUtil.createOwlApiConnection();
        c.close();
        c.connect();
        assertTrue(c.isAutoCommitMode());
        c.begin();
        assertTrue(!c.isAutoCommitMode());
        c.commit();
        assertTrue(c.isAutoCommitMode());
        c.close();
    }
}
