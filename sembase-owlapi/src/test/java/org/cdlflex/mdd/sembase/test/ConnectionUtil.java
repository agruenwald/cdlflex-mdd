package org.cdlflex.mdd.sembase.test;

import java.net.URL;
import java.util.Properties;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.owlapi.OwlApiConnection;

/**
 * Connection utility only for the test environment.
 */
public final class ConnectionUtil {
    private static final String OWL_CONFIG = "owlapi-config.properties";

    /**
     * Hide the default constructor.
     */
    private ConnectionUtil() {
        
    }
    
    /**
     * Access a property file at the main configuration repository.
     * @param propertyFilename the (short) name of the property file.
     * @return the properties file.
     * @throws MDDException is thrown if the access to the file fails.
     */
    public static Properties getPropertyFile(String propertyFilename) throws MDDException {
        Properties genConf = new ConfigLoader(true).findPropertyFile("{projectPath}" + propertyFilename);
        return genConf;
    }

    /**
     * Default access to a new OWLAPI connection.
     * @return a OWLAPI connection to work with (connect, commit, etc.).
     * @throws MDDException is thrown if the connection cannot be instantiated.
     */
    public static OwlApiConnection createOwlApiConnection() throws MDDException {
        Properties p = getPropertyFile(OWL_CONFIG);
        OwlApiConnection con =
            new OwlApiConnection(repPath(p.getProperty("ontologySchemaName")),
                    repPath(p.getProperty("extendedOntologyName")), repPath(p.getProperty("absoluteFilePath")),
                    repPath(p.getProperty("ontologySchemaIRI")), repPath(p.getProperty("extendedOntologyIRI")));
        return con;
    }

    private static String repPath(String s) {
        if (s.contains("{projectPath}")) {
            String absPath = s.replace("{projectPath}", "").trim();
            URL f = ClassLoader.getSystemResource(absPath);
            if (f != null) {
                return f.getFile();
            } else {
                return s;
            }
        } else {
            return s;
        }
    }
}
