package org.cdlflex.mdd.sembase.owlapi.semquery.test;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.test.ConnectionUtil;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;
import org.cdlflex.mdd.sembase.owlapi.semquery.internal.OWLDLImpl;
import org.cdlflex.mdd.sembase.owlapi.semquery.internal.SPARQLDLImpl;
import org.cdlflex.mdd.sembase.owlapi.semquery.internal.SPARQLImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test the execution of diferent types of query (languages) via the OWLAPI, and third-party SPARQL extensions.
 */
public class TestQuery {
    private static Connection con;

    /**
     * Open default connection. 
     * @throws MDDException is thrown if connecting fails.
     */
    @Before
    public void connect() throws MDDException {
        con = ConnectionUtil.createOwlApiConnection();
        con.connect();
    }

    /**
     * Close default connection.
     * @throws MDDException is thrown if disconnecting fails.
     */
    @After
    public void disconnect() throws MDDException {
        con.close();
    }

    /**
     * Test the execution of a simple SPARQL query.
     * 
     * @throws MDDException is thrown if the execution of the query fails.
     */
    @Test
    public void testSparql1() throws MDDException {
        QueryInterface queryInterface = new OWLDLImpl(null);
        String test =
            "SELECT " + "?id ?n ?workingDate " + "(COALESCE(SUM(?workingHours), '0.0') AS ?notAvId) "
                + "WHERE { ?x itpm:hasCollaboratorName ?n. " + "?x itpm:hasCollaboratorId ?id."
                + "?x itpm:hasCollaboratorEmail ?m."
                + "?x itpm:hasCollaboratorPhone ?p."
                + "?x itpm:hasCollaboratorIsExternal ?e."
                + "?x itpm:hasCollaboratorIsUnit ?u."
                + "?x itpm:hasCollaboratorDescription ?d."
                + "?x itpm:trackWork ?timeSheet."
                + "OPTIONAL { ?timeSheet itpm:hasTimeSheetTimeEntryComposition ?timeEntry ."
                + // Todo optional
                "OPTIONAL { ?timeEntry itpm:hasTimeEntryDate ?workingDate . "
                + "         ?timeEntry itpm:hasTimeEntryHours ?workingHours ."
                + "FILTER (xsd:dateTime(?workingDate) >= '2013-02-26T00:00:00Z'^^xsd:dateTime "
                + "&& xsd:dateTime(?workingDate) <= '2013-03-01T23:59:59Z'^^xsd:dateTime) " + "}}"
                + "} GROUP BY ?id ?n ?workingDate ORDER BY ASC(?n) " + "LIMIT 1000 " + "OFFSET 0";
        queryInterface = new SPARQLImpl(con);
        List<List<String>> query = queryInterface.query(test);
        assertTrue(query.size() >= 3);
    }

    /**
     * Test the execution of an invalid SPARQL query.
     * 
     * @throws MDDException is thrown if the invalid query is detected correctly.
     */
    @Test(expected = MDDException.class)
    public void negTestSparql() throws MDDException {
        QueryInterface queryInterface = new OWLDLImpl(null);
        String test = "SELECT XXX";
        queryInterface = new SPARQLImpl(con);
        queryInterface.query(test);
    }

    /**
     * Test the execution of a simple OWL DL query.
     * 
     * @throws MDDException is thrown if the DL query is not executed correctly.
     */
    @Test
    public void testOWLDLQuery() throws MDDException {
        QueryInterface queryInterface = new OWLDLImpl(con);
        String test = "Collaborator";
        List<List<String>> query = queryInterface.query(test);
        assertTrue(query.size() >= 3);
    }

    /**
     * Test the execution of a simple SPARQL query. The execution is based on the SPARQL DL library (third party
     * extension).
     * 
     * @throws MDDException is thrown if the execution of the SPARQL query fails.
     */
    @Test
    public void testSPARQLDLImpl() throws MDDException {
        QueryInterface queryInterface = new SPARQLDLImpl(con);
        String test =
            "SELECT " + "?id ?n ?workingDate " + "(COALESCE(SUM(?workingHours), '0.0') AS ?notAvId) "
                + "WHERE { ?x itpm:hasCollaboratorName ?n. " + "?x itpm:hasCollaboratorId ?id."
                + "?x itpm:hasCollaboratorEmail ?m."
                + "?x itpm:hasCollaboratorPhone ?p."
                + "?x itpm:hasCollaboratorIsExternal ?e."
                + "?x itpm:hasCollaboratorIsUnit ?u."
                + "?x itpm:hasCollaboratorDescription ?d."
                + "?x itpm:trackWork ?timeSheet."
                + "OPTIONAL { ?timeSheet itpm:hasTimeSheetTimeEntryComposition ?timeEntry ."
                + // Todo optional
                "OPTIONAL { ?timeEntry itpm:hasTimeEntryDate ?workingDate . "
                + "         ?timeEntry itpm:hasTimeEntryHours ?workingHours ."
                + "FILTER (xsd:dateTime(?workingDate) >= '2013-02-26T00:00:00Z'^^xsd:dateTime "
                + "&& xsd:dateTime(?workingDate) <= '2013-03-01T23:59:59Z'^^xsd:dateTime) " + "}}"
                + "} GROUP BY ?id ?n ?workingDate ORDER BY ASC(?n) " + "LIMIT 1000 " + "OFFSET 0";
        List<List<String>> query = queryInterface.query(test);
        assertTrue(query.size() >= 3);
    }

    /**
     * Test the extraction of names based on the passed query variables.
     * 
     * @throws MDDException is thrown if the execution of the query fails.
     */
    @Test
    public void testExtractionNames() throws MDDException {
        QueryInterface queryInterface = new SPARQLDLImpl(con);
        String test =
            "SELECT " 
                + "?id ?n ?workingDate " + "(COALESCE(SUM(?workingHours), '0.0') AS ?notAvId) " 
                + "WHERE { ?x itpm:hasCollaboratorName ?n. " + "?x itpm:hasCollaboratorId ?id."
                + "?x itpm:hasCollaboratorEmail ?m."
                + "?x itpm:hasCollaboratorPhone ?p."
                + "?x itpm:hasCollaboratorIsExternal ?e."
                + "?x itpm:hasCollaboratorIsUnit ?u."
                + "?x itpm:hasCollaboratorDescription ?d."
                + "?x itpm:trackWork ?timeSheet."
                + "OPTIONAL { ?timeSheet itpm:hasTimeSheetTimeEntryComposition ?timeEntry ."
                + // Todo optional
                "OPTIONAL { ?timeEntry itpm:hasTimeEntryDate ?workingDate . "
                + "         ?timeEntry itpm:hasTimeEntryHours ?workingHours ."
                + "FILTER (xsd:dateTime(?workingDate) >= '2013-02-26T00:00:00Z'^^xsd:dateTime "
                + "&& xsd:dateTime(?workingDate) <= '2013-03-01T23:59:59Z'^^xsd:dateTime) " + "}}"
                + "} GROUP BY ?id ?n ?workingDate ORDER BY ASC(?n) " + "LIMIT 1000 " + "OFFSET 0";
        List<String> names = queryInterface.extractNames(test);
        assertTrue(names.size() == 4);
    }

    /**
     * Test the execution of a simple query (requirements test data).
     * 
     * @throws MDDException is thrown if the execution of the query fails.
     */
    @Test
    public void testExtractionRequirements() throws MDDException {
        String test =
            "SELECT ?name ?prio \nWHERE { \n\t?x itpm:hasRequirementName ?name. \n\t" 
                    + "?x itpm:hasRequirementPriority ?prio.\n  }";
        QueryInterface queryInterface = new SPARQLDLImpl(con);
        List<String> names = queryInterface.extractNames(test);
        assertTrue(names.size() == 2);
    }
}
