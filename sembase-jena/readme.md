# CDL FLEX :: MDD :: Sembase Jena

This module contains classes and tests to access Apache Jena's TDB and SDB in
a systematic way. 

Note that Apache Jena and the Pellet OWL API reasoner are incompatible. Hence
the OWLAPI (incl. Pellet) and the Jena API are kept separate and can never be 
included together.

## Installation requirements
Please take into consideration that the Jena SDB tests require a relational database
system setup and be configured.

For the Jena TDB tests it is essential that the modelrepo within the sembase configuration
directory (typically included in custom-itpm/assembly/src/main/resources/sembase) is
available. Additionally, the Jena TDB directory where all the database files managed
by TDB reside, must be available and it is important that all files and sub-files are
equipped with write-permissions. 

The Jena TDB storage is typically included in /.../test|live/jena_tdb_store/itpm_jena.
For instance in Jenkins the two directories which must be set are
+ /var/lib/jenkins/jobs/custom-itpm/workspace/assembly/src/main/resources/sembase/test/jena_tdb_store/itpm_jena/
+ /var/lib/jenkins/jobs/custom-itpm/workspace/assembly/src/main/resources/sembase/live/jena_tdb_store/itpm_jena/

The test directory is used for (unit) tests, while the live-directory is used in running
environments.