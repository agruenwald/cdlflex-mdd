package org.cdlflex.mdd.sembase.jena.internal;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.OntAccessInfo;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.shared.Lock;
import com.hp.hpl.jena.tdb.TDB;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.TDBLoader;

/**
 * Default - Implementation of the Jena TDB connector. According to the Apache-Jena mailing list response by Andy
 * Seaborne Jena TDB is not capable of being executed in multiple processes at the same time. This type of connection
 * provides the default-implementation based on Jena's documentation and is completely capable of handling R/W access
 * with Jena TDB for SINGLE processes. Multi-processes are not supported (therefore the application of Fuseki is
 * recommended).
 */
public class JenaTDBConnectionSingleProcess extends JenaBaseConnection {
    private static boolean firstTime = true;
    private Dataset tdbDataSet;
    private boolean disableEmptynessCheck;
    // helper field to facilitate that nested transactions are not supported by Jena
    private static boolean isWriteTransactionInThreadOpen;

    public JenaTDBConnectionSingleProcess(OntAccessInfo ontAccessInfo, String jenaStorageTechnology) 
        throws MDDException {
        super(ontAccessInfo, jenaStorageTechnology);
        disableEmptynessCheck = false;
        tdbDataSet = null;
        isWriteTransactionInThreadOpen = false;
    }

    @Override
    protected void checkConnected(boolean checkEmptyness) throws MDDException {
        // if(model == null || !connected) {
        if (!connected) { // model still null after connecting
            throw new MDDException(String.format("Please connect first before beginning a new transaction."));
        } else if (checkEmptyness && !disableEmptynessCheck) {
            checkModelEmptyness();
        }
    }

    @Override
    public void connect() throws MDDException {
        if (this.connected) {
            throw new MDDException("Already connected to Jena.");
        }
        modelSpec = createModelSpec();
        modelSpec.getDocumentManager().setProcessImports(false); // avoid reading urls, such as http://www.itpm...
        if (firstTime) {
            logger.info(String.format("Connect to local Jena store %s (TDB).", getDirectory()));
        }
        connected = true;
    }

    protected String getDirectory() {
        return this.ontAccessInfo.getDbInfo().getHost() + "/" + this.ontAccessInfo.getDbInfo().getSchema();
    }

    private void beginTransInitModel(boolean isReadonly) throws MDDException {
        if (this.transactionStarted && tdbDataSet != null) {
            logger.error(String.format("Aborted transaction which was already/still open."));
            tdbDataSet.abort();
        } else if (isWriteTransactionInThreadOpen && !isReadonly) {
            throw new MDDException(
                    "Nested transactions are not supported by Jena TDB and " 
                  + "there has already been one transaction started (connect.begin()).");
        }
        startTransaction(isReadonly);
        if (firstTime || (model == null || tdbDataSet.getNamedModel(this.ontAccessInfo.getOntologyIRI()).isEmpty())) {
            TDBFactory.reset();
            // restart transaction
            tdbDataSet.abort();
            startTransaction(isReadonly);
            firstTime = false;
        }
    }

    private void startTransaction(boolean isReadonly) {
        String directory = getDirectory();
        tdbDataSet = TDBFactory.createDataset(directory);
        if (isReadonly) {
            tdbDataSet.begin(ReadWrite.READ);
        } else {
            if (tdbDataSet.isInTransaction()) {
                logger.info("Transaction is already open  in #startTransaction()");
            }
            tdbDataSet.begin(ReadWrite.WRITE);
        }
        model = tdbDataSet.getNamedModel(this.ontAccessInfo.getOntologyIRI());
        this.ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, model);
    }

    @Override
    public void begin() throws MDDException {
        this.begin(true, false);
    }

    @Override
    public void begin(boolean setAutoCommitTag, boolean isReadonly) throws MDDException {
        if (!connected) { // model is still null here
            checkConnected();
        }
        if (setAutoCommitTag) {
            tmpAutoCommitMode = this.autoCommitMode;
            this.autoCommitMode = false;
        }
        beginTransInitModel(isReadonly);
        if (!isReadonly) {
            isWriteTransactionInThreadOpen = true; // set before check because otherwise when connecting next TDB locks.
        }
        transactionStarted = true;
        transactionIsReadMode = true;
        checkConnected(true); // check emptyness etc.
    }

    @Override
    public void autoBeginReadOnly() throws MDDException {
        this.autoBegin(true);
    }

    @Override
    public void commit() throws MDDException {
        checkConnected();
        transactionStarted = false;
        isWriteTransactionInThreadOpen = false;
        try {
            ontModel.enterCriticalSection(Lock.WRITE);
            tdbDataSet.commit();
            ontModel.commit();
            model.commit();
            TDB.sync(ontModel);
            TDB.sync(model);
            TDB.sync(tdbDataSet);
        } finally {
            try {
                ontModel.leaveCriticalSection();
            //CHECKSTYLE:OFF
            } catch (Exception e) {
                logger.error("Could not leave critical section: {}", e.getMessage());
            }
            //CHECKSTYLE:ON
        }
        this.autoCommitMode = tmpAutoCommitMode;
    }

    @Override
    public void commitReadOnly() throws MDDException {
        super.commitReadOnly();
        this.tdbDataSet.abort();
        transactionStarted = false;
        isWriteTransactionInThreadOpen = false;
    }

    @Override
    public void rollback() throws MDDException {
        checkConnected();
        // TDB: abort not supported (in TDB <= 0.8 no transaction handling is supported in general)
        try {
            this.tdbDataSet.abort();
        } finally {
            transactionStarted = false;
            isWriteTransactionInThreadOpen = false;
            this.autoCommitMode = tmpAutoCommitMode;
        }
    }


    @Override
    public void close() throws MDDException {
        //CHECKSTYLE:OFF
        try {
            if (this.connected) { // on rollback connection is not open any more
                try {
                    tdbDataSet.end();
                } catch (Exception e) {

                }

                if (tdbDataSet != null) { // because connecting does not mean creating a transaction
                    tdbDataSet.close();
                }
                logger.debug("Closed TDB connection.");
            } else if (isWriteTransactionInThreadOpen) {
                String directory = getDirectory();
                logger.warn("Closed open, global TDB transaction in {}...", directory);
                try {
                    if (tdbDataSet == null) {
                        // only one global dataset allowed
                    }
                    tdbDataSet.close();
                } catch (Exception e) {

                }
            }
            connected = false;
            tdbDataSet = null;
        } catch (Exception e) {
            throw new MDDException("Failed closing Jena connnection: " + e.getMessage());
        } 
        //CHECKSTYLE:ON
    }

    @Override
    public void clearOntology() throws MDDException {
        checkConnected(false);
        try {
            disableEmptynessCheck = true;
            this.autoBegin();
            this.tdbDataSet.getNamedModel(this.ontAccessInfo.getOntologyIRI()).removeAll();
            this.autoCommit();
        } finally {
            disableEmptynessCheck = false;
        }
    }

    @Override
    public void clearAllOntologies() throws MDDException {
        String path = getDirectory();
        try {
            FileUtils.cleanDirectory(new File(path)); // also clean index files, data, etc.
        } catch (IOException e) {
            throw new MDDException(String.format("Could not clean Jena TDB store. Is %s an (existing) directory?",
                    path));
        }
    }

    @Override
    protected void loadFilesIntoDb(File tmpFileBasic, File tmpExtendedFile, String ontFormat) throws MDDException {
        try {
            disableEmptynessCheck = true;
            this.autoBegin();
            OntModel newmodel = ModelFactory.createOntologyModel(this.modelSpec, model);
            logger.info(String.format("load file %s(%s)", ConfigLoader.getUrl(tmpFileBasic.getAbsolutePath())
                    .getPath(), this.ontAccessInfo.getOntologyIRI()));
            TDBLoader.loadModel(newmodel, ConfigLoader.getUrl(tmpFileBasic.getAbsolutePath()).getPath(), true);
            logger.info(String.format("load file %s", ConfigLoader.getUrl(tmpExtendedFile.getAbsolutePath())
                    .getPath()));
            TDBLoader.loadModel(newmodel, ConfigLoader.getUrl(tmpExtendedFile.getAbsolutePath()).getPath());
            this.autoCommit();
            logger.info(String.format("loading into DB done (named model %s).", this.ontAccessInfo.getOntologyIRI()));
        } catch (IOException e) {
            throw new MDDException(e.getMessage());
        } finally {
            disableEmptynessCheck = false;
        }
    }

    @Override
    public String getNamespacePrefix() {
        return this.ontAccessInfo.getOntologySchemaName();
    }
}
