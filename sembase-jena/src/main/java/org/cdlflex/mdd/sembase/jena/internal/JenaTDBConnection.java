package org.cdlflex.mdd.sembase.jena.internal;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.tdb.TDB;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.tdb.TDBLoader;

import org.apache.commons.io.FileUtils;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.OntAccessInfo;

import java.io.File;
import java.io.IOException;

/**
 * Implementation of the Jena TDB connector, based on a Jena-internal storage technology, which stores data in the file
 * system and performs most operations in-memory. The implementation provides a workaround so that it can be used in
 * multi-process environments. The price for that is lower performance. In case that you are only using a single
 * application, use {@see JenaTDBConnectionSingleProcess} class.
 */
public class JenaTDBConnection extends JenaBaseConnection {
    private static boolean initDone;
    private Dataset tdbDataSet;
    private boolean disableEmptynessCheck;
    // helper field to facilitate that nested transactions are not supported by Jena

    // cannot control on a global context - this would mean that
    // no user at all can perform concurrent access.
    // remove static keyword
    private boolean isTransactionInThreadOpen;

    private boolean readonly;
    protected boolean isWriteMode;

    public JenaTDBConnection(OntAccessInfo ontAccessInfo, String jenaStorageTechnology) throws MDDException {
        super(ontAccessInfo, jenaStorageTechnology);
        this.init();
    }

    /**
     * Init the class fields.
     */
    protected void init() {
        disableEmptynessCheck = false;
        tdbDataSet = null;
        isTransactionInThreadOpen = false;
        readonly = false;
        isWriteMode = true;
        connected = false;
        this.setAutoCommitMode(true);
    }

    @Override
    protected void checkConnected(boolean checkEmptyness) throws MDDException {
        if (!connected) { // model still null after connecting
            this.transactionStarted = false;
            isTransactionInThreadOpen = false;
            throw new MDDException(String.format("Please connect first before beginning a new transaction."));
        } else if (checkEmptyness && !disableEmptynessCheck) {
            checkModelEmptyness();
        }
    }

    @Override
    public void connect() throws MDDException {
        if (this.connected) {
            throw new MDDException("Already connected to Jena.");
        }
        init();
        modelSpec = createModelSpec();
        modelSpec.getDocumentManager().setProcessImports(false); // avoid reading urls, such as http://www.itpm...
        String directory = this.ontAccessInfo.getDbInfo().getHost() 
                + "/" + this.ontAccessInfo.getDbInfo().getSchema();
        if (initDone) {
            logger.info(String.format("Connect to local Jena store %s (TDB) in app %s.", directory,
                    System.getProperty("java.home")));
            initDone = false;
        }
        connected = true;
    }

    /**
     * Begin a new transaction.
     * @throws MDDException is thrown in case of exception.
     */
    protected void beginTransInitModel() throws MDDException {
        logger.trace(String.format("TDB: beginTransInitModel() called."));
        String directory = this.ontAccessInfo.getDbInfo().getHost()
                + "/" + this.ontAccessInfo.getDbInfo().getSchema();
        this.checkBeforeStartTrans();
        if (this.transactionStarted && tdbDataSet != null) {
            logger.error(String.format("Aborted transaction which was already/still open."));
            tdbDataSet.abort();
        } else if (isTransactionInThreadOpen) {
            throw new MDDException(
                    "Nested transactions are not supported by Jena TDB and " 
                  + "there has already been one transaction started (connect.begin()).");
        }
        TDBFactory.reset();
        tdbDataSet = TDBFactory.createDataset(directory);
        // TDB.sync(tdbDataSet);
        if (readonly && !readonly) { // set to write to 100% avoid problems with R/W on nested access
            tdbDataSet.begin(ReadWrite.READ);
            isWriteMode = false;
        } else {
            tdbDataSet.begin(ReadWrite.WRITE);
            isWriteMode = true;
        }
        this.model = tdbDataSet.getNamedModel(this.ontAccessInfo.getOntologyIRI());
        this.ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, model);
    }

    /**
     * Hook to add additional checks before start transaction.
     * @throws MDDException is thrown in case of failures.
     */
    protected void checkBeforeStartTrans() throws MDDException {

    }

    @Override
    public void begin() throws MDDException {
        this.begin(true, false);
    }

    @Override
    public void begin(boolean setAutoCommitTag, boolean isReadonly) throws MDDException {
        logger.trace(String.format(
                "Begin TDB transaction with setAutoCommitTag = %s. connected = %s, transactionStarted = %s"
                    + "tdbDataSet=%s", setAutoCommitTag, connected, this.transactionStarted, tdbDataSet));
        if (!connected) { // model is still null here
            checkConnected();
        }
        if (setAutoCommitTag) {
            tmpAutoCommitMode = this.autoCommitMode;
            this.autoCommitMode = false;
        }
        beginTransInitModel();
        isTransactionInThreadOpen = true; // set before check because otherwise when connecting next TDB locks.
        transactionStarted = true;
        transactionIsReadMode = isReadonly;
        checkConnected(true); // check emptyness etc.
    }

    @Override
    public void autoBeginReadOnly() throws MDDException {
        logger.trace(String.format("Auto-Begin TDB transaction readonly."));
        this.readonly = true;
        this.autoBegin(readonly);
        this.readonly = false;
    }

    @Override
    protected boolean autoBegin(boolean isReadonly) throws MDDException {
        if (!this.isWriteMode && !isReadonly) {
            logger.warn("Transaction is currently in readonly-mode. " + " Switched to auto-write transaction mode.");
            this.commitReadOnly();
            /*
             * throw new MDDException("Transaction is currently in readonly-mode " +
             * " but the application tried to open an auto-write transaction");
             */
        }
        // there is the case where the last transaction wasn't ended properly
        // usually this is an indication that the code did not implement a proper close() - handling
        // of the connection when failing.
        return super.autoBegin(isReadonly);
    }

    @Override
    public void commit() throws MDDException {
        logger.trace(String.format("Commit TDB transaction with"));
        checkConnected();
        transactionStarted = false;
        isTransactionInThreadOpen = false;
        tdbDataSet.commit();
        ontModel.commit();
        model.commit();
        TDB.sync(ontModel);
        TDB.sync(model);
        TDB.sync(tdbDataSet);
        this.endTrans();
        this.autoCommitMode = tmpAutoCommitMode;
    }

    @Override
    public void commitReadOnly() throws MDDException {
        logger.trace(String.format("Commit TDB transaction readonly."));
        super.commitReadOnly();
        this.tdbDataSet.abort();
        transactionStarted = false;
        isTransactionInThreadOpen = false;
    }

    /**
     * Hook which is called after the end of a transaction.
     */
    protected void endTrans() {
    }

    @Override
    public void rollback() throws MDDException {
        logger.trace(String.format("Rollback TDB transaction."));
        checkConnected();
        // TDB: abort not supported (in TDB <= 0.8 no transaction handling is supported in general)
        this.tdbDataSet.abort();
        this.endTrans();
        transactionStarted = false;
        isTransactionInThreadOpen = false;
        this.autoCommitMode = tmpAutoCommitMode;
    }


    @Override
    public void close() throws MDDException {
        logger.trace(String.format("Close TDB connection."));
        //CHECKSTYLE:OFF
        try {
            if (this.connected) { // on rollback connection is not open any more
                try {
                    tdbDataSet.end();
                } catch (Exception e) {
                }        

                if (tdbDataSet != null) { // because connecting does not mean creating a transaction
                    try {
                        ontModel.close();
                    } catch (Exception e) {
                        logger.debug("Failed to close ontModel: " + e.getMessage());
                    }
                    tdbDataSet.close();
                }
                logger.trace("Closed TDB connection.");
            } else if (isTransactionInThreadOpen) {
                String directory = this.ontAccessInfo.getDbInfo().getHost() 
                        + "/" + this.ontAccessInfo.getDbInfo().getSchema();
                logger.warn("Closed open, global TDB transaction in {}. This may be not severe, " 
                        + "but transactions should be closed with a "
                            + "commit() or rollback().", directory);
                try {
                    tdbDataSet.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
            throw new MDDException("Failed closing Jena connnection: " + e.getMessage());
        } finally {
            connected = false;
            tdbDataSet = null;
            this.transactionStarted = false;
            isTransactionInThreadOpen = false;
        }
        //CHECKSTYLE:ON
    }

    @Override
    public void clearOntology() throws MDDException {
        checkConnected(false);
        try {
            disableEmptynessCheck = true;
            this.autoBegin();
            try {
                Model m = this.tdbDataSet.getNamedModel(this.ontAccessInfo.getOntologyIRI());
                if (m == null) {
                    logger.warn(String.format("Model %s not existed.", this.ontAccessInfo.getOntologyIRI()));
                    this.tdbDataSet = null;
                    beginTransInitModel();
                } else {
                    m.removeAll();
                    // TDB.sync(m); //test
                }
              //CHECKSTYLE:OFF
            } catch (Exception e) {
                e.printStackTrace();
                // corrupted data (?)
                logger.warn("Try again - reset data...");
                try {
                    TDBFactory.reset();
                } catch (Exception e2) {
                    logger.error("Also failed to reset TDBFactory.");
                    e2.printStackTrace();
                }
                //CHECKSTYLE:ON
            }

            this.autoCommit();
        } finally {
            disableEmptynessCheck = false;
        }
    }

    @Override
    public void clearAllOntologies() throws MDDException {
        String path = this.ontAccessInfo.getDbInfo().getHost() 
                + "/" + this.ontAccessInfo.getDbInfo().getSchema();
        try {
            FileUtils.cleanDirectory(new File(path)); // also clean index files, data, etc.
        } catch (IOException e) {
            throw new MDDException(String.format("Could not clean Jena TDB store. Is %s an (existing) directory?",
                    path));
        }
    }

    @Override
    protected void loadFilesIntoDb(File tmpFileBasic, File tmpExtendedFile, String ontFormat) throws MDDException {
        try {
            disableEmptynessCheck = true;
            this.autoBegin();
            OntModel newmodel = ModelFactory.createOntologyModel(this.modelSpec, model);
            if (newmodel == null) {
                logger.error("Newmodel is null!!!");
            }
            logger.info(String.format("Load content of temporary file into the TDB model (%s)",
                    tmpFileBasic.getAbsolutePath(), this.ontAccessInfo.getOntologyIRI(), newmodel));
            TDBLoader.loadModel(newmodel, ConfigLoader.getUrl(tmpFileBasic.getAbsolutePath()).getFile(), true);
            logger.info(String.format("Load content of temporary file into the TDB model (%s)",
                    tmpExtendedFile.getAbsolutePath()));
            TDBLoader.loadModel(newmodel, ConfigLoader.getUrl(tmpExtendedFile.getAbsolutePath()).getFile());
            this.autoCommit();
            // ?TDBFactory.reset();
            logger.info(String.format("TDB loading finished (named model %s).", this.ontAccessInfo.getOntologyIRI()));
        } catch (IOException e) {
            throw new MDDException(e.getMessage());
        } finally {
            disableEmptynessCheck = false;
        }
    }

    @Override
    public void resetOntology() throws MDDException {
        super.resetOntology();
        // TDBFactory.reset();
    }

    @Override
    public String getNamespacePrefix() {
        return this.ontAccessInfo.getExtendedOntologyName();
    }
}
