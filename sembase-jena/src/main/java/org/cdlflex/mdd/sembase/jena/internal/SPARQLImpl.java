package org.cdlflex.mdd.sembase.jena.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.jena.JenaConnection;
import org.cdlflex.mdd.sembase.semquery.QueryHelper;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;
import org.cdlflex.mdd.sembase.semquery.QueryMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QueryParseException;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;

/**
 * This class represents the standard access to the OWL ontology via SPARQL. Jena is wrapped onto the OWLAPI to support
 * SPARQL access.
 */
public class SPARQLImpl implements QueryInterface {
    private static final Logger LOGGER = LoggerFactory.getLogger(SPARQLImpl.class);
    private final JenaConnection con;
    private final QueryHelper qh;

    public SPARQLImpl(Connection con) {
        this.con = (JenaConnection) con;
        qh = new QueryHelper();
    }

    /**
     * Add default prefixes to a query.
     * @param query the query.
     * @return the query, prefixed with the default prefix annotations so that they can be used
     * within the essential SPARQL query.
     */
    protected String addPrefix(String query) {
        String sparql = con.getSparqlBase().trim() + " " + query;
        return sparql;
    }
    
    @Override
    public List<List<String>> query(String sparqlQuery) throws MDDException {
        LOGGER.debug(String.format("The SPARQL query: %s", sparqlQuery));
        con.autoBeginReadOnly();
        Transporter transporter = this.queryInternal(sparqlQuery);
        ResultSet results = transporter.results;
        try {
            return this.convertResultSetList(results, results.getResultVars());
        } finally {
            con.autoCommitReadOnly();
            try {
                transporter.qExec.close();
            //CHECKSTYLE:OFF it's best to catch all exeptions to avoid
            //unexpected Jena exceptions (Jena is still not very mature).    
            } catch (Exception e) {

            }
            //CHECKSTYLE:ON
        }
    } 

    @Override
    public List<QueryMap> queryMap(String sparqlQuery) throws MDDException {
        LOGGER.debug(String.format("The SPARQL query: %s", sparqlQuery));
        con.autoBeginReadOnly();
        Transporter transporter = this.queryInternal(sparqlQuery);
        ResultSet results = transporter.results;
        try {
            return this.convertResultSet(results, results.getResultVars());
        } finally {
            con.autoCommitReadOnly();
            try {
                transporter.qExec.close();
            //CHECKSTYLE:OFF it's best to catch all exeptions to avoid
            //unexpected Jena exceptions (Jena is still not very mature).
            } catch (Exception e) {

            }
            //CHECKSTYLE:ON
        }
    }

    @Override
    public void test(String sparqlQuery) throws MDDException {
        LOGGER.debug(String.format("The SPARQL query: %s", sparqlQuery));
        con.autoBeginReadOnly();
        Transporter transporter = this.queryInternal(sparqlQuery);
        ResultSet results = transporter.results;
        try {
            ResultSetFormatter.out(System.out, results);
            con.autoCommitReadOnly();
        } finally {
            try {
                transporter.qExec.close();
            //CHECKSTYLE:OFF it's best to catch all exeptions to avoid
            //unexpected Jena exceptions (Jena is still not very mature).
            } catch (Exception e) {
            }
            //CHECKSTYLE:ON
        }
    }

    /**
     * Create a query and return the result.
     * @param sparqlQuery the SPARQL query.
     * @return an ecapsulated result object.
     * @throws MDDException is thrown if the query cannot be executed properly.
     */
    protected Transporter queryInternal(String sparqlQuery) throws MDDException {
        QueryExecution qExec = null;
        sparqlQuery = addPrefix(sparqlQuery);
        LOGGER.debug("QUERY: " + sparqlQuery);
        try {
            Query query = QueryFactory.create(sparqlQuery);
            qExec = QueryExecutionFactory.create(query, this.con.getOntologyModel());
            ResultSet results = qExec.execSelect();
            return new Transporter(results, qExec);
        } catch (QueryParseException | java.util.regex.PatternSyntaxException e) {
            try {
                con.autoCommitReadOnly(); // important
            //CHECKSTYLE:OFF it's best to catch all exeptions to avoid
            //unexpected Jena exceptions (Jena is still not very mature).
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage());
            }
            try {
                qExec.close();
            } catch (Exception ex) {
             //CHECKSTYLE:ON
            }
            throw new MDDException(String.format("Could not parse SPARQL: %s. Complete SPARQL: %s.", e.getMessage(),
                    sparqlQuery));
        }
    }

    /**
     * Convert a result set object into a list of entries.
     * @param results the result (Jena internal object)
     * @param resultVars the variables that should be extracted, as a list.
     * @return a list of records. Each record consists of a list of values.
     */
    protected List<List<String>> convertResultSetList(ResultSet results, List<String> resultVars) {
        List<List<String>> resultList = new ArrayList<List<String>>();
        while (results.hasNext()) {
            QuerySolution solution = results.next();
            List<String> row = new ArrayList<String>();

            int noVarNames = 0;
            Iterator<String> itVarNames = solution.varNames();
            while (itVarNames.hasNext()) {
                itVarNames.next();
                noVarNames++;
            }

            boolean skip = false;
            for (String varName : resultVars) {
                String content = null;
                if (solution.get(varName) != null) {
                    String out = solution.get(varName).toString();
                    content = qh.fillValue(varName, out);
                }
                if (content == null) {
                    skip = true;
                    break;
                } else {
                    row.add(content);
                }
            }
            if (!checkIfSkip(row, skip, noVarNames)) {
                resultList.add(row);
            }
            
        }
        return resultList;
    }
    
    private boolean checkIfSkip(List<String> row, boolean skip, int noVarNames) {
        if (!skip) {
            if (row.size() < noVarNames) { // no of varies is not equals to the number in the sparql statement
                skip = true;
            } else if (row.size() == 1 && (row.get(0).equals("0.0") || row.get(0).equals("0"))) {
                skip = true;
            } 
        }
        return skip;
    }

    /**
     * Convert a result set into a list of maps.
     * @param results the result set from Jena.
     * @param resultVars the variables that should be extracted into the map.
     * @return a list of records. Each record is represented by a map, where the resultVars 
     * represent the key values.
     */
    protected List<QueryMap> convertResultSet(ResultSet results, List<String> resultVars) {
        List<QueryMap> resultList = new ArrayList<QueryMap>();
        while (results.hasNext()) {
            QuerySolution solution = results.next();
            QueryMap row = new QueryMap();
            boolean skip = false;
            for (String varName : resultVars) {
                String content = null;
                if (solution.get(varName) != null) {
                    String out = solution.get(varName).toString();
                    content = qh.fillValue(varName, out);
                }
                if (content == null) {
                    skip = true;
                    break;
                } else {
                    row.put(varName, content);
                }
            }
            if (!skip) {
                resultList.add(row);
            }
        }
        return resultList;
    }

    @Override
    public List<String> extractNames(String sparqlQuery) throws MDDException {
        Transporter transporter = this.queryInternal(sparqlQuery);
        ResultSet results = transporter.results;
        List<String> titles = null;
        try {
            titles = results.getResultVars();
            return titles;
        } finally {
            con.autoCommitReadOnly();
            try {
                transporter.qExec.close();
            //CHECKSTYLE:OFF safe way to ensure that no Jena connections
            //bubble up.
            } catch (Exception e) {

            }
            //CHECKSTYLE:ON
        }
    }

    @Override
    public String getSparqlDefaultHeader() throws MDDException {
        return con.getSparqlBase();
    }
    
    private class Transporter {
        private QueryExecution qExec;
        private ResultSet results;

        public Transporter(ResultSet r, QueryExecution qExec) {
            this.results = r;
            this.qExec = qExec;
        }

    }
}
