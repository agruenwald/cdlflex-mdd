package org.cdlflex.mdd.sembase.jena.internal;

import java.util.Properties;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.JenaConnection;
import org.cdlflex.mdd.sembase.jena.OntAccessInfo;

/**
 * Utilities for the creation of Jena connections (internal).
 */
public final class ConnectionUtil {
    private static final String OWL_CONFIG = "owlapi-config.properties";
    
    private ConnectionUtil() {
        
    }
    /**
     * Create a new Jena connection.
     * @param configLoader the configuration loader.
     * @param type the type of Jena connection.
     * @return a connection to the Jena data store.
     * @throws MDDException is thrown if connecting fails.
     */
    public static JenaConnection createJenaConnection(ConfigLoader configLoader, String type) throws MDDException {
        return createJenaConnectionInternal(configLoader, type, true);
    }

    /**
     * Create a new Jena connection, assuming single process environment (single JVM).
     * @param configLoader the configuration loader.
     * @param type the type of Jena connection.
     * @return a connection to the Jena data store.
     * @throws MDDException is thrown if connecting fails.
     */
    public static JenaConnection createJenaConnectionSingleProcess(ConfigLoader configLoader, String type)
        throws MDDException {
        return createJenaConnectionInternal(configLoader, type, false);
    }

    /**
     * Create a new Jena connection, assuming multiple process environment (multiple JVM).
     * Naive lock is implemented with low performance (not recommended to use).
     * @param configloader the configuration loader.
     * @param type the type of Jena connection.
     * @return a connection to the Jena data store.
     * @throws MDDException is thrown if connecting fails.
     */
    public static JenaConnection createJenaConnectionNaivLock(ConfigLoader configloader, String type)
        throws MDDException {
        return createJenaConnectionInternal(configloader, type, true);
    }

    private static JenaConnection createJenaConnectionInternal(ConfigLoader configLoader, String type,
        boolean isMultJVM) throws MDDException {
        JenaConnection con = new JenaConnection(configLoader, type, isMultJVM);
        return con;
        /*
        Properties prop = null;
        if (type.toLowerCase().equals("tdb")) {
            prop = configLoader.findPropertyFile(JENA_CONFIG_TDB);
        } else {
            prop = configLoader.findPropertyFile(JENA_CONFIG_SDB);
        }

        if (prop == null) {
            throw new MDDException(String.format("Could not find config file for Jena %s.", type));
        }
        
        //OntAccessInfo oai = new OntAccessInfo();
        //oai.setAbsolutePath(configLoader.extractFilePath(prop.getProperty("absoluteFilePath")));
        //oai.setOntologySchemaName(configLoader.extractFilePath(prop.getProperty("ontologySchemaName")));
       
        JenaConnection con =
            new JenaConnection(configLoader, configLoader.extractFilePath(prop.getProperty("ontologySchemaName")),
                    configLoader.extractFilePath(prop.getProperty("extendedOntologyName")),
                    configLoader.extractFilePath(prop.getProperty("absoluteFilePath")),
                    configLoader.extractFilePath(prop.getProperty("ontologySchemaIRI")),
                    configLoader.extractFilePath(prop.getProperty("extendedOntologyIRI")),
                    configLoader.extractFilePath(prop.getProperty("jenaStorageTechnology")),
                    configLoader.extractFilePath(prop.getProperty("databasetype")), configLoader.extractFilePath(prop
                            .getProperty("host")), prop.getProperty("port"), prop.getProperty("schema"),
                    prop.getProperty("username"), prop.getProperty("pw"), isMultJVM);
        return con;
        */
    }

    /**
     * Create a facet which basically contains the same features as the OWL API connection to retrieve the file
     * locations, but does not contain any connectivity services. All features, including the repository information are
     * taken from a local file.
     * @param configLoader the configuration loader.
     * @return the owl facet with an interface similar to the OWLAPI connection.
     * @throws MDDException is thrown if initialization failed.
     */
    public static OwlApiFacet createOwlApiFacetComplete(ConfigLoader configLoader) throws MDDException {
        Properties p = configLoader.findPropertyFile(OWL_CONFIG);
        OntAccessInfo oai = new OntAccessInfo(configLoader, p);
        return createOwlApiFacet(oai);
    }

    /**
     * Create a new OWL API access facet, which is necessary to serialize / deserialize
     * OWL ontology files.
     * @param ontAccessInfo an object containing the relevant parameters about the ontology 
     * and the extended ontology. The parameters must be filled (except the dbInfo).
     * @return an object with access to the basic OWL API serialization methods.
     * @throws MDDException is thrown if initialization failed.
     */
    public static OwlApiFacet createOwlApiFacet(OntAccessInfo ontAccessInfo) throws MDDException {
        OwlApiFacet con = new OwlApiFacet(ontAccessInfo);
        return con;
    }

}
