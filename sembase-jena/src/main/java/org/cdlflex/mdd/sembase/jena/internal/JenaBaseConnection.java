package org.cdlflex.mdd.sembase.jena.internal;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.jena.OntAccessInfo;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sdb.Store;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * Abstract connection for different Jena Technologies 
 * (SDB based on relational databases, TDB, etc.). 
 */
public abstract class JenaBaseConnection implements Connection {
    protected Model model;
    protected OntModelSpec modelSpec;
    protected OntModel ontModel;
    protected static Logger logger = LoggerFactory.getLogger(JenaBaseConnection.class);
    protected Store store;
    protected final OntAccessInfo ontAccessInfo;
    protected final String jenaStorageTechnology;   
    protected boolean inferring;
    protected boolean connected;
    protected boolean autoCommitMode;
    protected boolean transactionStarted;
    protected boolean transactionIsReadMode;
    protected boolean tmpAutoCommitMode;

    private final OwlApiFacet owlApiFacet;
    private static final String ONT_FORMAT = "rdf/xml"; // "turtle";
    
    /**
     * Get access to the extended ontology.
     * @return the file of the extended ontology.
     */
    public File getExtendedFile() {
        File extendedOntology = new File(ontAccessInfo.getAbsolutePath() 
                + ontAccessInfo.getExtendedOntologyName() + ".owl");
        return extendedOntology;
    }
    
    /**
     * Create a new Jena base connection.
     * @param ontAccessData all connection data and ontology data (all parameters must be filled).
     * @param jenaStorageTechnology the storage technology according to the static variables 
     *        {@code JenaConnection}.
     * @throws MDDException is thrown if the connection to the ontology fails.
     */
    public JenaBaseConnection(OntAccessInfo ontAccessData, String jenaStorageTechnology) throws MDDException {
        this.ontAccessInfo = ontAccessData;
        this.jenaStorageTechnology = jenaStorageTechnology == null ? "" : jenaStorageTechnology;
        this.autoCommitMode = true;
        this.transactionStarted = false;
        this.connected = false;
        this.inferring = false;
        tmpAutoCommitMode = false;

        this.ontModel = null;
        this.model = null;
        this.modelSpec = null;

        owlApiFacet = ConnectionUtil.createOwlApiFacet(ontAccessInfo);
        logger.trace("OntologySchemaName: " + ontAccessInfo.getOntologySchemaName());
        logger.trace("ExtendedOntologyName: " + ontAccessInfo.getExtendedOntologyName());
    }

    /**
     * Delete the entire model (not only the instances, but the OWL model itself too).
     * 
     * @throws MDDException is thrown if the cleaning of the ontology fails.
     */
    protected abstract void clearOntology() throws MDDException;

    /**
     * Get the header of the SPARQL query expression which contain the prefixes.
     * 
     * @return a String which contains the SPARQL prefix shortcuts.
     */
    public String getSparqlBase() {
        return "prefix xsd: <http://www.w3.org/2001/XMLSchema#> \n"
            + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
            + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  \n"
            + "prefix owl: <http://www.w3.org/2002/07/owl#>  \n" + "prefix " 
            + this.ontAccessInfo.getOntologySchemaName() + ": <"
            + this.ontAccessInfo.getOntologyIRI() + "#> \n" + "prefix " 
            + this.ontAccessInfo.getExtendedOntologyName() + ": <"
            + this.ontAccessInfo.getExtendedIRI() + "#> \n";
    }

    /**
     * Execute a SPARQL query.
     * @param sparql the SPARQL query.
     * @param variable a single variable name to extract.
     * @param ont the ontology model
     * @return a list of extracted variable values.
     */
    protected List<String> sparqlQueryHelper(String sparql, String variable, OntModel ont) {
        List<String> result = new LinkedList<String>();
        Query query = QueryFactory.create(sparql);
        QueryExecution qe = QueryExecutionFactory.create(query, ont);
        ResultSet rs = null;
        try {
            rs = qe.execSelect();
            while (rs.hasNext()) {
                QuerySolution rec = rs.next();
                result.add(rec.get(variable).toString());
            }
            qe.close();
        } finally {
            qe.close();
        }
        return result;
    }

    /**
     * Create a ontology specification with the type of OWL processing in Jena.
     * @return a ontology specification object of Jena.
     */
    protected OntModelSpec createModelSpec() {
        return new OntModelSpec(OntModelSpec.OWL_MEM);
    }

    /**
     * Check whether the ontology is connected or not.
     * @throws MDDException is thrown in case of failures.
     */
    protected void checkConnected() throws MDDException {
        this.checkConnected(true);
    }

    /**
     * Check whether the ontology is connected or not. 
     * @param checkEmptyness if true then empty ontologies also lead to an
     * exception.
     * @throws MDDException is thrown in case of connection failures, if the
     * model is null, or if the model is empty.
     */
    protected abstract void checkConnected(boolean checkEmptyness) throws MDDException;

    /**
     * Check whether the model is empty or not.
     * @throws MDDException is thrown if the model is empty or null.
     */
    protected void checkModelEmptyness() throws MDDException {
        if (model == null || model.isEmpty()) {
            throw new MDDException(
                    String.format(
                            "The Jena model is still empty. Did you initialize it with OWL data? "
                                + "Please also check if the Jena directory is accessible in write-mode. " 
                                + "Jena information: %s",
                            this));
        }
    }

    public OntModel getOntologyModel() {
        return this.ontModel;
    }

    /**
     * Build a connection to a (remote) database server.
     * @throws MDDException is thrown if connecting fails.
     */
    public abstract void connect() throws MDDException;

    @Override
    public void autoBegin() throws MDDException {
        this.autoBegin(false);
    }

    /**
     * Create a new connection (auto commit mode).
     * @param isReadonly if true then only read requests are performed.
     * @return true if the transaction started.
     * @throws MDDException is thrown if starting the transaction failed.
     */
    protected boolean autoBegin(boolean isReadonly) throws MDDException {
        if (this.autoCommitMode) {
            if (!transactionStarted) {
                this.begin(false, isReadonly);
                this.autoCommitMode = true;
                transactionStarted = true;
                return true;
            } else if (transactionStarted && transactionIsReadMode && !isReadonly) {
                logger.info("Switch TDB transaction from read-mode to write-mode...");
                this.rollback();
                this.begin(false, isReadonly);
                this.autoCommitMode = true;
                transactionStarted = true;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public void autoBeginReadOnly() throws MDDException {
        this.autoBegin(true);
    }

    @Override
    public void begin() throws MDDException {
        checkConnected();
        this.begin(true, false);
    }

    @Override
    public void connectOnDemand() throws MDDException {
        if (!this.isConnected()) {
            this.connect();
        }
    }

    @Override
    public boolean isConnected() {
        return this.connected;
    }

    /**
     * Begin a new transaction (manual mode).
     * @param setAutoCommitTag if true then after each operation auto-commit is performed.
     * @param isReadonly if true then only read operations can be executed.
     * @throws MDDException is thrown in case that the transaction could not be started successfully. 
     */
    protected abstract void begin(boolean setAutoCommitTag, boolean isReadonly) throws MDDException;

    @Override
    public abstract void commit() throws MDDException;

    @Override
    public abstract void rollback() throws MDDException;

    @Override
    public void autoCommit() throws MDDException {
        if (this.autoCommitMode) {
            this.commit();
            this.autoCommitMode = true;
            transactionStarted = false;
        }
    }

    /**
     * Properly close a db connection again.
     * @throws MDDException is thrown if the transaction cannot be closed properly.
     */
    @Override
    public abstract void close() throws MDDException;

    public boolean isAutoCommitMode() {
        return autoCommitMode;
    }

    public void setAutoCommitMode(boolean autoCommitMode) {
        this.autoCommitMode = autoCommitMode;
    }

    @Override
    public void commitReadOnly() throws MDDException {
        this.checkConnected();
        transactionStarted = false;
    }

    @Override
    public void autoCommitReadOnly() {
        if (this.transactionStarted && this.autoCommitMode) {
            // abort
            try {
                this.commitReadOnly();
            } catch (MDDException e) {
                logger.error(String.format("AutoCommit read only failed: ", e.getMessage()));
            }
        }
    }

    @Override
    public void setInferring(boolean inferring) {
        this.inferring = inferring;
    }

    @Override
    public boolean isInferring() {
        return inferring;
    }

    /**
     * Read the base ontology file.
     * @return the ontology file (base; T-Box).
     */
    public File getBaseOntologyFile() {
        File baseOntology = new File(this.ontAccessInfo.getAbsolutePath() 
                + this.ontAccessInfo.getOntologySchemaName() + ".owl");
        return baseOntology;
    }

    /**
     * Get the access info to the ontology (encapsulated).
     * @return an ontology access information object.
     */
    public OntAccessInfo getOntAccessInfo() {
        return ontAccessInfo;
    }
    /**
     * Get a list of all individuals within the ontology.
     * @return a list of individuals.
     */
    public List<Individual> listAllIndividuals() {
        List<Individual> list = new LinkedList<Individual>();
        ExtendedIterator<Individual> individuals = this.ontModel.listIndividuals();
        while (individuals.hasNext()) {
            list.add(individuals.next());
        }
        return list;
    }

    /**
     * Remove all individuals of a specific class name.
     * @param ontClassName the ontology calss name, e.g. Requirements
     * @return the number of removed entries >= 0.
     * @throws MDDException is thrown if the deletion failed.
     */
    public int removeIndividuals(String ontClassName) throws MDDException {
        int removed = 0;
        OntClass ontClass = ontModel.getOntClass(this.ontAccessInfo.getOntologyIRI() + "#" + ontClassName);
        if (ontClass == null) {
            throw new MDDException(String.format("Did not find ontology class: %s.", ontClassName));
        }
        ExtendedIterator<? extends OntResource> instances = ontClass.listInstances();
        List<Individual> removals = new LinkedList<Individual>();
        while (instances.hasNext()) {
            Individual indi = (Individual) instances.next();
            removals.add(indi);
        }

        while (!removals.isEmpty()) {
            Individual indi = removals.get(0);
            logger.debug("Remove " + indi);
            removals.remove(0);
        }

        logger.debug(String.format("Removed %d individuals of type %s", removed, ontClassName));
        return removed;
    }

    @Override
    public void resetData() throws MDDException {
        boolean selfConnected = false;
        try {
            this.checkConnected(false);
        } catch (MDDException e) {
            // not connected
            this.connect();
            selfConnected = true;
        }
        this.autoBegin();
        String sparql =
            getSparqlBase() + "SELECT DISTINCT ?sub ?obj " + "WHERE { ?sub rdf:type ?obj. FILTER regex(str(?obj), \""
                + "^" + this.ontAccessInfo.getOntologyIRI() + "\") }";
        List<String> indNames = sparqlQueryHelper(sparql, "?sub", this.getOntologyModel());
        for (String indName : indNames) {
            logger.debug(String.format("Delete individual %s.", indName));
            Individual linkedInd = this.getOntologyModel().getIndividual(indName);
            if (linkedInd == null) {
                logger.warn(String.format("Individual %s does not exist and hence wasn't removed.", indName));
                sparqlQueryHelper(sparql, "?sub", this.getOntologyModel()); // just a hook to simplify debugging
            } else {
                this.getOntologyModel().getIndividual(indName).remove();
            }
        }
        this.autoCommit();
        if (selfConnected) {
            selfConnected = false;
            this.close();
        }
    }

    /**
     * Load a model from a OWL/XML file created with Protege, and insert it into the Jena store.
     * @throws MDDException is thrown if a given location is wrong (retrieved from the configuration files) or if any
     *         incompatibility or problem with the ontology occurred.
     */
    @Override
    public void resetOntology() throws MDDException {
        this.clearOntology();
        logger.info(String.format("Location of base ontology file: %s", owlApiFacet.getBaseOntologyFile()
                .getAbsolutePath()));
        logger.info(String.format("Location of extended ontology file: %s", owlApiFacet.getExtendedFile()
                .getAbsolutePath()));
        File tmpFileBasic = null;
        File tmpExtendedFile = null;
        try {
            tmpFileBasic = File.createTempFile("temp", Long.toString(System.nanoTime()) + ".tmp");
            tmpExtendedFile = File.createTempFile("temp", Long.toString(System.nanoTime()) + ".tmp");
        } catch (IOException e) {
            throw new MDDException(String.format("Could not create temporary file for OWL serialization (%s).",
                    e.getMessage()));
        }

        owlApiFacet.createDataFactoryInternal();

        try {
            owlApiFacet.serializeOntology(tmpFileBasic.getAbsolutePath(), 
                    this.ontAccessInfo.getOntologyIRI(), ONT_FORMAT);
            owlApiFacet.serializeOntology(tmpExtendedFile.getAbsolutePath(), ONT_FORMAT);
            logger.info(String.format("Serialized basic ontology into %s (format %s).",
                    tmpFileBasic.getAbsolutePath(), ONT_FORMAT));
            logger.info(String.format("Serialized ontology into %s (format %s).", tmpExtendedFile.getAbsolutePath(),
                    ONT_FORMAT));
            this.loadFilesIntoDb(tmpFileBasic, tmpExtendedFile, ONT_FORMAT);
            logger.trace("Commited and closed.");
        } catch (OWLOntologyStorageException | URISyntaxException | IOException e) {
            throw new MDDException("Could not serialize ontology into " + tmpExtendedFile.getAbsolutePath() + " ("
                + e.getMessage() + ")");
        } finally {
            tmpFileBasic.delete();
            tmpExtendedFile.delete();
        }
        /* to speed up the reset we save a copy of the model in the db */
    }

    /**
     * loads the ontology from the serialized files into the triple store.
     * @param tmpFileBasic the temporary file for the basic ontology.
     * @param tmpExtendedFile the temporary file for the extended ontology.
     * @param ontFormat the format of the ontology
     * @throws MDDException thrown if something fails.
     */
    protected abstract void loadFilesIntoDb(File tmpFileBasic, File tmpExtendedFile, String ontFormat)
        throws MDDException;

    @Override
    public String toString() {
        return String.format("Jena %s: Host: %s:%s (OWL data: %s - %s)", 
                this.ontAccessInfo.getDbInfo().getDatabasetype(), 
                this.ontAccessInfo.getDbInfo().getHost(), 
                this.ontAccessInfo.getDbInfo().getPort(),
                this.ontAccessInfo.getAbsolutePath(),
                this.ontAccessInfo.getOntologySchemaName());
    }

    /**
     * Reset all ontologies / memory clean up (if possible).
     * @throws MDDException is thrown if cleaning fails.
     */
    public abstract void clearAllOntologies() throws MDDException;
}
