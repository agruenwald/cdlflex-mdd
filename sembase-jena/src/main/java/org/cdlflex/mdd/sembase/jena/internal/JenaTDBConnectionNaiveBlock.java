package org.cdlflex.mdd.sembase.jena.internal;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.URISyntaxException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.OntAccessInfo;

/**
 * Because Jena TDB turned out to be not multi thread/daemon safe, a simple lock mechanism is used to ensure that
 * different Java VMs do not modify the data concurrently.
 */
public class JenaTDBConnectionNaiveBlock extends JenaTDBConnection {
    private static final int MAX_IDLE_TIME = 60000;
    private static final int THREAD_SLEEP_TIME = 1000;
    private static final int IDLE_TIME = 99999;
    
    public JenaTDBConnectionNaiveBlock(OntAccessInfo ontAccessInfo, String jenaStorageTechnology) throws MDDException {
        super(ontAccessInfo, jenaStorageTechnology);
    }

    private File getLockFile() throws MDDException {
        try {
            return new File(ConfigLoader.getUrl(this.ontAccessInfo.getDbInfo().getHost() 
                    + "/tdb_lock.info").toURI().getPath());
        } catch (URISyntaxException | IOException e) {
            throw new MDDException(e.getMessage());
        }
    }

    private void writeLockFile() throws MDDException {
        try {
            File lockFile = new File(ConfigLoader.getUrl(this.ontAccessInfo.getDbInfo().getHost() 
                    + "/tdb_lock.info").toURI().getPath());
            String appId = getProcessId();
            FileUtils.writeStringToFile(lockFile, appId);
        } catch (IOException | URISyntaxException e) {
            throw new MDDException(e.getMessage());
        }
    }

    private void releaseLock() throws MDDException {
        File lockFile;
        try {
            lockFile = new File(ConfigLoader.getUrl(this.ontAccessInfo.getDbInfo().getHost() 
                    + "/tdb_lock.info").toURI().getPath());
            lockFile.delete();
        } catch (URISyntaxException | IOException e) {
            throw new MDDException(e.getMessage());
        }
    }

    private void waitForRelease() throws MDDException {
        File lock = getLockFile();
        if (!lock.exists()) {
            return;
        } else {
            String actAppId = getProcessId();
            String appId = "";
            try {
                appId = IOUtils.toString(ConfigLoader.getUrl(lock.getAbsolutePath()).toURI());
            } catch (IOException | URISyntaxException e1) {
                logger.error("TDB lock: " + e1.getMessage());
            }

            long lastModified = lock.lastModified();
            int idle = 0;
            while (idle < MAX_IDLE_TIME && (lastModified > (new Date().getTime() - MAX_IDLE_TIME))) {
                try {
                    logger.debug("TDB locked.");
                    if (!getLockFile().exists()) {
                        return;
                    } else if (appId.equals(actAppId)) {
                        // same app is locking
                        logger.info("Removed lock.");
                        releaseLock();
                        return;
                    }
                    logger.warn(String.format("Jena TDB locked by JVM with id %s (act=%s). Wait until unlock...",
                            appId, actAppId));
                    Thread.sleep(THREAD_SLEEP_TIME);
                    idle += THREAD_SLEEP_TIME;
                    if (actAppId.equals(appId)) {
                        idle += THREAD_SLEEP_TIME * 2; // speed up waiting
                    }
                } catch (InterruptedException e) {
                    idle = IDLE_TIME;
                }
            }
        }
    }

    /**
     * Get the process id of the JVM process.
     * @return the process id.
     */
    public static String getProcessId() {
        String pname = ManagementFactory.getRuntimeMXBean().getName();
        String pid = pname;
        int i = pname.indexOf("@");
        if (i != -1) {
            pid = pname.substring(0, i);
        }
        return pid;
    }

    @Override
    protected void beginTransInitModel() throws MDDException {
        super.beginTransInitModel();
        writeLockFile();
    }

    @Override
    protected void checkBeforeStartTrans() throws MDDException {
        waitForRelease();
    }

    @Override
    public void commit() throws MDDException {
        try {
            super.commit();
            releaseLock();
        } catch (MDDException e) {
            throw e;
        }
    }

    @Override
    public void commitReadOnly() throws MDDException {
        super.commitReadOnly();
        releaseLock();
    }

    @Override
    protected void endTrans() {
    }

    @Override
    public void rollback() throws MDDException {
        super.rollback();
        releaseLock();
    }

    @Override
    public void close() throws MDDException {
        try {
            super.close();
        } finally {
            releaseLock();
        }
    }

}
