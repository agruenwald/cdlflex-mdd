package org.cdlflex.mdd.sembase.jena.internal;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.OntAccessInfo;
import org.coode.owlapi.turtle.TurtleOntologyFormat;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.io.RDFXMLOntologyFormat;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.util.SimpleIRIMapper;

/**
 * This class is a facet for the OWL API connection. Because the OWLAPI Pellet bundle and Jena API are incompatible,
 * this class only contains the most relevant OWLAPI connection features without providing any reasoning service.
 */
public class OwlApiFacet implements Serializable {
    private static final long serialVersionUID = 1304305777161762852L;
    private final String ontologySchemaName;
    private final String extendedOntologyName;
    private final String ontologySchemaIRI;
    private final String extendedOntologyIRI;
    private final String absoluteFilePath;
    private OWLOntologyManager manager;
    private OWLOntology mainOnt;

    /**
     * Initialize a new object that facets the access to OWL files (serialized files) via the OWL API.
     * @param ontologySchemaName the name of the basic ontology, which contains the generated C-Box and T-Box elements.
     *        e.g., itpm (not itpm.owl!)
     * @param ontAccessInfo the ontology access info.
     */
    public OwlApiFacet(OntAccessInfo ontAccessInfo) {
        this.ontologySchemaName = ontAccessInfo.getOntologySchemaName();
        this.extendedOntologyName = ontAccessInfo.getExtendedOntologyName();
        this.absoluteFilePath = ontAccessInfo.getAbsolutePath();
        this.extendedOntologyIRI = ontAccessInfo.getExtendedIRI();
        this.ontologySchemaIRI = ontAccessInfo.getOntologyIRI();
        manager = null;
        mainOnt = null;
    }

    /**
     * The base ontology file.
     * @return the ontology file.
     */
    public File getBaseOntologyFile() {
        File baseOntology = new File(absoluteFilePath + this.ontologySchemaName + ".owl");
        return baseOntology;
    }

    /**
     * The extended ontology file.
     * @return the extended ontology file.
     */
    public File getExtendedFile() {
        File extendedOntology = new File(absoluteFilePath + extendedOntologyName + ".owl");
        return extendedOntology;
    }

    public String getOntologySchemaIRI() {
        return ontologySchemaIRI;
    }

    public String getExtendedOntologyIRI() {
        return extendedOntologyIRI;
    }

    /**
     * Create an internal data factory to be used for the serialization of ontologies.
     * @throws MDDException is thrown in case that the file paths are wrong.
     */
    public void createDataFactoryInternal() throws MDDException {
        manager = OWLManager.createOWLOntologyManager();
        File basicOnt = this.getBaseOntologyFile();
        File extendedOnt = this.getExtendedFile();
        manager.addIRIMapper(new SimpleIRIMapper(IRI.create(this.getOntologySchemaIRI()), IRI.create(basicOnt)));
        manager.addIRIMapper(new SimpleIRIMapper(IRI.create(this.getExtendedOntologyIRI()), IRI.create(extendedOnt)));
        try {
            mainOnt = manager.loadOntologyFromOntologyDocument(basicOnt);
            mainOnt = manager.loadOntologyFromOntologyDocument(extendedOnt);
            manager.getOWLDataFactory();
        } catch (OWLOntologyCreationException e) {
            throw new MDDException("Could not initialize semantic reasoning facility: " + e.getMessage());
        }
    }

    /**
     * Serialize the OWL ontology.
     * @param filepath the target path.
     * @param format the format of the serialization, e.g. OWL/XML.
     * @throws OWLOntologyStorageException is storage of the OWL ontology fails.
     * @throws URISyntaxException is thrown if URIs/IRIs are wrong.
     * @throws IOException is thrown if basic file access problems occur.
     */
    public void serializeOntology(String filepath, String format) throws OWLOntologyStorageException,
        URISyntaxException, IOException {
        this.serializeOntology(filepath, null, format);
    }

    /**
     * Serialize the OWL ontology.
     * @param filepath the absolute file name. 
     * @param ontSchemaIRI the IRI of the (sub-) ontology to be serialized.
     * @param format turtle or rdf/xml. default = owl/xml
     * @throws OWLOntologyStorageException is storage of the OWL ontology fails.
     * @throws URISyntaxException is thrown if URIs/IRIs are wrong.
     * @throws IOException is thrown if basic file access problems occur.
     */
    public void serializeOntology(String filepath, String ontSchemaIRI, String format)
        throws OWLOntologyStorageException, URISyntaxException, IOException {
        OWLOntology persistOnt = null;
        if (ontSchemaIRI != null && !ontSchemaIRI.isEmpty()) {
            persistOnt = manager.getOntology(IRI.create(ontSchemaIRI));
        } else {
            persistOnt = mainOnt;
        }
        URL fileURL = null;
        if (format.toLowerCase().equals("turtle")) {
            manager.saveOntology(persistOnt, new TurtleOntologyFormat(), IRI.create(ConfigLoader.getUrl(filepath)));
        } else if (format.toLowerCase().equals("rdf/xml")) {
            try {
                fileURL = ConfigLoader.getUrl(filepath);
            } catch (MalformedURLException e) {
                throw new IOException(e.getMessage());
            }
            manager.saveOntology(persistOnt, new RDFXMLOntologyFormat(), IRI.create(fileURL.toURI()));
        } else {
            manager.saveOntology(persistOnt, new OWLXMLOntologyFormat(),
                    IRI.create(ConfigLoader.getUrl(filepath).toURI()));
        }
    }
}
