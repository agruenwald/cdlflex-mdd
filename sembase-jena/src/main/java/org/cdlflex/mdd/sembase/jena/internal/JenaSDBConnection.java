package org.cdlflex.mdd.sembase.jena.internal;

import java.io.File;
import java.sql.SQLException;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.jena.OntAccessInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sdb.SDBFactory;
import com.hp.hpl.jena.sdb.StoreDesc;
import com.hp.hpl.jena.sdb.sql.JDBC;
import com.hp.hpl.jena.sdb.sql.SDBConnection;
import com.hp.hpl.jena.sdb.store.DatabaseType;
import com.hp.hpl.jena.sdb.store.LayoutType;
import com.hp.hpl.jena.sdb.util.StoreUtils;
import com.hp.hpl.jena.util.FileManager;

/**
 * Implementation of the Jena SDB storage technology connector, based on relational databases.
 */
public class JenaSDBConnection extends JenaBaseConnection {
    private static final Logger LOGGER = LoggerFactory.getLogger(JenaSDBConnection.class);

    public JenaSDBConnection(OntAccessInfo ontAccessInfo, String jenaStorageTechnology) throws MDDException {
        super(ontAccessInfo, jenaStorageTechnology);
    }

    @Override
    public void clearOntology() throws MDDException {
        checkConnected(false); // check if connected, do not check for emptyness
        ontModel.removeAll();
        // works for all: store.getTableFormatter().truncate();
    }

    @Override
    protected void checkConnected(boolean checkEmptyness) throws MDDException {
        if (store == null || model == null || !connected) {
            throw new MDDException(String.format("Please connect first before beginning a new transaction."));
        } else if (checkEmptyness) {
            checkModelEmptyness();
        }
    }

    /**
     * Build a connection to a (remote) database server.
     * @throws MDDException is thrown if connecting fails.
     */
    public void connect() throws MDDException {
        if (this.connected) {
            throw new MDDException("Already connected to Jena.");
        }

        String jdbcUrl = "jdbc:" + ontAccessInfo.getDbInfo().getDatabasetype() 
                + "://" + ontAccessInfo.getDbInfo().getHost() 
                + (ontAccessInfo.getDbInfo().getPort().isEmpty() ? "" : ":" + ontAccessInfo.getDbInfo().getPort()) 
                + "/" + ontAccessInfo.getDbInfo().getSchema();
        LOGGER.info(String.format("Connect with %s (SDB).", jdbcUrl));
        StoreDesc storeDescription = null;
        storeDescription = new StoreDesc(LayoutType.LayoutTripleNodesHash, DatabaseType.MySQL);
        JDBC.loadDriverMySQL();
        SDBConnection connection = new SDBConnection(jdbcUrl, ontAccessInfo.getDbInfo().getUsername(), 
                ontAccessInfo.getDbInfo().getPw());
        store = SDBFactory.connectStore(connection, storeDescription);
        try {
            if (!StoreUtils.isFormatted(store)) {
                store.getTableFormatter().create();
            }
            modelSpec = createModelSpec();
            Model model = SDBFactory.connectNamedModel(store, ontAccessInfo.getOntologyIRI());
            this.model = model;
            modelSpec.getDocumentManager().setProcessImports(false); // avoid reading urls, such as http://www.itpm...
            this.ontModel = ModelFactory.createOntologyModel(modelSpec, model);
        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage());
            throw new MDDException(ex.getMessage());
        }
        connected = true;
    }

    @Override
    public void begin(boolean setAutoCommitTag, boolean isReadonly) throws MDDException {
        if (setAutoCommitTag) {
            tmpAutoCommitMode = this.autoCommitMode;
            this.autoCommitMode = false;
            if (!this.transactionStarted) {
                ontModel.begin();
            }
        } else {
            if (this.transactionStarted) {
                throw new MDDException(String.format("Transaction already openened."));
            }
            ontModel.begin();
        }
        transactionStarted = true;
    }

    @Override
    public void commitReadOnly() throws MDDException {
        super.commitReadOnly();
        this.ontModel.abort();
    }

    @Override
    public void commit() throws MDDException {
        checkConnected();
        transactionStarted = false;
        ontModel.commit();
        this.autoCommitMode = tmpAutoCommitMode;
    }

    @Override
    public void rollback() throws MDDException {
        checkConnected();
        this.ontModel.abort();
        transactionStarted = false;
        this.autoCommitMode = tmpAutoCommitMode;
    }

    @Override
    public void close() throws MDDException {
        try {
            ontModel.close();
            model.close();
            store.getConnection().close(); // close JDBC connection
            store.close();
            connected = false;
        //CHECKSTYLE:OFF
        } catch (Exception e) {
            throw new MDDException("Failed closing Jena connnection: " + e.getMessage());
        } finally {
            LOGGER.info("Closed SDB connection.");
        }
        //CHECKSTYLE:ON
    }

    @Override
    protected void loadFilesIntoDb(File tmpFileBasic, File tmpExtendedFile, String ontFormat) throws MDDException {
        OntModel newmodel = ModelFactory.createOntologyModel(this.modelSpec, model);
        FileManager fileManager = FileManager.get();
        LOGGER.info(String.format("load file %s(%s)", tmpFileBasic.getAbsolutePath(), 
                this.ontAccessInfo.getOntologyIRI()));
        fileManager.readModel(newmodel, "file://" + tmpFileBasic.getAbsolutePath(), this.ontAccessInfo.getOntologyIRI(),
                ontFormat.toUpperCase());
        LOGGER.info(String.format("load file %s", tmpExtendedFile.getAbsolutePath()));
        fileManager.readModel(newmodel, "file://" + tmpExtendedFile.getAbsolutePath(), ontFormat.toUpperCase());
        LOGGER.info(String.format("loading into DB done (named model %s).", this.ontAccessInfo.getOntologyIRI()));
        newmodel.commit();
    }

    @Override
    public String getNamespacePrefix() {
        return this.ontAccessInfo.getOntologySchemaName();
    }

    @Override
    public void clearAllOntologies() {
        LOGGER.warn(String.format("Clear All ontologies not implemented for Jena SDB!"));
    }

}
