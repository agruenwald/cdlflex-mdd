package org.cdlflex.mdd.sembase.jena;

import java.io.File;
import java.util.Properties;

import org.cdlflex.mdd.modelrepo.AdvancedModelStoreImpl;
import org.cdlflex.mdd.modelrepo.NamingManager;
import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.internal.JenaBaseConnection;
import org.cdlflex.mdd.sembase.jena.internal.JenaSDBConnection;
import org.cdlflex.mdd.sembase.jena.internal.JenaTDBConnection;
import org.cdlflex.mdd.sembase.jena.internal.JenaTDBConnectionNaiveBlock;
import org.cdlflex.mdd.sembase.jena.internal.JenaTDBConnectionSingleProcess;
import org.cdlflex.mdd.umltuowl.metamodel.MetaModel;

import com.hp.hpl.jena.ontology.OntModel;

/**
 * Facilitates the access to the specific Jena connectors. The actual Jean connectors provide either access via SDB or
 * TDB. They are wrapped within this connector to enable flexible access, also without the model repository which is
 * used (and recommended) by default CDLFlex projects.
 */
//CHECKSTYLE:OFF Coupling to 9 classes (max. 7) is ok. Reason: Different Jena connections.
public class JenaConnection implements Connection {
//CHECKSTYLE:ON
    private static final String SDB = "SDB";
    private static final String TDB = "TDB";
    private static final String NAIVE_BLOCK = "naiveBlock";
    private static final String MULTI_JVM = "multiJvm";
    private static final String SINGLE_PROCESS = "singleProcess";
    
    private static final String JENA_CONFIG_SDB = "jena-config-sdb.properties";
    private static final String JENA_CONFIG_TDB = "jena-config-tdb.properties";

    private static final String CONFIG_FILE = "jena-config.properties";
    private final JenaBaseConnection con;
    private final MetaModel metaModel;
    
    public JenaConnection(ConfigLoader configLoader, String jenaStorageTechnology, boolean isMultJVM) 
        throws MDDException {
        metaModel = null;
        jenaStorageTechnology = jenaStorageTechnology == null ? "" : jenaStorageTechnology;
        
        Properties prop = null;
        if (jenaStorageTechnology.toLowerCase().equals("tdb")) {
            prop = configLoader.findPropertyFile(JENA_CONFIG_TDB);
        } else {
            prop = configLoader.findPropertyFile(JENA_CONFIG_SDB);
        }

        if (prop == null) {
            throw new MDDException(String.format("Could not find config file for Jena %s.", jenaStorageTechnology));
        }
        
        OntAccessInfo oai = new OntAccessInfo();
        oai.setAbsolutePath(configLoader.extractFilePath(prop.getProperty("absoluteFilePath")));
        oai.setOntologyIRI(configLoader.extractFilePath(prop.getProperty("ontologySchemaIRI")));
        oai.setExtendedIRI(configLoader.extractFilePath(prop.getProperty("extendedOntologyIRI")));
        oai.setOntologySchemaName(configLoader.extractFilePath(prop.getProperty("ontologySchemaName")));
        oai.setExtendedOntologyName(configLoader.extractFilePath(prop.getProperty("extendedOntologyName")));
        oai.getDbInfo().setDatabasetype(prop.getProperty("databasetype"));
        oai.getDbInfo().setHost(configLoader.extractFilePath(prop.getProperty("host")));
        oai.getDbInfo().setPort(prop.getProperty("port"));
        oai.getDbInfo().setSchema(prop.getProperty("schema"));
        oai.getDbInfo().setUsername(prop.getProperty("username"));
        oai.getDbInfo().setPw(prop.getProperty("pw"));
        
        con = initCon(oai, jenaStorageTechnology, isMultJVM ? MULTI_JVM : SINGLE_PROCESS);       
    }

    public JenaConnection(ConfigLoader configLoader, String projectId, String ontNamePrefix) throws MDDException {
        Properties prop = configLoader.findPropertyFile(CONFIG_FILE);
        if (prop == null) {
            throw new MDDException(String.format("Cannot load Jena configuration %s.", CONFIG_FILE));
        }

        AdvancedModelStoreImpl store = new AdvancedModelStoreImpl(configLoader);
        metaModel = store.findMetaModel(ontNamePrefix);
        if (metaModel == null) {
            throw new MDDException(String.format("Failed to load ontology model \"%s\".", ontNamePrefix));
        }
        NamingManager nm = new NamingManager(metaModel);
        String jenaStorageTechnology = prop.getProperty("jenaStorageTechnology");
        jenaStorageTechnology = jenaStorageTechnology == null ? "" : jenaStorageTechnology;

        OntAccessInfo oai = new OntAccessInfo();
              
        if (jenaStorageTechnology.equals(SDB) || jenaStorageTechnology.equals(TDB)) {
            oai.setOntologySchemaName(nm.getNamspacePrefix());
            oai.setExtendedOntologyName(nm.getExtendedNamespacePrefix());
            oai.setOntologyIRI(nm.getNamespace());
            oai.setExtendedIRI(nm.getExtendedNamespace());
            //TODO check oai.setOntologySchemaName(new File(nm.getPhysicalOWL(configLoader)).getParent() + "/");
            oai.setAbsolutePath(new File(nm.getPhysicalOWL(configLoader)).getParent() + "/");
            
            DbInfo di = oai.getDbInfo();
            di.setDatabasetype(prop.getProperty("databasetype"));
            di.setHost(configLoader.extractFilePath(prop.getProperty("host")));
            di.setPort(prop.getProperty("port"));
            di.setSchema(prop.getProperty("schema"));
            di.setUsername(prop.getProperty("username"));
            di.setPw(prop.getProperty("pw"));
        }
       
        String tech = "";
        if (jenaStorageTechnology.equals(TDB)) {
            tech = prop.getProperty("impl", SINGLE_PROCESS).trim();
        }       
        con = initCon(oai, jenaStorageTechnology, tech);
    }
    
    private JenaBaseConnection initCon(OntAccessInfo oai, String jenaStorageTechnology, String tech) 
        throws MDDException {
        JenaBaseConnection baseCon;
        if (jenaStorageTechnology.equals(SDB)) {
            baseCon = new JenaSDBConnection(oai, jenaStorageTechnology);
        } else if (jenaStorageTechnology.equals(TDB)) {
            if (tech.equals(NAIVE_BLOCK)) {
                // modify depending on which implementation you prefer to use
                baseCon = new JenaTDBConnectionNaiveBlock(oai, jenaStorageTechnology);
            } else if (tech.equals(MULTI_JVM)) {
                baseCon = new JenaTDBConnection(oai, jenaStorageTechnology);
            } else if (tech.equals(SINGLE_PROCESS)) {
                baseCon = new JenaTDBConnectionSingleProcess(oai, jenaStorageTechnology);
            } else {
                throw new MDDException(String.format("Invalid jenaStorage Impementation: %s, %s",
                        jenaStorageTechnology, tech));
            }
        } else {
            throw new MDDException(String.format("Invalid jenaStorageTechnology: %s", jenaStorageTechnology));
        }
        return baseCon;
    }

    @Override
    public void connect() throws MDDException {
        con.connect();
    }

    @Override
    public void autoBegin() throws MDDException {
        con.autoBegin();
    }

    @Override
    public void begin() throws MDDException {
        con.begin();
    }

    @Override
    public void autoCommit() throws MDDException {
        con.autoCommit();
    }

    @Override
    public void commit() throws MDDException {
        con.commit();
    }

    @Override
    public void autoCommitReadOnly() throws MDDException {
        con.autoCommitReadOnly();
    }

    @Override
    public void commitReadOnly() throws MDDException {
        con.commitReadOnly();
    }

    @Override
    public void rollback() throws MDDException {
        con.rollback();
    }

    @Override
    public void close() throws MDDException {
        con.close();
    }

    @Override
    public void resetData() throws MDDException {
        con.resetData();
    }

    @Override
    public void resetOntology() throws MDDException {
        con.resetOntology();
    }

    @Override
    public boolean isInferring() {
        return con.isInferring();
    }

    @Override
    public void setInferring(boolean isInferring) {
        con.setInferring(isInferring);
    }

    public OntModel getOntologyModel() {
        return con.getOntologyModel();
    }

    public String getOntologySchemaIRI() {
        return con.getOntAccessInfo().getOntologyIRI();
    }

    public String getSparqlBase() {
        return con.getSparqlBase();
    }

    public MetaModel getMetaModel() {
        return this.metaModel;
    }

    /**
     * Remove all individuals with a specific class name.
     * @param ontClassName the class name, e.g., requirement.
     * @throws MDDException is thrown if the removal of individuals failed.
     * If zero individuals are removed, no exception is thrown by default.
     */
    public void removeIndividuals(String ontClassName) throws MDDException {
        con.removeIndividuals(ontClassName);
    }

    /**
     * Set auto commit mode.
     * @param autocommitMode if true then auto commit mode is turned on.
     */
    public void setAutoCommitMode(boolean autocommitMode) {
        con.setAutoCommitMode(autocommitMode);
    }

    /**
     * Get the status of the autocommit mode.
     * @return true if auto commit mode is enabled.
     */
    public boolean isAutoCommitMode() {
        return con.isAutoCommitMode();
    }

    @Override
    public String getNamespacePrefix() {
        return con.getNamespacePrefix();
    }

    @Override
    public void autoBeginReadOnly() throws MDDException {
        con.autoBeginReadOnly();
    }

    @Override
    public void connectOnDemand() throws MDDException {
        con.connectOnDemand();
    }

    @Override
    public boolean isConnected() throws MDDException {
        return con.isConnected();
    }

    /**
     * Clear all ontologies.
     * @throws MDDException is thrown if clearing fails.
     */
    public void clearAllOntologies() throws MDDException {
        con.clearAllOntologies();
    }
}
