package org.cdlflex.mdd.sembase.jena.semquery;

import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.jena.JenaConnection;
import org.cdlflex.mdd.sembase.jena.internal.SPARQLImpl;
import org.cdlflex.mdd.sembase.semquery.QueryInterface;
import org.cdlflex.mdd.sembase.semquery.QueryInterfaceFactory;

/**
 * Factory to get the default SPARQL implementation.
 */
public class QueryFactoryJena implements QueryInterfaceFactory {
    private final JenaConnection con;

    /**
     * Create a new object.
     * @param con the specific Jena connection.
     */
    public QueryFactoryJena(Connection con) {
        this.con = (JenaConnection) con;
    }

    @Override
    public QueryInterface getDefaultSparql() {
        return new SPARQLImpl(con);
    }

    @Override
    public QueryInterface getQueryInterface(String arg0) {
        switch (arg0.toLowerCase()) {
            case "sparql":
                return new SPARQLImpl(con);
            default:
                return null;
        }
    }
}
