package org.cdlflex.mdd.sembase.jena;

import java.util.Properties;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;


/**
 * The class couples the access information for specific ontologies.
 */
public class OntAccessInfo {
    private String ontologySchemaName;
    private String extendedOntologyName;
    private String absolutePath;
    private String ontologyIRI;
    private String extendedIRI;
    private DbInfo dbInfo;

    public OntAccessInfo() {
        this.dbInfo = new DbInfo();
    }
    
    /**
     * Create a new object and initialize it with the default parameters of the properties file.
     * @param configLoader the configuration loader.
     * @param p the related properties file.
     * @throws MDDException is thrown if the properties cannot be accessed.
     */
    public OntAccessInfo(ConfigLoader configLoader, Properties p) throws MDDException {
        this.ontologySchemaName = configLoader.extractFilePath(p.getProperty("ontologySchemaName"));
        this.extendedOntologyName = configLoader.extractFilePath(p.getProperty("extendedOntologyName"));
        this.absolutePath = configLoader.extractFilePath(p.getProperty("absoluteFilePath"));
        this.ontologyIRI = configLoader.extractFilePath(p.getProperty("ontologySchemaIRI"));
        this.extendedIRI = configLoader.extractFilePath(p.getProperty("extendedOntologyIRI"));
        this.dbInfo = new DbInfo();
    }
    
    /**
     * Get the ontology schema name.
     * @return the ontologySchemaName (absolute file path eg.)
     */
    public final String getOntologySchemaName() {
        return ontologySchemaName;
    }
    
    /** Set the ontology file name.
     * @param ontologySchemaName the ontologySchemaName to set
     */
    public final void setOntologySchemaName(String ontologySchemaName) {
        this.ontologySchemaName = ontologySchemaName;
    }
    
    /**
     * get the extended ontology name (the ontology which imports the 
     * basic ontology and extends it).
     * @return the extendedOntologyName
     */
    public final String getExtendedOntologyName() {
        return extendedOntologyName;
    }
    
    /**
     * Set the extended ontology name.
     * @param extendedOntologyName the extendedOntologyName to set
     */
    public final void setExtendedOntologyName(String extendedOntologyName) {
        this.extendedOntologyName = extendedOntologyName;
    }
    
    /**
     * Get the absolute path in which the ontology resides.
     * @return the absolutePath
     */
    public final String getAbsolutePath() {
        return absolutePath;
    }
    
    /**
     * Set the absolute path.
     * @param absolutePath the absolutePath to set
     */
    public final void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }
    
    /**
     * get the IRI of the base ontology.
     * @return the ontologyIRI
     */
    public final String getOntologyIRI() {
        return ontologyIRI;
    }
    
    /**
     * Set the IRI of the base ontology.
     * @param ontologyIRI the ontologyIRI to set
     */
    public final void setOntologyIRI(String ontologyIRI) {
        this.ontologyIRI = ontologyIRI;
    }
    
    /**
     * Get the IRI of the extended ontology.
     * @return the extendedIRI
     */
    public final String getExtendedIRI() {
        return extendedIRI;
    }
    
    /**
     * Set the IRI of the extended ontology.
     * @param extendedIRI the extendedIRI to set
     */
    public final void setExtendedIRI(String extendedIRI) {
        this.extendedIRI = extendedIRI;
    }

    /**
     * Get the related database connection info.
     * @return the db connection data.
     */
    public DbInfo getDbInfo() {
        return dbInfo;
    }

    /**
     * Set the related database connection info.
     * @param dbInfo the db connection info.
     */
    public void setDbInfo(DbInfo dbInfo) {
        this.dbInfo = dbInfo;
    }
}
