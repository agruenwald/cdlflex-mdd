package org.cdlflex.mdd.sembase.jena;

/**
 * Encapsulates database connection information to
 * pass parameters more efficient.
 */
public class DbInfo { 
    private String databasetype;
    private String host;
    private String port;
    private String schema;
    private String username;
    private String pw;
    
    /**
     * get the database type.
     * @return the databasetype
     */
    public final String getDatabasetype() {
        return databasetype;
    }
    
    /**
     * set the database type.
     * @param databasetype the databasetype to set
     */
    public final void setDatabasetype(String databasetype) {
        this.databasetype = databasetype;
    }
    
    /**
     * get the host.
     * @return the host.
     */
    public final String getHost() {
        return host;
    }
    
    /**
     * set the host.
     * @param host the host to set
     */
    public final void setHost(String host) {
        this.host = host;
    }
    
    /**
     * get the port.
     * @return the port
     */
    public final String getPort() {
        return port;
    }
    
    /**
     * set the port.
     * @param port the port to set
     */
    public final void setPort(String port) {
        this.port = port;
    }
    
    /**
     * get the schema.
     * @return the schema.
     */
    public final String getSchema() {
        return schema;
    }
    
    /**
     * set the schema.
     * @param schema the schema to set
     */
    public final void setSchema(String schema) {
        this.schema = schema;
    }
    
    /**
     * get the user name.
     * @return the username.
     */
    public final String getUsername() {
        return username;
    }
    
    /**
     * set the user name.
     * @param username the username to set
     */
    public final void setUsername(String username) {
        this.username = username;
    }
    
    /**
     * get the (optional) password.
     * @return the pw.
     */
    public final String getPw() {
        return pw;
    }
    
    /**
     * set the password.
     * @param pw the pw to set
     */
    public final void setPw(String pw) {
        this.pw = pw;
    }
}
