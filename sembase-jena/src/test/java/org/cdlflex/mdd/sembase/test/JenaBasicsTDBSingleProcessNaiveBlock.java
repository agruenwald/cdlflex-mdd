package org.cdlflex.mdd.sembase.test;

import org.cdlflex.mdd.modelrepo.Installer;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.JenaConnection;
import org.cdlflex.mdd.sembase.jena.internal.ConnectionUtil;
import org.junit.BeforeClass;

/**
 * Test cases for the execution of Jena TDB in multiple JVM environments
 * (naive implementation with low performance).
 */
public class JenaBasicsTDBSingleProcessNaiveBlock extends TestJenaBasicsTDB {

    @Override
    public JenaConnection createConnection() throws MDDException {
        return ConnectionUtil.createJenaConnectionNaivLock(CONFIGLOADER, "TDB");
    }

    /**
     * Reset the database.
     * @throws MDDException is thrown if the database initialisation fails.
     */
    @BeforeClass
    public static void setupClean() throws MDDException {
        Installer.installModelsFromBackupDir(new ConfigLoader(true));
        JenaConnection c = new JenaBasicsTDBSingleProcessNaiveBlock().createConnection();
        c.connect();
        c.resetOntology();
        c.close();
    }

}
