package org.cdlflex.mdd.sembase.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;

import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.jena.JenaConnection;
import org.cdlflex.mdd.sembase.jena.internal.ConnectionUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hp.hpl.jena.ontology.AnnotationProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;

/**
 * Abstract test class which provides the basic test cases and the
 * basic setup methods for the Jena data store implementations
 * (either TDB or SDB).
 */
public abstract class AbstractTestJenaBasics extends AbstractTestJenaBasicsWithoutComplexity {
    private static final String COMMENT_URL = "http://www.w3.org/2000/01/rdf-schema#comment"; 
    
    /**
     * Establish new connection to the Jena data store.
     * @throws MDDException is thrown if connecting fails.
     */
    @Before
    public void connect() throws MDDException {
        c = ConnectionUtil.createJenaConnection(CONFIGLOADER, "TDB");
        c.connect();
    }

    /**
     * Create a new Jena connection reusable for the rest of the project.
     * @return an abstract connection.
     * @throws MDDException is thrown if connecting fails.
     */
    public abstract JenaConnection createConnection() throws MDDException;

    /**
     * Recover after tests.
     * @throws MDDException is thrown in case of failure.
     */
    @After
    public void disconnect() throws MDDException {
        c.close();
    }

    /**
     * Insert an individual, including object properties.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void testInsertIndividualWithObjectProperties() throws MDDException {
        c.begin();
        c.removeIndividuals("Requirement");
        c.commit();
        c.begin();
        OntModel ont = c.getOntologyModel();
        String ns = c.getOntologySchemaIRI() + "#";
        OntClass requirement = ont.getOntClass(ns + "Requirement");
        OntClass stakeholder = ont.getOntClass(ns + "Stakeholder");
        OntProperty hasRequirementName = ont.getOntProperty(ns + "hasRequirementName");
        OntProperty hasRequirementId = ont.getOntProperty(ns + "hasRequirementId");
        ObjectProperty reqParent = ont.getObjectProperty(ns + "subRequirementParent"); // self-ref
        ObjectProperty reqStakeholder = ont.getObjectProperty(ns + "define"); // stakeholder
        OntProperty hasStakeholderName = ont.getOntProperty(ns + "hasStakeholderName");
        String reqId = "reqId007";
        String reqId008 = "reqId008";
        Individual req007 = ont.createIndividual(ns + reqId, requirement);
        Individual req008 = ont.createIndividual(ns + reqId008, requirement);
        Individual stakeholderAndi = ont.createIndividual(ns + "stakeholder-andi", stakeholder);
        if (requirement == null || hasRequirementId == null || hasRequirementName == null || reqParent == null) {
            throw new MDDException(String.format("Some of the linked ontology classes or properties" 
                    + "could not have been initialized."));
        } else if (hasStakeholderName == null || reqStakeholder == null || stakeholder == null) {
            throw new MDDException(String.format("Some of the linked ontology classes or properties" 
                    + "could not have been initialized."));
        } 
        req007.addProperty(hasRequirementId, "req007");
        req007.addProperty(hasRequirementName, "req007 name");
        req007.addProperty(reqParent, req008);
        req007.addProperty(reqStakeholder, stakeholderAndi);
        req008.addProperty(hasRequirementId, "req008");
        req008.addProperty(hasRequirementName, "req008 name");
        stakeholderAndi.addProperty(hasStakeholderName, "stakeholder andi name");
        c.commit();
        c.close();
        c.connect();
        c.begin();
        ont = c.getOntologyModel();   
        testInsertIndividualWithObjectPropertiesPart2(ns, ont, reqId);
    }

    private void testInsertIndividualWithObjectPropertiesPart2(String ns, OntModel ont, String reqId) 
        throws MDDException {
        Individual reqBack = ont.getIndividual(ns + reqId);
        assertTrue(reqBack != null);
        Statement reqIdStatement = reqBack.getProperty(ont.getOntProperty(ns + "hasRequirementId"));
        String reqBackId = reqIdStatement.getObject().asLiteral().toString();
        assertEquals("req007", reqBackId);

        Statement reqNameStatement = reqBack.getProperty(ont.getOntProperty(ns + "hasRequirementName"));
        String reqBackName = reqNameStatement.getObject().asLiteral().toString();
        assertEquals(reqBackName, "req007 name");

        ObjectProperty op = ont.getObjectProperty(ns + "subRequirementParent");
        if (op == null) {
            throw new MDDException("Could not read requirement parent property.");
        }
        Statement reqParentStatement = reqBack.getProperty(op);
        Individual linkedReq = reqParentStatement.getObject().as(Individual.class);
        Statement requirementNameRef = linkedReq.getProperty(ont.getOntProperty(ns + "hasRequirementName"));
        String reqParentSt = requirementNameRef.getObject().asLiteral().toString();

        assertEquals(reqParentSt, "req008 name");

        op = ont.getObjectProperty(ns + "define");
        if (op == null) {
            throw new MDDException("Could not read stakeholder object property.");
        }

        Statement stakeholderBack = reqBack.getProperty(op);
        Individual linkedStakeholder = stakeholderBack.getObject().as(Individual.class);
        Statement linkedStakeholderStatement =
            linkedStakeholder.getProperty(ont.getOntProperty(ns + "hasStakeholderName"));
        String stName = linkedStakeholderStatement.getObject().asLiteral().toString();
        assertEquals(stName, "stakeholder andi name");
    }

    /**
     * Insert an ontology annotation.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void testAnnotationAdd() throws MDDException {
        c.begin();
        String ns = c.getOntologySchemaIRI() + "#";
        Individual req1 = buildRequirement("req1");
        AnnotationProperty annotation =
            c.getOntologyModel().createAnnotationProperty("http://www.w3.org/2000/01/rdf-schema#comment");
        req1.addProperty(annotation, "This is a comment");
        c.commit();

        c.begin();
        req1 = c.getOntologyModel().getIndividual(ns + "req1");

        RDFNode s = req1.getPropertyValue(annotation);
        c.rollback();
        assertTrue(req1 != null);
        assertEquals("This is a comment", s.asLiteral().toString());
    }

    /**
     * Test the insertion and retrieval of several OWL annotations / comments.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void testAddSeveralComments() throws MDDException {
        c.begin();
        String ns = c.getOntologySchemaIRI() + "#";
        Individual req1 = buildRequirement("req1");
        AnnotationProperty annotation1 = c.getOntologyModel().createAnnotationProperty(COMMENT_URL + "-1");
        AnnotationProperty annotation2 = c.getOntologyModel().createAnnotationProperty(COMMENT_URL + "-2");

        RDFNode sx = req1.getPropertyValue(annotation1);
        if (sx != null) {
            req1.removeProperty(annotation1, sx);
        }
        req1.addProperty(annotation1, "This is a comment");
        req1.addProperty(annotation2, "Comment 2");
        c.commit();

        c.begin();
        annotation1 = c.getOntologyModel().getAnnotationProperty(COMMENT_URL + "-1");
        annotation2 = c.getOntologyModel().getAnnotationProperty(COMMENT_URL + "-2");

        req1 = c.getOntologyModel().getIndividual(ns + "req1");

        RDFNode s = req1.getPropertyValue(annotation1);
        c.rollback();
        assertTrue(req1 != null);
        assertEquals("This is a comment", s.asLiteral().toString());
        assertEquals("Comment 2", req1.getPropertyValue(annotation2).asLiteral().toString());
    }

    /**
     * Retrieve all data properties from a specific individual.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    //CHECKSTYLE:OFF complexity of test methods is not that important
    @Test
    public void retrieveAllDataPropertyValuesOfIndividual() throws MDDException {
        c.begin();
        c.removeIndividuals("Requirement");
        c.commit();
        c.begin();
        OntModel ont = c.getOntologyModel();
        String ns = c.getOntologySchemaIRI() + "#";
        OntClass requirement = ont.getOntClass(ns + "Requirement");
        OntClass stakeholder = ont.getOntClass(ns + "Stakeholder");
        OntProperty hasRequirementName = ont.getOntProperty(ns + "hasRequirementName");
        OntProperty hasRequirementId = ont.getOntProperty(ns + "hasRequirementId");
        ObjectProperty reqParent = ont.getObjectProperty(ns + "subRequirementParent"); // self-ref
        ObjectProperty reqStakeholder = ont.getObjectProperty(ns + "define"); // stakeholder
        OntProperty hasStakeholderName = ont.getOntProperty(ns + "hasStakeholderName");

        String reqId = "reqId007";
        String reqId008 = "reqId008";
        Individual req007 = ont.createIndividual(ns + reqId, requirement);
        Individual req008 = ont.createIndividual(ns + reqId008, requirement);
        Individual stakeholderAndi = ont.createIndividual(ns + "stakeholder-andi", stakeholder);
        if (requirement == null || hasRequirementId == null || hasRequirementName == null || reqParent == null) {
            throw new MDDException(
                    String.format("Some of the linked ontology classes / properties could not have been initialized."));
        } else if (reqStakeholder == null || stakeholder == null || hasStakeholderName == null) {
            throw new MDDException(
                    String.format("Some of the linked ontology classes / properties could not have been initialized."));
        }

        req007.addProperty(hasRequirementId, "req007");
        req007.addProperty(hasRequirementName, "req007 name");
        req007.addProperty(reqParent, req008);
        req007.addProperty(reqStakeholder, stakeholderAndi);
        req008.addProperty(hasRequirementId, "req008");
        req008.addProperty(hasRequirementName, "req008 name");
        stakeholderAndi.addProperty(hasStakeholderName, "stakeholder andi name");
        c.commit();
        c.close();

        c.connect();
        c.begin();
        ont = c.getOntologyModel();

        Individual reqBack = ont.getIndividual(ns + reqId);
        assertTrue(reqBack != null);

        // iterate over existing data (and object) properties
        List<String> propertyNames = new LinkedList<String>();
        String sparql =
            "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  \n"
                + "prefix owl: <http://www.w3.org/2002/07/owl#>  \n" + "prefix itpm: <" + c.getOntologySchemaIRI()
                + "#> \n" + "SELECT DISTINCT ?property ?ind ?value WHERE { ?property a owl:DatatypeProperty. itpm:"
                + reqId + " ?property ?value} ORDER BY ASC(?property)";

        Query query = QueryFactory.create(sparql);
        QueryExecution qe = QueryExecutionFactory.create(query, ont);
        ResultSet rs = null;
        try {
            rs = qe.execSelect();
            while (rs.hasNext()) {
                QuerySolution rec = rs.next();
                propertyNames.add(rec.get("?property").toString());
            }
            qe.close();
        } finally {
            qe.close();
            c.close();
        }

        assertEquals(2, propertyNames.size());
    }
    //CHECKSTYLE:ON

    /**
     * Delete specific individuals based on their class relations.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    //CHECKSTYLE:OFF Complexity of test methods is not that important.
    @Test
    public void testDeleteAllViaPartlySPARQL() throws MDDException {
        c.begin();

        OntModel ont = c.getOntologyModel();
        String ns = c.getOntologySchemaIRI() + "#";
        OntClass requirement = ont.getOntClass(ns + "Requirement");
        OntClass stakeholder = ont.getOntClass(ns + "Stakeholder");
        OntProperty hasRequirementName = ont.getOntProperty(ns + "hasRequirementName");
        OntProperty hasRequirementId = ont.getOntProperty(ns + "hasRequirementId");
        ObjectProperty reqParent = ont.getObjectProperty(ns + "subRequirementParent"); // self-ref
        ObjectProperty reqStakeholder = ont.getObjectProperty(ns + "define"); // stakeholder
        OntProperty hasStakeholderName = ont.getOntProperty(ns + "hasStakeholderName");

        String reqId = "reqId007";
        String reqId008 = "reqId008";
        Individual req007 = ont.createIndividual(ns + reqId, requirement);
        Individual req008 = ont.createIndividual(ns + reqId008, requirement);
        Individual stakeholderAndi = ont.createIndividual(ns + "stakeholder-andi", stakeholder);
        if (requirement == null || hasRequirementId == null || hasRequirementName == null || reqParent == null
            || reqStakeholder == null || stakeholder == null || hasStakeholderName == null) {
            throw new MDDException(
                    String.format("Some of the linked ontology classes or properties could not have been initialized."));
        }

        req007.addProperty(hasRequirementId, "req007");
        req007.addProperty(hasRequirementName, "req007 name");
        req007.addProperty(reqParent, req008);
        req007.addProperty(reqStakeholder, stakeholderAndi);
        req008.addProperty(hasRequirementId, "req008");
        req008.addProperty(hasRequirementName, "req008 name");
        stakeholderAndi.addProperty(hasStakeholderName, "stakeholder andi name");
        c.commit();
        c.close();

        c.connect();
        c.begin();
        ont = c.getOntologyModel();

        Individual reqBack = ont.getIndividual(ns + reqId);
        assertTrue(reqBack != null);
        c.resetData();
        c.commit();

        assertTrue(ont.getIndividual(ns + reqId) == null);

        List<String> propertyNames = new LinkedList<String>();
        String sparql =
            c.getSparqlBase()
                + "SELECT DISTINCT ?property ?ind ?value WHERE { ?property a owl:DatatypeProperty. itpm:" + reqId
                + " ?property ?value} ORDER BY ASC(?property)";

        Query query = QueryFactory.create(sparql);
        QueryExecution qe = QueryExecutionFactory.create(query, ont);
        ResultSet rs = null;
        try {
            rs = qe.execSelect();
            while (rs.hasNext()) {
                QuerySolution rec = rs.next();
                propertyNames.add(rec.get("?property").toString());
            }
            qe.close();
        } finally {
            qe.close();
            c.close();
        }

        assertEquals(0, propertyNames.size());
        assertEquals(0, propertyNames.size());
    }
    //CHECKSTYLE:OFF

    /**
     * Delete individuals (extended SPARQL).
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void deleteViaSPARQL() throws MDDException {
        c.begin();
        c.removeIndividuals("Requirement");
        c.commit();
        c.begin();
        OntModel ont = c.getOntologyModel();
        String ns = c.getOntologySchemaIRI() + "#";
        OntClass requirement = ont.getOntClass(ns + "Requirement");
        OntClass stakeholder = ont.getOntClass(ns + "Stakeholder");
        OntProperty hasRequirementName = ont.getOntProperty(ns + "hasRequirementName");
        OntProperty hasRequirementId = ont.getOntProperty(ns + "hasRequirementId");
        ObjectProperty reqParent = ont.getObjectProperty(ns + "subRequirementParent"); // self-ref
        ObjectProperty reqStakeholder = ont.getObjectProperty(ns + "define"); // stakeholder
        OntProperty hasStakeholderName = ont.getOntProperty(ns + "hasStakeholderName");

        String reqId = "reqId007";
        String reqId008 = "reqId008";
        Individual req007 = ont.createIndividual(ns + reqId, requirement);
        Individual req008 = ont.createIndividual(ns + reqId008, requirement);
        Individual stakeholderAndi = ont.createIndividual(ns + "stakeholder-andi", stakeholder);
        if (requirement == null || hasRequirementId == null || hasRequirementName == null || reqParent == null
            || reqStakeholder == null || stakeholder == null || hasStakeholderName == null) {
            throw new MDDException(
                    String.format("Some of the linked ontology classes or properties" 
                            + "could not have been initialized."));
        }

        req007.addProperty(hasRequirementId, "req007");
        req007.addProperty(hasRequirementName, "req007 name");
        req007.addProperty(reqParent, req008);
        req007.addProperty(reqStakeholder, stakeholderAndi);
        req008.addProperty(hasRequirementId, "req008");
        req008.addProperty(hasRequirementName, "req008 name");
        stakeholderAndi.addProperty(hasStakeholderName, "stakeholder andi name");
        c.commit();
        c.close();

        c.connect();
        c.begin();
        ont = c.getOntologyModel();

        Individual reqBack = ont.getIndividual(ns + reqId);
        assertTrue(reqBack != null);
        List<String> propertyNames = new LinkedList<String>();
        String sparql =
            "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  \n"
                + "prefix owl: <http://www.w3.org/2002/07/owl#>  \n" + "prefix itpm: <" + c.getOntologySchemaIRI()
                + "#> \n" + "SELECT DISTINCT ?property ?ind ?value WHERE { ?property a owl:DatatypeProperty. itpm:"
                + reqId + " ?property ?value} ORDER BY ASC(?property)";

        Query query = QueryFactory.create(sparql);
        QueryExecution qe = QueryExecutionFactory.create(query, ont);
        ResultSet rs = null;
        try {
            rs = qe.execSelect();
            while (rs.hasNext()) {
                QuerySolution rec = rs.next();
                propertyNames.add(rec.get("?property").toString());
            }
            qe.close();
        } finally {
            qe.close();
            c.commitReadOnly();
            c.close();
        }
    }

    /**
     * Retrieve individuals via SPARQL.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void readIndividualsViaSPARQL() throws MDDException {
        c.begin();
        buildRequirement("req1");
        buildRequirement("req2");
        buildRequirement("req3");
        c.commit();

        c.close();
        c.connect();
        c.begin();
        Individual reqBack = c.getOntologyModel().getIndividual(c.getOntologySchemaIRI() + "#" + "req1");
        assertTrue(reqBack != null);

        // Execute the query and obtain results
        OntModel m = c.getOntologyModel();
        String sparqlQueryString =
            "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  \n" + "prefix itpm: <"
                + c.getOntologySchemaIRI() + "#> \n" + "select ?req WHERE {?req a itpm:Requirement} ";

        Query query = QueryFactory.create(sparqlQueryString);
        QueryExecution qe = QueryExecutionFactory.create(query, m);
        ResultSet rs = null;
        try {
            rs = qe.execSelect();
            int num = 0;
            while (rs.hasNext()) {
                rs.next();
                num++;
            }
            qe.close();
            assertTrue(num >= 3);
        } finally {
            qe.close();
            c.commitReadOnly();
            c.close();
        }
    }

}
