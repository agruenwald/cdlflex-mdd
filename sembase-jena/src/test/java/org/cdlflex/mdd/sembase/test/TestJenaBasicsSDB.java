package org.cdlflex.mdd.sembase.test;

import static org.junit.Assert.assertTrue;

import org.cdlflex.mdd.modelrepo.Installer;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.JenaConnection;
import org.cdlflex.mdd.sembase.jena.internal.ConnectionUtil;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tests for the Jena SDB library (deprecated).
 * The tests are ignored, because the interaction with the test-server 
 * (falschparken.at) took very long, and the server is not available in
 * the CDL-Flex test environment.
 */
@Ignore
public class TestJenaBasicsSDB extends AbstractTestJenaBasics {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestJenaBasicsSDB.class);

    @Override
    public JenaConnection createConnection() throws MDDException {
        return ConnectionUtil.createJenaConnection(CONFIGLOADER, "SDB");
    }

    /**
     * Reset the test directories / data base.
     * @throws MDDException is thrown if initialization fails.
     */
    @BeforeClass
    public static void initOnFailure() throws MDDException {
        Installer.installModelsFromBackupDir(new ConfigLoader(true));
        JenaConnection c = ConnectionUtil.createJenaConnection(CONFIGLOADER, "SDB");
        c.connect();
        //CHECKSTYLE:OFF: IllegalCatch - Much more readable than catching n exceptions
        try {
            LOGGER.warn(String.format("The SDB ontology is going to be reset. This may take up to 25 minutes."));
            c.resetOntology();
        } catch (Exception e) {
            c.close();
        } finally {
            try {
                c.close();
            } catch (Exception ex) {
            }
        }
        //CHECKSTYLE:ON: IllegalCatch
    }

    @Test
    @Ignore
    @Override
    public void testResetDataInEmptyModelAuto() throws MDDException {
        assertTrue(true); // can take very long for SDB on remote servers
    }

    /**
     * Reload ontology into Jena (manual hook).
     * @param args arguments
     * @throws MDDException is thrown if loading fails.
     */
    public static void main(String[] args) throws MDDException {
        JenaConnection c = ConnectionUtil.createJenaConnection(CONFIGLOADER, "SDB");
        c.connect();
        c.resetOntology();
        c.close();
    }
}
