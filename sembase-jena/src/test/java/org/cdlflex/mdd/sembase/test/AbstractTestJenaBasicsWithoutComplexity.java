package org.cdlflex.mdd.sembase.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.cdlflex.mdd.sembase.Connection;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.JenaConnection;
import org.cdlflex.mdd.sembase.jena.internal.ConnectionUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.Statement;

/**
 * Abstract test class which provides the basic test cases and the
 * basic setup methods for the Jena data store implementations
 * (either TDB or SDB).
 */
public abstract class AbstractTestJenaBasicsWithoutComplexity {
    protected static JenaConnection c;
    protected static final ConfigLoader CONFIGLOADER = new ConfigLoader(true);
    
    /**
     * Establish new connection to the Jena data store.
     * @throws MDDException is thrown if connecting fails.
     */
    @Before
    public void connect() throws MDDException {
        c = ConnectionUtil.createJenaConnection(CONFIGLOADER, "TDB");
        c.connect();
    }

    /**
     * Create a new Jena connection reusable for the rest of the project.
     * @return an abstract connection.
     * @throws MDDException is thrown if connecting fails.
     */
    public abstract JenaConnection createConnection() throws MDDException;

    /**
     * Recover after tests.
     * @throws MDDException is thrown in case of failure.
     */
    @After
    public void disconnect() throws MDDException {
        c.close();
    }

    /**
     * Test establishing a basic connection.
     * @throws MDDException is thrown if connecting fails.
     */
    @Test
    public void connectTest() throws MDDException {
        Connection cx = createConnection();
        cx.connect();
        cx.close();
        assertTrue(true);
    }

    /**
     * Test basic data base operations (connect, disconnect, transaction handling).
     * @throws MDDException is thrown if connecting fails.
     */
    @Test
    public void testServalConnections() throws MDDException {
        c.begin();
        c.commit();
        c.close();

        c.connect();
        c.close();

        c.connect();
        c.begin();
        c.rollback();
        c.close();

        c.connect();

        assertTrue(true);
    }

    /**
     * Test reseting data.
     * @throws MDDException is thrown if connecting fails.
     */
    @Test
    public void testResetNoExceptions() throws MDDException {
        JenaConnection cx = createConnection();
        cx.connect();
        cx.begin();
        cx.resetData();
        cx.commit();
        cx.close();
        assertTrue(true);
    }

    /**
     * Test loading the data base based on OWL ontologies (mainly T-Box).
     * @throws MDDException is thrown if connecting fails.
     */
    @Test
    @Ignore
    public void testLoadDBModel() throws MDDException {
        // Takes very long hence disabled
        // c.loadDBModelFromFiles();
        assertTrue(true);
    }

    private Individual buildRequirement() throws MDDException {
        return this.buildRequirement("req1");
    }

    /**
     * Create a new requirement.
     * @param name the name of the requirement.
     * @return a new individual within the ontology.
     * @throws MDDException is thrown in case of failure.
     */
    protected Individual buildRequirement(String name) throws MDDException {
        return this.buildRequirement(c, name);
    }

    /**
     * Same as {@code #buildRequirement(String)}, but with additional connection.
     * @param cx the connection.
     * @param name the name of the requirement.
     * @return a new individual within the ontology.
     * @throws MDDException is thrown in case of failure.
     */
    protected Individual buildRequirement(JenaConnection cx, String name) throws MDDException {
        OntModel ont = cx.getOntologyModel();
        String ns = cx.getOntologySchemaIRI() + "#";
        OntClass requirement = ont.getOntClass(ns + "Requirement");
        OntProperty p = ont.getOntProperty(ns + "hasRequirementName");
        DatatypeProperty pd = ont.getDatatypeProperty(ns + "hasRequirementName"); // don't know yet which one is correct

        if (requirement == null || p == null) {
            throw new MDDException(String.format("Ontology class %s or property %s are not existing in Jena.",
                    "Requirement", "hasRequirementName"));
        }

        Individual req1 = ont.createIndividual(ns + name, requirement);
        req1.addProperty(pd, "individual requirement named 1");
        return req1;
    }

    /**
     * Test insert and roll back (individual must not exist afterwards).
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void testInsertIndividualButRollback() throws MDDException {
        String reqId = "req777";
        c.begin();
        buildRequirement(reqId);
        c.rollback();
        c.close();
        c.connect();
        c.begin();
        Individual reqBack = c.getOntologyModel().getIndividual(c.getOntologySchemaIRI() + "#" + reqId);
        c.rollback();
        c.close();
        assertTrue(reqBack == null);
    }

    /**
     * Insert a new individual.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void testInsertIndivdual() throws MDDException {
        c.begin();
        Individual req1 = buildRequirement();
        c.commit();
        assertTrue(req1 != null);
    }

    /**
     * Insert a new individual via auto commit mode.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void testInsertIndivdualAutocommit() throws MDDException {
        c.autoBegin();
        buildRequirement("req1");
        c.autoCommit();
        c.close();
        c.connect();
        c.begin();
        Individual reqBack = c.getOntologyModel().getIndividual(c.getOntologySchemaIRI() + "#" + "req1");
        assertTrue(reqBack != null);
    }

    /**
     * Read an individual record.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void readIndividual() throws MDDException {
        c.begin();
        buildRequirement();
        c.commit();
        c.close();
        c.connect();
        c.begin();
        Individual reqBack = c.getOntologyModel().getIndividual(c.getOntologySchemaIRI() + "#" + "req1");
        assertTrue(reqBack != null);
    }

    /**
     * Insert an individual including some simple data properties.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void testInsertIndividualWithDataProperties() throws MDDException {
        c.begin();
        OntModel ont = c.getOntologyModel();
        String ns = c.getOntologySchemaIRI() + "#";
        OntClass requirement = ont.getOntClass(ns + "Requirement");
        OntProperty hasRequirementName = ont.getOntProperty(ns + "hasRequirementName");
        OntProperty hasRequirementId = ont.getOntProperty(ns + "hasRequirementId");
        String reqId = "reqId007";
        Individual req007 = ont.createIndividual(ns + reqId, requirement);

        if (requirement == null || hasRequirementId == null || hasRequirementName == null) {
            throw new MDDException(String.format(
                    "Ontology class %s or property %s or property %s are not existing in Jena.", "Requirement",
                    "hasRequirementName", "hasRequirementId"));
        }

        req007.addProperty(hasRequirementId, "req007");
        req007.addProperty(hasRequirementName, "req007 name");

        c.commit();
        c.close();

        c.connect();
        c.begin();
        ont = c.getOntologyModel();
        Individual reqBack = ont.getIndividual(ns + reqId);
        assertTrue(reqBack != null);

        Statement reqIdStatement = reqBack.getProperty(ont.getOntProperty(ns + "hasRequirementId"));
        String reqBackId = reqIdStatement.getObject().asLiteral().toString();
        assertEquals("req007", reqBackId);

        Statement reqNameStatement = reqBack.getProperty(ont.getOntProperty(ns + "hasRequirementName"));
        String reqBackName = reqNameStatement.getObject().asLiteral().toString();
        assertEquals("req007 name", reqBackName);
    }
    

    /**
     * Delete an individual.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void deleteIndividual() throws MDDException {
        c.begin();
        buildRequirement("req1");
        buildRequirement("req2");
        buildRequirement("req3");
        c.commit();

        c.close();
        c.connect();
        c.begin();
        Individual reqBack1 = c.getOntologyModel().getIndividual(c.getOntologySchemaIRI() + "#" + "req1");
        Individual reqBack2 = c.getOntologyModel().getIndividual(c.getOntologySchemaIRI() + "#" + "req2");
        Individual reqBack3 = c.getOntologyModel().getIndividual(c.getOntologySchemaIRI() + "#" + "req3");
        assertTrue(reqBack1 != null);
        assertTrue(reqBack2 != null);
        assertTrue(reqBack3 != null);

        reqBack1.remove();
        reqBack2.remove();
        reqBack3.remove();
        c.commit();
        c.close();

        c.connect();
        c.begin();
        reqBack1 = c.getOntologyModel().getIndividual(c.getOntologySchemaIRI() + "#" + "req1");
        reqBack2 = c.getOntologyModel().getIndividual(c.getOntologySchemaIRI() + "#" + "req2");
        reqBack3 = c.getOntologyModel().getIndividual(c.getOntologySchemaIRI() + "#" + "req3");
        assertTrue(reqBack1 == null);
        assertTrue(reqBack2 == null);
        assertTrue(reqBack3 == null);
        c.commitReadOnly();
    }

    /**
     * Reset data base.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void testResetDataInEmptyModelAuto() throws MDDException {
        c.resetOntology();
        c.close();
        c.connect();
        c.resetData();
    }

    /**
     * Test the handling of the auto commit mode in conjunction 
     * with manual commit mode.
     * @throws MDDException is thrown if any of the db operations fails.
     */
    @Test
    public void testBackToAutocommitMode() throws MDDException {
        c.close();
        c.connect();
        assertTrue(c.isAutoCommitMode());
        c.begin();
        assertTrue(!c.isAutoCommitMode());
        c.commit();
        assertTrue(c.isAutoCommitMode());
    }
}
