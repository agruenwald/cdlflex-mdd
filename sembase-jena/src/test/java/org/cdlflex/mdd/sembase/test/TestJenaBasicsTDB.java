package org.cdlflex.mdd.sembase.test;

import static org.junit.Assert.assertTrue;

import org.cdlflex.mdd.modelrepo.Installer;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.JenaConnection;
import org.cdlflex.mdd.sembase.jena.internal.ConnectionUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import com.hp.hpl.jena.ontology.Individual;
/**
 * Test cases for the Jena TDB default implementation.
 */
public class TestJenaBasicsTDB extends AbstractTestJenaBasics {
    @Override
    public JenaConnection createConnection() throws MDDException {
        return ConnectionUtil.createJenaConnection(CONFIGLOADER, "TDB");
    }

    /**
     * Setup a new connection.
     * @throws MDDException thrown in case of failure.
     */
    @BeforeClass
    public static void setupClean() throws MDDException {
        Installer.installModelsFromBackupDir(new ConfigLoader(true));
        JenaConnection c = new TestJenaBasicsTDB().createConnection();
        c.connect();
        c.resetOntology();
        c.close();
    }

    /**
     * Nested transactions are not supported in Jena TDB. Nested
     * transactions must cause an exception and must not result in
     * deadlocks.
     * @throws MDDException thrown in case of failure.
     */
    @Test(expected = MDDException.class)
    public void negTestNestedTransactions() throws MDDException {
        JenaConnection c1 = createConnection();
        JenaConnection c2 = createConnection();
        try {
            c1.connect();
            c1.begin();
            c2.connect();
            c2.begin();

            buildRequirement(c1, "req1"); // build and add requirements
            buildRequirement(c2, "req2");

            c1.commit();
            c2.commit();

            c1.close();
            c2.close();

            c1.connect();
            Individual reqBack = c1.getOntologyModel().getIndividual(c1.getOntologySchemaIRI() + "#" + "req1");
            assertTrue(reqBack != null);
        } catch (NullPointerException e) {
            // at com.hp.hpl.jena.tdb.nodetable.NodeTableCache._idForNode(NodeTableCache.java:125)
            throw new MDDException("that's fine");
        } finally {
            //CHECKSTYLE:OFF
            try {
                c1.close();
            } catch (Exception e) {

            }
            c2.close();
            try {
                c2.close();
            } catch (Exception e) {
            }
            //CHECKSTYLE:ON
        }
    }

    /**
     * Test nested transactions.
     * @throws MDDException is thrown if any db operation fails.
     */
    @Test
    public void posTestNestedTransactions() throws MDDException {
        JenaConnection c1 = createConnection();
        JenaConnection c2 = createConnection();
        c1.connect();
        c2.connect();

        c1.begin();
        buildRequirement(c1, "req1"); // build and add requirements
        c1.commit();

        c2.begin();
        buildRequirement(c2, "req2");
        c2.commit();

        c1.close();
        c2.close();

        c1.connect();
        c1.autoBeginReadOnly();
        Individual reqBack = c1.getOntologyModel().getIndividual(c1.getOntologySchemaIRI() + "#" + "req1");
        assertTrue(reqBack != null);
    }
}
