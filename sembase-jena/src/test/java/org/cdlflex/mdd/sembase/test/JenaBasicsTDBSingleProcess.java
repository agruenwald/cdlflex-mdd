package org.cdlflex.mdd.sembase.test;

import static org.junit.Assert.assertTrue;

import org.cdlflex.mdd.modelrepo.Installer;
import org.cdlflex.mdd.sembase.MDDException;
import org.cdlflex.mdd.sembase.config.ConfigLoader;
import org.cdlflex.mdd.sembase.jena.JenaConnection;
import org.cdlflex.mdd.sembase.jena.internal.ConnectionUtil;
import org.junit.BeforeClass;
import org.junit.Test;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Test cases for the implementation of the Jena TDB for single process
 * environments (single JVM).
 */
public class JenaBasicsTDBSingleProcess extends AbstractTestJenaBasics {

    @Override
    public JenaConnection createConnection() throws MDDException {
        return ConnectionUtil.createJenaConnectionSingleProcess(CONFIGLOADER, "TDB");
    }

    /**
     * Reset database.
     * @throws MDDException thrown in case of failure.
     */
    @BeforeClass
    public static void setupClean() throws MDDException {
        Installer.installModelsFromBackupDir(new ConfigLoader(true));
        JenaConnection c = new JenaBasicsTDBSingleProcess().createConnection();
        c.connect();
        c.resetOntology();
        c.close();
    }

    /**
     * Nested transactions are not supported in Jena TDB. Nested
     * transactions must cause an exception and must not result in
     * deadlocks.
     * @throws MDDException thrown in case of failure.
     */
    @Test(expected = MDDException.class)
    public void negTestNestedTransactions() throws MDDException {
        JenaConnection c1 = createConnection();
        JenaConnection c2 = createConnection();
        try {
            c1.connect();
            c1.begin();
            c2.connect();
            c2.begin();

            buildRequirement(c1, "req1"); // build and add requirements
            buildRequirement(c2, "req2");

            c1.commit();
            c2.commit();

            c1.close();
            c2.close();

            c1.connect();
            Individual reqBack = c1.getOntologyModel().getIndividual(c1.getOntologySchemaIRI() + "#" + "req1");
            assertTrue(reqBack != null);
        } finally {
            //CHECKSTYLE:OFF
            try {
                c1.close();
            } catch (Exception e) {
            }
            try {
                c2.close();
            } catch (Exception e) {
            }
            //CHECKSTYLE:ON
        }
    }

    /**
     * Nested transactions are not supported in Jena TDB. This
     * test checks the execution of transactions based on different
     * connections, sequentially.
     * @throws MDDException thrown in case of failure.
     */
    @Test
    public void posTestNestedTransactions() throws MDDException {
        JenaConnection c1 = createConnection();
        JenaConnection c2 = createConnection();
        c1.connect();
        c2.connect();

        c1.begin();
        buildRequirement(c1, "req1"); // build and add requirements
        c1.commit();

        c2.begin();
        buildRequirement(c2, "req2");
        c2.commit();

        c1.close();
        c2.close();

        c1.connect();
        
        c1.autoBeginReadOnly();
        Individual reqBack = c1.getOntologyModel().getIndividual(c1.getOntologySchemaIRI() + "#" + "req1");
        assertTrue(reqBack != null);
    }
}
