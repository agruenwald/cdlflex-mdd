#CDLFlex :: MDD 
Code Generation and abstracted storage of data for Semantic Technologies.

##Motivation
You want to give a new storage technology a try, but you are not sure yet?
You want to avoid lock-in, steep learning curves, and cumbersome technology-switching?
umlTUmany is an xtend2 based code-generator which helps you to abstract (semantic)
storage technologies and creates models, data access (DAOs) and unit-tests for you.
Additionally it provides connectors to store data based on the OWLAPI format or in
Jena TDB/SDB data stores.

## Further Information
More documentation can be found at Confluence under [Custom ITPM](http://cdl-ind.ifs.tuwien.ac.at/confluence/display/UC/Custom+ITPM 
"Custom ITPM"). 

